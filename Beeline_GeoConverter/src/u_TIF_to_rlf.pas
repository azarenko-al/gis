unit u_TIF_to_rlf;


interface

uses Classes, Sysutils, Dialogs, forms, IOUtils,

  u_GDAL_envi,
  u_GDAL_classes,

  u_Rel_to_Clutter_map,
 // u_3D_rel_to_bmp,

  u_GDAL_Bat,

  u_CustomTask,

///  u_Rel_to_clutter_map,

  //T_Rel_to_Clutter_map.Exec(aFileName: string);

  dm_Clutters,

  I_rel_Matrix1,
  u_rlf_Matrix,

  u_geo,

  u_files,
  u_Run,

  d_Progress,
  u_clutter_classes;


type
  TTif_to_RLF = class(TCustomTask)
  private
 //   FUTM_zone : Integer;
 //   FZone_GK : Integer;

    // -------------------------
    // index_file
    // -------------------------

    FDEM_Envi_File: TEnvi_File;
    FClutter_Envi_File: TEnvi_File;
    FClutter_H_Envi_File: TEnvi_File;

    FZone_Pulkovo: integer;
//    FRlf_Step    : word;


 //   FFiles: TStringList;

    FClutterInfo: TClutterModel;
    FMemStream: TMemoryStream;

    procedure Prepare_TIF_to_ENVI;
//    FProjection_Txt_file: TProjection_Txt_file;

    procedure RunFile;
   protected
  //  procedure CloseFiles; override;

   public

     Params: record
            FileName_Dem : string;

//           Zone_UTM : byte;

  //            Projection_Txt_FileName : string;

            // -----------------------------------
            IsUse_Clutter : Boolean;
            FileName_Clutter : string;

            // -----------------------------------
            IsUse_Clutter_H       : Boolean;
            FileName_Clutter_H : string;

            Clutters_section : string;

     //       Null_value : smallint;


            RLF_FileName : string;
//            Rlf_Step  : Integer;
        end;

    constructor Create;
    destructor Destroy; override;

    procedure Execute;
    procedure Execute_Dlg;
  end;



implementation

// ---------------------------------------------------------------
constructor TTif_to_RLF.Create;
// ---------------------------------------------------------------
begin
  inherited;

  FDEM_Envi_File:=TEnvi_File.Create;
  FClutter_Envi_File:=TEnvi_File.Create;
  FClutter_H_Envi_File:=TEnvi_File.Create;


  FClutterInfo := TClutterModel.Create();

end;


// ---------------------------------------------------------------
destructor TTif_to_RLF.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FClutterInfo);

  FreeAndNil(FDEM_Envi_File);
  FreeAndNil(FClutter_Envi_File);
  FreeAndNil(FClutter_H_Envi_File);

  inherited;
end;


// ---------------------------------------------------------------
procedure TTif_to_RLF.RunFile;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  rXYRect_GK: TXYRect;

  xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
//  eHeight: Double;

  iColCount: int64;
  iRowCount: int64;
//  iStep: word;
  iRow: int64;
  iCol: int64;


  rlf_rec: Trel_Record;
  iValue: integer;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
  iStep: Integer;
  iStep_col: Integer;
  iStep_row: Integer;

//  k: Integer;
//  s2: string;
//  s1: string;
  iW: Integer;
  wCluCode: Integer;

  rClutter: TClutterRec;
//  sDir: string;


  oFile: TEnvi_File;

  oMatrix: TrlfMatrix;
//  sRLF_FileName: string;

begin
//   T_Rel_to_Clutter_map.Exec(Params.RLF_FileName);
//  exit;

  Params.RLF_FileName :=ChangeFileExt(Params.RLF_FileName, '.rlf');

  params.FileName_Dem:=ChangeFileExt(params.FileName_Dem, '.envi');

//
//  T_Rel_to_Clutter_map.Exec(Params.RLF_FileName);
//  exit;


//  FDEM_Envi_File.OpenFile('E:\Tasks\beeline\ASC\oblast\1.envi');
//  FDEM_Envi_File.GetDataByIndex(0);

  FDEM_Envi_File.OpenFile (params.FileName_Dem);
//  FDEM_Envi_File.GetDataByIndex(0);

 // FDEM_Envi_File.GetDataByIndex(54100 * 4700);

//  FDEM_Envi_File.GetDataByIndex((FDEM_Envi_File.RowCount-1) * (FDEM_Envi_File.ColCount-1));



  if params.IsUse_Clutter then
    FClutter_Envi_File.OpenFile(params.FileName_Clutter);

  if params.IsUse_Clutter_H then
    FClutter_H_Envi_File.OpenFile(params.FileName_Clutter_H);


  //  FDEM_Envi_File:=TEnvi_File.Create;
//  FClutter_Envi_File:=TEnvi_File.Create;
//  FClutter_H_Envi_File:=TEnvi_File.Create;


//   ExportToClutterTab(Params.RLF_FileName);
//   Exit;
//------------------------------------------------

//
//
//  if Terminated then
//    Exit;


//   if Params.Is_Use_Clutter_bounds then
//     oFile:=FClutter_Envi_File
//   else

  oFile:=FDEM_Envi_File;


//  FDEM_Envi_File:=TEnvi_File.Create;
//  FClutter_Envi_File:=TEnvi_File.Create;
//  FClutter_H_Envi_File:=TEnvi_File.Create;
//

//
//  if Params.Rlf_Step=0 then
//    iStep :=Round(oFile.CellSize_lat)
//  else
//    iStep := Params.Rlf_Step;


 // iStep :=Round(FDEM_index_file.CellSize);

  Assert(oFile.RowCount>0);


  iStep:=Round((oFile.bounds.lat_max - oFile.bounds.lat_min)  / oFile.RowCount);

  iStep_row:=Round((oFile.bounds.lat_max - oFile.bounds.lat_min)  / oFile.RowCount);
  iStep_col:=Round((oFile.bounds.lon_max - oFile.bounds.lon_min)  / oFile.ColCount);

  Assert(iStep>0, 'Value <=0');

  iRowCount := oFile.RowCount;
  iColCount := oFile.ColCount;


  iRowCount := Round((oFile.bounds.lat_max - oFile.bounds.lat_min)  / iStep);
  iColCount := Round((oFile.bounds.lon_max - oFile.bounds.lon_min)  / iStep);


  rXYRect_GK.TopLeft.X:=oFile.bounds.lat_max;
  rXYRect_GK.TopLeft.Y:=oFile.bounds.lon_min;
//  rXYRect_GK.TopLeft.Y:= 1000000*(FZone_Pulkovo) + Round(oFile.bounds.lon_min) mod 1000000;

  //Params.Zone_UTM-30;

  rXYRect_GK.BottomRight.X:=rXYRect_GK.TopLeft.X - iRowCount * iStep;
  rXYRect_GK.BottomRight.Y:=rXYRect_GK.TopLeft.Y + iColCount * iStep;


 //   header_Rec.ZoneNum_GK:=FGK_zone;
//  FDEM_Envi_File.GK_zone:=Params.Zone_UTM-30;

//  FDEM_Envi_File.SaveToIniFile (ChangeFileExt(Params.RLF_FileName,'.ini') );
//  oFile.SaveToXmlFile (ChangeFileExt(Params.RLF_FileName,'.xml') );


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;
//  header_Rec.ZoneNum_GK:=FZone_ -30;
  header_Rec.ZoneNum_GK:=FZone_Pulkovo;

  header_Rec.XYBounds:=rXYRect_GK;
//  header_Rec.XYBounds.TopLeft.Y     :=header_Rec.XYBounds.TopLeft.Y     + 1000000*(Params.Zone_UTM-30);
//  header_Rec.XYBounds.BottomRight.Y :=header_Rec.XYBounds.BottomRight.Y + 1000000*(Params.Zone_UTM-30);

 // sDir:=ExtractFilePath(aRLF_FileName);
//  sDir:=ExtractFilePath(Params.RLF_FileName);
//  ForceDirectories(sDir);


//  sRLF_FileName:=ChangeFileExt(Params.RLF_FileName, '.rlf');

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);
//  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------
  FMemStream:=TMemoryStream.Create;

  FMemStream.SetSize (iColCount * SizeOf(rlf_rec));
  FMemStream.Position:=0;


  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

//    DoOnProgress(iRow, iRowCount, bTerminated);

    if iRow mod 100 = 0 then
      Progress_SetProgress1(iRow, iRowCount, bTerminated);

    //


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

      if FDEM_Envi_File.FindValueByXY(xy.x, xy.y, iW) then
      begin
        if (header_Rec.GroundMinH=0) or (header_Rec.GroundMinH > iW) then
          header_Rec.GroundMinH := iW;

        if (header_Rec.GroundMaxH=0) or (header_Rec.GroundMaxH < iW )then
          header_Rec.GroundMaxH := iW;

       // if iW > High(Smallint) then
        //  iW := High(Smallint);

        rlf_rec.Rel_H :=iW;

        if Params.IsUse_Clutter then
          if FClutter_Envi_File.FindValueByXY(xy.x, xy.y, wCluCode) then
        begin
          if (wCluCode>=1) and (wCluCode<100) then
          begin
            if FClutterInfo.GetOnegaCode(wCluCode,  rClutter) then
            begin
              rlf_rec.Clutter_Code := rClutter.Onega_Code;
              rlf_rec.Clutter_H    := rClutter.Onega_Height;
            end;


          //--------------------------------
          if Params.IsUse_Clutter_H then
            if FClutter_H_Envi_File.FindValueByXY(xy.x, xy.y, iW) then
            begin
             // if w>250 then
              //   w:=0;//250;

              try
                rlf_rec.Clutter_H := iW;
              except
                rlf_rec.Clutter_H :=0;

//CodeSite.SendError(IntToStr(iW));

              end;

//..              if rlf_rec.Clutter_Code in [0,2,4,5] then //������
//..                rlf_rec.Clutter_H := 0;


            end;

        end;

        end;

      end
      else
        rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;
//    FMemStream.Position:=0;
  end;

  // insert GroundMinH, GroundMaxH
  oRlfFileStream.Position:=0;
  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);


  FreeAndNil(oRlfFileStream);

  FreeAndNil(FMemStream);

  // ---------------------------------------------------------------
//  oMatrix:=TrlfMatrix.Create;
//  oMatrix.LoadHeaderFromFile (Params.RLF_FileName);
//  FreeAndNil(oMatrix);

  // ---------------------------------------------------------------


  if Terminated then
    Exit;

  T_Rel_to_Clutter_map.Exec(Params.RLF_FileName);

//  TRel_to_3D_map.Exec(Params.RLF_FileName);


//  if not Terminated then
//    T_Rel_to_Clutter_map.Exec(aRLF_FileName);
//    T_Rel_to_Clutter_map.Exec(aFileName: string);
//    ExportToClutterTab(aRLF_FileName);


end;

// ---------------------------------------------------------------
procedure TTif_to_RLF.Prepare_TIF_to_ENVI;
// ---------------------------------------------------------------
var
  iEPSG: Integer;
  oBAT: TGDAL_Bat;
  sFile: string;
  sBat: string;
  sFileNoExt: string;

  json_rec: TGDAL_json_rec;

begin

  assert (FileExists(params.FileName_Dem) , Params.FileName_Dem);
//  assert (params.Zone_UTM>0);

  oBAT:=TGDAL_Bat.Create;



//  TFile.Delete( ChangeFileExt(Params.FileName_Dem,'.envi'));
//  TFile.Delete( ChangeFileExt(Params.FileName_Clutter,'.envi'));
//  TFile.Delete( ChangeFileExt(Params.FileName_Clutter_H,'.envi'));
    oBAT.Init_Header;

    sFile      := ExtractFileName      (Params.FileName_Dem);
    sFileNoExt := ExtractFileNameNoExt (Params.FileName_Dem);


    oBAT.Add(Format('gdalinfo.exe  -json %s.tif > %s.json',  [sFileNoExt, sFileNoExt ]));

    sBat:=ChangeFileExt(Params.FileName_Dem,'_json.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));

    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');

    sFile:=ChangeFileExt(Params.FileName_Dem,'.json');
    if FileExists(sFile) then
    begin
      json_rec.LoadFromFile(sFile);

      Assert(json_rec.Zone_UTM>0, 'json_rec.Zone_UTM>0');

      FZone_Pulkovo:=json_rec.Zone_UTM - 30;
      if FZone_Pulkovo<0 then
        FZone_Pulkovo:=json_rec.Zone_UTM + 30;

      iEPSG:=28400 + FZone_Pulkovo;

    end else
      raise Exception.Create('Error Message');


  // -----------------------------------------------------
  //  FileName_Dem
  // -----------------------------------------------------


  if not FileExists( ChangeFileExt(Params.FileName_Dem,'.envi'))  then
  begin
    oBAT.Init_Header;

    sFile      := ExtractFileName      (Params.FileName_Dem);
    sFileNoExt := ExtractFileNameNoExt (Params.FileName_Dem);

   //-----------------------------------------------------------------


    oBAT.Add(Format('gdalwarp  -t_srs EPSG:%d  "%s.tif" "%s.gk.tif"' ,  [iEPSG, sFileNoExt, sFileNoExt ] ));
    oBAT.Add(Format('gdal_translate   -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));   //-ot int16
    //-ot int16

    oBAT.Add(Format('gdalinfo.exe  -json "%s.envi" > "%s.envi.json"',          [sFileNoExt, sFileNoExt ]));
//    oBAT.Add(Format('gdalinfo.exe  -json %s.gk.tif > %s.gk.json',       [sFileNoExt, sFileNoExt ]));

//    oBAT.Add(Format('del "%s.gk.tif"' ,  [ sFileNoExt ] ));


    oBAT.Add(Format('rem gdal_translate -of AAIGrid   "%s.tif" "%s.txt"' , [sFileNoExt, sFileNoExt ]));

    //    /
//     gdal_translate.exe  -of AAIGrid   T_03_02m_48_Buratia_Cities.tif  T_03_02m_48_Buratia_Cities.txt

//    gdalwarp -t_srs EPSG:28407  "C_77_50m37_Moskowskaya_1.tif" "C_77_50m37_Moskowskaya_2.tif"


    sBat:=ChangeFileExt(Params.FileName_Dem,'.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));

    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');


  end;


  // -----------------------------------------------------
  //  FileName_Dem
  // -----------------------------------------------------
  if Params.IsUse_Clutter then
    if not FileExists( ChangeFileExt(Params.FileName_Clutter,'.envi'))  then
  begin
    oBAT.Init_Header;

    sFile      := ExtractFileName      ( Params.FileName_Clutter);
    sFileNoExt := ExtractFileNameNoExt ( Params.FileName_Clutter);

    oBAT.Add(Format('gdalwarp  -t_srs EPSG:%d  "%s.tif" "%s.gk.tif"' ,  [iEPSG, sFileNoExt, sFileNoExt ] ));
    oBAT.Add(Format('gdal_translate   -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ])); // ..-ot int16

    //-ot int16

    oBAT.Add(Format('gdalinfo.exe  -json "%s.envi" > "%s.envi.json"',          [sFileNoExt, sFileNoExt ]));
//    oBAT.Add(Format('gdalinfo.exe  -json %s.gk.tif > %s.gk.json',       [sFileNoExt, sFileNoExt ]));

//    oBAT.Add(Format('gdalinfo.exe  -json %s.tif > %s.json',          [sFileNoExt, sFileNoExt ]));

//    oBAT.Add(Format('gdal_translate -of AAIGrid   "%s.tif" "%s.txt"' , [sFileNoExt, sFileNoExt ]));


////    oBAT.Add(Format('gdal_translate  -co compress=lzw  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));
//    oBAT.Add(Format('gdal_translate -a_srs EPSG:%d  -of GTiff  "%s" "%s.utm.tif"', [iEPSG_UTM,  sFile, sFileNoExt ]));
//    oBAT.Add(Format('gdalwarp  -overwrite  -t_srs EPSG:%d   "%s.utm.tif" "%s.gk.tif"', [iEPSG_GK,  sFileNoExt, sFileNoExt ]));
//
//    oBAT.Add(Format('gdal_translate -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));
//    oBAT.Add(Format('gdalinfo.exe   -json %s.envi > %s.json',[sFileNoExt, sFileNoExt]));
//

//    oBAT.Add(Format('gdal_translate  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));

//    gdal_translate -of AAIGrid  -b 1    "grd.tif" "grd_2.asc"


    sBat:=ChangeFileExt(Params.FileName_Clutter,'.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));

    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');

  end;


  // -----------------------------------------------------
  //  FileName_Dem
  // -----------------------------------------------------
  if Params.IsUse_Clutter_H then
    if not FileExists( ChangeFileExt(Params.FileName_Clutter_H,'.envi'))  then
  begin
    oBAT.Init_Header;

    sFile      := ExtractFileName ( Params.FileName_Clutter_H);
    sFileNoExt := ExtractFileNameNoExt ( Params.FileName_Clutter_H);

    oBAT.Add(Format('gdalwarp  -t_srs EPSG:%d  "%s.tif" "%s.gk.tif"' ,  [iEPSG, sFileNoExt, sFileNoExt ] ));
    oBAT.Add(Format('gdal_translate -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));  // -ot int16

    //-ot int16

    oBAT.Add(Format('gdalinfo.exe  -json "%s.envi" > "%s.envi.json"',          [sFileNoExt, sFileNoExt ]));
//    oBAT.Add(Format('gdalinfo.exe  -json %s.gk.tif > %s.gk.json',       [sFileNoExt, sFileNoExt ]));

    oBAT.Add(Format('rem gdal_translate -of AAIGrid   "%s.tif" "%s.txt"' , [sFileNoExt, sFileNoExt ]));


////    oBAT.Add(Format('gdal_translate -co compress=lzw  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));
//    oBAT.Add(Format('gdal_translate -a_srs EPSG:%d  -of GTiff  "%s" "%s.utm.tif"', [iEPSG_UTM,  sFile, sFileNoExt ]));
//    oBAT.Add(Format('gdalwarp   -overwrite -t_srs EPSG:%d   "%s.utm.tif" "%s.gk.tif"', [iEPSG_GK,  sFileNoExt, sFileNoExt ]));
//    oBAT.Add('');
//
//    oBAT.Add(Format('gdal_translate -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));
//    oBAT.Add(Format('gdalinfo.exe   -json %s.envi > %s.json',[sFileNoExt, sFileNoExt]));


    sBat:=ChangeFileExt(Params.FileName_Clutter_H,'.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));

    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');

  end;


  FreeAndNil(oBAT);


end;

//procedure TAsset_to_RLF.CloseFiles;
//begin
//  FDEM_Envi_File.CloseFile;
//  FClutter_Envi_File.CloseFile;
//  FClutter_H_Envi_File.CloseFile;
//end;


// ---------------------------------------------------------------
procedure TTif_to_RLF.Execute;
// ---------------------------------------------------------------

begin
  Terminated := False;

  // -------------------------------------------

  assert(Params.RLF_FileName<>'', Params.RLF_FileName);

  assert (FileExists(params.FileName_Dem) ,params.FileName_Dem );
//  assert (FileExists(params.Projection_Txt_FileName) );

  params.IsUse_Clutter   :=FileExists(params.FileName_Clutter);
  params.IsUse_Clutter_H :=FileExists(params.FileName_Clutter_H);

  // -------------------------------------------

  if Params.IsUse_Clutter then
    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);

  Prepare_TIF_to_ENVI;

  RunFile();

end;

// ---------------------------------------------------------------
procedure TTif_to_RLF.Execute_Dlg;
// ---------------------------------------------------------------
begin
  Progress_ExecDlg('', Execute);

end;



end.



{

function Progress_ExecDlg(aCaption: string; aOnStartEvent: TNotifyEvent): Boolean;
function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''): Boolean;

procedure Progress_SetProgress1(aProgress,aTotal: integer; var aTerminated: boolean);
procedure Progress_SetProgress2(aProgress,aTotal: integer; var aTerminated: boolean);

procedure Progress_SetProgressMsg1(aMsg: string);
procedure Progress_SetProgressMsg2(aMsg: string);

