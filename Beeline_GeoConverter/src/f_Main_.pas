﻿unit f_Main_;

interface

uses
  u_TIF_to_rlf,

  u_Rel_to_Clutter_map,
  u_3D_rel_to_bmp,

  u_cx_ini,
  u_cx,

  d_Clutters,

  u_db_mdb,
  u_db,

  u_func,
  u_files,

  fr_Custom_Form,


  Types,
  Vcl.Dialogs,  Mask, ActnList, Menus, cxShellBrowserDialog,
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls,
  IniFiles,   IOUtils, Variants, cxLookAndFeels, cxVGrid,
  Data.DB, cxDBVGrid,  cxGridDBTableView, Data.Win.ADODB,
  cxGridLevel, cxGridCustomView, cxGrid, Vcl.StdActns, cxSplitter,
  RxPlacemnt,  Vcl.AppEvnts,  cxBarEditItem, dxBarExtItems, dxBar, dxBarExtDBItems, Vcl.DBActns,

  cxGridCustomTableView, cxGraphics, cxControls, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxCheckBox, cxTextEdit, cxButtonEdit,
  cxDBLookupComboBox, cxClasses, cxInplaceContainer, cxGridTableView,
  System.Actions, Vcl.Grids, Vcl.DBGrids, cxSpinEdit, RxToolEdit, cxCurrencyEdit,
  Data.Bind.EngExt, Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, Data.Bind.DBScope, dxmdaset;



type
  Tfrm_Main = class(Tfrm_Custom_Form)
    PopupMenu1: TPopupMenu;
    actSelectfolder1: TMenuItem;
    cxShellBrowserDialog1: TcxShellBrowserDialog;
    ADOConnection_Tasks: TADOConnection;
    t_Task: TADOTable;
    ds_Tasks: TDataSource;
    ActionList1: TActionList;
    FileOpen_DEM: TFileOpen;
    FileOpen_CLU: TFileOpen;
    FileOpen_CLU_H: TFileOpen;
    act_clu_schema: TAction;
    FormStorage1: TFormStorage;
    act_Load_From_Dir: TAction;
    cxLookAndFeelController1: TcxLookAndFeelController;
    act_Run_all: TAction;
    N1: TMenuItem;
    actRunall1: TMenuItem;
    Run1: TMenuItem;
    MainMenu1: TMainMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Run2: TMenuItem;
    actRunall2: TMenuItem;
    Yfcnhjqrf1: TMenuItem;
    act_clu_schema_setup: TAction;
    N5: TMenuItem;
    dxBarManager1: TdxBarManager;
    q_Tasks_checked: TADOQuery;
    BrowseForFolder1: TBrowseForFolder;
    dxBarButton1: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    PageControl1: TPageControl;
    TabSheet1_Tasks: TTabSheet;
    cxGrid_tasks: TcxGrid;
    cxGridDBTableView1_tasks: TcxGridDBTableView;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    col_Clutter_schema: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxSplitter2: TcxSplitter;
    Delete1: TMenuItem;
    N6: TMenuItem;
    act_Project_add: TAction;
    pn_bottom: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_DTM: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid1filename_CLU: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow3: TcxCategoryRow;
    cxDBVerticalGrid1filename_CLU_H: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow4: TcxCategoryRow;
    cxDBVerticalGrid1filename_onega_rlf: TcxDBEditorRow;
    col_is_file_exists: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    t_Taskid: TAutoIncField;
    t_TaskDir: TWideStringField;
    t_Taskchecked: TBooleanField;
    t_Taskfilename_DTM: TWideStringField;
    t_Taskfilename_CLU: TWideStringField;
    t_Taskfilename_CLU_H: TWideStringField;
    t_Taskclutter_schema: TWideStringField;
    t_Taskfilename_onega_rlf: TWideStringField;
    t_Taskis_file_exists: TBooleanField;
    FileSaveAs_relief: TFileSaveAs;
    DirectoryEdit1: TDirectoryEdit;
    dxBarControlContainerItem1: TdxBarControlContainerItem;
    act_RLF_to_clu: TAction;
    Splitter1: TSplitter;
    actreltoclu1: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    actreltoclu2: TMenuItem;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow;
    cxGridDBTableView1_tasksColumn1: TcxGridDBColumn;
    cxGridDBTableView1_tasksColumn2: TcxGridDBColumn;
    cxGridDBTableView1_tasksColumn3: TcxGridDBColumn;
    cxGridDBTableView1_tasksColumn4: TcxGridDBColumn;
    t_TaskFileSize_DTM_: TLargeintField;
    t_TaskFileSize_RLF: TLargeintField;
    t_TaskFileSize_clu_h: TLargeintField;
    t_TaskFileSize_Clu: TLargeintField;
    act_Run: TAction;
    DatasetDelete1: TDataSetDelete;
    cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow;
    act_RLF_to_3D: TAction;
    actRLFto3D1: TMenuItem;
    actRLFto3D2: TMenuItem;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    cxBarEditItem1: TcxBarEditItem;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure act_clu_schemaExecute(Sender: TObject);
//    procedure act_clu_schema_change_in_tasksExecute(Sender: TObject);
    procedure act_Load_From_DirExecute(Sender: TObject);
    procedure act_RLF_to_3DExecute(Sender: TObject);
    procedure act_RLF_to_cluExecute(Sender: TObject);
    procedure act_RunExecute(Sender: TObject);
//    procedure act_Project_addExecute(Sender: TObject);
    procedure act_Run_allExecute(Sender: TObject);
//    procedure act_StopExecute(Sender: TObject);
    procedure BrowseForFolder1Accept(Sender: TObject);
//    procedure act_StopExecute(Sender: TObject);
//    procedure act_Select_folderExecute(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBVerticalGrid1filename_DTMEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FileOpen_DEMAccept(Sender: TObject);
    procedure cxDBVerticalGrid1filename_CLU_HEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1filename_onega_rlfEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1filename_CLUEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
//    procedure FileOpen_reliefAccept(Sender: TObject);
    procedure cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FileOpen_CLUAccept(Sender: TObject);
    procedure FileOpen_CLU_HAccept(Sender: TObject);
//    procedure Yfcnhjqrf1Click(Sender: TObject);
    procedure cxDBVerticalGrid1DBEditorRow3EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FileSaveAs_reliefAccept(Sender: TObject);
//    procedure FormShow(Sender: TObject);
    procedure t_TaskCalcFields(DataSet: TDataSet);
//    procedure t_TaskNewRecord(DataSet: TDataSet);
//    procedure frame_Clutter_1Button1Click(Sender: TObject);

  private
  //  FTerminated : Boolean;
    FRegPath: string;
    FDataset: TDataset;

 //   FIniFileName: string;


//    function Find_Index_txt(aDir, aPath: string; var aFileName: string): Boolean;
//    FDataset: TDataset;
    
    procedure Process_Dir(aDir: string);
    procedure Run_All;
//    procedure Run_asset(aDataset: TDataset);
//    procedure Run_ENVI(aDataset: TDataset);


//    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run(aDataset: TDataset); //override;
  public

//    procedure LoadFromIniFile(aFileName: String);

  end;

//
var
  frm_Main: Tfrm_Main;


implementation


{$R *.dfm}

//const
//  DEF_SECTION = 'ASSET_to_RLF';


const
  FLD_Filename_onega_rlf      = 'filename_onega_rlf';
  FLD_Filename_CLU_H          = 'filename_CLU_H';
  FLD_Filename_CLU            = 'filename_CLU';
//  FLD_Is_Use_Clutter_bounds   = 'Is_Use_Clutter_bounds';
//  FLD_Is_use_Clutter_Heights  = 'is_use_Clutter_Heights';
//  FLD_Is_use_clutter          = 'is_use_clutter';
  FLD_Filename_DTM            = 'filename_DTM';
//  FLD_filename_projection_txt = 'filename_projection_txt';  //--filename_projection_txt
  FLD_clutter_schema          = 'clutter_schema';
//  FLD_step                    = 'step';
//  FLD_UTM_zone                = 'UTM_zone';

  FLD_DIR = 'Dir';
  

  
//-----------------------------------------------------------------
procedure Tfrm_Main.ActionList1Update(Action: TBasicAction; var  Handled: Boolean);
//-----------------------------------------------------------------
var
  b: Boolean;
begin
  b:=not t_Task.IsEmpty;

  act_Run.Enabled:=b;
  act_Run_all.Enabled:=b;

  act_RLF_to_clu.Enabled:=b;
  act_RLF_to_3D.Enabled:=b;

end;


procedure Tfrm_Main.FormDestroy(Sender: TObject);
begin
  cxGridDBTableView1_tasks.StoreToRegistry ( FRegPath +  cxGridDBTableView1_tasks.Name);


//  cxGrid1DBTableView1.StoreTo (cxPropertiesStore1.Name);

end;

// ---------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
//  CodeSite.Send('procedure Tframe_ASSET_to_RLF.FormCreate(Sender: TObject);');

  inherited;

//  Confi

//  ShowMessage('procedure Tframe_ASSET_to_RLF.FormCreate(Sender: TObject);');

  Caption := 'Beeline GeoConverter ' + '| создан: ' + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));
//  Caption := 'Asset -> Onega rlf ' + '| создан:' GetAppVersionStr;

//    Caption:= 'Ïîñòðîåíèå LOS | '+  'ñîçäàí: '  + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));



  if ADOConnection_Tasks.Connected then
    ShowMessage('ADOConnection1.Connected');
 
  

  FRegPath := 'Software\Onega\ASSET_to_RLF\v3\';


 // cxShellBrowserDialog1.Path:='D:\_beeline_mrr\03_Buryatia';

  PageControl1.Align:=alClient;
//  cxDBVerticalGrid1.Align:=alClient;

  cxGrid_tasks.Align:=alClient;
  //cxGrid1_projects.Align:=alClient;
  
  //  FRegPath := REGISTRY_COMMON_FORMS + ClassName +'\';


//..  FRegPath := g_Storage.GetPathByClass(ClassName)+ '1';

  
//s:=  FormStorage1.RegIniFile.FileName;
  

//  cxDBVerticalGrid1.Align:=alLeft;


  act_Run.Caption:='Построить';
  act_Run_all.Caption:='Построить все';

  
  //cxShellBrowserDialog1.Path:='P:\_megafon\83_NW_NenetskyAO_UTM40\';
  
//  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

 // LoadFromIniFile(FIniFileName);

 ADOConnection_Tasks.Close;

  if not mdb_OpenConnection(ADOConnection_Tasks, GetApplicationDir() + 'GeoConverter.tasks.mdb') then
    Halt;


//ShowMessage('111');

  t_Task.Open;
//  t_Project.Open;

//  t_Project.Locate('name', dxBarLookupCombo1.Text, []);


  FDataset:=t_Task;


  cxGridDBTableView1_tasks.RestoreFromRegistry ( FRegPath +  cxGridDBTableView1_tasks.Name);
  cxGridDBTableView1_tasks.OptionsView.GroupByBox:=true;


{

  cx_LoadColumnCaptions_FromIni(cxGridDBTableView1_tasks);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);
 }

  cxGridDBTableView1_tasks.Navigator.Visible:=True;


  db_SetFieldCaptions(t_Task, [
   'DIR',      'Папка' ,
   'CHECKED',  'Вкл',
   'Filename_DTM',  'Height',

   'Filename_CLU',         'Clutter',
   'Filename_CLU_H',       'Clutter Height',

   'clutter_schema',       'Цветовая схема',

   'Filename_onega_rlf',   'Onega RLF',
   'is_file_exists',       'RLF сущ'

  ]);





//
//DIR=   Папка
//
//CHECKED=  Вкл
//
//Filename_DTM=            DTM (index.txt, asc)
//
//Filename_CLU=            Clutter (index.txt, asc, grc)
//Filename_CLU_H=          Clutter Height (index.txt, asc)
//
//Is_use_clutter=          Использовать Сlutter
//Is_use_Clutter_Heights=  Использовать Clutter Height
//Is_Use_Clutter_bounds=   Использовать рамку из Сlutter
//
//filename_projection_txt= Projection.txt
//clutter_schema=          Цветовая схема
//UTM_zone=                UTM зона
//
//Filename_onega_rlf=      Onega RLF
//step=                    Шаг


//  cxGrid1DBTableView1.re 

end;

// ---------------------------------------------------------------
procedure Tfrm_Main.act_clu_schemaExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  if Sender=act_clu_schema then
  begin
    s:=FDataset.FieldByName(FLD_clutter_schema).AsString;

    if Tdlg_Clutters.ExecDlg(s) then
      db_UpdateRecord_NoPost(FDataset, [FLD_clutter_schema, s]);
  end else


  if Sender=act_clu_schema_setup then
    Tdlg_Clutters.ExecDlg(s);

  
end;


// ---------------------------------------------------------------
procedure Tfrm_Main.act_Load_From_DirExecute(Sender: TObject);
// ---------------------------------------------------------------
//var
//  sClutter_schema: string;
var
  sClutter_schema: string;
begin
// cxShellBrowserDialog1.Path:=DirectoryEdit1.Text;

// if cxShellBrowserDialog1.Execute then
//   if Tdlg_Clutters.ExecDlg(sClutter_schema) then
//   begin
     Process_Dir (IncludeTrailingBackslash(DirectoryEdit1.Text));

  //   DirectoryEdit1.Text:=cxShellBrowserDialog1.Path;
 //  end;

//     dmAsset_to_RLF.Process_Dir (IncludeTrailingBackslash(cxShellBrowserDialog1.Path), FDataset, sClutter_schema );

end;

procedure Tfrm_Main.act_RLF_to_3DExecute(Sender: TObject);
var
  sFile: string;
begin
  sFile := FDataset.FieldByName(FLD_Filename_onega_rlf).AsString;
//  T_Rel_to_Clutter_map.Exec(sFile);
  TRel_to_3D_map.Exec(sFile);
end;


procedure Tfrm_Main.act_RLF_to_cluExecute(Sender: TObject);
var
  sFile: string;
begin
  sFile := FDataset.FieldByName(FLD_Filename_onega_rlf).AsString;
  T_Rel_to_Clutter_map.Exec(sFile);
end;

procedure Tfrm_Main.act_RunExecute(Sender: TObject);
begin
  Run (FDataset);
end;


// ---------------------------------------------------------------
procedure Tfrm_Main.Process_Dir(aDir: string);
// ---------------------------------------------------------------

    function DoFindByMask(aMask: string; aStrArr: TStringDynArray): string;
    var
      I: Integer;
    begin
      result:='';

      for I := 0 to High(aStrArr) do
        if pos('.gk.', LowerCase(aStrArr[i]))=0 then
          if Pos(LowerCase(aMask), LowerCase(aStrArr[i]))>0 then
            Exit(aStrArr[i]);
    end;



var
  j: Integer;

  arr: TStringDynArray;
  arr_dem: TStringDynArray;
  arr_clu: TStringDynArray;
  arr_clu_h: TStringDynArray;

  I: Integer;
  sClutter_schema: string;
  sDir_Clutter: string;
  sDir_Clutter_H: string;
  sDir_Heights: string;
  sDir_root: string;
  sFile_clu: string;
  sFile_clu_H: string;
  sFile_DTM: string;
  sFile_rlf: string;

begin
  FDataset.DisableControls;

  aDir:= IncludeTrailingBackslash(aDir);

  arr:=TDirectory.GetDirectories(aDir, '*', TSearchOption.soAllDirectories);


  for I := 0 to High(arr) do
    if Eq('Heights', ExtractFileName( arr[i] )) then
//    if Eq('Geodata', ExtractFileName( arr[i] )) then
    begin
//      CheckListBox1.Items.Add(arr[i]);

//      sDir_root:=ExtractFilePath( arr[i] );

      sDir_root:=IncludeTrailingBackslash(ExtractFilePath( arr[i] ));

      sDir_Heights:=sDir_root + 'Heights\';
      sDir_Clutter:=sDir_root + 'Clutter\';
      sDir_Clutter_H:=sDir_root + 'Clutter height\';

      if DirectoryExists(sDir_Clutter) then
        arr_clu  :=TDirectory.GetFiles (sDir_Clutter ,   '*.tif', TSearchOption.soAllDirectories) ;

      if DirectoryExists(sDir_Clutter_H) then
        arr_clu_h:=TDirectory.GetFiles (sDir_Clutter_H , '*.tif', TSearchOption.soAllDirectories) ;

      if DirectoryExists(sDir_Heights) then
        arr_dem:=TDirectory.GetFiles (sDir_Heights , '*.tif', TSearchOption.soAllDirectories) ;

      for j := 0 to High(arr_dem) do
      begin
        if pos('.gk.', arr_dem[j])>0 then
          Continue;


         sFile_DTM:=arr_dem[j];
         sFile_rlf:=sDir_root + ChangeFileExt (ExtractFileName( sFile_DTM ), '.rlf');

         sFile_clu  := sDir_Clutter + 'C' + Copy(ExtractFileName( sFile_DTM ),2,1000);
         sFile_clu_H:= sDir_Clutter + 'H' + Copy(ExtractFileName( sFile_DTM ),2,1000);

         if not FileExists(sFile_clu) then
           sFile_clu:='';

         if not FileExists(sFile_clu_h) then
           sFile_clu_h:='';


         if (sFile_clu='') and (Pos('_cities',  LowerCase(sFile_DTM))>0) then
           sFile_clu:=DoFindByMask('_cities', arr_clu);

         if (sFile_clu_h='') and (Pos('_cities',  LowerCase(sFile_DTM))>0) then
           sFile_clu_h:=DoFindByMask('_cities', arr_clu_h);


         if Pos('_cities',  LowerCase(sFile_DTM))>0 then
           sClutter_schema:='Beeline_cities'
         else
           sClutter_schema:='Beeline_region';



         if not FDataset.Locate (FLD_Filename_DTM, sFile_DTM, [])  then
           db_AddRecord_(FDataset, [

              FLD_DIR                     , sDir_root,
              FLD_CHECKED                 , True,

              FLD_Filename_DTM            , sFile_DTM,
              FLD_Filename_CLU            , IIF_NULL (sFile_clu),
              FLD_Filename_CLU_H          , IIF_NULL (sFile_clu_h),

              FLD_clutter_schema          , sClutter_schema,

              FLD_Filename_onega_rlf      , sFile_rlf //aDir + GetParentFolderName_v1 + sName + '.rlf',

           ]);
      end;

    end;

  FDataset.EnableControls;


end;


procedure Tfrm_Main.cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  act_clu_schema.Execute;

end;

procedure Tfrm_Main.cxDBVerticalGrid1DBEditorRow3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BrowseForFolder1.Folder:= FDataset.FieldByName(FLD_DIR).AsString;
  BrowseForFolder1.Execute;


end;

procedure Tfrm_Main.cxDBVerticalGrid1filename_CLUEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  sDir: string;
begin
  sDir := FDataset.FieldByName(FLD_DIR).AsString;

  FileOpen_CLU.Dialog.FileName := FDataset.FieldByName(FLD_filename_CLU).AsString;

  FileOpen_CLU.Dialog.InitialDir:= IncludeTrailingBackslash(sDir) + 'Clutter\';
  FileOpen_CLU.Execute;

end;

procedure Tfrm_Main.cxDBVerticalGrid1filename_CLU_HEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  sDir: string;
begin
  sDir := FDataset.FieldByName(FLD_DIR).AsString;

  FileOpen_CLU_H.Dialog.FileName := FDataset.FieldByName(FLD_filename_CLU_H).AsString;

 // FileOpen_CLU_H.Dialog.InitialDir:= ExtractFilePath(FileOpen_CLU_H.Dialog.FileName);
  FileOpen_CLU_H.Dialog.InitialDir:= IncludeTrailingBackslash (sDir) + 'Clutter Height\';

  FileOpen_CLU_H.Execute;
end;


procedure Tfrm_Main.cxDBVerticalGrid1filename_DTMEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
var
  sDir: string;
begin
  sDir := FDataset.FieldByName(FLD_DIR).AsString;

  FileOpen_DEM.Dialog.FileName := FDataset.FieldByName(FLD_filename_DTM).AsString;
//  FileOpen_DEM.Dialog.InitialDir:= ExtractFilePath(FileOpen_DEM.Dialog.FileName);

  FileOpen_DEM.Dialog.InitialDir:= IncludeTrailingBackslash(sDir) + 'Heights\';
  FileOpen_DEM.Execute;
end;


procedure Tfrm_Main.cxDBVerticalGrid1filename_onega_rlfEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  sDir: string;
begin
  sDir := FDataset.FieldByName(FLD_DIR).AsString;


  FileSaveAs_relief.Dialog.FileName := FDataset.FieldByName(FLD_filename_onega_rlf).AsString;
//  FileOpen_relief.Dialog.InitialDir:= ExtractFilePath(FileOpen_relief.Dialog.FileName);

  FileSaveAs_relief.Dialog.InitialDir:= IncludeTrailingBackslash(sDir);
  FileSaveAs_relief.Execute;

end;


procedure Tfrm_Main.FileOpen_CLUAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_clu, TFileOpen (Sender).Dialog.FileName]);
end;

procedure Tfrm_Main.FileOpen_CLU_HAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_clu_h, TFileOpen (Sender).Dialog.FileName]);
end;

procedure Tfrm_Main.FileOpen_DEMAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_DTM, TFileOpen (Sender).Dialog.FileName]);
end;

procedure Tfrm_Main.BrowseForFolder1Accept(Sender: TObject);
begin

  db_UpdateRecord_NoPost(FDataset, [FLD_DIR, TBrowseForFolder (Sender).Folder]);
  
end;


// ---------------------------------------------------------------
procedure Tfrm_Main.Run(aDataset: TDataset);
// ---------------------------------------------------------------
var
  obj: TTif_to_RLF;
//  sFile: string;

begin
//  sFile:=aDataset.FieldByName(FLD_Filename_DTM).AsString;

  //----------------------------------------------------------------
  obj := TTif_to_RLF.Create();
//  obj.OnProgress :=DoOnProgress;


 // FTerminated := False;

  //btn_Run.Action := act_Stop;

  obj.Params.FileName_Dem := aDataset.FieldByName(FLD_Filename_DTM).AsString;
  obj.Params.FileName_Clutter := aDataset.FieldByName(FLD_filename_CLU).AsString;
  obj.Params.FileName_Clutter_H := aDataset.FieldByName(FLD_Filename_CLU_H).AsString;

  obj.Params.Clutters_section :=aDataset.FieldByName(FLD_clutter_schema).AsString;

  obj.Params.RLF_FileName :=aDataset.FieldByName(FLD_filename_onega_rlf).AsString;


  try
    obj.Execute_Dlg;
  except

  end;

  FreeAndNil(obj);

  t_Task.Refresh;


//  btn_Run.Action := act_Run;
  //ProgressBar1.Position :=0;
end;



procedure Tfrm_Main.act_Run_allExecute(Sender: TObject);
begin
  Run_All;
end;



procedure Tfrm_Main.FileSaveAs_reliefAccept(Sender: TObject);
begin

  db_UpdateRecord_NoPost(FDataset, [FLD_filename_onega_rlf, TFileSaveAs (Sender).Dialog.FileName]);
end;



// ---------------------------------------------------------------
procedure Tfrm_Main.Run_All;
// ---------------------------------------------------------------
var
  bTerminated: Boolean;
begin
  q_Tasks_checked.Close;
  q_Tasks_checked.Open;

 // db_View(q_Tasks_checked);
  
  bTerminated:=False;

  with q_Tasks_checked do
    while not EOF and not bTerminated do
    begin
      Run (q_Tasks_checked) ;

      Next;
    end;

end;

//-----------------------------------------------------------------
procedure Tfrm_Main.t_TaskCalcFields(DataSet: TDataSet);
//-----------------------------------------------------------------
var
  s: string;
begin
  s:=DataSet.FieldByName(FLD_Filename_onega_rlf).AsString;
  DataSet['is_file_exists']:=FileExists(s);


  DataSet['FileSize_rlf']:=GetFileSize_(s);

  s:=DataSet.FieldByName(FLD_Filename_DTM).AsString;
  DataSet['FileSize_DTM']:=GetFileSize_(s);


  s:=DataSet.FieldByName(FLD_Filename_Clu).AsString;
  DataSet['FileSize_Clu']:=GetFileSize_(s);

  s:=DataSet.FieldByName(FLD_Filename_Clu_h).AsString;
  DataSet['FileSize_Clu_h']:=GetFileSize_(s);


end;



begin


end.


