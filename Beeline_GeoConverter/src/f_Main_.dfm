inherited frm_Main: Tfrm_Main
  Left = 743
  Top = 501
  Caption = 'frm_Main'
  ClientHeight = 579
  ClientWidth = 1473
  Menu = MainMenu1
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  ExplicitWidth = 1489
  ExplicitHeight = 638
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 28
    Width = 945
    Height = 532
    ActivePage = TabSheet1_Tasks
    Align = alLeft
    TabOrder = 1
    object TabSheet1_Tasks: TTabSheet
      Caption = #1047#1072#1076#1072#1085#1080#1103
      object cxGrid_tasks: TcxGrid
        Left = 0
        Top = 0
        Width = 937
        Height = 185
        Align = alTop
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView1_tasks: TcxGridDBTableView
          PopupMenu = PopupMenu1
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          FilterBox.Visible = fvNever
          DataController.DataSource = ds_Tasks
          DataController.KeyFieldNames = 'id'
          DataController.Options = []
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = cxGridDBColumn3
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.Indicator = True
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Width = 29
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'Dir'
            Width = 143
          end
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'filename_DTM'
            SortIndex = 0
            SortOrder = soAscending
            Width = 209
          end
          object cxGridDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'filename_CLU'
            Width = 166
          end
          object cxGridDBColumn8: TcxGridDBColumn
            DataBinding.FieldName = 'filename_CLU_H'
            Width = 155
          end
          object cxGridDBColumn14: TcxGridDBColumn
            DataBinding.FieldName = 'filename_onega_rlf'
            Width = 172
          end
          object col_Clutter_schema: TcxGridDBColumn
            DataBinding.FieldName = 'Clutter_schema'
            Width = 85
          end
          object col_is_file_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_file_exists'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 46
          end
          object cxGridDBTableView1_tasksColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'FileSize_Clu'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0'
          end
          object cxGridDBTableView1_tasksColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'FileSize_clu_h'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0'
          end
          object cxGridDBTableView1_tasksColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'FileSize_DTM'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0'
          end
          object cxGridDBTableView1_tasksColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'FileSize_RLF'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0'
          end
        end
        object cxGridLevel1: TcxGridLevel
          Caption = #1047#1072#1076#1072#1085#1080#1103
          GridView = cxGridDBTableView1_tasks
        end
      end
      object cxSplitter2: TcxSplitter
        Left = 0
        Top = 224
        Width = 937
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salBottom
        Control = pn_bottom
      end
      object pn_bottom: TPanel
        Left = 0
        Top = 232
        Width = 937
        Height = 272
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object Splitter1: TSplitter
          Left = 785
          Top = 0
          Height = 272
          ExplicitLeft = 1288
          ExplicitTop = 272
          ExplicitHeight = 100
        end
        object cxDBVerticalGrid1: TcxDBVerticalGrid
          Left = 0
          Top = 0
          Width = 785
          Height = 272
          Align = alLeft
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          LookAndFeel.ScrollbarMode = sbmClassic
          OptionsView.ScrollBars = ssVertical
          OptionsView.RowHeaderWidth = 108
          OptionsData.Appending = False
          Navigator.Buttons.CustomButtons = <>
          ParentFont = False
          TabOrder = 0
          DataController.DataSource = ds_Tasks
          Version = 1
          object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1DBEditorRow3EditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'Dir'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
            Properties.Caption = #1056#1077#1083#1100#1077#1092
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_DTM: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_DTMEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_DTM'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 2
            ParentID = 1
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.DisplayFormat = ',0'
            Properties.DataBinding.FieldName = 'FileSize_DTM'
            Properties.Options.Editing = False
            ID = 3
            ParentID = 1
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
            Properties.Caption = 'Clutters'
            ID = 4
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid1filename_CLU: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_CLUEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_CLU'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 5
            ParentID = 4
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'clutter_schema'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 6
            ParentID = 4
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.DisplayFormat = ',0'
            Properties.DataBinding.FieldName = 'FileSize_Clu'
            Properties.Options.Editing = False
            ID = 7
            ParentID = 4
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow3: TcxCategoryRow
            Properties.Caption = #1042#1099#1089#1086#1090#1099' '#1089#1090#1088#1086#1077#1085#1080#1081
            ID = 8
            ParentID = -1
            Index = 3
            Version = 1
          end
          object cxDBVerticalGrid1filename_CLU_H: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_CLU_HEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_CLU_H'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 9
            ParentID = 8
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.DisplayFormat = ',0'
            Properties.DataBinding.FieldName = 'FileSize_clu_h'
            Properties.Options.Editing = False
            ID = 10
            ParentID = 8
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow4: TcxCategoryRow
            Properties.Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' - '#1052#1072#1090#1088#1080#1094#1072' '#1074#1099#1089#1086#1090' rlf'
            ID = 11
            ParentID = -1
            Index = 4
            Version = 1
          end
          object cxDBVerticalGrid1filename_onega_rlf: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_onega_rlfEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_onega_rlf'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 12
            ParentID = 11
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.DisplayFormat = ',0'
            Properties.DataBinding.FieldName = 'FileSize_RLF'
            Properties.Options.Editing = False
            ID = 13
            ParentID = 11
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.NullStyle = nssUnchecked
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.DataBinding.FieldName = 'is_file_exists'
            ID = 14
            ParentID = 11
            Index = 2
            Version = 1
          end
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 560
    Width = 1473
    Height = 19
    Panels = <>
  end
  object DirectoryEdit1: TDirectoryEdit
    Left = 967
    Top = 480
    Width = 290
    Height = 21
    DialogKind = dkWin32
    NumGlyphs = 1
    TabOrder = 4
    Text = 'c:\'
  end
  object PopupMenu1: TPopupMenu
    Left = 1008
    Top = 112
    object Run1: TMenuItem
      Action = act_Run
    end
    object actRunall1: TMenuItem
      Action = act_Run_all
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object actreltoclu1: TMenuItem
      Action = act_RLF_to_clu
    end
    object actRLFto3D1: TMenuItem
      Action = act_RLF_to_3D
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object actSelectfolder1: TMenuItem
      Action = act_Load_From_Dir
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Delete1: TMenuItem
      Action = DatasetDelete1
    end
  end
  object cxShellBrowserDialog1: TcxShellBrowserDialog
    Left = 1016
    Top = 296
  end
  object ADOConnection_Tasks: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\GI' +
      'S\Beeline_MRR_geoTif\bin\GeoConverter.tasks.mdb;Mode=Share Deny ' +
      'None;Persist Security Info=False;Jet OLEDB:System database="";Je' +
      't OLEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLED' +
      'B:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Glob' +
      'al Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet O' +
      'LEDB:New Database Password="";Jet OLEDB:Create System Database=F' +
      'alse;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Copy Local' +
      'e on Compact=False;Jet OLEDB:Compact Without Replica Repair=Fals' +
      'e;Jet OLEDB:SFP=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 1136
    Top = 52
  end
  object t_Task: TADOTable
    Connection = ADOConnection_Tasks
    CursorType = ctStatic
    OnCalcFields = t_TaskCalcFields
    TableDirect = True
    TableName = 'Task'
    Left = 1144
    Top = 136
    object t_Taskid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object t_TaskDir: TWideStringField
      FieldName = 'Dir'
      Size = 255
    end
    object t_Taskchecked: TBooleanField
      FieldName = 'checked'
    end
    object t_Taskfilename_DTM: TWideStringField
      FieldName = 'filename_DTM'
      Size = 255
    end
    object t_Taskfilename_CLU: TWideStringField
      FieldName = 'filename_CLU'
      Size = 255
    end
    object t_Taskfilename_CLU_H: TWideStringField
      FieldName = 'filename_CLU_H'
      Size = 255
    end
    object t_Taskclutter_schema: TWideStringField
      FieldName = 'clutter_schema'
      Size = 50
    end
    object t_Taskfilename_onega_rlf: TWideStringField
      FieldName = 'filename_onega_rlf'
      Size = 250
    end
    object t_Taskis_file_exists: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'is_file_exists'
      Calculated = True
    end
    object t_TaskFileSize_DTM_: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'FileSize_DTM'
      ReadOnly = True
      DisplayFormat = ',0'
      Calculated = True
    end
    object t_TaskFileSize_RLF: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'FileSize_RLF'
      ReadOnly = True
      DisplayFormat = ',0'
      Calculated = True
    end
    object t_TaskFileSize_clu_h: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'FileSize_clu_h'
      ReadOnly = True
      DisplayFormat = ',0'
      Calculated = True
    end
    object t_TaskFileSize_Clu: TLargeintField
      FieldKind = fkCalculated
      FieldName = 'FileSize_Clu'
      ReadOnly = True
      DisplayFormat = ',0'
      Calculated = True
    end
  end
  object ds_Tasks: TDataSource
    DataSet = t_Task
    Left = 1144
    Top = 184
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 1008
    Top = 160
    object FileOpen_DEM: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.Filter = 'TIF (*.tif)|*.tif'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_DEMAccept
    end
    object FileOpen_CLU: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.Filter = 'TIF (*.tif)|*.tif'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_CLUAccept
    end
    object FileOpen_CLU_H: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.Filter = 'TIF (*.tif)|*.tif'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_CLU_HAccept
    end
    object act_clu_schema: TAction
      Category = 'File'
      Caption = 'act_clu_schema'
      OnExecute = act_clu_schemaExecute
    end
    object act_Load_From_Dir: TAction
      Category = 'File'
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1087#1072#1087#1082#1080
      OnExecute = act_Load_From_DirExecute
    end
    object act_Run_all: TAction
      Category = 'File'
      Caption = 'act_Run_all'
      OnExecute = act_Run_allExecute
    end
    object act_clu_schema_setup: TAction
      Category = 'File'
      Caption = #1062#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072
      OnExecute = act_clu_schemaExecute
    end
    object FileSaveAs_relief: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Dialog.Filter = 'Rlf (*.rlf)|*.rlf'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = FileSaveAs_reliefAccept
    end
    object BrowseForFolder1: TBrowseForFolder
      Category = 'File'
      Caption = 'BrowseForFolder1'
      DialogCaption = 'BrowseForFolder1'
      BrowseOptions = []
      BrowseOptionsEx = []
      OnAccept = BrowseForFolder1Accept
    end
    object act_Project_add: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    end
    object act_RLF_to_clu: TAction
      Category = 'rlf_to_bmp'
      Caption = 'RLF -> Clutter map'
      OnExecute = act_RLF_to_cluExecute
    end
    object act_Run: TAction
      Category = 'File'
      Caption = 'act_Run'
      OnExecute = act_RunExecute
    end
    object DatasetDelete1: TDataSetDelete
      Category = 'Dataset'
      Caption = '&'#1059#1076#1072#1083#1080#1090#1100
      Hint = 'Delete'
      ImageIndex = 5
      DataSource = ds_Tasks
    end
    object act_RLF_to_3D: TAction
      Category = 'rlf_to_bmp'
      Caption = 'RLF -> 3D'
      OnExecute = act_RLF_to_3DExecute
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'cxShellBrowserDialog1.Path'
      'pn_bottom.Height'
      'PageControl1.TabIndex'
      'DirectoryEdit1.Text'
      'cxDBVerticalGrid1.Width')
    StoredValues = <>
    Left = 1144
    Top = 296
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = False
    Left = 1016
    Top = 352
  end
  object MainMenu1: TMainMenu
    Left = 1008
    Top = 56
    object N2: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object N3: TMenuItem
        Action = act_Load_From_Dir
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Run2: TMenuItem
        Caption = 'Run'
      end
      object actRunall2: TMenuItem
        Action = act_Run_all
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object actreltoclu2: TMenuItem
        Action = act_RLF_to_clu
      end
      object actRLFto3D2: TMenuItem
        Action = act_RLF_to_3D
      end
    end
    object Yfcnhjqrf1: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      object N5: TMenuItem
        Action = act_clu_schema_setup
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.NativeStyle = False
    NotDocking = [dsNone]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 1016
    Top = 240
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 1
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 569
      FloatTop = 313
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarControlContainerItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Run_all
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Run
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Load_From_Dir
      Category = 0
    end
    object dxBarControlContainerItem1: TdxBarControlContainerItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Control = DirectoryEdit1
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
    end
  end
  object q_Tasks_checked: TADOQuery
    Connection = ADOConnection_Tasks
    Parameters = <>
    SQL.Strings = (
      '  select * '
      'from Task '
      'where '
      ''
      ' checked=true'
      ''
      'order by'
      ''
      ' Filename_DTM')
    Left = 1144
    Top = 248
  end
  object BindSourceDB1: TBindSourceDB
    ScopeMappings = <>
    Left = 728
    Top = 304
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 20
    Top = 5
  end
end
