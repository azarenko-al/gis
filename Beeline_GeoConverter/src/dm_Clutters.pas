unit dm_Clutters;

interface

uses
//  VM_Grids,

  u_func,
  u_db,
  u_db_mdb,

  u_clutter_classes,

  SysUtils, Classes,  Forms,  Variants,  Dialogs,
  DB, ADODB
  ;

//type

//  TAsset_Menu_TXT_rec = record
//  public
//    Items: array of record
//                      Code: integer;
//                      Name: string;
//                    end;
//
//    procedure LoadFromFile(aFileName: string);
//
//  end;

  ////   ReliefClutters_ : TDictionary<byte, double>;
  
  
type
  TdmClutters = class(TDataModule)
    ADOConnection1: TADOConnection;
    t_schema_clutters: TADOTable;
    ds_schema_clutters: TDataSource;
    qry_Schema_clutters: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
//    procedure t_schema_cluttersNewRecord(DataSet: TDataSet);

  public
//    procedure AddItem(aName, aOnega_name: string; aHeight: Integer);
 //   procedure AddSection(aName: string);
//    procedure Open;

//    procedure LoadFromFile_Mapinfo_GRC(aFileName: string);

 //   procedure LoadFromFile_Menu_TXT(aSchema_ID: integer; aFileName: string);

//    procedure OpenData(aSchema_name: string);

    procedure LoadCluttersFromDB(aClutterInfo: TClutterModel; aSchema_name: string);

  end;


  //function dmClutters: TdmClutters;

  var
  dmClutters: TdmClutters;


const
  FLD_Name = 'name';
  FLD_ID = 'ID';
  FLD_CODE = 'CODE';
  FLD_Height = 'Height';

implementation
{$R *.dfm}
                         


const
  DEF_FILENAME = 'GeoConverter.mdb';

//  FLD_Name = 'name';
  FLD_Schema_ID = 'schema_ID';
  FLD_Onega_NAME = 'Onega_NAME';
           
  FLD_Onega_code = 'Onega_code';


// ---------------------------------------------------------------
procedure TdmClutters.LoadCluttersFromDB(aClutterInfo: TClutterModel;  aSchema_name: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;
  s1: string;
  i1: integer;
  iOnega_Height: Integer;
  iPos: Integer;
  s2: string;
  s3: string;

  rec: TClutterRec;

begin
  qry_Schema_clutters.SQL.Text:=
     Format('SELECT * FROm %s WHERE Schema_name =''%s'' ORDER BY code',
       ['view_schema_clutters_enabled', aSchema_name]);

  qry_Schema_clutters.Open;



//  if aSchema_name='' then
//    aSchema_name:=DEF_SECTION_ASC;

 // TdmClutters.Init;

//  dmClutters.view_schema_clutters.Close;
//  dmClutters.view_schema_clutters.Filter:='schema_name='''+ aSchema_name + '''';

 // OpenData(aSchema_name);

(*
  view_schema_clutters.SQL.Text:=
     Format('SELECT * FROm %s WHERE %s=''%s'' ',
       [view_schema_clutters, 'schema_name', ]);

  dmClutters.view_schema_clutters.Open;
*)
//  dmClutters.t_schema.Locate(FLD_NAME, aSchema_name, []);

//  SetLength(aClutterInfo.FClutterArr, qry_Schema_clutters.RecordCount);


  with qry_Schema_clutters do
    while not EOF do
  begin
//    i:=RecNo-1;

    rec.Code := FieldByName(FLD_Code).AsInteger;
    rec.Name := FieldByName(FLD_NAME).AsString;

    rec.Onega_code := FieldByName(FLD_Onega_code).AsInteger;
    rec.Onega_Height := FieldByName(FLD_Height).AsInteger;


    aClutterInfo.ClutterDic.Add( rec.Code, rec);
//    aClutterInfo.ClutterList.Add(rec);

    Next;
  end;


end;
 


procedure TdmClutters.DataModuleCreate(Sender: TObject);
begin

//ShowMessage('procedure TdmClutters.DataModuleCreate(Sender: TObject);');


  if ADOConnection1.Connected then
    ShowMessage('ADOConnection1.Connected');


//  if not mdb_OpenConnection(ADOConnection1, GetApplicationDir() + 'GeoConverter.tasks.mdb') then
//    Halt;
    


  if not mdb_OpenConnection(ADOConnection1, GetApplicationDir + DEF_FILENAME) then
    Halt;
    
 
end;


end.


{


// ---------------------------------------------------------------
procedure TdmClutters.LoadFromFile_Menu_TXT(aSchema_ID: integer; aFileName:  string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCode: Integer;
  iPos: Integer;
  oStrList: TStringList;
  s,s1,s2: string;
  sName: string;
//  sSchema_NAME: string;
begin
  oStrList:=TStringList.Create;
  oStrList.LoadFromFile (aFileName);


  t_schema_clutters.Open;

 // sSchema_NAME := t_schema [FLD_NAME];


//  SetLength(Items, oStrList.Count);

  for I := 0 to oStrList.Count - 1 do
  begin
    s:=LowerCase(Trim(oStrList[i]));
    if s='' then
      break;

    
    iPos:=Pos(Chr(9),s);
    if iPos=0 then
      iPos:=Pos(' ',s);

      
      
      assert (iPos>0, format ('error in line: %d', [i] ));
      
    s1:=Trim(Copy(s,1,iPos-1));
    s2:=Trim(Copy(s,iPos+1,100));

    iCode := StrToInt(s1);
    sName := s2;


    if t_schema_clutters.Locate(FLD_schema_ID +';'+ FLD_CODE, VarArrayOf([aSchema_ID, iCode]), []) then
    begin
      db_UpdateRecord(t_schema_clutters, [db_Par(FLD_NAME, sName)]);
    end else
      db_AddRecord_(t_schema_clutters, 
         [
          FLD_NAME, sName, 
          FLD_CODE, iCode, 
          FLD_Schema_ID, aSchema_ID

         ]);

  end;

  FreeAndNil(oStrList);


  t_schema_clutters.Close;
  
end;   




// ---------------------------------------------------------------
procedure TdmClutters.LoadFromFile_Mapinfo_GRC(aFileName: string);
// ---------------------------------------------------------------

var
  I: Integer;
  iCode: Integer;
  oSList: TStringList;
  sInfo: string;
  sName: string;
  sSchema_NAME: string;
begin

//
//  Assert(aFileName<>'');
//
//
//  oSList:=TStringList.Create;
//
//  TvmGRID.ExtractClassificatorList(aFileName, oSList, sInfo);
//
//  sSchema_NAME := t_schema [FLD_NAME];
//
//  Assert(sSchema_NAME<>'');
//
//
//  for I := 0 to oSList.Count - 1 do
//  begin
//    sName:=oSList[i];
//    iCode:=Integer(oSList.Objects[i]);
//
//    if t_schema_clutters.Locate(FLD_schema_NAME +';'+ FLD_NAME,
//                VarArrayOf([sSchema_NAME, sName]), []) then
//    begin
//      db_UpdateRecord(t_schema_clutters, [db_Par(FLD_CODE, iCode)]);
//    end else
//    begin
//      db_AddRecord_(t_schema_clutters, [FLD_NAME, sName,
//                                        FLD_CODE, iCode]);
//    end;
//  end;
//
// // ShowMessage(oSList.Text);
//
//  FreeAndNil(oSList);


end;



procedure TdmClutters.AddItem(aName, aOnega_name: string; aHeight: Integer);
var
  s: string;
begin
  s:=t_schema[FLD_NAME];

  if not t_schema_clutters.Locate(FLD_Schema_name+';'+FLD_NAME, VarArrayOf([s,aName]), []) then
    db_AddRecord_(t_schema_clutters,
       [
 //        DataSet[FLD_Schema_name] := t_schema[FLD_Name];

        FLD_NAME, aName,
        FLD_Onega_NAME, aOnega_Name,
        FLD_Height, IIF(aHeight<>0, aHeight, NULL )
        ]);

end;



procedure TdmClutters.t_schema_cluttersNewRecord(DataSet: TDataSet);
begin
  DataSet[FLD_Schema_name] := t_schema[FLD_Name];

end;




procedure TdmClutters.AddSection(aName: string);
begin
  if not t_schema.Locate(FLD_NAME, aName, []) then
    db_AddRecord_(t_schema, [FLD_NAME, aName]);
      
end;



// ---------------------------------------------------------------
procedure TdmClutters.OpenData(aSection: string);
// ---------------------------------------------------------------
begin
  qry_Schema_clutters.SQL.Text:=
     Format('SELECT * FROm %s WHERE schema_name=''%s'' ORDER BY code',
       ['view_schema_clutters', aSection]);

  qry_Schema_clutters.Open;

end;



//
//// ---------------------------------------------------------------
//function dmClutters: TdmClutters;
//// ---------------------------------------------------------------
//begin
//  if not Assigned(FdmClutters) then
//    FdmClutters := TdmClutters.Create(Application);
//
//  Result := FdmClutters;
//end;




// ---------------------------------------------------------------
procedure TAsset_Menu_TXT_rec.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCode: Integer;
  iPos: Integer;
  oStrList: TStringList;
  s,s1,s2: string;
  sName: string;
//  sSchema_NAME: string;
begin
  SetLength(Items, 0);


  oStrList:=TStringList.Create;
  oStrList.LoadFromFile (aFileName);


 // sSchema_NAME := t_schema [FLD_NAME];


//  SetLength(Items, oStrList.Count);

  for I := 0 to oStrList.Count - 1 do
  begin
    s:=LowerCase(Trim(oStrList[i]));
    if s='' then
      break;


    iPos:=Pos(Chr(9),s);
    if iPos=0 then
      iPos:=Pos(' ',s);



      assert (iPos>0, format ('error in line: %d', [i] ));

    s1:=Trim(Copy(s,1,iPos-1));
    s2:=Trim(Copy(s,iPos+1,100));

    iCode := StrToInt(s1);
    sName := s2;

    SetLength(Items, Length(Items)+1);

    Items[High(Items)].Code:=iCode;
    Items[High(Items)].Name:=s2;


//
//    if t_schema_clutters.Locate(FLD_schema_ID +';'+ FLD_CODE, VarArrayOf([aSchema_ID, iCode]), []) then
//    begin
//      db_UpdateRecord(t_schema_clutters, [db_Par(FLD_NAME, sName)]);
//    end else
//      db_AddRecord_(t_schema_clutters,
//         [
//          FLD_NAME, sName,
//          FLD_CODE, iCode,
//          FLD_Schema_ID, aSchema_ID
//
//         ]);

  end;

  FreeAndNil(oStrList);


end;




// ---------------------------------------------------------------
procedure TdmClutters.LoadFromFile_Menu_TXT(aSchema_ID: integer; aFileName:
    string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCode: Integer;
  iPos: Integer;
  oStrList: TStringList;
  s,s1,s2: string;
  sName: string;
//  sSchema_NAME: string;

  rMenu: TAsset_Menu_TXT_rec;

begin
  rMenu.LoadFromFile(aFileName);

  oStrList:=TStringList.Create;
  oStrList.LoadFromFile (aFileName);


  t_schema_clutters.Open;

 // sSchema_NAME := t_schema [FLD_NAME];


//  SetLength(Items, oStrList.Count);

  for I := 0 to oStrList.Count - 1 do
  begin
    s:=LowerCase(Trim(oStrList[i]));
    if s='' then
      break;


    iPos:=Pos(Chr(9),s);
    if iPos=0 then
      iPos:=Pos(' ',s);



      assert (iPos>0, format ('error in line: %d', [i] ));

    s1:=Trim(Copy(s,1,iPos-1));
    s2:=Trim(Copy(s,iPos+1,100));

    iCode := StrToInt(s1);
    sName := s2;


    if t_schema_clutters.Locate(FLD_schema_ID +';'+ FLD_CODE, VarArrayOf([aSchema_ID, iCode]), []) then
    begin
      db_UpdateRecord(t_schema_clutters, [FLD_NAME, sName]);
    end else
      db_AddRecord_(t_schema_clutters,
         [
          FLD_NAME, sName,
          FLD_CODE, iCode,
          FLD_Schema_ID, aSchema_ID

         ]);

  end;

  FreeAndNil(oStrList);


  t_schema_clutters.Close;

end;

