object dlg_Clutters: Tdlg_Clutters
  Left = 739
  Top = 687
  BorderWidth = 5
  Caption = 'dlg_Clutters'
  ClientHeight = 544
  ClientWidth = 1192
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 514
    Width = 1192
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 447
    ExplicitWidth = 1158
    object Panel2: TPanel
      Left = 985
      Top = 0
      Width = 207
      Height = 30
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 951
      object Button1: TButton
        Left = 32
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Ok'
        ModalResult = 1
        TabOrder = 0
      end
      object Button2: TButton
        Left = 120
        Top = 5
        Width = 75
        Height = 23
        Cancel = True
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object cxGrid_left: TcxGrid
    Left = 0
    Top = 0
    Width = 265
    Height = 514
    Align = alLeft
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    ExplicitLeft = 2
    ExplicitTop = -6
    object cxGridDBTableView1_group: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      DataController.DataSource = ds_schema
      DataController.KeyFieldNames = 'name'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsSelection.InvertSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1_groupColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object col_group_name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 121
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1_group
    end
  end
  object cxGrid_main: TcxGrid
    Left = 273
    Top = 0
    Width = 600
    Height = 514
    Align = alLeft
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    RootLevelOptions.DetailTabsPosition = dtpTop
    ExplicitLeft = 271
    ExplicitTop = -6
    object cxGrid_mainDBTableView1_items: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      DataController.DataSource = ds_schema_clutters
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end
        item
          Kind = skCount
          Column = cxGrid_mainDBTableView1_itemsschema_name
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid_mainDBTableView1_itemsschema_name: TcxGridDBColumn
        DataBinding.FieldName = 'schema_name'
        Visible = False
        Options.Sorting = False
        Width = 102
      end
      object cxGrid_mainDBTableView1_itemsColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Sorting = False
      end
      object col_enabled: TcxGridDBColumn
        DataBinding.FieldName = 'enabled'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Width = 57
      end
      object col_code: TcxGridDBColumn
        DataBinding.FieldName = 'code'
        SortIndex = 0
        SortOrder = soAscending
        Width = 62
      end
      object cxGrid_mainDBTableView1_itemsname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 146
      end
      object col_onega_code: TcxGridDBColumn
        DataBinding.FieldName = 'onega_code'
        Width = 74
      end
      object col_onega_name: TcxGridDBColumn
        Caption = 'onega_name'
        DataBinding.FieldName = 'onega_code'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownRows = 20
        Properties.KeyFieldNames = 'code'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = ds_onega_clutters
        Width = 133
      end
      object cxGrid_mainDBTableView1_itemsheight: TcxGridDBColumn
        DataBinding.FieldName = 'height'
        Width = 42
      end
      object col_comment: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        Width = 287
      end
    end
    object cxGrid_mainLevel1: TcxGridLevel
      Caption = 'Main'
      GridView = cxGrid_mainDBTableView1_items
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 265
    Top = 0
    Width = 8
    Height = 514
    Control = cxGrid_left
    ExplicitHeight = 447
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'txt'
    FileName = 'menu.txt'
    Left = 968
    Top = 144
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'OpenDialog1.FileName'
      'OpenDialog1.InitialDir'
      'cxGrid_left.Width')
    StoredValues = <>
    Left = 968
    Top = 88
  end
  object ActionList1: TActionList
    Left = 968
    Top = 200
  end
  object MainMenu1: TMainMenu
    Left = 968
    Top = 24
    object N1: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object MenuTXT1: TMenuItem
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1092#1072#1081#1083':  Menu.TXT'
        OnClick = act_Load_Menu_TXTExecute
      end
      object OpenMapinfoGRC1: TMenuItem
        Caption = 'Open Mapinfo GRC'
        Hint = 'Open|Opens an existing file'
        ImageIndex = 7
        ShortCut = 16463
      end
    end
  end
  object t_schema: TADOTable
    Connection = dmClutters.ADOConnection1
    CursorType = ctStatic
    TableDirect = True
    TableName = 'schema'
    Left = 1080
    Top = 24
  end
  object t_onega_clutters: TADOTable
    Connection = dmClutters.ADOConnection1
    TableDirect = True
    TableName = 'onega_clutters'
    Left = 1080
    Top = 152
  end
  object ds_schema: TDataSource
    DataSet = t_schema
    Left = 1080
    Top = 80
  end
  object ds_schema_clutters: TDataSource
    DataSet = t_schema_clutters
    Left = 960
    Top = 368
  end
  object ds_onega_clutters: TDataSource
    DataSet = t_onega_clutters
    Left = 1080
    Top = 208
  end
  object t_schema_clutters: TADOTable
    Active = True
    Connection = dmClutters.ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'schema_id'
    MasterFields = 'id'
    MasterSource = ds_schema
    TableDirect = True
    TableName = 'schema_clutters'
    Left = 960
    Top = 304
  end
end
