﻿unit Unit7;

interface

uses
  u_Tif_to_RLF,
  d_Clutters,

  u_files,

  System.IOUtils, Types,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RxPlacemnt, Vcl.Mask,
  RxToolEdit, Vcl.CheckLst, Vcl.ComCtrls;

type
  TForm7 = class(TForm)
    Button1: TButton;
    ListBox_T: TListBox;
    ListBox_CLU_H: TListBox;
    ListBox_CLU: TListBox;
    DirectoryEdit1: TDirectoryEdit;
    FormStorage1: TFormStorage;
    Button2: TButton;
    Button3: TButton;
    CheckListBox1: TCheckListBox;
    ProgressBar1: TProgressBar;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    procedure DoOnProgress(aProgress,aMax: integer; var aTerminated: boolean);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.Button1Click(Sender: TObject);
var
  obj: TTif_to_RLF;
begin
  obj:=TTif_to_RLF.Create;

  obj.Params.FileName_Dem      :='D:\_beeline_mrr\03_Buryatia\Geodata\Heights\T_03_02m_48_Buratioa_Cities.tif';
  obj.Params.FileName_Clutter  :='D:\_beeline_mrr\03_Buryatia\Geodata\Clutter\C_03_02m_48_Buratia_Cities.tif';
  obj.Params.FileName_Clutter_H:='D:\_beeline_mrr\03_Buryatia\Geodata\Clutter Height\H_03_02m_48_Buratia_Cities.tif';

  //C_03_02m_48_Buratia_Cities.tif

  obj.Params.RLF_FileName:='D:\_beeline_mrr\03_Buryatia\Geodata\_T_03_02m_48_Buratioa_Cities.rlf';

  obj.Params.Clutters_section:='beeline';

  obj.OnProgress:=DoOnProgress;
  obj.Execute;

  FreeAndNil(obj);


  ProgressBar1.Position:=0;

end;



procedure TForm7.DoOnProgress(aProgress,aMax: integer; var aTerminated:
    boolean);
begin
  ProgressBar1.Max:=aMax;
  ProgressBar1.Position:=aProgress;
end;



procedure TForm7.Button2Click(Sender: TObject);
var
  sName: string;
begin
  Tdlg_Clutters.ExecDlg(sName);
end;


procedure TForm7.Button3Click(Sender: TObject);
var
  sDir: string;
begin

  sDir:= IncludeTrailingBackslash ( DirectoryEdit1.Text );


  ScanDir(sDir + 'clutter',         '*.tif', ListBox_CLU.Items );
  ScanDir(sDir + 'Clutter Height',  '*.tif', ListBox_CLU_H.Items );
  ScanDir(sDir + 'Heights',         '*.tif', ListBox_T.Items );

end;


procedure TForm7.Button4Click(Sender: TObject);
var
  arr: TStringDynArray;
  arr_dem: TStringDynArray;

  I: Integer;
  s: string;
  sDir_DEM: string;
begin

//  arr:=TDirectory.GetDirectories (IncludeTrailingBackslash(DirectoryEdit1.Text), TSearchOption.soAllDirectories ) ;

  arr:=TDirectory.GetDirectories(IncludeTrailingBackslash(DirectoryEdit1.Text), '*', TSearchOption.soAllDirectories);


  for I := 0 to High(arr) do
    if 'Clutter'= ExtractFileName( arr[i] ) then
    begin
      s:= ExtractFilePath ( arr[i] );

      CheckListBox1.Items.Add(arr[i]);

      sDir_DEM:=arr[i] + '\Heights';
      arr_dem:=TDirectory.GetFiles (sDir_DEM , '*.tif', TSearchOption.soAllDirectories) ;


    end;

    //Geodata

end;

end.
