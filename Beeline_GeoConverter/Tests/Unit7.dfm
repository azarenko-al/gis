object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Form7'
  ClientHeight = 737
  ClientWidth = 1129
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 24
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object ListBox_T: TListBox
    Left = 160
    Top = 64
    Width = 521
    Height = 97
    ItemHeight = 13
    TabOrder = 1
  end
  object ListBox_CLU_H: TListBox
    Left = 160
    Top = 301
    Width = 521
    Height = 132
    ItemHeight = 13
    TabOrder = 2
  end
  object ListBox_CLU: TListBox
    Left = 160
    Top = 176
    Width = 521
    Height = 119
    ItemHeight = 13
    TabOrder = 3
  end
  object DirectoryEdit1: TDirectoryEdit
    Left = 160
    Top = 16
    Width = 385
    Height = 21
    NumGlyphs = 1
    TabOrder = 4
    Text = 'D:\_beeline_mrr\03_Buryatia\Geodata'
  end
  object Button2: TButton
    Left = 24
    Top = 104
    Width = 75
    Height = 25
    Caption = 'dlg'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 576
    Top = 14
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 6
    OnClick = Button3Click
  end
  object CheckListBox1: TCheckListBox
    Left = 160
    Top = 496
    Width = 393
    Height = 97
    ItemHeight = 13
    TabOrder = 7
  end
  object ProgressBar1: TProgressBar
    Left = 744
    Top = 344
    Width = 249
    Height = 17
    TabOrder = 8
  end
  object Button4: TButton
    Left = 24
    Top = 472
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 9
    OnClick = Button4Click
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 40
    Top = 200
  end
end
