program test_TIF_to_rlf;

uses
  Vcl.Forms,
  Unit7 in 'Unit7.pas' {Form7},
  u_clutter_classes in '..\src\u_clutter_classes.pas',
  u_CustomTask in '..\src\u_CustomTask.pas',
  dm_Clutters in '..\src\dm_Clutters.pas' {dmClutters: TDataModule},
  d_Clutters in '..\src\d_Clutters.pas' {dlg_Clutters},
  u_TIF_to_rlf in '..\src\u_TIF_to_rlf.pas',
  u_Rel_to_clutter_map in '..\..\rel_to_bmp_3D\Src\u_Rel_to_clutter_map.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmClutters, dmClutters);
  //  Application.CreateForm(Tdlg_Clutters, dlg_Clutters);
  Application.CreateForm(TForm7, Form7);
  Application.Run;
end.
