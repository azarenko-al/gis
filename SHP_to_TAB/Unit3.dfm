object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'ArcGIS Shape -> Mapinfo TAB'
  ClientHeight = 301
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    630
    301)
  PixelsPerInch = 96
  TextHeight = 13
  object DirectoryEdit1: TDirectoryEdit
    Left = 8
    Top = 255
    Width = 481
    Height = 21
    DialogKind = dkWin32
    NumGlyphs = 1
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 0
    Text = ''
  end
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 481
    Height = 233
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 1
  end
  object Button1: TButton
    Left = 513
    Top = 8
    Width = 109
    Height = 25
    Action = FileOpen1
    Anchors = [akTop, akRight]
    TabOrder = 2
  end
  object Button2: TButton
    Left = 513
    Top = 253
    Width = 109
    Height = 25
    Action = act_Run
    Anchors = [akRight, akBottom]
    TabOrder = 3
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'DirectoryEdit1.Text'
      'ListBox1.Items')
    StoredValues = <>
    Left = 536
    Top = 72
  end
  object ActionList1: TActionList
    Left = 536
    Top = 128
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = #1054#1090#1082#1088#1099#1090#1100'...'
      Dialog.DefaultExt = '*.shp'
      Dialog.Filter = 'Shape|*.shp'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen1Accept
    end
    object act_Run: TAction
      Category = 'File'
      Caption = #1050#1086#1085#1074#1077#1088#1090#1080#1088#1086#1074#1072#1090#1100
      OnExecute = act_RunExecute
    end
  end
end
