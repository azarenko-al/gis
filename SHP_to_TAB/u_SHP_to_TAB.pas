unit u_SHP_to_TAB;

interface
uses
  u_GDAL_bat,
  u_run,

  Classes,
  IOUtils, Dialogs, System.SysUtils;


procedure Run_SHP_to_TAB(aList: TStrings; aDir: string);


implementation

procedure Test;
const
  DEF_DIR = 'C:\OneDrive\._KMZ\REGION.7\1030\STANDARD.3\MAPS\';
begin

//  Run_SHP_to_TAB (DEF_DIR + 'kup.7.3.dm', DEF_DIR + 'kup.7.3.new1.kmz' );
//  Run_SHP_to_TAB (DEF_DIR + 'kmo.7.3.dm', DEF_DIR + 'kmo.7.3.new1.kmz' );
//  Run_SHP_to_TAB (DEF_DIR + 'kgr.7.3.dm', DEF_DIR + 'kgr.7.3.new1.kmz' );
//  Run_SHP_to_TAB (DEF_DIR + 'prb.7.3.dm', DEF_DIR + 'prb.7.3.new1.kmz' );
// // exit;

end;

//-----------------------------------------------------------------
procedure Run_SHP_to_TAB(aList: TStrings; aDir: string);
//-----------------------------------------------------------------
//const
//  DEF_FILE = 'C:\OneDrive\._KMZ\xml project\SCENARIO.1\REGION.7\1000\STANDARD.3\MAPS\kmo.7.3.dm';

//  DEF_FILE = 'C:\OneDrive\._KMZ\REGION.7\1020\STANDARD.3\MAPS\kgr.7.3.dm';
//  DEF_FILE = 'C:\OneDrive\._KMZ\REGION.7\1010\STANDARD.3\MAPS\kmo.7.3.dm';

var
  I: Integer;
  oGDAL_Bat: TGDAL_Bat;
  sBat: string;
  sFile: string;

//
//  s: string;
//  sColorTif: string;
//  sDem: string;
//  sDir: string;
//  sEnvi: string;
//  sFile: string;
//  sKMZ: string;
//  sTif: string;
//  sTxt: string;

begin

  aDir:=IncludeTrailingBackslash(aDir);

  Assert(aDir <> '');

  ForceDirectories(aDir);


  //aFileName:=DEF_FILE;
  sBat:=aDir + 'run.bat';



//set path=%path%;C:\OSGeo4W64\bin\;
//set GDAL_DATA=C:\OSGeo4W64\share\epsg_csv
//set GDAL_FILENAME_IS_UTF8=YES
//
//rem set params=-lco SCHEMA=import  -lco UPLOAD_GEOM_FORMAT=wkt  -lco OVERWRITE=YES -t_srs "EPSG:4326"  -overwrite -f MSSQLSpatial "MSSQL:server=ACER;database=TMS;trusted_connection=yes"
//
//
//ogr2ogr  _IvnObl_hydrography_a.shp  "IvnObl_hydrography_a.TAB"


  oGDAL_Bat:= TGDAL_Bat.Create;
  oGDAL_Bat.Init_Header;


  for I := 0 to aList.Count-1 do
  begin
    sFile:=aDir + ChangeFileExt ( ExtractFileName(aList[i]), '.tab');

    oGDAL_Bat.Add( Format( 'ogr2ogr "%s" "%s"', [sFile, aList[i] ]));

  end;


//  oGDAL_Bat.SaveToFile(sBat, TEncoding.GetEncoding(GetOEMCP));
  oGDAL_Bat.SaveToFile(sBat, TEncoding.GetEncoding(866) );


  RunApp (sBat);


  FreeAndNil(oGDAL_Bat);

end;


begin
  test;


end.
