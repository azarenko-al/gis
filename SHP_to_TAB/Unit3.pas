unit Unit3;

interface

uses
  u_SHP_to_TAB,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RxPlacemnt, System.Actions,
  Vcl.ActnList, Vcl.StdActns, Vcl.StdCtrls, Vcl.Mask, RxToolEdit;

type
  TForm3 = class(TForm)
    FormStorage1: TFormStorage;
    DirectoryEdit1: TDirectoryEdit;
    ListBox1: TListBox;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    Button1: TButton;
    act_Run: TAction;
    Button2: TButton;
    procedure act_RunExecute(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.act_RunExecute(Sender: TObject);
begin
  Run_SHP_to_TAB (ListBox1.Items, DirectoryEdit1.Text);
end;

procedure TForm3.FileOpen1Accept(Sender: TObject);
var
  I: Integer;
begin
  ListBox1.Clear;

  with TFileOpen(Sender) do
    for I := 0 to Dialog.Files.Count-1 do
      ListBox1.AddItem(Dialog.Files[i], nil);

end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  Caption:='ArcGIS Shape -> Mapinfo TAB'
end;

end.
