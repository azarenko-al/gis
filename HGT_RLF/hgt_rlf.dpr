program hgt_rlf;

uses
  Vcl.Forms,
  f_main in 'f_main.pas' {frm_Main},
  u_GDAL_bat in 'W:\common-xe\GDAL\u_GDAL_bat.pas',
  u_GDAL_classes in 'W:\common-xe\GDAL\u_GDAL_classes.pas',
  u_GDAL_envi in 'W:\common-xe\GDAL\u_GDAL_envi.pas',
  u_HGT_to_RLF in 'src\u_HGT_to_RLF.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tfrm_Main, frm_Main);
  Application.Run;
end.
