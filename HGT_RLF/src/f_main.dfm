﻿object frm_Main: Tfrm_Main
  Left = 0
  Top = 0
  BorderWidth = 2
  Caption = 'HGT > RLF'
  ClientHeight = 537
  ClientWidth = 658
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    658
    537)
  PixelsPerInch = 96
  TextHeight = 13
  object pn_bottom: TPanel
    Left = 0
    Top = 496
    Width = 658
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      658
      41)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 658
      Height = 3
      Align = alTop
      Shape = bsTopLine
      ExplicitLeft = 48
      ExplicitTop = 8
      ExplicitWidth = 50
    end
    object pn_BottomRight: TPanel
      Left = 358
      Top = 3
      Width = 300
      Height = 38
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        300
        38)
      object иет_Щл: TButton
        Left = 173
        Top = 3
        Width = 120
        Height = 25
        Action = act_Run
        Anchors = [akRight, akBottom]
        TabOrder = 0
      end
    end
    object ProgressBar1: TProgressBar
      Left = 10
      Top = 13
      Width = 462
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
    end
  end
  object Button4: TButton
    Left = 551
    Top = 107
    Width = 107
    Height = 25
    Caption = '_2_Run_ENVI_to_RLF'
    TabOrder = 1
    Visible = False
    OnClick = Button4Click
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 448
    Width = 658
    Height = 48
    Align = alBottom
    Caption = #1052#1072#1090#1088#1080#1094#1072' '#1074#1099#1089#1086#1090' RLF'
    TabOrder = 2
    object FilenameEdit_RLF: TFilenameEdit
      Left = 2
      Top = 15
      Width = 654
      Height = 21
      OnAfterDialog = FilenameEdit_RLFAfterDialog
      Align = alTop
      Filter = 'All files (*.rlf)|*.rlf'
      NumGlyphs = 1
      TabOrder = 0
      Text = 'E:\test.rlf'
    end
  end
  object Button3: TButton
    Left = 551
    Top = 76
    Width = 109
    Height = 25
    Caption = '_1_Run_HGT_to_ENVI'
    TabOrder = 3
    Visible = False
    OnClick = Button3Click
  end
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 537
    Height = 376
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 4
  end
  object Button1: TButton
    Left = 565
    Top = 39
    Width = 85
    Height = 25
    Action = act_Del
    Anchors = [akTop, akRight]
    TabOrder = 5
  end
  object Button5: TButton
    Left = 565
    Top = 8
    Width = 85
    Height = 25
    Action = act_Add
    Anchors = [akTop, akRight]
    TabOrder = 6
  end
  object box_rel: TGroupBox
    Left = 0
    Top = 397
    Width = 658
    Height = 51
    Align = alBottom
    Caption = #1052#1072#1090#1088#1080#1094#1072' '#1074#1099#1089#1086#1090' REL ('#1053#1077#1074#1072') '#1080#1083#1080' '#1080#1089#1093#1086#1076#1085#1072#1103' '#1084#1072#1090#1088#1080#1094#1072' '#1074#1099#1089#1086#1090' RLF'
    TabOrder = 7
    object FilenameEdit_REL: TFilenameEdit
      Left = 2
      Top = 15
      Width = 654
      Height = 21
      Align = alTop
      Filter = 'All files (*.rlf)|*.rlf|All files (*.rel)|*.rel'
      NumGlyphs = 1
      TabOrder = 0
      Text = 'E:\________59-60\rlf\r-59-60_29z_50.rlf'
    end
  end
  object Button6: TButton
    Left = 551
    Top = 138
    Width = 75
    Height = 25
    Caption = 'Button6'
    TabOrder = 8
    Visible = False
    OnClick = Button6Click
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\hgt_rel_rlf'
    UseRegistry = True
    StoredProps.Strings = (
      'FilenameEdit_RLF.FileName'
      'FilenameEdit_REL.FileName'
      'ListBox1.Items')
    StoredValues = <>
    Left = 587
    Top = 294
  end
  object ActionList1: TActionList
    Left = 586
    Top = 339
    object act_Run: TAction
      Caption = #1050#1086#1085#1074#1077#1088#1090#1072#1094#1080#1103
      OnExecute = act_RunExecute
    end
    object act_Add: TFileOpen
      Category = 'File'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Dialog.DefaultExt = '*.hgt'
      Dialog.Filter = '*.hgt|*.hgt'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = act_AddAccept
    end
    object act_Del: TAction
      Category = 'File'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_DelExecute
    end
  end
end
