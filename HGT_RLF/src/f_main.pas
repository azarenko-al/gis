unit f_main;

interface

uses
 // MapXLib_TLB,
  u_geo_convert,
  u_func,

  u_geo,
  u_run,

  I_rel_Matrix1,
  u_rlf_Matrix,
  u_rel_Matrix_base,

  u_rel_neva_Matrix,

//  u_rel_Matrix_base,


  u_GDAL_bat,
  u_GDAL_envi,
  u_GDAL_classes,


  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Actions, Vcl.ActnList, RxPlacemnt,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.StdActns, RxToolEdit, Vcl.Mask, Vcl.ComCtrls;

type
  Tfrm_Main = class(TForm)
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    act_Run: TAction;
    pn_bottom: TPanel;
    pn_BottomRight: TPanel;
    act_Add: TFileOpen;
    Button4: TButton;
    GroupBox1: TGroupBox;
    FilenameEdit_RLF: TFilenameEdit;
    Bevel1: TBevel;
    Button3: TButton;
    ListBox1: TListBox;
    Button1: TButton;
    Button5: TButton;
    ProgressBar1: TProgressBar;
    box_rel: TGroupBox;
    act_Del: TAction;
    FilenameEdit_REL: TFilenameEdit;
    Button6: TButton;
    procedure FormCreate(Sender: TObject);
    procedure act_AddAccept(Sender: TObject);
    procedure act_DelExecute(Sender: TObject);
    procedure act_RunExecute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FilenameEdit_RLFAfterDialog(Sender: TObject; var AName: string; var
        Action: Boolean);
  private
    FDir: string;

    FZone_GK: Integer;


    oRlfMatrix_clutter : TrlfMatrix;
    oRelMatrix_clutter : TnevaRelMatrix;


    procedure _2_Run_ENVI_to_RLF;
    procedure _1_Run_HGT_to_ENVI;
    procedure _3_Run_RLF_to_RLF;

  public
    function Get_Zone_GK: integer;

  end;

var
  frm_Main: Tfrm_Main;

implementation

{$R *.dfm}

procedure Tfrm_Main.FormCreate(Sender: TObject);
begin
  Caption:=Caption + GetAppVersionStr();
end;

procedure Tfrm_Main.act_AddAccept(Sender: TObject);
var
  i: Integer;
begin
  with TFileOpen(Sender) do
//     if Dialog.Execute then
//      begin
        for i:=0 to Dialog.Files.Count-1 do    // Iterate
        begin
          if ListBox1.Items.IndexOf(Dialog.Files[i])<0 then
            ListBox1.AddItem(Dialog.Files[i], nil);
        end;    // for

      //  UpdateFileLists();
  //    end;
end;


procedure Tfrm_Main.act_DelExecute(Sender: TObject);
var
  i: Integer;
begin
  for i:=ListBox1.Count-1 downto 0 do
    if ListBox1.Selected[i] then
      ListBox1.Items.Delete(i)
end;


procedure Tfrm_Main.act_RunExecute(Sender: TObject);
begin
  _1_Run_HGT_to_ENVI;

  if FileExists (FilenameEdit_REL.FileName) then
    _3_Run_RLF_to_RLF
  else
    _2_Run_ENVI_to_RLF;

end;


procedure Tfrm_Main.Button3Click(Sender: TObject);
begin
  _1_Run_HGT_to_ENVI
end;




//-----------------------------------------------------------------
procedure Tfrm_Main._1_Run_HGT_to_ENVI;
//-----------------------------------------------------------------
var

  json_rec: TGDAL_json_rec;
  oBAT: TGDAL_Bat;
  s: string;
  sFiles: string;

  sFile_bat: string;
  sProj4: string;



begin
  FDir:=ExtractFilePath ( FilenameEdit_RLF.FileName) ;
  ForceDirectories(FDir);

  sFiles:=FDir + 'all_files.txt';
  ListBox1.Items.SaveToFile(sFiles, TEncoding.GetEncoding(866));

  //, TEncoding.GetEncoding(866));

//rem gdalbuildvrt.exe   -vrtnodata -3624  -overwrite  all.vrt  -input_file_list  all.files

//gdalbuildvrt.exe   -vrtnodata -3624  -overwrite  all.vrt   *.envi
//rem -input_file_list  all.files
//
//rem gdalbuildvrt.exe   -vrtnodata -3624  -overwrite  all.vrt  -input_file_list  all.files
//gdalinfo.exe -json all.vrt > all.vrt.json


//  sDir:=IncludeTrailingBackslash ( Trim(DirectoryEdit1.Text) );


  oBAT:=TGDAL_Bat.Create;
  oBAT.Init_Header;


  oBAT.Add ('gdalbuildvrt.exe   -vrtnodata -0  -overwrite  all.vrt   -input_file_list  all_files.txt' );
  oBAT.Add ('gdalinfo.exe -json all.vrt > all.vrt.json');
  oBAT.Add ('gdal_translate.exe  -co "COMPRESS=LZW"   all.vrt all.tif');

  sFile_bat:=FDir + '_1.bat';
  oBAT.SaveToFile(sFile_bat, TEncoding.GetEncoding(866));

//    oBAT.SaveToFile(ChangeFileExt(aRec.FileName,'.bat') , TEncoding.GetEncoding(866));


  SetCurrentDir(FDir);
  RunApp (sFile_bat,'');

//----------------------------------------------
  if FileExists (FilenameEdit_REL.FileName) then
  begin
    FZone_GK:=Get_Zone_GK();

  end else begin
    json_rec.LoadFromFile(FDir + 'all.vrt.json');

    if json_rec.Bounds.Lon_min < 0 then
      json_rec.Bounds.Lon_min:=json_rec.Bounds.Lon_min+360;

    FZone_GK:=geo_Get6ZoneL(json_rec.Bounds.Lon_min);
  end;


  sProj4 := Format('+proj=tmerc +lat_0=0 +lon_0=%d +k=1 +x_0=%d +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs',
           [ FZone_GK * 6 -3,  FZone_GK * 1000000 + 500000]);

//----------------------------------------------

  oBAT.Clear;
  oBAT.Init_Header;


  oBAT.Add(Format('gdalwarp.exe -overwrite -t_srs  "%s"  all.tif all_proj_GK.tif',[sProj4]));
  oBAT.Add (      'gdalinfo.exe -json all_proj_GK.tif > all_proj_GK.json');
  oBAT.Add ('');

  oBAT.Add (      'gdal_translate.exe  -of envi   all_proj_GK.tif  all_proj_GK.envi');
  oBAT.Add (      'gdalinfo.exe -json all_proj_GK.envi > all_proj_GK.json');

  oBAT.Add ('');
  oBAT.Add (      'gdal_translate.exe  -of AAIGrid   all_proj_GK.envi  all_proj_GK.txt');

//

  sFile_bat:=FDir + '_2.bat';
  oBAT.SaveToFile(sFile_bat, TEncoding.GetEncoding(866));
  RunApp (sFile_bat,'');

end;


//-----------------------------------------------------------------
function Tfrm_Main.Get_Zone_GK: integer;
//-----------------------------------------------------------------
var
  s: string;
  rec: TrelMatrixInfoRec;
begin
  result:=-999;
//    class function GetInfo (aFileName: string; var aRec: TrelMatrixInfoRec): boolean;

   s:= LowerCase(ExtractFileExt(FilenameEdit_REL.FileName));

   if s='.rel' then begin
     TnevaRelMatrix.GetInfo(FilenameEdit_REL.FileName, rec);
     result:=rec.ZoneNum_GK;

//     FRelMatrix_clutter.Open(FilenameEdit_REL.FileName);
   end else

   if s='.rlf' then begin
     TrlfMatrix.GetInfo(FilenameEdit_REL.FileName, rec);
     result:=rec.ZoneNum_GK;

   end;

  // TODO -cMM: Tfrm_Main.Get_Zone_GK default body inserted
end;



procedure Tfrm_Main.Button6Click(Sender: TObject);
begin
  Get_Zone_GK
end;




//-----------------------------------------------------------------
procedure Tfrm_Main._2_Run_ENVI_to_RLF;
//-----------------------------------------------------------------
var
//  bFindPoint: Boolean;
  json_rec: TGDAL_json_rec;
//  sDir: string;

  header_Rec: TrelMatrixInfoRec;
  rlf_rec: Trel_Record;

  iValue: integer;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
  iCol: Integer;
  iRow: Integer;

  oDEM_Envi_File: TEnvi_File;

  oMemStream: TMemoryStream;

//  orlfMatrix: TrlfMatrix;

  xyPos: TXYPoint;
//  blPoint: TblPoint;

  rec_clu: Trel_Record;


  //sFile: string;

begin
  FDir:=ExtractFilePath ( FilenameEdit_RLF.FileName) ;
  ForceDirectories(FDir);


//  FZone_GK:=29;



{
   if FileExists (FilenameEdit_REL.FileName)  then
   begin
     s:= ExtractFileExt(FilenameEdit_REL.FileName);

     if s='.rel' then begin
       oRelMatrix_clutter :=TnevaRelMatrix.Create;
       oRelMatrix_clutter.Open(FilenameEdit_REL.FileName);
     end else

     if s='.rlf' then begin
       oRlfMatrix_clutter :=TrlfMatrix.Create;
       oRlfMatrix_clutter.OpenFile(FilenameEdit_REL.FileName);
     end;

   end;
 }



//  sDir:=IncludeTrailingBackslash ( Trim(DirectoryEdit1.Text) );
//  ForceDirectories(sDir);

  oMemStream:=TMemoryStream.Create;


  oDEM_Envi_File:=TEnvi_File.Create;
  oDEM_Envi_File.OpenFile(FDir + 'all_proj_GK.envi');


//----------------------------------------------
  json_rec.LoadFromFile(FDir + 'all_proj_GK.json');

 // if json_rec.Bounds.Lon_min < 0 then
 //   json_rec.Bounds.Lon_min:=json_rec.Bounds.Lon_min+360;


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=json_rec.Size.cols;
  header_Rec.RowCount:=json_rec.Size.rows;

  header_Rec.StepX:=(json_rec.Bounds.Lat_max - json_rec.Bounds.Lat_min) / json_rec.Size.rows ;
  header_Rec.StepY:=(json_rec.Bounds.Lon_max - json_rec.Bounds.Lon_min) / json_rec.Size.cols ;
  header_Rec.ZoneNum_GK:=FZone_GK;

  header_Rec.XYBounds.TopLeft.X:=json_rec.Bounds.Lat_max;
  header_Rec.XYBounds.TopLeft.Y:=json_rec.Bounds.Lon_min;

  header_Rec.XYBounds.BottomRight.X:=json_rec.Bounds.Lat_min;
  header_Rec.XYBounds.BottomRight.Y:=json_rec.Bounds.Lon_max;

  // ---------------------------------------------------------------


  oRlfFileStream:=TFileStream.Create(FilenameEdit_RLF.FileName, fmCreate);
//  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  FillChar(rlf_rec, SizeOf(rlf_rec), 0);
  ProgressBar1.Max:=header_Rec.RowCount;


  iValue:=oDEM_Envi_File.GetDataByIndex(0);

  for iRow := 0 to header_Rec.RowCount - 1 do
  begin
    ProgressBar1.Position:=iRow;
    Application.ProcessMessages;

 //   if Terminated then
   //   Break;

 //   DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to header_Rec.ColCount - 1 do
    begin
      xyPos.X:=header_Rec.XYBounds.TopLeft.x - header_Rec.StepX*iRow;
      xyPos.Y:=header_Rec.XYBounds.TopLeft.y + header_Rec.StepY*iCol;

      //-------------------------------------------------

      iValue:=oDEM_Envi_File.GetDataByIndex(iRow * header_Rec.ColCount + iCOl);

      if iValue<>0 then
      begin
        rlf_rec.Rel_H :=iValue;
        //------------------------------

      end
      else
       rlf_rec.Rel_H:= EMPTY_HEIGHT;



      oMemStream.Write(rlf_rec, SizeOf(rlf_rec));
    end;

    oRlfFileStream.CopyFrom(oMemStream, 0);

    oMemStream.Clear;

  end;


 // ProgressBar1.Position:=0;

  FreeAndNil(oMemStream);
  FreeAndNil(oRlfFileStream);
  FreeAndNil(oDEM_Envi_File);

  FreeAndNil  (oRelMatrix_clutter);
  FreeAndNil  (oRlfMatrix_clutter);


  {
  orlfMatrix:=TrlfMatrix.Create;
  orlfMatrix.OpenFile(FilenameEdit1.FileName);
  orlfMatrix.SaveToDebugFiles('e:\11111');
  FreeAndNil(orlfMatrix);
  }

  ProgressBar1.Position:=0;


end;



//-----------------------------------------------------------------
procedure Tfrm_Main._3_Run_RLF_to_RLF;
//-----------------------------------------------------------------
var
 // bFindPoint: Boolean;
//  json_rec: TGDAL_json_rec;
//  sDir: string;

  header_Rec: TrelMatrixInfoRec;
  rlf_rec: Trel_Record;

  iValue: integer;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
  iCol: Integer;
  iRow: Integer;

  oDEM_Envi_File: TEnvi_File;

  oMemStream: TMemoryStream;

  orlfMatrix: TrlfMatrix;
  oRelMatrix_clu: TnevaRelMatrix;

  xyPos: TXYPoint;
  blPoint: TblPoint;

  rec_clu: Trel_Record;


  //sFile: string;

begin
  orlfMatrix:=nil;
  oRelMatrix_clu:=nil;


  FDir:=ExtractFilePath ( FilenameEdit_RLF.FileName) ;
  ForceDirectories(FDir);



   if FileExists (FilenameEdit_REL.FileName)  then
   begin
     s:= LowerCase(ExtractFileExt(FilenameEdit_REL.FileName));

     if s='.rel' then begin
       oRelMatrix_clu :=TnevaRelMatrix.Create;
       oRelMatrix_clu.Open(FilenameEdit_REL.FileName);
     end else

     if s='.rlf' then begin
       oRlfMatrix_clutter :=TrlfMatrix.Create;
       oRlfMatrix_clutter.OpenFile(FilenameEdit_REL.FileName);
     end;

   end;




//  sDir:=IncludeTrailingBackslash ( Trim(DirectoryEdit1.Text) );
//  ForceDirectories(sDir);

  oMemStream:=TMemoryStream.Create;


  oDEM_Envi_File:=TEnvi_File.Create;
  oDEM_Envi_File.OpenFile(FDir + 'all_proj_GK.envi');


//----------------------------------------------
//  json_rec.LoadFromFile(FDir + 'all_proj_GK.json');

 // if json_rec.Bounds.Lon_min < 0 then
 //   json_rec.Bounds.Lon_min:=json_rec.Bounds.Lon_min+360;


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  if assigned(oRelMatrix_clu) then
  begin
    header_Rec.ColCount:=oRelMatrix_clu.ColCount;
    header_Rec.RowCount:=oRelMatrix_clu.RowCount;
    header_Rec.XYBounds:=oRelMatrix_clu.XYBounds;

    header_Rec.StepX:=oRelMatrix_clu.StepX;//  (  json_rec.Bounds.Lat_max - json_rec.Bounds.Lat_min) / json_rec.Size.rows ;
    header_Rec.StepY:=oRelMatrix_clu.Stepy;//(json_rec.Bounds.Lon_max - json_rec.Bounds.Lon_min) / json_rec.Size.cols ;

  end else

  if assigned(oRlfMatrix_clutter) then
  begin
    header_Rec.ColCount:=oRlfMatrix_clutter.ColCount;
    header_Rec.RowCount:=oRlfMatrix_clutter.RowCount;
    header_Rec.XYBounds:=oRlfMatrix_clutter.XYBounds;

    header_Rec.StepX:=oRlfMatrix_clutter.StepX;//  (  json_rec.Bounds.Lat_max - json_rec.Bounds.Lat_min) / json_rec.Size.rows ;
    header_Rec.StepY:=oRlfMatrix_clutter.Stepy;//(json_rec.Bounds.Lon_max - json_rec.Bounds.Lon_min) / json_rec.Size.cols ;

  end else
    raise Exception.Create('Error Message');



  header_Rec.ZoneNum_GK:=FZone_GK;


  // ---------------------------------------------------------------


  oRlfFileStream:=TFileStream.Create(FilenameEdit_RLF.FileName, fmCreate);
//  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  FillChar(rlf_rec, SizeOf(rlf_rec), 0);
  ProgressBar1.Max:=header_Rec.RowCount;


//  iValue:=oDEM_Envi_File.GetDataByIndex(0);

  for iRow := 0 to header_Rec.RowCount - 1 do
  begin
    ProgressBar1.Position:=iRow;
    Application.ProcessMessages;

 //   if Terminated then
   //   Break;

 //   DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to header_Rec.ColCount - 1 do
    begin
      xyPos.X:=header_Rec.XYBounds.TopLeft.x - header_Rec.StepX*iRow;
      xyPos.Y:=header_Rec.XYBounds.TopLeft.y + header_Rec.StepY*iCol;

      //-------------------------------------------------

      if assigned(oRelMatrix_clu) then
        rlf_rec:=oRelMatrix_clu.Items[iRow, iCol];

      if assigned(oRlfMatrix_clutter) then
        rlf_rec:=oRlfMatrix_clutter.Items[iRow, iCol];


//      iValue:=oDEM_Envi_File.GetDataByIndex(iRow * header_Rec.ColCount + iCOl);

      if rlf_rec.Rel_H<>EMPTY_HEIGHT then
      begin
        if oDEM_Envi_File.FindValueByXY(xyPos.X, xyPos.y, iValue) then
          rlf_rec.Rel_H:=iValue;

      end;

      oMemStream.Write(rlf_rec, SizeOf(rlf_rec));
    end;

    oRlfFileStream.CopyFrom(oMemStream, 0);

    oMemStream.Clear;

  end;


 // ProgressBar1.Position:=0;

  FreeAndNil(oMemStream);
  FreeAndNil(oRlfFileStream);
  FreeAndNil(oDEM_Envi_File);

  FreeAndNil  (oRelMatrix_clutter);
  FreeAndNil  (oRlfMatrix_clutter);


  {
  orlfMatrix:=TrlfMatrix.Create;
  orlfMatrix.OpenFile(FilenameEdit1.FileName);
  orlfMatrix.SaveToDebugFiles('e:\11111');
  FreeAndNil(orlfMatrix);
  }

  ProgressBar1.Position:=0;


end;



procedure Tfrm_Main.Button4Click(Sender: TObject);
begin
  _2_Run_ENVI_to_RLF;
end;


procedure Tfrm_Main.FilenameEdit_RLFAfterDialog(Sender: TObject; var AName:
    string; var Action: Boolean);
begin
  AName:=ChangeFileExt (AName, '.rlf');
end;


end.

    {


    IsCrypted : boolean;
    NoData : array[0..2] of byte;

