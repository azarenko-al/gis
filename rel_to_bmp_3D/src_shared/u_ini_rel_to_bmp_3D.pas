unit u_ini_rel_to_bmp_3D;

interface
uses
  IniFiles, SysUtils, Graphics, 

//  u_assert,

  u_func
  ;

const
  EXE_REL_to_BMP_3D = 'REL_to_BMP_3D.exe';

type
  TTaskType = (ttClutters,tt3D); // ttRelief,


const                                                 //'Relief',
  DEF_TaskTypeStrArr : array[TTaskType] of string = ( 'Clutters',  '3D');


type
  Tini_rel_to_bmp_3D_Params = class(TObject)
  private
    //Files: TStringList;

  //  FClutters: array[0..High(256)] of Integer;

  //  FTaskStr: string;
    procedure GetTaskTypeFromStr(aValue: string);
    function TaskTypeToStr: string;
  public

    TaskType: TTaskType;

    RelFileName: string;
  //  ImgFileName: string;

    TabFileName: string;

//    TabFileName_CLU_: string;
//    TabFileName_REL_: string;

    constructor Create;
//    destructor Destroy; override;

//    function GetColorByClutter(aClutterCode: Integer): integer;

    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);
  end;

var
  g_Params: Tini_rel_to_bmp_3D_Params;


implementation

const
  DEF_MAIN = 'main';

const
  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
  DEF_CLU_WATER     = 2;  // 3- ���
  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // 31- ���
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���


// ---------------------------------------------------------------
constructor Tini_rel_to_bmp_3D_Params.Create;
// ---------------------------------------------------------------
begin
  inherited Create;
 // Files := TStringList.Create();

 (* FClutters[DEF_CLU_OPEN_AREA] := clWhite;  // 1- ��� (1)
  FClutters[DEF_CLU_FOREST   ] := clGreen;  // 1- ��� (1)
  FClutters[DEF_CLU_WATER    ] := clNavy;  // 3- ���
  FClutters[DEF_CLU_COUNTRY  ] := clRed;  // 3- ���
  FClutters[DEF_CLU_ROAD     ] := clYellow;
  FClutters[DEF_CLU_RailROAD ] := clYellow;
  FClutters[DEF_CLU_CITY     ] := clRed;  // 7- �����
  FClutters[DEF_CLU_BOLOTO   ] := clBlue; // 31- ���
  FClutters[DEF_CLU_ONE_BUILD] := clRed; // 73- ���.����
  FClutters[DEF_CLU_ONE_HOUSE] := clMaroon; // 31- ���

*)
end;
 
// ---------------------------------------------------------------
procedure Tini_rel_to_bmp_3D_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
//  s1: string;
begin
  Assert(FileExists(aFileName), aFileName);


  with TIniFile.Create(aFileName) do
  begin
    s :=ReadString (DEF_MAIN, 'Task', '');
    Assert (s<>'', 'Task empty');

    GetTaskTypeFromStr(s);

    RelFileName :=ReadString (DEF_MAIN, 'RelFileName', '');


    Assert (FileExists(RelFileName), RelFileName);

    
 //   ImgFileName :=ReadString (DEF_MAIN, 'ImgFileName', '');

    TabFileName :=ReadString (DEF_MAIN, 'TabFileName', '');

//    TabFileName_CLU_ :=ReadString (DEF_MAIN, 'TabFileName_CLU_', '');
//    TabFileName_REL_ :=ReadString (DEF_MAIN, 'TabFileName_REL_', '');

//    Assert(TabFileName_CLU_ <> '', 'Value not assigned');

    {
    ReadSection('files', Files);

    for I := 0 to Files.Count - 1 do
    begin
      s:=Files[i];
      s1:=Files.ValueFromIndex[i];


    end;
     }

    Free;

  end;


end;

// ---------------------------------------------------------------
procedure Tini_rel_to_bmp_3D_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oIniFile: TIniFile;
  s: string;
  s1: string;
begin
  ForceDirectories (ExtractFileDir (aFileName));


  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
    WriteString(DEF_MAIN, 'Task', TaskTypeToStr());

    WriteString(DEF_MAIN, 'RelFileName',  RelFileName);
  //  WriteString(DEF_MAIN, 'ImgFileName', ImgFileName);

    WriteString(DEF_MAIN, 'TabFileName', TabFileName);

//    WriteString(DEF_MAIN, 'TabFileName_CLU_', TabFileName_CLU_);
//    WriteString(DEF_MAIN, 'TabFileName_REL_', TabFileName_REL_);

(*
    for I := 0 to Files.Count - 1 do
    begin
      s:=Files[i];
      s1:=Files.ValueFromIndex[i];

      WriteString('files', Files[i], Files.ValueFromIndex[i]);
    end;

*)
  end;




  FreeAndNil(oIniFile);
end;




// ---------------------------------------------------------------
procedure Tini_rel_to_bmp_3D_Params.GetTaskTypeFromStr(aValue: string);
// ---------------------------------------------------------------
var
  tt: TTaskType;
begin
 
  for tt:= Low(TTaskType) to High(TTaskType) do
    if Eq(DEF_TaskTypeStrArr[tt], aValue) then
    begin
      TaskType := tt;
      Exit;
    end;

(*
  if Eq(aValue,'3D') then TaskType := tt3D else
  if Eq(aValue,'Clutters') then TaskType := ttClutters else
 // if Eq(aValue,'Relief') then TaskType := ttRelief
*) //   else
      raise Exception.Create('procedure Tini_rel_to_bmp_3D_Params.GetTaskTypeFromStr;');


end;





// ---------------------------------------------------------------
function Tini_rel_to_bmp_3D_Params.TaskTypeToStr: string;
// ---------------------------------------------------------------
begin
  Result := DEF_TaskTypeStrArr[TaskType];

  case TaskType of
     tt3D:        Result := '3D';
     ttClutters:  Result := 'Clutters';
   //  ttRelief:    Result := 'Relief';
   else
     raise Exception.Create('');
  end;

//  if Eq(FTaskStr,'Clutters') then TaskType :=  else
//  if Eq(FTaskStr,'Relief') then TaskType := ttRelief else

end;

     
end.

