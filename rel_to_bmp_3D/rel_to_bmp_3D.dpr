program rel_to_bmp_3D;

uses
  dm_App in 'Src\dm_App.pas' {dmMainApp: TDataModule},
  Forms,
  u_Rel_to_clutter_map in 'Src\u_Rel_to_clutter_map.pas',
 // u_GDAL_classes in 'W:\common XE\GDAL\u_GDAL_classes.pas',
  u_ini_rel_to_bmp_3D in 'src_shared\u_ini_rel_to_bmp_3D.pas',
  u_2D_array in 'Src\3D\u_2D_array.pas',
  u_3d_func in 'Src\3D\u_3d_func.pas',
  u_3D_rel_to_bmp in 'Src\3D\u_3D_rel_to_bmp.pas',
  u_3d_types in 'Src\3D\u_3d_types.pas';
 // u_BufferedFileStream in 'W:\common XE\File\u_BufferedFileStream.pas',
//  u_vars in 'W:\RPLS_DB XE\src\u_vars.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TdmMainApp, dmMainApp);
  Application.Run;
end.
