﻿unit u_Rel_to_clutter_map;

interface

//9.5. ÓÑËÎÂÍÛÅ ÎÁÎÇÍÀ×ÅÍÈß ÐÅËÜÅÔÀ
//http://topography.ltsu.org/kartography/k9_relef.html

uses Graphics, SysUtils, IOUtils,   iniFiles, Windows,  Classes, Forms,  Dialogs,

  u_GDal_bat,
  u_GDal_classes,

  u_run,
  u_geo,

//  u_rel_engine,

  u_rlf_Matrix,

  u_Progress,
            
  I_rel_Matrix1,

  u_rel_matrix_base
  ;


type
  T_Rel_to_Clutter_map = class(TProgress)
  private
    FClutters: array[0..High(256)] of Integer;

    FRelMatrix: TrlfMatrix;

    procedure Draw_Clutters;
//    procedure Draw_Relif;

//    function GetColorByClutter(aClutterCode: Integer): integer;
//    function GetItemByRowCol(aRow,aCol: Integer; var aRec: Trel_Record): Boolean;

    procedure SaveCluttersToEnvi(aDir: string);
//    procedure SaveReliefToEnvi(aDir: string);

  protected
    procedure ExecuteProc; override;

  public
    Params: record
              RelFileName: string;
              TabFileName_CLU: string;
              TabFileName_REL: string;
            end;

    constructor Create;
    
    class procedure Exec(aFileName: string);

  end;


implementation


//const
//  DEF_WATER_REL_H = -10;
 
const

  DEF_CLU_OPEN_AREA = 0;
  DEF_CLU_FOREST    = 1;
  DEF_CLU_WATER     = 2;
  DEF_CLU_COUNTRY   = 3;
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
  DEF_CLU_CITY      = 7;
  DEF_CLU_BOLOTO    = 8;
  DEF_CLU_ONE_BUILD = 73;
  DEF_CLU_ONE_HOUSE = 31;




// ---------------------------------------------------------------
class procedure T_Rel_to_Clutter_map.Exec(aFileName: string);
// ---------------------------------------------------------------    
var
  obj: T_Rel_to_Clutter_map;
begin
  obj:=T_Rel_to_Clutter_map.Create;

  obj.Params.RelFileName := aFileName;
  obj.Params.TabFileName_CLU := ChangeFileExt(aFileName, '_clu.tab');
  obj.ExecuteDlg();

  FreeAndNil(obj);                                     
end;



constructor T_Rel_to_Clutter_map.Create;
begin
  inherited;


  FClutters[DEF_CLU_OPEN_AREA] := clBlack;  // 1- Ëåñ (1)
//  FClutters[DEF_CLU_OPEN_AREA] := clWhite;  // 1- Ëåñ (1)
  FClutters[DEF_CLU_FOREST   ] := clGreen;  // 1- Ëåñ (1)
  FClutters[DEF_CLU_WATER    ] := clNavy;  // 3- ÏÃÒ
  FClutters[DEF_CLU_COUNTRY  ] := clGray;  // 3- ÏÃÒ
  FClutters[DEF_CLU_ROAD     ] := clYellow;
  FClutters[DEF_CLU_RailROAD ] := clYellow;
  FClutters[DEF_CLU_CITY     ] := clRed;  // 7- Ãîðîä
  FClutters[DEF_CLU_BOLOTO   ] := clOlive; // 31- ÏÑÒ
  FClutters[DEF_CLU_ONE_BUILD] := clRed; // 73- Îòä.äîìà
  FClutters[DEF_CLU_ONE_HOUSE] := clMaroon; // 31- ÏÑÒ

end;



//-------------------------------------------------------------------
procedure T_Rel_to_Clutter_map.Draw_Clutters;
//-------------------------------------------------------------------
var
  bool: Boolean;

  iRow, iCol : integer;

  k: Integer;

  rRel : Trel_Record;

  sFileName: string;
  xyRect: TXYRect;

  i: Integer;

  R,G,B: byte;

//  hdr: TGDAL_Envi_HDR;
  iEPSG: word;
//  oBAT: TStringList;
//  oColorList: TStringList;
  oGDAL_bat: TGDAL_bat;
  //oIni: TIniFile;

  s: string;
  sFile: string;

  sPath_bin: string;

  rGDAL_json: TGDAL_json_rec;
  sDestFile: string;
  sDir: string;
  sFile_bat: string;
  sPath: string;


const
  DEF_TEMP_DIR = '_temp\';


begin
  oGDAL_bat:=TGDAL_bat.Create;
//  oGDAL_bat:=TGDAL_bat_1111111.Create;
  oGDAL_bat.Init_Header; // (ExtractFilePath(Params.TabFileName_CLU) + DEF_TEMP_DIR);

//  oGDAL_bat.Add('del *.* /Q');


  DoProgressMsg ('Выполнение...');

  sDir:=ExtractFilePath(Params.TabFileName_CLU) + DEF_TEMP_DIR;
//  sDir:=ExtractFilePath(Params.TabFileName_CLU) ;

  ForceDirectories( sDir);


  SaveCluttersToEnvi (sDir);

 // SaveReliefToEnvi;

  // ---------------------------------------------------------------
  oGDAL_bat.ColorList_Add_Value_Color (0, 0);


//  oColorList:=TStringList.Create;
//  oColorList.

//   oColorList.Add('0 0 0 0');

  for I := 0 to High(FClutters) do
    if FClutters[i]>0 then
    begin
  //    ColorTo_R_G_B(FClutters[i], r,g,b);

      oGDAL_bat.ColorList_Add_Value_Color (i, FClutters[i]);

   //   s:= Format('%d %d %d %d',[i,  r,g,b]);
    //  oColorList.Add(s);
    end;

 // sFile_colors:=ChangeFileExt(Params.RelFileName, '.color.txt');

 // oColorList.SaveToFile(ChangeFileExt(Params.RelFileName, '.color.txt'));
//  oColorList.SaveToFile(ExtractFilePath(Params.TabFileName_CLU) +  '_color.txt');

//  oColorList.SaveToFile(sDir + '_color.txt');

  oGDAL_bat.ColorList.SaveToFile(sDir + '_color.txt');

//  oColorList.SaveToFile(ExtractFilePath(Params.TabFileName_CLU) + DEF_TEMP_DIR +  '_color.txt');

//  FreeAndNil(oColorList);


// oBAT.Add('set path='+ sPath_bin + ';'+ ExtractFilePath(Params.TabFileName_CLU) );




  sFile :=ChangeFileExt( ExtractFileName( Params.RelFileName), '');

// ---------------------------------------------------------------
// ENVI -> TIF
// ---------------------------------------------------------------
  iEPSG:=28400+ FRelMatrix.ZoneNum_GK;


  //------------------------------------
  //-co compress=lzw
  //------------------------------------

  s:=  Format('gdal_translate.exe -a_nodata 255 -co compress=lzw -a_srs EPSG:%d  "%s_clu.envi" "%s_clu.%d.tif"',
           [iEPSG, sFile, sFile, iEPSG]); // --_TIF]);
//  oBAT.Add(s);
  oGDAL_bat.Add(s);

  s:=Format('gdalinfo -json   "%s_clu.envi" >  "%s_clu.%d.json" ',[sFile, sFile, iEPSG]);  //_TIF,sFile_TIF]);
 // oBAT.Add(s);
 // oBAT.Add('');

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');


// ---------------------------------------------------------------
//  TIF -> TIF EPSG:3395
// ---------------------------------------------------------------

  s:=  Format('gdalwarp -t_srs EPSG:3395 "%s_clu.%d.tif"  "%s_clu.3395.tif" ',[sFile, iEPSG, sFile]);  //_TIF]);
//  oBAT.Add(s);
//  oBAT.Add('');

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');


  //3395

  s:= Format('gdaldem color-relief "%s_clu.3395.tif" "_color.txt" "%s_clu.3395.color.tif" ',[sFile, sFile]);   //_TIF, sFile_TIF]);
  oGDAL_bat.Add(s);

  s:= Format('gdaldem color-relief "%s_clu.%d.tif" "_color.txt" "%s_clu.%d.color.tif" ',[sFile, iEPSG, sFile, iEPSG ]);   //_TIF, sFile_TIF]);
  oGDAL_bat.Add(s);


  //-stats

  s:=Format('gdalinfo -json   "%s_clu.3395.color.tif" >  "%s_clu.3395.color.json" ',[sFile,sFile]);  //_TIF,sFile_TIF]);
//  oBAT.Add(s);
//  oBAT.Add('');

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');


//  s:=Format('copy /y  "%s_clu.%d.json"  "%s_clu.json"', [sFile, iEPSG,  sFile ]);
//  oGDAL_bat.Add(s);

  //(Params.TabFileName_CLU, '.3395.tif')
//  sDestFile :=ExtractFileName( Params.TabFileName_CLU);
  sDestFile :=ChangeFileExt( ExtractFileName( Params.TabFileName_CLU), '');


  if FRelMatrix.StepX < 5 then
    s:=Format('gdal_translate.exe  -co compress=lzw  "%s_clu.%d.color.tif"    "..\%s.gk.tif"', [sFile, iEPSG, sDestFile  ])
  else
    s:=Format('gdal_translate.exe  -co compress=lzw "%s_clu.3395.color.tif"  "..\%s.3395.tif"', [sFile, sDestFile ]);
  oGDAL_bat.Add(s);

{
  s:=Format('gdal_translate.exe  -co compress=lzw "%s_clu.3395.color.tif"  "..\%s.3395.tif"', [sFile, sDestFile ]);
//  s:=Format('gdal_translate.exe  -co compress=lzw "%s_clu.3395.color.tif"  "..\%s_clu.3395.tif"', [sFile, sFile ]);
  oGDAL_bat.Add(s);

 // s:=Format('copy /y  "%s_clu.3395.color.tif"  "%s"', [sFile, ChangeFileExt(Params.TabFileName_CLU, '.3395.tif') ]);
//  s:=Format('copy /y  "%s_clu.3395.color.tif"  "%s"', [sFile, ChangeFileExt(Params.TabFileName_CLU, '.3395.tif') ]);
 // oGDAL_bat.Add(s);

//  s:=Format('copy /y  "%s_clu.%d.color.tif"    "%s"', [sFile, iEPSG, ChangeFileExt(Params.TabFileName_CLU, '.tif') ]);
//  s:=Format('copy /y  "%s_clu.%d.color.tif"    "%s"', [sFile, iEPSG, ChangeFileExt(Params.TabFileName_CLU, '.tif') ]);
//  s:=Format('copy /y  "%s_clu.%d.color.tif"       "%s"', [sFile, iEPSG, ChangeFileExt(Params.TabFileName_CLU, '.gk.tif') ]);

//  oBAT.Add(s);
//  oGDAL_bat.Add(s);

  s:=Format('gdal_translate.exe  -co compress=lzw  "%s_clu.%d.color.tif"    "..\%s_clu.tif"', [sFile, iEPSG, sFile  ]);
  oGDAL_bat.Add(s);
   }

//  s:=Format('copy /y  "%s_clu.%d.color.tif"    "%s"', [sFile, iEPSG, ChangeFileExt(Params.TabFileName_CLU, '.tif') ]);
 // oGDAL_bat.Add('del *.*');

//    TFile.Copy(ChangeFileExt(sFile_bat, '_clu.tif'), ChangeFileExt(Params.TabFileName_CLU, '_clu.tif'));

  
 

//  oBAT.Load SaveToFile(ChangeFileExt(Params.RelFileName, '.bat'),myEncoding);


  sFile_bat:= sDir +  ChangeFileExt( ExtractFileName(Params.RelFileName) , '.bat');

//  oBAT.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));

  oGDAL_bat.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));

//  oBAT.SaveToFile(ChangeFileExt(Params.RelFileName, '.bat'), TEncoding.GetEncoding(866));

//  FreeAndNil(oBAT);

  SetCurrentDir(sDir);

  RunApp(sFile_bat,'');

//  TDirectory.Delete (sDir);

//  RunApp(ChangeFileExt(Params.RelFileName, '.bat'),'');

  // ---------------------------------------------------------------

  sFile:=ChangeFileExt(sFile_bat, '_clu.3395.color.json');
  Assert(FileExists(sFile));
                                                                 // '_clu.3395.color.json'
  sFile:=ChangeFileExt(sFile_bat, Format('_clu.%d.json', [iEPSG]));
  Assert(FileExists(sFile));

  if FRelMatrix.StepX < 5 then
  begin
//
    if oGDAL_bat.GDAL_json.LoadFromFile (ChangeFileExt(sFile_bat, Format('_clu.%d.json', [iEPSG]))) then
    begin
      oGDAL_bat.GDAL_json.Zone_UTM:=FRelMatrix.ZoneNum_GK + 30;
      oGDAL_bat.GDAL_json.SaveToFile_TAB_Pulkovo (
                                              ChangeFileExt(Params.TabFileName_CLU, '.tab'),
                                              ChangeFileExt(Params.TabFileName_CLU, '.gk.tif') );
    end;
//
//
  end else
  begin
    if oGDAL_bat.GDAL_json.LoadFromFile (ChangeFileExt(sFile_bat, '_clu.3395.color.json')) then
      oGDAL_bat.GDAL_json.SaveToFile_TAB_3395( //Params.TabFileName_CLU,
                                              ChangeFileExt(Params.TabFileName_CLU, '.tab'),
  //                                            ChangeFileExt(Params.TabFileName_CLU, '.3395.tab'),
                                              ChangeFileExt(Params.TabFileName_CLU, '.3395.tif')
                                               );
//
  end;


{
    if oGDAL_bat.GDAL_json.LoadFromFile (ChangeFileExt(sFile_bat, '_clu.3395.color.json')) then
      oGDAL_bat.GDAL_json.SaveToFile_TAB_3395( //Params.TabFileName_CLU,
                                              ChangeFileExt(Params.TabFileName_CLU, '.tab'),
  //                                            ChangeFileExt(Params.TabFileName_CLU, '.3395.tab'),
                                              ChangeFileExt(Params.TabFileName_CLU, '.3395.tif')
                                               );
}

//
//  if oGDAL_bat.GDAL_json.LoadFromFile (ChangeFileExt(sFile_bat, '_clu.3395.color.json')) then
//    oGDAL_bat.GDAL_json.SaveToFile_TAB_3395( //Params.TabFileName_CLU,
//                                            ChangeFileExt(Params.TabFileName_CLU, '.3395.tab'),
////                                            ChangeFileExt(Params.TabFileName_CLU, '.3395.tab'),
//                                            ChangeFileExt(Params.TabFileName_CLU, '.3395.tif')
//                                             );

end;



//
//function T_Rel_to_Clutter_map.GetItemByRowCol(aRow,aCol: Integer; var aRec:  Trel_Record): Boolean;
//begin
//  result:= FRelMatrix.GetItemByRowCol (ARow, aCol, aRec);
//end;
//


//--------------------------------------------
procedure T_Rel_to_Clutter_map.ExecuteProc;
//--------------------------------------------
//var
 /// FPNGFileName: string;
//  sPNGFileName: string;

 // vIRasterToolsX: IRasterToolsX;

 // sTifFileName: string;
begin
  Assert (FileExists(Params.RelFileName), 'Params.RelFileName - '+ Params.RelFileName);

  FRelMatrix:=TrlfMatrix.Create;   //  rel_CreateRelMatrixByFileName(Params.RelFileName);
//  FRelMatrix.OpenFile(Params.RelFileName);

//  if not Assigned(FRelMatrix) then
//    Exit;

  if FRelMatrix.OpenFile (Params.RelFileName) then
  begin

/////    Draw_Clutters ; //(ChangeFileExt(Params.TabFileName, '.bmp'));
    if Params.TabFileName_CLU<>'' then
      Draw_Clutters;


  end;

  FreeAndNil(FRelMatrix);

end;

// ---------------------------------------------------------------
procedure T_Rel_to_Clutter_map.SaveCluttersToEnvi(aDir: string);
// ---------------------------------------------------------------
var
  bool: Boolean;
  iRow: integer;
  iCol: integer;
  rRel: Trel_Record;
//  xyRect: TXYRect;
  oFileStream: TBufferedFileStream;
  bt_value:  byte;
  hdr: TGDAL_Envi_HDR;
 // oMemStream: TMemoryStream;
  sFile: string;

  //oFileStream:  FFileTBufferedFileStream;

begin
//  xyRect:=FRelMatrix.XYBounds;


  sFile:= aDir +  ChangeFileExt( ExtractFileName(Params.RelFileName) , '_clu.envi');


//  sFile:=ChangeFileExt(Params.RelFileName, '_clu.envi');
                                              

   
  oFileStream:=TBufferedFileStream.create(sFile, fmCreate);
 // oMemStream:=TMemoryStream.create;



  for iRow := 0 to FRelMatrix.RowCount - 1 do
    if not Terminated then
  begin
    if iRow mod 200 = 0 then
     DoProgress (iRow, FRelMatrix.RowCount);


    for iCol := 0 to FRelMatrix.ColCount - 1 do
    begin
      rRel:=FRelMatrix.Items[iRow, iCol];

//      bool:= GetItemByRowCol(iRow, iCol, rRel);
//      if (not b) then

      bt_value:=255;

  //    if bool then
        if (rRel.Rel_H <> EMPTY_HEIGHT) then
          bt_value:=rRel.Clutter_Code;
         
          
      oFileStream.Write(@bt_value, 1);
    end;
  end;

//  oFileStream.CopyFrom(oMemStream, 0);

  FreeAndNil(oFileStream);
//  FreeAndNil(oMemStream);

  // ---------------------------------------------------------------

//  FillChar (hdr, SIZEOF(hdr), 0);
  hdr.Clear;

  hdr.ColCount:=FRelMatrix.ColCount;
  hdr.RowCount:=FRelMatrix.RowCount;
  hdr.CellSize:=FRelMatrix.StepX;
  hdr.DataType:=1;
//  hdr.DataType:=2;

  hdr.Lon_min :=FRelMatrix.XYBounds.TopLeft.Y;
  hdr.Lat_max :=FRelMatrix.XYBounds.TopLeft.X;

  hdr.Zone_GK :=FRelMatrix.ZoneNum_GK;

  hdr.SaveToFile(ChangeFileExt(sFile, '.hdr'));
//  hdr.SaveToFile_HDR_Pulkovo(ChangeFileExt(sFile, '.hdr'));

end;




end.



