unit u_2D_array;

interface
 uses
  System.SysUtils, System.Classes;
                   

type
  T2DMatrix = class
  private   

    FItems: array of array of smallint;

    procedure SetItems(aRow,aCol: smallint; const Value: smallint);
    function  GetItems(aRow,aCol: smallint): smallint;

  public
    Header, 
    Footer: array of smallint;

    RowCount, 
    ColCount: word;        
   
    procedure SetSize(aRowCount, aColCount: word);

    property Items[aRowCount,aColCount: smallint]: smallint read GetItems write SetItems; default;
  end;


implementation



// ---------------------------------------------------------------
function T2DMatrix.GetItems(aRow,aCol: smallint): smallint;
// ---------------------------------------------------------------
//var
//  k: Integer;

begin
  try
  
  //  if aRow<0 then
   //   aRow:=0;

//    if aRow>RowCount-1 then
//      aRow:=RowCount-1;
      
    if aRow>RowCount-1 then
      Result:= Footer[aCol]
      
    else
  
    if aRow<0 then
      Result:= Header[aCol]
      
    else begin
  //    Assert(aRow>=0);
   //   Assert( Length( FItems[aRow])>=0);

    //  k:=Length( FItems[aRow]);
    
      Result:= FItems[aRow,aCol];

    end;  
     
  except
    Result:=-9999;

  end;
  
end;

// ---------------------------------------------------------------
procedure T2DMatrix.SetItems(aRow,aCol: smallint; const Value: smallint);
// ---------------------------------------------------------------
begin        

  FItems[aRow,aCol]:=Value;
end;

// ---------------------------------------------------------------
procedure T2DMatrix.SetSize(aRowCount, aColCount: word);
// ---------------------------------------------------------------
var
  oFileStream: TFileStream;
begin
  RowCount:=aRowCount;
  ColCount:=aColCount;


  try
    SetLength(FItems,  aRowCount, aColCount);    

    SetLength(Header,  aColCount);    
    SetLength(Footer,  aColCount);    

  except //on E: Exception do
  end;
  
end;



end.

