unit u_3d_func;

interface
uses Graphics,Math,

    u_3d_types;

Const
   // ������������ ��������
    HLSMAX = 240;
    RGBMAX = 255;
    UNDEFINED = (HLSMAX*2) div 3;

type
  TRGBColor = packed record
      R, G, B : Integer;
  end;

  THLSRecord = packed record
      H, L, S : Integer; // H-�������, L-�������, S-������������
  end;

  function _FindOptimalColor(aKoof : double; aColorInfo : TRelColorsRec): TColor;

  function _FindOptimalLightColor(aHeightKoof, aLightKoof : double; aColors : TRelLightColorsRec):
      TColor;

  function _RGBToColor (R, G, B : byte ) : TColor;
  procedure _ColorToRGB (aClr : TColor; var R, G, B : byte );

  //-------------------------------------------------------------------
  // ��������������� RGB <--> HLS
  //-------------------------------------------------------------------

  function _FromColor(aColor : TColor ) : TRGBColor;
  function _ToColor  (aRGB : TRGBColor ) : TColor ;

  function _RGBtoHLS(aRGB : TRGBColor): THLSRecord;
   function _HLStoRGB(aHLS : THLSRecord): TRGBColor;


//==================================================================
implementation
//==================================================================

procedure _ColorToRGB( aClr : TColor; var R, G, B : byte );
begin
  R := aClr and $FF;
  G := ( aClr shr 8 ) and $FF;
  B := ( aClr shr 16 ) and $FF;
end;


function _RGBToColor( R, G, B : byte ) : TColor;
begin
  Result := R or ( G shl 8 ) or ( B shl 16 );
end;

//-------------------------------------------------------------------
function _FindOptimalColor(aKoof : double; aColorInfo : TRelColorsRec): TColor;
//-------------------------------------------------------------------
var MinR, MinG, MinB : byte;
    MaxR, MaxG, MaxB : byte;
begin
  Result := aColorInfo.MaxHColor;
  if aKoof > 1.0 then
    exit;

  _ColorToRGB( aColorInfo.MinHColor, MinR, MinG, MinB );
  _ColorToRGB( aColorInfo.MaxHColor, MaxR, MaxG, MaxB );

  Result := _RGBToColor( Trunc( MinR + ( MaxR - MinR ) * aKoof ),
                         Trunc( MinG + ( MaxG - MinG ) * aKoof ),
                         Trunc( MinB + ( MaxB - MinB ) * aKoof ));
end;

// =============================================================================
// ��������������� RGB <--> HLS ================================================

function _FromColor( aColor : TColor ) : TRGBColor;
begin
    Result.R := aColor and $FF;
    Result.G := ( aColor shr 8 ) and $FF;
    Result.B := ( aColor shr 16 )  and $FF;
end;


function _ToColor (aRGB: TRGBColor ): TColor;
begin
    Result := ((aRGB.B and $FF) shl 8 or (aRGB.G and $FF)) shl 8 or ( aRGB.R and $FF );
end;


//-------------------------------------------------------------------
function _RGBtoHLS(aRGB : TRGBColor): THLSRecord;
//-------------------------------------------------------------------
Var cMax, cMin  : integer;
    Rdelta, Gdelta, Bdelta : single;
Begin
  with aRGB do
  begin
      cMax := max( max( R, G ), B );
      cMin := min( min( R, G ), B );
  end;

  Result.L := Round ((( cMax + cMin ) * HLSMAX + RGBMAX ) / ( 2 * RGBMAX ));

  if ( cMax = cMin ) then begin
      Result.S := 0;
      Result.H := UNDEFINED;
  end else begin
      if ( Result.L <= ( HLSMAX / 2 )) then
          Result.S := Round((( cMax - cMin ) * HLSMAX + ( cMax + cMin ) / 2 ) / ( cMax + cMin ))
      else
          Result.S := Round(((( cMax - cMin ) * HLSMAX + ( 2 * RGBMAX - cMax - cMin ) / 2 ))
                                                          / ( 2 * RGBMAX - cMax - cMin ));

      Rdelta := ((( cMax - aRGB.R ) * HLSMAX / 6 ) + (( cMax - cMin ) / 2 )) / ( cMax - cMin );
      Gdelta := ((( cMax - aRGB.G ) * HLSMAX / 6 ) + (( cMax - cMin ) / 2 )) / ( cMax - cMin );
      Bdelta := ((( cMax - aRGB.B ) * HLSMAX / 6 ) + (( cMax - cMin ) / 2 )) / ( cMax - cMin );
      if ( aRGB.R = cMax ) then Result.H := Round( Bdelta - Gdelta )
      else if ( aRGB.G = cMax ) then Result.H := Round(( HLSMAX / 3 ) + Rdelta - Bdelta )
      else Result.H := Round((( 2 * HLSMAX ) / 3 ) + Gdelta - Rdelta );

      if ( Result.H < 0 ) then Result.H := Result.H + HLSMAX;
      if ( Result.H > HLSMAX ) then Result.H := Result.H - HLSMAX;
  end;
  if Result.S < 0 then Result.S := 0;
  if Result.S > HLSMAX then Result.S := HLSMAX;
  if Result.L < 0 then Result.L := 0;
  if Result.L > HLSMAX then Result.L := HLSMAX;
end;

//-------------------------------------------------------------------
function HueToRGB (n1, n2, aHue : single ) : single;
//-------------------------------------------------------------------
begin
  if ( aHue < 0 ) then aHue := aHue + HLSMAX;
  if ( aHue > HLSMAX ) then aHue := aHue - HLSMAX;
  if ( aHue < ( HLSMAX / 6 )) then
      Result := ( n1 + ((( n2 - n1 ) * aHue + ( HLSMAX / 12 )) / ( HLSMAX / 6 )))
  else
      if ( aHue < ( HLSMAX / 2 )) then Result := n2
      else
          if ( aHue < (( HLSMAX * 2 ) / 3 )) then
              Result := ( n1 + ((( n2 - n1 ) * ((( HLSMAX * 2 ) / 3 ) - aHue ) +
                          ( HLSMAX / 12 )) / ( HLSMAX / 6 )))
      else
          Result := n1;
end;


//-------------------------------------------------------------------
function _HLStoRGB(aHLS : THLSRecord): TRGBColor;
//-------------------------------------------------------------------
var Magic1, Magic2 : single;
begin
  if ( aHLS.S = 0 ) then
      with Result do begin
          B := Round(( aHLS.L * RGBMAX ) / HLSMAX );
          R := B; G := B;
      end
  else begin
      if ( aHLS.L <= ( HLSMAX / 2 )) then
          Magic2 := ( aHLS.L * ( HLSMAX + aHLS.S ) + ( HLSMAX / 2 )) / HLSMAX
      else
          Magic2 := aHLS.L + aHLS.S - (( aHLS.L * aHLS.S ) + ( HLSMAX / 2 )) / HLSMAX;
      Magic1 := 2 * aHLS.L - Magic2;
      Result.R := Round(( HueToRGB( Magic1, Magic2, aHLS.H + ( HLSMAX / 3 )) * RGBMAX +
                                                                  ( HLSMAX / 2 )) / HLSMAX );
      Result.G := Round(( HueToRGB( Magic1, Magic2, aHLS.H ) * RGBMAX +
                                                                  ( HLSMAX / 2)) / HLSMAX );
      Result.B := Round(( HueToRGB( Magic1, Magic2, aHLS.H -( HLSMAX / 3 )) * RGBMAX +
                                                                  ( HLSMAX / 2 )) / HLSMAX );
  end;

  with Result do
  begin
      if R < 0 then R := 0; if R > RGBMAX then R := RGBMAX;
      if G < 0 then G := 0; if G > RGBMAX then G := RGBMAX;
      if B < 0 then B := 0; if B > RGBMAX then B := RGBMAX;
  end;
end;


//-------------------------------------------------------------------
function _FindOptimalLightColor(aHeightKoof, aLightKoof : double; aColors : TRelLightColorsRec):
    TColor;
//-------------------------------------------------------------------
var
  Res : THLSRecord;
  ResRGB : TRGBColor;
  
begin
  with aColors do
  begin
      Res.H := Round( HMin + ( HMax - HMin ) * aHeightKoof );
      Res.L := Round( LMin + ( LMax - LMin ) * aLightKoof );
      Res.S := Round( SMin + ( SMax - SMin ) * aLightKoof );
  end;

  ResRGB := _HLStoRGB( Res );

  with ResRGB do
      Result := _RGBToColor( R, G, B );
end;


end.
