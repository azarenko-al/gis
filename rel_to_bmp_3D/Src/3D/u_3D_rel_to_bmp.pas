﻿unit u_3D_rel_to_bmp;

interface
uses
  Graphics, SysUtils, Dialogs, Windows, Classes, Forms, IOUtils, IniFiles,

  u_GDAL_Bat,
  u_GDAL_classes,
//
  u_run,
//
 u_rel_engine,
//
  u_BMP_file,

  u_Progress,

  I_rel_Matrix1,

  u_rel_matrix_base,

  u_2D_array,
  u_3d_func,
  u_3d_types;

  
type

    TRel_to_3D_map = class(TProgress)
    private

      FBmp: TBmpFile;

      FLightInfoRec : TLightSrcInfoRec;
               
      FBmpFileName : string;

      FNullValue : Integer;

      FRowCount: Integer;
      FBlockRowCount: Integer;


//      FFileStream_Rel: TWriteCachedFileStream;
      
      FRelArray: T2DMatrix ;//array of array of smallint;
      FCluArray: T2DMatrix ;//array of array of smallint;
      
      FGradMatrix : T2DMatrix ;//array of array of smallint;  // Ìàòðèöà ãðàäàöèé ÿðêîñòåé òî÷åê
      FLightMatrix: T2DMatrix ;//array of array of smallint;  // Ìàòðèöà îñâåùåííîñòè òî÷åê..


      
      FRelLightColors : TRelLightColorsRec; //
      FRelColors    : TRelColorsRec;


      FRelMatrix: TrelMatrixBase;

      procedure CreateBitmap;
      
      procedure Draw_block;
      procedure Draw_Close_bmp;
      procedure Draw_Open_bmp;
      
      procedure GDAL_Bmp_to_TIF(aBmpFileName, aTifFileName: string);

      procedure Load_Block(aRowStart, aRowEnd: Integer);

    protected

      procedure CalcHeight; // Îòðèñîâàòü èçîáðàæåíèå
      procedure CalcShadow; // (aLightInfoRec : TLightSrcInfoRec );

      procedure ExecuteProc; override;

    public
      Params: record
                RelFileName: string;
                TabFileName: string;

              end;

      constructor Create ();
      destructor Destroy; override;

      class procedure Exec(aFileName: string);

    end;



implementation

uses
  System.Math;



// ---------------------------------------------------------------
class procedure TRel_to_3D_map.Exec(aFileName: string);
// ---------------------------------------------------------------
var
  obj: TRel_to_3D_map;
begin
  obj:=TRel_to_3D_map.Create;

  obj.Params.RelFileName := aFileName;
  obj.Params.TabFileName := ChangeFileExt(aFileName, '_relief_3D.tab');
  obj.ExecuteDlg();

  FreeAndNil(obj);                                     
end;

  

//-------------------------------------------------------------------
constructor TRel_to_3D_map.Create ();
//-------------------------------------------------------------------
begin
  inherited;   

  FLightInfoRec.LCourceAngle    := 45;
  FLightInfoRec.LIncidenceAngle := 45;

  FRelColors.MinHColor:=clBlue;
  FRelColors.MaxHColor:=clRed;    


  FRelArray:=T2DMatrix.Create; ;//array of array of smallint;
  FCluArray:=T2DMatrix.Create; ;//array of array of smallint;
      
  FGradMatrix := T2DMatrix.Create; ;//array of array of smallint;  // Ìàòðèöà ãðàäàöèé ÿðêîñòåé òî÷åê
  FLightMatrix:= T2DMatrix.Create; ;//array of array of smallint;  // Ìàòðèöà îñâåùåííîñòè òî÷åê..
  
end;

destructor TRel_to_3D_map.Destroy;
begin
  inherited;

  FreeAndNil(FRelArray);
  FreeAndNil(FCluArray);
                       
                   
  FreeAndNil(FGradMatrix);
  FreeAndNil(FLightMatrix);
  
end;
                 



//-------------------------------------------------------------------
procedure TRel_to_3D_map.CalcHeight;
//-------------------------------------------------------------------
var iRow, iCol : integer;
  b: Boolean;
//    rRel : Trel_Record;
    fKoof : Double;
  k: integer;

  iRel_H: SmallInt;
  
begin
//  FShadowWasDone := False;

//  Assert(FRelMatrix.GroundMaxH >= FRelMatrix.GroundMinH );

//  Assert(FRelMatrix.GroundMaxH>0, 'FRelMatrix.GroundMaxH>0');

  DoProgressMsg ('Расчет...');

  for iRow := 0 to FRowCount - 1 do
//  for iRow := 0 to FRelMatrix.RowCount - 1 do
    if not Terminated then
  begin
//    if iRow mod 200 = 0 then 
//      DoProgress2 (iRow, FRelMatrix.RowCount);


    for iCol := 0 to FRelMatrix.ColCount - 1 do
    begin
//      if iCol=0 then
 //       DoProgress (iRow, FRelMatrix.RowCount);

//      rRel := FRelMatrix[iRow, iCol];
//              FFRelArray[aRow,aCol]
      iRel_H:=FRelArray [iRow, iCol];


     if iRel_H = EMPTY_HEIGHT then // åñëè íå çàäàíà âûñîòà, òî ñ÷èòàåì, ÷òî ýòî îáúåêò
        fKoof := 1 / (FRelMatrix.GroundMaxH - FRelMatrix.GroundMinH + 1 ) // íà ìèíèìàëüíîé âûñîòå
      else      

      
        fKoof := (iRel_H - FRelMatrix.GroundMinH + 1 ) /
                 (FRelMatrix.GroundMaxH - FRelMatrix.GroundMinH + 1 );

    //  FGradMatrix[iRow, iCol] := Round( GRAD_MATR_MULTIPLE * fKoof );
      k:=Round( GRAD_MATR_MULTIPLE * fKoof );
                             

      if k>High(Smallint) then
      begin
        k:=High(Smallint);
  //      ShowMessage('k>255*256');
      end;

      try
        FGradMatrix[iRow, iCol] := Smallint(k);//Round( GRAD_MATR_MULTIPLE * fKoof );
      except      

      end;

    end;

//      if iCol=0 then

  end;
end;



//-------------------------------------------------------------------
procedure TRel_to_3D_map.CalcShadow;
//-------------------------------------------------------------------
var
  b: Boolean;
  iRow, iCol : integer;
  eCosAngle : double; // Êîñèíóñ óãëà ìåæäó âåêòîðàìè...
  eLightVect_X, eLightVect_Y, eLightVect_Z : double;
  eLightVect_Len : Double;
  eNorma_A, eNorma_B, eNorma_C, eNorma_Len : double;

  k: integer;
 

  recLightSrcInfo : TLightSrcInfoRec;

begin

  recLightSrcInfo := FLightInfoRec;
  recLightSrcInfo.LCourceAngle    := recLightSrcInfo.LCourceAngle mod 360;
  recLightSrcInfo.LIncidenceAngle := recLightSrcInfo.LIncidenceAngle mod 360; // Íàêëîí ëó÷à ê ïëîñêîñòè

  eLightVect_X := Cos( PI * recLightSrcInfo.LCourceAngle / 180 );
  eLightVect_Y := Sin( PI * recLightSrcInfo.LCourceAngle / 180 );
  eLightVect_Z := 1 / Cos( PI * recLightSrcInfo.LIncidenceAngle / 180 );
  eLightVect_Len := Sqrt( Sqr( eLightVect_X ) + Sqr( eLightVect_Y ) + Sqr( eLightVect_Z ));


  DoProgressMsg ('Расчет...');


    for iRow := 0 to FRowCount - 1 do
      if not Terminated then
    begin
    //  if iRow mod 200 = 0 then 
     //   DoProgress2 (iRow, FRelMatrix.RowCount);

    
//    DoProgress (iRow, FRelMatrix.RowCount);

      for iCol := 1 to FRelMatrix.ColCount - 2 do
      begin
      //  if iCol=1 then
        //  DoProgress (iRow, FRelMatrix.RowCount-1);

// ---------------------------------------------------------------


        eNorma_A := (FRelArray[iRow+1, iCol-1] + 2 * FRelArray[iRow+1, iCol] + FRelArray[iRow+1, iCol+1] ) -
                    (FRelArray[iRow-1, iCol-1] + 2 * FRelArray[iRow-1, iCol] + FRelArray[iRow-1, iCol+1] );

        eNorma_B := (FRelArray[iRow-1, iCol+1] + 2 * FRelArray[iRow, iCol+1] + FRelArray[iRow+1, iCol+1]  ) -
                    (FRelArray[iRow-1, iCol-1] + 2 * FRelArray[iRow, iCol-1] + FRelArray[iRow+1, iCol-1]  );

// ---------------------------------------------------------------
                    
        eNorma_A := eNorma_A / Sqrt( Sqr( eNorma_A ) + Sqr( eNorma_B ) + 1 );
        eNorma_B := eNorma_B / Sqrt( Sqr( eNorma_A ) + Sqr( eNorma_B ) + 1 );
        eNorma_C := 1;

        eNorma_Len := Sqrt( Sqr( eNorma_A ) + Sqr( eNorma_B ) + Sqr( eNorma_C ));

        if ( eNorma_Len * eLightVect_Len ) <> 0 then
        begin
          eCosAngle := ( eNorma_A * eLightVect_X + eNorma_B * eLightVect_Y + eNorma_C * eLightVect_Z ) /
                       ( eNorma_Len * eLightVect_Len );


            k:=Round( 127 * eCosAngle ) + 128;

            if k>High(Smallint) then
            begin
              k:=High(Smallint);
              ShowMessage('k>255*256');
              
              raise Exception.Create('k>255*256');
            end;


          FLightMatrix[iRow, iCol] := Smallint(k); //Round( 127 * eCosAngle ) + 128;
             
        end else
        begin
          FLightMatrix[iRow, iCol] := 0;

        end;
      end;

//      DoProgress (iRow, FRelMatrix.RowCount-1);
   end;
//  FShadowWasDone := True;


end;


//-------------------------------------------------------------------
procedure TRel_to_3D_map.Draw_Open_bmp;
//-------------------------------------------------------------------

begin
  DoProgressMsg ('Расчет...');

  FBmp:=TBmpFile.Create_Width_Height(FBmpFileName, FRelMatrix.ColCount, FRelMatrix.RowCount);

end;

//-------------------------------------------------------------------
procedure TRel_to_3D_map.Draw_Close_bmp;
//-------------------------------------------------------------------

begin
  DoProgressMsg ('Расчет...');

  FreeAndNil(FBmp);

end;


//-------------------------------------------------------------------
procedure TRel_to_3D_map.Draw_block;
//-------------------------------------------------------------------

var
  b: Boolean;
  iRow, iCol : integer;
  iColor : integer;
  k: Integer;

  iRel: smallint;
   
begin
  DoProgressMsg ('Расчет...');


  for iRow := 0 to FRowCount - 1 do
  begin              
    for iCol := 0 to FRelMatrix.ColCount - 1 do
    begin

      iRel:= FRelArray[iRow, iCol];


      if iRel = EMPTY_HEIGHT then
        iColor:=clBlack  
      else

      if FCluArray[iRow, iCol] = DEF_CLUTTER_WATER then
        iColor:=clNavy
        
      else
        iColor := _FindOptimalLightColor(FGradMatrix [iRow, iCol] / GRAD_MATR_MULTIPLE,
                                         FLightMatrix[iRow, iCol] / GRAD_MATR_MULTIPLE,
                                         FRelLightColors )    ;


      FBmp.Write(iColor);

    end;

    FBmp.Writeln;
  end;              
  
end;



//-------------------------------------------------------------------
procedure TRel_to_3D_map.CreateBitmap;
//-------------------------------------------------------------------
var
  i: Integer;
  sImgFileName: string;
  sJpgFileName: string;

  sPNGFileName: string;
  sTifFileName: string;

  tmpRGB : TRGBColor;
  tmpHLS : THLSRecord;

begin     

  tmpRGB := _FromColor( FRelColors.MinHColor );
  tmpHLS := _RGBtoHLS( tmpRGB );
  with FRelLightColors do
  begin
      HMin := tmpHLS.H;
      SMin := 120;
      LMin := 50;
  end;

  tmpRGB := _FromColor( FRelColors.MaxHColor );
  tmpHLS := _RGBtoHLS( tmpRGB );
  with FRelLightColors do
  begin
      HMax := tmpHLS.H;
      SMax := 220;
      LMax := 130;
  end;


(*  if FRelMatrix.GroundMaxH=0 then
//  CalcMinMaxHeights(var aMinH,aMaxH: integer);
    FRelMatrix.GetMinMaxHeights(FRelMatrix.GroundMinH, FRelMatrix.GroundMaxH);

*)

  CalcHeight;   
  CalcShadow;                    
  Draw_block;
           
end;
 

//--------------------------------------------
procedure TRel_to_3D_map.Load_Block(aRowStart, aRowEnd: Integer);
//--------------------------------------------
var
  c: Integer;
  iRows: Integer;
  r: Integer;
begin
//  DoProgressMsg ('Çàãðóçêà ìàòðèöû âûñîò...');

  FRowCount:=aRowEnd - aRowStart +1;

  FCluArray.SetSize   (FRowCount, FRelMatrix.ColCount );

  FRelArray.SetSize   (FRowCount, FRelMatrix.ColCount );
  FGradMatrix.SetSize (FRowCount, FRelMatrix.ColCount );
  FLightMatrix.SetSize(FRowCount, FRelMatrix.ColCount );


  // ----------------------------------------------------

  for r := 0 to FRowCount-1 do
  begin

    for c := 0 to FRelMatrix.ColCount - 1 do
      FRelArray[r,c] := FRelMatrix.Items[r+aRowStart, c].Rel_H;

    for c := 0 to FRelMatrix.ColCount - 1 do
      FCluArray[r,c] := FRelMatrix.Items[r+aRowStart, c].Clutter_Code;

  end;

  // ---------------------------------------------------------------
  // Header
  // ---------------------------------------------------------------
  if aRowStart=0 then
    for c := 0 to FRelMatrix.ColCount - 1 do
      FRelArray.Header[c] := FNullValue
  else
//  if aRowStart>0 then
    for c := 0 to FRelMatrix.ColCount - 1 do
      FRelArray.Header[c] := FRelMatrix.Items[aRowStart-1, c].Rel_H;

  // ---------------------------------------------------------------
  // Footer
  // ---------------------------------------------------------------
  if aRowEnd=FRelMatrix.RowCount-1 then
    for c := 0 to FRelMatrix.ColCount - 1 do
      FRelArray.Footer[c] := FNullValue
  else    
    for c := 0 to FRelMatrix.ColCount - 1 do
      FRelArray.Footer[c] := FRelMatrix.Items[aRowEnd+1, c].Rel_H;

  
end; 


//-------------------------------------------------------------------
procedure TRel_to_3D_map.GDAL_Bmp_to_TIF(aBmpFileName, aTifFileName: string);
//-------------------------------------------------------------------
var
  bool: Boolean;

  k: Integer;

  sFileName: string;
  s: string;

  sFile: string;
  sFileName_json: string;
  sFile_bat: string;

  sDir: string;
  sTab: string;

  oGDAL_bat: TGDAL_bat;
    
begin
  Assert(FileExists(aBmpFileName));


  oGDAL_bat:=TGDAL_bat.Create;
  oGDAL_bat.Init_Header;
//  Init (ExtractFilePath(Params.TabFileName));


  sDir:=ExtractFilePath(Params.TabFileName) ; //+ DEF_TEMP_DIR;

  sFile:=ChangeFileExt( ExtractFileName(aTifFileName), '');
 
                                         
  s:= Format('gdal_translate.exe  -a_srs EPSG:%d -of GTiff  -co "COMPRESS=LZW" -a_ullr %d %d %d %d  "%s.bmp" "%s.tif" ',
                             [28400+ FRelMatrix.ZoneNum_GK,

                      Round(FRelMatrix.XYBounds.TopLeft.Y),
                      Round(FRelMatrix.XYBounds.TopLeft.X),

                      Round(FRelMatrix.XYBounds.BottomRight.Y),
                      Round(FRelMatrix.XYBounds.BottomRight.X),

                      sFile, 
                      sFile

                     ]);

   oGDAL_bat.Add(s);
   
// ---------------------------------------------------------------
//  TIF -> TIF EPSG:3395
// ---------------------------------------------------------------
  
  s:=  Format('gdalwarp -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ',[sFile, sFile]);  //_TIF]);

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');

  
  s:=Format('gdalinfo -json   "%s.3395.tif" >  "%s.3395.json" ',[sFile,sFile]);  //_TIF,sFile_TIF]);

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');

 
  // ---------------------------------------------------------------
  

  sFile_bat:=  ChangeFileExt( aBmpFileName , '.bat');
  
  oGDAL_bat.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));
  
//  FreeAndNil(oBAT);

 //!!!!!!!!!!!!!!!!!!!
  SetCurrentDir(sDir);

  RunApp(sFile_bat,'');

  sTab          :=ChangeFileExt(Params.TabFileName, '.tab');

//  sFileName_json:=ChangeFileExt(Params.TabFileName, '.3395.json');  //sFile + '%s.3395.tif';
//  sFileName_json:=ChangeFileExt(Params.TabFileName, '.3395.json');  //sFile + '%s.3395.tif';

  sFileName_json :=ChangeFileExt(Params.TabFileName, '.3395.json');  //sFile + '%s.3395.tif';
  aTifFileName   :=ChangeFileExt(Params.TabFileName, '.3395.tif');  //sFile + '%s.3395.tif';

  Assert (FileExists (sFileName_json), sFileName_json);
  Assert (FileExists (aTifFileName),   aTifFileName);


  // ---------------------------------------------------------------
//  sFileName_json:=ChangeFileExt(Params.TabFileName, '.json');  //sFile + '%s.3395.tif';
//  aTifFileName  :=ChangeFileExt(Params.TabFileName, '.tif');  //sFile + '%s.3395.tif';


//  if FRelMatrix.StepB < 5 then

//  if oGDAL_bat.GDAL_json.LoadFromFile (sFileName_json) then
//    oGDAL_bat.GDAL_json.SaveToFile_TAB_Pulkovo (sTab, aTifFileName );


  if oGDAL_bat.GDAL_json.LoadFromFile (sFileName_json) then
    oGDAL_bat.GDAL_json.SaveToFile_TAB_3395 (sTab, aTifFileName );

 // assert(FileExists(sTab));
  
end;



//--------------------------------------------
procedure TRel_to_3D_map.ExecuteProc;
//--------------------------------------------
var
  c: Integer;
  I: Integer;
  iMemSize: Integer;
  iParts: Integer;
  iRel_H: Integer;
  iRowEnd: Integer;
  iRowStart: Integer;
  r: Integer;
  sTifFileName: string;
begin
  FRelMatrix:= rel_CreateRelMatrixByFileName(Params.RelFileName);

  if not Assigned(FRelMatrix) then
    Exit;

  FBmpFileName:= ChangeFileExt(Params.TabFileName, '.bmp');
   
    

  if FRelMatrix.OpenFile (Params.RelFileName) then
  begin
    if (FRelMatrix.GroundMinH=0) and (FRelMatrix.GroundMaxH=0) then
    begin
      for r := 0 to FRelMatrix.RowCount-1 do
        for c := 0 to FRelMatrix.ColCount-1 do
        begin
          iRel_H:=FRelMatrix.Items[r, c].Rel_H;
          if FRelMatrix.NullValue = iRel_H then
            Continue;

          if (FRelMatrix.GroundMinH = 0) and (FRelMatrix.GroundMinH > iRel_H) then
            FRelMatrix.GroundMinH := iRel_H;

          if (FRelMatrix.GroundMaxH = 0) and (FRelMatrix.GroundMaxH < iRel_H) then
            FRelMatrix.GroundMaxH := iRel_H;

        end;

    end;





   // if FRelMatrix.GroundMaxH = 0 then
    //  FRelMatrix.CalcMinMaxHeights();

    FNullValue:=FRelMatrix.NullValue;
 
    iMemSize:=FRelMatrix.RowCount * FRelMatrix.ColCount * SizeOf(smallint);

    //
    
//    CodeSite.Send('MemSize: '+ IntToStr(iMemSize));
    
    
    Draw_Open_bmp;
    
    //------------------------
    // ðàçáèòü íà ÷àñòè
    //------------------------
    FBlockRowCount:=100;
    
    iParts := Ceil (FRelMatrix.RowCount / FBlockRowCount) ;

//    CodeSite.Send('iParts: '+ IntToStr(iParts));
    
    for I:= 0 to iParts-1 do
      if not Terminated then
    begin
      DoProgress (i, iParts);
    
      iRowStart:=i*FBlockRowCount;      
      
  //    if iRowEnd>FRelMatrix.RowCount-1 then
      iRowEnd:= Min((i+1)*FBlockRowCount - 1, FRelMatrix.RowCount-1);
      

//      DoProgress (i, iParts);
        
      Load_Block(iRowStart, iRowEnd);

      CreateBitmap;
      
     // Draw_block;
      
    end;


    
 //   Analyse;
    
//    LoadToArray();
      

  //  CreateBitmap;
    Draw_Close_bmp;

    Assert(FileExists(FBmpFileName));

    sTifFileName:= ChangeFileExt(Params.TabFileName, '.tif');
    GDAL_Bmp_to_TIF (FBmpFileName, sTifFileName);
 //   SysUtils.DeleteFile (FBmpFileName);
  
  
  end;

  FreeAndNil(FRelMatrix);

end;




end.


