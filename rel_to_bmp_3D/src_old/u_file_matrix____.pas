unit u_file_matrix____;

interface
uses

  Classes, SysUtils;


type
  TFileMatrix = class(TObject)
  private
    FFileName : string;

    FFileStream: TFileStream;
    FMemoryStream: TMemoryStream;

    FRecordSize: Integer;
    FRowCount: Integer;
    FColCount: Integer;


    function GetValue(aRow,aCol: integer): Smallint;
    procedure SetValue(aRow,aCol: integer; Value:Smallint);

  public

    constructor Create(AFileName: string; aRowCount, aColCount, aRecordSize:
        integer);
    destructor Destroy; override;

    function ReadBuffer(aRow, aCol: integer; var aBuffer): boolean;
    function WriteBuffer(aRow, aCol: integer; var aBuffer): Boolean;

    property Items[Row,Col: integer]: SmallInt read GetValue write SetValue;
        default;

  end;


implementation


// ---------------------------------------------------------------
constructor TFileMatrix.Create(AFileName: string; aRowCount, aColCount,
    aRecordSize: integer);
// ---------------------------------------------------------------

begin
  inherited Create;

  FFileName := AFileName;

  FRowCount  :=aRowCount;
  FColCount  :=aColCount;
  FRecordSize:=aRecordSize;


  FFileStream := TFileStream.Create(AFileName, fmCreate);
  FFileStream.Size := aRecordSize * aColCount * aRowCount;

  
(*  FMemoryStream:=TMemoryStream.Create;
  FMemoryStream.Size :=aRecordSize * aColCount * aRowCount;
*)


//
//  if FBitmapSize < BMP_SIZE_LIMIT then
//  begin
//    FFileStream := TFileStream.Create(AFileName, fmCreate);
//    FFileStream.Size := aRecordSize * aColCount * aRowCount;
//
//  end else
//    raise Exception.Create('Image is too big! Maximum size = 4Gb (current size = '
//      + IntToStr(FFileStream.Size div (1024*1024*1024)) + ' Gb)');
//

end;

destructor TFileMatrix.Destroy;
begin
  FreeAndNil(FFileStream);

  FreeAndNil(FMemoryStream);

  DeleteFile(FFileName);
  inherited;
end;

function TFileMatrix.GetValue(aRow,aCol: integer): Smallint;
begin
  ReadBuffer(aRow, aCol, Result);

end;

//-------------------------------------------------
function TFileMatrix.ReadBuffer(aRow, aCol: integer; var aBuffer): boolean;
//-------------------------------------------------
var
  i: Integer;
begin

  FFileStream.Position:=FRecordSize * (aRow*FColCount + aCol);
  i:=FFileStream.read(aBuffer, FRecordSize);

  Result := i=FRecordSize;

(*

  FMemoryStream.Position:=FRecordSize * (aRow*FColCount + aCol);
  i:=FMemoryStream.read(aBuffer, FRecordSize);

  Result := i=FRecordSize;
*)


end;

procedure TFileMatrix.SetValue(aRow,aCol: integer; Value:Smallint);
begin
  WriteBuffer(aRow,aCol, Value);
end;

//-------------------------------------------------
function TFileMatrix.WriteBuffer(aRow, aCol: integer; var aBuffer): Boolean;
//-------------------------------------------------
var
  i: Integer;
begin

  FFileStream.Position:=FRecordSize * (aRow*FColCount + aCol);
  i:=FFileStream.Write(aBuffer, FRecordSize);
  Result := i=FRecordSize;

(*
  FMemoryStream.Position:=FRecordSize * (aRow*FColCount + aCol);
  i:=FMemoryStream.Write(aBuffer, FRecordSize);
  Result := i=FRecordSize;
*)


end;



end.
