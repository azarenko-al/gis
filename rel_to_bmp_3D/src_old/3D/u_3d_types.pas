unit u_3d_types;

interface
uses Graphics;

const
  DEFAULT_COURSE    : Word = 45;
  DEFAULT_DIRECTION : WORD = 0;

  GRAD_MATR_MULTIPLE = 255;

 // MaxMatrixWidth1  = 10000; // ������������ ���������� ���� �� �����������
//  MaxMatrixHeight1 = 10000; // ������������ ���������� ���� �� ���������

  WATER_COEF : double = 0.0; // ���������� ��� ����...

  DEF_CLUTTER_WATER = 2;

 // LO_SEA1  =  2;  // ������,  ������ ������� - ����


type
  TRelColorsRec = record // ����� ��� ����������� ��������
    MinHColor   : TColor; // �� ����������� ������
    MaxHColor    : TColor; // �� ������������ ������
   // WaterColor   : TColor; // ����

  //  ShadowColor1 : TColor; // ����
  end;

  TRelLightColorsRec = record
    HMin, HMax : Integer; // "�������" ��� ����������� � ������������ ������
    LMin, LMax : Integer; // "�������" ��� ����������� � ������������ ������
    SMin, SMax : Integer; // "������������" ����������� � ������������ ������
  end;

  TLightSrcInfoRec = record  // ���� ���� �����������...
    LHeight :        SmallInt; // ������ ��������� �����...
    LCourceAngle :   Word; // ���� ����� ����� ����� � �������� � �����������...
    LIncidenceAngle : Word; // ���� ����� ��������� ���� �� ��������� X0Y �
                            // ���� X
  end;


implementation

end.

