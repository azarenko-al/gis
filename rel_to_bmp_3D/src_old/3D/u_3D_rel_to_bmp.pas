unit u_3D_rel_to_bmp;

interface
uses 
  Graphics, SysUtils, Dialogs, Windows, Classes, Forms,
  IniFiles,

  u_BufferedFileStream,
  
  u_run,          

  u_rel_engine,

  
  u_BMP_file,

//  u_dll_img,

  u_Progress,

  I_rel_Matrix1,

  u_rel_matrix_base,

  u_2D_array,
  
  u_3d_func,
  u_3d_types;


  
type

    T3D_Rel_to_Bmp = class(TProgress)
    private
      FLightInfoRec : TLightSrcInfoRec;
               
      FBmpFileName : string;

      FNullValue : Integer;



//      FFileStream_Rel: TWriteCachedFileStream;
      
      FRelArray: T2DMatrix ;//array of array of smallint;
      
      FGradMatrix : T2DMatrix ;//array of array of smallint;  // ������� �������� �������� �����
      FLightMatrix: T2DMatrix ;//array of array of smallint;  // ������� ������������ �����..

//      FRelArray: array of array of smallint;
//      
//      FGradMatrix : array of array of smallint;  // ������� �������� �������� �����
//      FLightMatrix: array of array of smallint;  // ������� ������������ �����..

      
      FRelLightColors : TRelLightColorsRec; //
      FRelColors    : TRelColorsRec;


      FRelMatrix: TrelMatrixBase;

//      procedure Bmp_to_TIF(aBmpFileName, aTifFileName: string);
      procedure CreateBitmap;
      procedure Bmp_to_TIF(aBmpFileName, aTifFileName: string);
      
      function GetItem(aRow,aCol: integer): smallint;


      procedure LoadToArray;

    protected
      procedure Draw;

      procedure CalcHeight; // ���������� �����������
      procedure CalcShadow; // (aLightInfoRec : TLightSrcInfoRec );

      procedure ExecuteProc; override;

    public
      Params: record
                RelFileName: string;
                TabFileName: string;

              //  TaskType: TTaskType;

            //    IsGrayScale: Boolean; // ����� �����������: True - �������� ������

              end;

      constructor Create ();
      destructor Destroy; override;

      class procedure Exec(aFileName: string);

    end;



implementation



// ---------------------------------------------------------------
class procedure T3D_Rel_to_Bmp.Exec(aFileName: string);
// ---------------------------------------------------------------
var
  obj: T3D_Rel_to_Bmp;                              
begin
  obj:=T3D_Rel_to_Bmp.Create;

  obj.Params.RelFileName := aFileName;
  obj.Params.TabFileName := ChangeFileExt(aFileName, '_relief.tab');
  obj.ExecuteDlg();

  FreeAndNil(obj);                                     
end;

  

//-------------------------------------------------------------------
constructor T3D_Rel_to_Bmp.Create ();
//-------------------------------------------------------------------
begin
  inherited;   

  FLightInfoRec.LCourceAngle    := 45;
  FLightInfoRec.LIncidenceAngle := 45;

  FRelColors.MinHColor:=clBlue;
  FRelColors.MaxHColor:=clRed;    


  FRelArray:=T2DMatrix.Create; ;//array of array of smallint;
      
  FGradMatrix := T2DMatrix.Create; ;//array of array of smallint;  // ������� �������� �������� �����
  FLightMatrix:= T2DMatrix.Create; ;//array of array of smallint;  // ������� ������������ �����..
  
end;

destructor T3D_Rel_to_Bmp.Destroy;
begin
  inherited;

  FreeAndNil(FRelArray);
                   
  FreeAndNil(FGradMatrix);
  FreeAndNil(FLightMatrix);
  
end;
                 



//-------------------------------------------------------------------
procedure T3D_Rel_to_Bmp.CalcHeight;
//-------------------------------------------------------------------
var iRow, iCol : integer;
  b: Boolean;
//    rRel : Trel_Record;
    fKoof : Double;
  k: integer;

  iRel_H: SmallInt;
  
begin
//  FShadowWasDone := False;

//  Assert(FRelMatrix.GroundMaxH >= FRelMatrix.GroundMinH );

//  Assert(FRelMatrix.GroundMaxH>0, 'FRelMatrix.GroundMaxH>0');

  DoProgressMsg ('������ �����...');

  for iRow := 0 to FRelMatrix.RowCount - 1 do
    if not Terminated then
  begin
    if iRow mod 200 = 0 then 
      DoProgress2 (iRow, FRelMatrix.RowCount);


    for iCol := 0 to FRelMatrix.ColCount - 1 do
    begin
//      if iCol=0 then
 //       DoProgress (iRow, FRelMatrix.RowCount);

//      rRel := FRelMatrix[iRow, iCol];

      iRel_H:=GetItem (iRow, iCol);

//      b:= GetItemByRowCol_ (iRow, iCol, iRel_H);
 //     if not b then
  //      Continue;

    //  if ( rRel.Loc_Code = LO_SEA ) then

  {
      if ( rRel.Rel_H <= LO_SEA ) then
          fKoof := SeaKoof
  }

     if iRel_H = EMPTY_HEIGHT then // ���� �� ������ ������, �� �������, ��� ��� ������
        fKoof := 1 / (FRelMatrix.GroundMaxH - FRelMatrix.GroundMinH + 1 ) // �� ����������� ������
      else

//      if ( rRel.Clutter_Code = DEF_CLUTTER_WATER ) then
//        fKoof := WATER_COEF
//      else

      
        fKoof := (iRel_H - FRelMatrix.GroundMinH + 1 ) /
                 (FRelMatrix.GroundMaxH - FRelMatrix.GroundMinH + 1 );

    //  FGradMatrix[iRow, iCol] := Round( GRAD_MATR_MULTIPLE * fKoof );
      k:=Round( GRAD_MATR_MULTIPLE * fKoof );

    //  if k>2000 then
     //   k:=2000;



      if k>High(Smallint) then
      begin
        k:=High(Smallint);
  //      ShowMessage('k>255*256');
      end;

      try
        FGradMatrix[iRow, iCol] := Smallint(k);//Round( GRAD_MATR_MULTIPLE * fKoof );
      except      

      end;

    end;

//      if iCol=0 then

  end;
end;



//-------------------------------------------------------------------
procedure T3D_Rel_to_Bmp.CalcShadow;
//-------------------------------------------------------------------
var
  b: Boolean;
  iRow, iCol : integer;
  eCosAngle : double; // ������� ���� ����� ���������...
  eLightVect_X, eLightVect_Y, eLightVect_Z : double;
  eLightVect_Len : Double;
  eNorma_A, eNorma_B, eNorma_C, eNorma_Len : double;

  k: integer;
 

  recLightSrcInfo : TLightSrcInfoRec;

begin

  recLightSrcInfo := FLightInfoRec;
  recLightSrcInfo.LCourceAngle    := recLightSrcInfo.LCourceAngle mod 360;
  recLightSrcInfo.LIncidenceAngle := recLightSrcInfo.LIncidenceAngle mod 360; // ������ ���� � ���������

  eLightVect_X := Cos( PI * recLightSrcInfo.LCourceAngle / 180 );
  eLightVect_Y := Sin( PI * recLightSrcInfo.LCourceAngle / 180 );
  eLightVect_Z := 1 / Cos( PI * recLightSrcInfo.LIncidenceAngle / 180 );
  eLightVect_Len := Sqrt( Sqr( eLightVect_X ) + Sqr( eLightVect_Y ) + Sqr( eLightVect_Z ));


  DoProgressMsg ('������ �����...');


  with FRelMatrix do
    for iRow := 1 to RowCount - 2 do
      if not Terminated then
    begin
      if iRow mod 200 = 0 then 
        DoProgress2 (iRow, FRelMatrix.RowCount);

    
//    DoProgress (iRow, FRelMatrix.RowCount);

      for iCol := 1 to ColCount - 2 do
      begin
      //  if iCol=1 then
        //  DoProgress (iRow, FRelMatrix.RowCount-1);

// ---------------------------------------------------------------

        eNorma_A := (GetItem(iRow+1, iCol-1) + 2 * GetItem(iRow+1, iCol) + GetItem(iRow+1, iCol+1) ) -
                    (GetItem(iRow-1, iCol-1) + 2 * GetItem(iRow-1, iCol) + GetItem(iRow-1, iCol+1) );

        eNorma_B := (GetItem(iRow-1, iCol+1) + 2 * GetItem(iRow, iCol+1) + GetItem(iRow+1, iCol+1)  ) -
                    (GetItem(iRow-1, iCol-1) + 2 * GetItem(iRow, iCol-1) + GetItem(iRow+1, iCol-1)  );

// ---------------------------------------------------------------
                    
        eNorma_A := eNorma_A / Sqrt( Sqr( eNorma_A ) + Sqr( eNorma_B ) + 1 );
        eNorma_B := eNorma_B / Sqrt( Sqr( eNorma_A ) + Sqr( eNorma_B ) + 1 );
        eNorma_C := 1;

        eNorma_Len := Sqrt( Sqr( eNorma_A ) + Sqr( eNorma_B ) + Sqr( eNorma_C ));

        if ( eNorma_Len * eLightVect_Len ) <> 0 then
        begin
          eCosAngle := ( eNorma_A * eLightVect_X + eNorma_B * eLightVect_Y + eNorma_C * eLightVect_Z ) /
                       ( eNorma_Len * eLightVect_Len );

       //   FLightMatrix[iRow, iCol] := Round( 127 * eCosAngle ) + 128;

            k:=Round( 127 * eCosAngle ) + 128;

            if k>High(Smallint) then
            begin
              k:=High(Smallint);
              ShowMessage('k>255*256');
              
              raise Exception.Create('k>255*256');
            end;


//          FLightMatrix[iRow, iCol] := Smallint(k); //Round( 127 * eCosAngle ) + 128;

          FLightMatrix[iRow, iCol] := Smallint(k); //Round( 127 * eCosAngle ) + 128;

//          Assert(FLightMatrix[iRow, iCol] = k);

        end else
        begin
        //  FLightMatrix[iRow, iCol] := 0;
          FLightMatrix[iRow, iCol] := 0;
//          FLightMatrix[iRow, iCol] := 0;

        end;
      end;

//      DoProgress (iRow, FRelMatrix.RowCount-1);
   end;
//  FShadowWasDone := True;


end;


//-------------------------------------------------------------------
procedure T3D_Rel_to_Bmp.Draw;
//-------------------------------------------------------------------

var
  b: Boolean;
  iRow, iCol : integer;
  iColor : integer;
  k: Integer;
  oBmp: TBmpFile;

 // rRel : Trel_Record;

   iRel: smallint;

//  rHDR: TGDAL_HDR;

//  oEnvi: TFileStream;
   
begin
  DoProgressMsg ('���������� � ����...');

  oBmp:=TBmpFile.Create_Width_Height(FBmpFileName, FRelMatrix.ColCount, FRelMatrix.RowCount);

//  oEnvi:=TFileStream.Create (ChangeFileExt(FBmpFileName,'.envi'));


  for iRow := 0 to FRelMatrix.RowCount - 1 do
    if not Terminated then
  begin
    if iRow mod 100 = 0 then 
       DoProgress2 (iRow, FRelMatrix.RowCount);

    for iCol := 0 to FRelMatrix.ColCount - 1 do
    begin
//      if iCol=1 then
 //       DoProgress (iRow, FRelMatrix.RowCount-1);

      iRel:= GetItem(iRow, iCol);
   //   if not b then
    //    Continue;
        

//      if FRelMatrix[iRow, iCol].Rel_H = EMPTY_HEIGHT then

      if iRel = EMPTY_HEIGHT then
        iColor:=clBlack  
      else

//      if FRelMatrix[iRow, iCol].Clutter_Code = DEF_CLUTTER_WATER then
  //    if rRel.Clutter_Code = DEF_CLUTTER_WATER then
//        iColor:=clBlue
  //    else //ColorSchema.AquaColor  else

        begin
        // -------------------------

(*           if FLightMatrix_[iRow, iCol]<>0 then
             k:=k;
*)



//         if Params.IsGrayScale then
//           iColor := _RGBToColor( FLightMatrix[iRow, iCol], FLightMatrix[iRow, iCol], FLightMatrix[iRow, iCol] )
  //       else
          iColor := _FindOptimalLightColor(FGradMatrix [iRow, iCol] / GRAD_MATR_MULTIPLE,
                                           FLightMatrix[iRow, iCol] / GRAD_MATR_MULTIPLE,
                                           FRelLightColors )    ;



  //         iColor := _RGBToColor( FLightMatrix[iRow, iCol], FLightMatrix[iRow, iCol], FLightMatrix[iRow, iCol] )

                                            
        end;


      oBmp.Write(iColor);

  //    oEnvi.WriteBuffer(iColor, SizeOf(iColor));
      
//      if iCol = FRelMatrix.ColCount - 1 then
    end;

    oBmp.Writeln;
  end;

  FreeAndNil(oBmp);

//  FreeAndNil(oEnvi);

//
//  rHDR.ColCount:=FRelMatrix.ColCount;
//  rHDR.RowCount:=FRelMatrix.RowCount;
//  rHDR.Lon_min :=FRelMatrix.XYBounds.TopLeft.Y;
//  rHDR.Lat_max :=FRelMatrix.XYBounds.TopLeft.X;
//  rHDR.Zone    :=FRelMatrix.ZoneNum_GK + 30;
//  rHDR.CellSize:=FRelMatrix.StepX;
//                                
////  rHDR. datatype:=4;
//
//  rHDR.datatype:=4;
//
//
//  rHDR.SaveToFile(ChangeFileExt(FBmpFileName,'.hdr'));
  
end;


//-------------------------------------------------------------------
procedure T3D_Rel_to_Bmp.CreateBitmap;
//-------------------------------------------------------------------
var
  i: Integer;
  sImgFileName: string;
  sJpgFileName: string;
//  sJpgFileName: string;
  sPNGFileName: string;
  sTifFileName: string;
//  sPNGFileName: string;

//  sTemp_Grad: string;
//  sTemp_Light: string;

  tmpRGB : TRGBColor;
  tmpHLS : THLSRecord;
//  iMinH,iMaxH: integer;
begin
//  sTemp_Grad :=ChangeFileExt(Params.TabFileName, '_Grad.tmp');

//  FGradMatrix:=TFileMatrix.Create( sTemp_Grad,
//             FRelMatrix.RowCount, FRelMatrix.ColCount, SizeOf(Smallint));

(*  FGradMatrix_[0,0] := 111;
  i:=FGradMatrix_[0,0];

*)

//  sTemp_Light:=ChangeFileExt(Params.TabFileName, '_Light.tmp');

//  FLightMatrix:=TFileMatrix.Create(
//             sTemp_Light,
//             FRelMatrix.RowCount, FRelMatrix.ColCount, SizeOf(Smallint));



  tmpRGB := _FromColor( FRelColors.MinHColor );
  tmpHLS := _RGBtoHLS( tmpRGB );
  with FRelLightColors do
  begin
      HMin := tmpHLS.H;
      SMin := 120;
      LMin := 50;
  end;

  tmpRGB := _FromColor( FRelColors.MaxHColor );
  tmpHLS := _RGBtoHLS( tmpRGB );
  with FRelLightColors do
  begin
      HMax := tmpHLS.H;
      SMax := 220;
      LMax := 130;
  end;


(*  if FRelMatrix.GroundMaxH=0 then
//  CalcMinMaxHeights(var aMinH,aMaxH: integer);
    FRelMatrix.GetMinMaxHeights(FRelMatrix.GroundMinH, FRelMatrix.GroundMaxH);

*)

  DoProgress (1, 3);
  CalcHeight;

  DoProgress (2, 3);
  CalcShadow;


  FBmpFileName:= ChangeFileExt(Params.TabFileName, '.bmp');
//  sPNGFileName:= ChangeFileExt(Params.TabFileName, '.png');
//  sJpgFileName:= ChangeFileExt(Params.TabFileName, '.jpg');
  sTifFileName:= ChangeFileExt(Params.TabFileName, '.tif');
  
//  FDrawCanvas.SaveToFile (Params.ImgFileName);

  DoProgress (3, 3);

  Draw;


//  img_BMP_to_PNG(FBmpFileName, sPNGFileName, true, clBlack);


//  Bmp2Jpeg (FBmpFileName, sJpgFileName);

  Bmp_to_TIF (FBmpFileName, sTifFileName);
  
 // BitmapFileToPNG (FBmpFileName, sPngFileName);
  
//  Bmp2PNG  (FBmpFileName, sPNGFileName); //(const aBmpFileName, aPNGFileName: string);
    

//  DeleteFile (sTemp_Grad);
//  DeleteFile (sTemp_Light);

  SysUtils.DeleteFile (FBmpFileName);


  FRelMatrix.SaveHeaderToMIF (Params.TabFileName, sTifFileName);
//  FRelMatrix.SaveHeaderToMIF (Params.TabFileName, sJpgFileName);

end;
 
 

//--------------------------------------------
procedure T3D_Rel_to_Bmp.LoadToArray;
//--------------------------------------------
var
  I: Integer;

//  arrH: array of array of Smallint;
  c: Integer;
  k: Integer;
  r: Integer;

begin
  DoProgressMsg ('�������� ������� �����...');


  FNullValue:=FRelMatrix.NullValue;

  
    

  k:=FRelMatrix.RowCount * FRelMatrix.ColCount * SizeOf(smallint);

  FRelArray.SetSize   (FRelMatrix.RowCount, FRelMatrix.ColCount, ChangeFileExt(Params.TabFileName,'_rel.bin') );
  FGradMatrix.SetSize (FRelMatrix.RowCount, FRelMatrix.ColCount, ChangeFileExt(Params.TabFileName,'_grad.bin') );
  FLightMatrix.SetSize(FRelMatrix.RowCount, FRelMatrix.ColCount, ChangeFileExt(Params.TabFileName,'_light.bin') );
  
//  SetLength(FRelArray,    FRelMatrix.RowCount, FRelMatrix.ColCount);  
//  SetLength(FGradMatrix,  FRelMatrix.RowCount, FRelMatrix.ColCount);
//  SetLength(FLightMatrix, FRelMatrix.RowCount, FRelMatrix.ColCount);


//   DoProgressMsg ('������ �����...');
//
//  for iRow := 0 to FRelMatrix.RowCount - 1 do
//    if not Terminated then
//  begin
//    if iRow mod 50 = 0 then 
//      DoProgress2 (iRow, FRelMatrix.RowCount);

  

  for r := 0 to FRelMatrix.RowCount - 1 do
  begin
    if r mod 200 = 0 then 
      DoProgress2 (r, FRelMatrix.RowCount);


//    SetLength(FRelArray[r], FRelMatrix.ColCount);
    
//    SetLength(FGradMatrix[r],  FRelMatrix.ColCount);
//    SetLength(FLightMatrix[r], FRelMatrix.ColCount);
  
//  SetLength(FRelArray, k);

    for c := 0 to FRelMatrix.ColCount - 1 do
//    begin
      FRelArray[r,c]:=FRelMatrix.Items[r,c].Rel_H;

  //  end;
  
  end;


end;


//--------------------------------------------
procedure T3D_Rel_to_Bmp.ExecuteProc;
//--------------------------------------------
begin
  FRelMatrix:= rel_CreateRelMatrixByFileName(Params.RelFileName);

  if not Assigned(FRelMatrix) then
    Exit;

  if FRelMatrix.OpenFile (Params.RelFileName) then
  begin
    LoadToArray();


    if FRelMatrix.GroundMaxH = 0 then
      FRelMatrix.CalcMinMaxHeights();


    CreateBitmap;

  end;

  FreeAndNil(FRelMatrix);

end;


function T3D_Rel_to_Bmp.GetItem(aRow,aCol: integer): smallint;
begin
  try
    Result := FRelArray[aRow,aCol];
    
  except
    Result :=FNullValue
  end;
   

end;



//-------------------------------------------------------------------
procedure T3D_Rel_to_Bmp.Bmp_to_TIF(aBmpFileName, aTifFileName: string);
//-------------------------------------------------------------------
var
  bool: Boolean;

  k: Integer;

  sFileName: string;


  oBAT: TStringList;
  oIni: TIniFile;
  s: string;

 // s: string;
//  sFile: string;

  sPath_bin: string;

//  sDir: string;
  sFile: string;
  sFile_bat: string;
  sPath: string;
 
    
begin

   
  oBAT:=TStringList.Create;

 
//set GDAL_DATA=D:\OSGeo4W\share\epsg_csv
//set GDAL_FILENAME_IS_UTF8=NO


  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile));

  // ---------------------------------------------------------------
  
  oIni:=TIniFile.Create(sFile);
  sPath:=oIni.ReadString('main','path','');
  FreeAndNil(oIni);
  
                
  // ---------------------------------------------------------------
  sPath:=ExtractFilePath(Application.ExeName) + IncludeTrailingBackslash(sPath);
  sPath_bin:=sPath + 'bin\';
  

  if not FileExists (sPath_bin+ 'gdal_translate.exe') then
  begin
    ShowMessage('check file: ' + sPath_bin+ 'gdal_translate.exe');
    Exit;
  end;


  
//  sPath_bin:='D:\OSGeo4W\bin\';

//////////////
//s:='set path='+ sPath_bin + ';'+ sDir;
//s:='set path='+ sPath_bin + ';'+ ExtractFilePath(Params.TabFileName_CLU);
//////////////


//sDir

//  oBAT.Add('cd '+ sDir);

  oBAT.Add('set path='+ sPath_bin ); //+ ';'+ sDir);
//  oBAT.Add('set path='+ sPath_bin + ';'+ ExtractFilePath(Params.TabFileName_CLU) );
 
  s:=Format('set GDAL_DATA=%s',[sPath])+ 'share\epsg_csv' ;      
  oBAT.Add(s);

//  oBAT.Add('set GDAL_DATA=D:\OSGeo4W\share\epsg_csv');

  oBAT.Add('set GDAL_FILENAME_IS_UTF8=NO');
  oBAT.Add('');
  
//  sFile    :=ChangeFileExt( ExtractFileName( Params.RelFileName), '');
//  sFile_TIF:=ChangeFileExt(Params.TabFileName_CLU, '');

  
//  sFile    :=ChangeFileExt(Params.RelFileName, '');
//  sFile_TIF:=ChangeFileExt(Params.TabFileName_CLU, '');


  
  //gdal_translate -a_srs EPSG:28408    nnovg_pl_50_2D.envi nnovg_pl_50_2D.tif
  s:=  Format('gdal_translate.exe   -co "COMPRESS=LZW"  "%s" "%s"',[aBmpFileName, aTifFileName]); // --_TIF]);
//  s:=  Format('gdal_translate.exe   -co "COMPRESS=Deflate"  "%s" "%s"',[aBmpFileName, aTifFileName]); // --_TIF]);
  oBAT.Add(s);


//  oBAT.Add(Format('copy /y  "%s_clu.tif"   "%s"', [sFile,  ChangeFileExt(Params.TabFileName_CLU, '.tif') ]));
  
 
  
//  oBAT.Load SaveToFile(ChangeFileExt(Params.RelFileName, '.bat'),myEncoding);
  

  sFile_bat:=  ChangeFileExt( aBmpFileName , '.bat');
    
  oBAT.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));
//  oBAT.SaveToFile(ChangeFileExt(Params.RelFileName, '.bat'), TEncoding.GetEncoding(866));
  
  FreeAndNil(oBAT);
  
 // SetCurrentDir(sDir);
  
  RunApp(sFile_bat,'');
  
    
end;




end.


 
