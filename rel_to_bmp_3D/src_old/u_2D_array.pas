unit u_2D_array;

interface
                    

type
  T2DMatrix = class
  private
 

    FItems: array of array of smallint;

    function GetItems(aRow,aCol: word): smallint;
    procedure SetItems(aRow,aCol: word; const Value: smallint);

  public
    RowCount, 
    ColCount: word;        
   
    procedure SetSize(aRowCount, aColCount: word; aFileName: string);

    property Items[aRowCount,aColCount: word]: smallint read GetItems write SetItems; default;
  end;


implementation

uses
  System.SysUtils, System.Classes;


// ---------------------------------------------------------------
function T2DMatrix.GetItems(aRow,aCol: word): smallint;
// ---------------------------------------------------------------
begin
  Result:= FItems[aRow,aCol];
  
end;

// ---------------------------------------------------------------
procedure T2DMatrix.SetItems(aRow,aCol: word; const Value: smallint);
// ---------------------------------------------------------------
begin        

  FItems[aRow,aCol]:=Value;
end;

// ---------------------------------------------------------------
procedure T2DMatrix.SetSize(aRowCount, aColCount: word; aFileName: string);
// ---------------------------------------------------------------
var
  oFileStream: TFileStream;
begin
  RowCount:=aRowCount;
  ColCount:=aColCount;


  try
    SetLength(FItems,  aRowCount, aColCount);
    
  except //on E: Exception do
  
  end;


  
end;



end.

