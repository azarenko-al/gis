unit dm_Main_app;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComObj,

  dm_Params,
//  u_rel_to_bmp,
  u_3D_rel_to_bmp
  ;

type

  TdmMainApp = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    FInFileName: string;

//    procedure DoOnGetClutterColor(aLoc_Code, aLoc_H: integer; var aColor: integer);
//    procedure DoOnGetReliefColor(aLoc_Code, aLoc_H: integer; var aColor: integer);
  end;

var
  dmMainApp: TdmMainApp;

//-------------------------------------------------------------------
implementation {$R *.DFM}
//-------------------------------------------------------------------

const
  TEST_FILE_NAME = 'c:\temp\rpls\rel_to_bmp.xml';


//--------------------------------------------------------------
procedure TdmMainApp.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
var
//  obj: TRel_to_bmp;
  obj1: T3D_Rel_to_Bmp;
begin
  FInFileName:=ParamStr(1);

//  ShowMessage (sFileName);

  if FInFileName='' then begin
    FInFileName:=TEST_FILE_NAME;
  end;

  if not FileExists(FInFileName) then begin
    ShowMessage ('���� �� ������: '+ FInFileName);

    Exit;
  end;

  dmParams.LoadFromXML (FInFileName);


{  obj:=TRel_to_bmp.Create;

  obj.OnGetReliefColor:=DoOnGetReliefColor;
  obj.OnGetClutterColor:=DoOnGetClutterColor;
  obj.RelFileName:= dmParams.RelFileName;
//  obj.Params.ImgFileName:=ChangeFileExt(dmParams.MifFileName, '.jpg');

  obj.ExecuteDlg ('');
  obj.Free;}


  obj1:=T3D_Rel_to_Bmp.Create;

 //// obj.OnGetReliefColor:=DoOnGetReliefColor;
 // obj.OnGetClutterColor:=DoOnGetClutterColor;
  obj1.Params.RelFileName:= dmParams.RelFileName;
  obj1.Params.ImgFileName:= dmParams.ImgFileName;
  obj1.Params.TaskType:= dmParams.TaskType;
//  obj1.Params.ImgFileName:= ChangeFileExt(dmParams.ImgFileName, '.bmp');

  obj1.ExecuteDlg ('');
  obj1.Free;


//  if Terminated then
//    ExitProcess (1);
end;

{procedure TdmMainApp.DoOnGetClutterColor(aLoc_Code, aLoc_H: integer; var aColor: integer);
begin
  aColor:=dmParams.GetClutterColor(aLoc_Code, aLoc_H);
end;

procedure TdmMainApp.DoOnGetReliefColor(aLoc_Code, aLoc_H: integer; var aColor: integer);
begin
  aColor:=dmParams.GetReliefColor(aLoc_Code, aLoc_H);
end;}


begin
  //CoInitialize(nil);

end.
