set path=%path%;C:\OSGeo4W64\bin\;
set GDAL_DATA=C:\OSGeo4W64\share\epsg_csv
set GDAL_FILENAME_IS_UTF8=YES

rem zone : 7
gdal_translate  -a_srs "+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=7500000 +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs"  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  7325599  6059729 7365599 6019729  "los_875536399.bmp" "los_875536399.tif" 
gdalinfo -json "los_875536399.tif" >  "los_875536399.json" 
gdalwarp -t_srs EPSG:3395 "los_875536399.tif"  "los_875536399.3395.tif" 
gdalinfo -json "los_875536399.3395.tif" >  "los_875536399.3395.json" 
