unit u_main;

interface
uses
  u_run,

  u_GDAL_bat,
  u_GDAL_classes,

  u_gdal_bmp_to_tab_types,

  IniFiles, System.SysUtils
  ;

  procedure Main();

implementation





//-----------------------------------------------------------------
procedure Main();
//-----------------------------------------------------------------
var
  oBAT: TGDAL_Bat;
  oIni: TIniFile;
  sFile_bat: string;
  sParam: string;
  sProj4: string;

  rec: TGDAL_XYRect;
  sParam_file: string;
  sFile_no_ext: string;

  rGDAL_json: TGDAL_json_rec;
  s: string;
  sDir: string;


begin
  if ParamCount>0 then
    sParam_file:=ParamStr(1);

//  sParam_file:='E:\1\los_875536399.bmp.ini';

  TGDAL_XYRect_LoadFromFile (sParam_file, rec);



  sFile_no_ext:=ChangeFileExt(rec.BmpFileName, '');

  //-------------------------------------------------
  sDir:=ExtractFilePath (sParam_file);
  sFile_bat:=  ChangeFileExt(rec.BmpFileName,'.bat' );

  ForceDirectories ( ExtractFilePath(sFile_bat ));


//  rec.BmpFileName:=sDir + rec.BmpFileName;
//  rec.TabFileName:=sDir + rec.TabFileName;

  //-------------------------------------------------


  oBAT:=TGDAL_Bat.Create;
  oBAT.Init_Header;

  sProj4 := Format('+proj=tmerc +lat_0=0 +lon_0=%d +k=1 +x_0=%d +y_0=0 +ellps=krass +towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12 +units=m +no_defs',
             [ rec.Zone * 6 -3,  rec.Zone * 1000000 + 500000]);
//             [ 20 * 6 -3,  20 * 1000000 + 500000]);

  oBAT.Add('rem zone : ' + IntToStr( rec.Zone));

//  s:= Format('gdal_translate  -projwin_srs "%s"  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  %d  %d %d %d  "%s" "%s" ',
  sParam:= Format('gdal_translate  -a_srs "%s"  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  %d  %d %d %d  "%s" "%s" ',
//  sParam:= Format('gdal_translate  -a_srs EPSG:%d  -of GTiff  -co "INTERLEAVE=PIXEL" -a_ullr  %d  %d %d %d  "%s" "%s" ',

                           [  sProj4,

                          //    28400 + aParams.Zone,

                              Round(rec.Lon_min),
                              Round(rec.Lat_max),

                              Round(rec.Lon_max),
                              Round(rec.Lat_min),

                              rec.BmpFileName,
                              ChangeFileExt(rec.BmpFileName,'.tif' )

//                              ExtractFileName( rec.BmpFileName),
//                              ExtractFileName( ChangeFileExt(rec.BmpFileName,'.tif') )
                             ]);


//  codeSite.SendMsg(sParam);

  oBAT.Add(sParam);

  s:=  Format('gdalinfo -json "%s.tif" >  "%s.json" ',[sFile_no_ext, sFile_no_ext]);
//  s:=  Format('gdalinfo -json "%s.3395.tif" >  "%s.3395.json" ',[sFile_no_ext,sFile_no_ext]);
  oBAT.Add(s);


  //gdal_translate -a_srs EPSG:28408    nnovg_pl_50_2D.envi nnovg_pl_50_2D.tif
//  s:= sPath_bin+ Format('gdal_translate.exe -a_srs EPSG:%d  "%s.envi" "%s.tif"',[28400+ FRelMatrix.ZoneNum,sFile,sFile]);
//  oBAT.Add(s);

  s:= Format('gdalwarp -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ', [sFile_no_ext,  sFile_no_ext ]);
//  s:= Format('gdalwarp -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ', [sFile_no_ext, sFile_no_ext]);


//  s:= '"' + sPath_bin + Format('gdalwarp" -t_srs EPSG:3395 "%s.tif"  "%s.3395.tif" ',[sFile_no_ext,sFile_no_ext]);
  oBAT.Add(s);



  s:=  Format('gdalinfo -json "%s.3395.tif" >  "%s.3395.json" ',[sFile_no_ext, sFile_no_ext]);
//  s:=  Format('gdalinfo -json "%s.3395.tif" >  "%s.3395.json" ',[sFile_no_ext,sFile_no_ext]);
  oBAT.Add(s);




  //TEncoding.GetEncoding(CP_866));

//  oBAT.SaveToFile(sFile_bat, TEncoding.UTF8);
  oBAT.SaveToFile(sFile_bat, TEncoding.GetEncoding(866));

  FreeAndNil(oBAT);

  //-----------------------------------------------------------------
//  oBAT.SaveToFile(sFile_bat);

  SetCurrentDir(sDir);
  RunApp (sFile_bat,'');

//  s:=ChangeFileExt(aBmpFileName, '.3395.json');
//  Assert (FileExists (s));


  // ---------------------------------------------------------------
  rGDAL_json.LoadFromFile(ChangeFileExt(rec.BmpFileName, '.3395.json'));
  rGDAL_json.SaveToFile_TAB_3395(ChangeFileExt(rec.BmpFileName, '.tab'),  ChangeFileExt(rec.BmpFileName, '.3395.tif'));



end;

end.
