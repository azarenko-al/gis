program gdal_bmp_to_tab;

uses
  Vcl.Forms,
  u_main in 'u_main.pas',
  dm_Main in 'dm_Main.pas' {DataModule4: TDataModule},
  u_gdal_bmp_to_tab_types in 'u_gdal_bmp_to_tab_types.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDataModule4, DataModule4);
  Application.Run;
end.
