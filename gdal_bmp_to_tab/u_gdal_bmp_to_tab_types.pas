unit u_gdal_bmp_to_tab_types;

interface
uses
  IniFiles, SysUtils;

type

   TGDAL_XYRect = record
                        Lat_min: Double;
                        Lat_max: double;

                        Lon_min: Double;
                        Lon_max: double;

                        Zone: byte;

                        BmpFileName: string;
                        TabFileName: string;

                     end;

procedure TGDAL_XYRect_LoadFromFile(aFileName: String; var aParams: TGDAL_XYRect);
procedure TGDAL_XYRect_SaveToFile(aFileName: String; aParams: TGDAL_XYRect);

implementation



// ---------------------------------------------------------------
procedure TGDAL_XYRect_LoadFromFile(aFileName: String; var aParams: TGDAL_XYRect);
// ---------------------------------------------------------------

var
  oIni: TIniFile;
begin
//  aFileName:=ChangeFileExt(aFileName, '.ini');

  Assert (FileExists(aFileName));

  oIni:=TIniFile.Create (aFileName);

  aParams.Lat_min:=oIni.ReadFloat('main', 'Lat_min',0);
  aParams.Lat_max:=oIni.ReadFloat('main', 'Lat_max',0);

  aParams.Lon_min:=oIni.ReadFloat('main', 'Lon_min',0);
  aParams.Lon_max:=oIni.ReadFloat('main', 'Lon_max',0);

  aParams.Zone:=oIni.ReadInteger('main', 'Zone', 0);

  aParams.BmpFileName:=oIni.ReadString('main', 'BmpFileName', '');
  aParams.TabFileName:=oIni.ReadString('main', 'TabFileName', '');


  FreeAndNil(oIni);

end;


// ---------------------------------------------------------------
procedure TGDAL_XYRect_SaveToFile(aFileName: String; aParams: TGDAL_XYRect);
// ---------------------------------------------------------------

var
  oIni: TIniFile;
begin
 // aFileName:=ChangeFileExt(aFileName, '.ini');


  oIni:=TIniFile.Create (aFileName);

  oIni.WriteFloat('main', 'Lat_min', aParams.Lat_min);
  oIni.WriteFloat('main', 'Lat_max', aParams.Lat_max);

  oIni.WriteFloat('main', 'Lon_min', aParams.Lon_min);
  oIni.WriteFloat('main', 'Lon_max', aParams.Lon_max);

  oIni.WriteInteger('main', 'Zone', aParams.Zone);

  oIni.WriteString('main', 'BmpFileName', aParams.BmpFileName);
  oIni.WriteString('main', 'TabFileName', aParams.TabFileName);



  FreeAndNil(oIni);

end;



end.
