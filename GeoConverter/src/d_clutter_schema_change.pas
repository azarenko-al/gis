unit d_clutter_schema_change;

interface

uses
  System.SysUtils, System.Classes, 
  Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.ExtCtrls, RxPlacemnt,
  Data.Win.ADODB;

type
  Tdlg_clutter_schema_change = class(TForm)
    FormStorage1: TFormStorage;
    LabeledEdit_Src: TLabeledEdit;
    LabeledEdit_Dest: TLabeledEdit;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
  private
   
  public
    class function ExecDlg(aADOConnection1: TADOConnection): Boolean;

  end;

  

implementation

{$R *.dfm}

procedure Tdlg_clutter_schema_change.FormCreate(Sender: TObject);
begin
  caption:='�������� ����� � �������';
end;

// ---------------------------------------------------------------
class function Tdlg_clutter_schema_change.ExecDlg(aADOConnection1:
    TADOConnection): Boolean;
// ---------------------------------------------------------------
var
  s: string;
begin
   with Tdlg_clutter_schema_change.Create(Application) do
   begin
     Result := ShowModal=mrOk ;

     if Result then
     begin
       s:=Format('update task set Clutter_schema=''%s'' where Clutter_schema=''%s''',
            [LabeledEdit_Dest.Text,  LabeledEdit_Src.Text ]);
     
       aADOConnection1.Execute(s);         
     end;                               


     Free;
   end;

end;


end.
