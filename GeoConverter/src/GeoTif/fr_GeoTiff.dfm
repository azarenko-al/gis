inherited frame_GeoTiff: Tframe_GeoTiff
  Left = 1346
  Top = 362
  Caption = 'frame_GeoTiff'
  ClientHeight = 407
  ClientWidth = 534
  OnDestroy = FormDestroy
  ExplicitLeft = 1346
  ExplicitTop = 362
  ExplicitWidth = 542
  ExplicitHeight = 435
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 391
    Width = 534
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 391
    ExplicitWidth = 534
  end
  inline frm_RLF1: Tframe_RLF_ [1]
    Left = 0
    Top = 313
    Width = 534
    Height = 78
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 272
    ExplicitWidth = 534
    ExplicitHeight = 78
    DesignSize = (
      534
      78)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 534
      ExplicitWidth = 534
    end
    inherited ed_RLF: TFilenameEdit
      Width = 515
      ExplicitWidth = 515
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 534
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      534
      65)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 35
      Height = 13
      Caption = 'GeoTiff'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 518
      Height = 21
      Filter = 'TIF (*.tif)|*.tif'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = '"S:\! projects\ASTGTM2_N59E029_dem.tif"'
    end
  end
  inherited ActionList_custom: TActionList
    Left = 48
    Top = 72
  end
end
