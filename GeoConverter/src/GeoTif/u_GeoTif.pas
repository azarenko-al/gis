unit u_GeoTif;

interface

uses  Classes, Sysutils,

  u_geo,

  DSpGlobals,
  DSpGeoTIFF;


type
  TGeoTif = class
  private
    FFileName: string ;

    FArr: TDSpExtentArray;

    FInfo: TDSpRasterInfo;

    buff_: array of
           array of SmallInt;

  public
    BLRect_WGS: TBLRect;

    WGS_zone: SmallInt;

    NullValue : SmallInt;

    constructor Create;

    function FindByBL(aB, aL: double; var aValue: SmallInt): Boolean;

    procedure Open(aFileName: string);

    procedure SaveToBin;
  end;



implementation

uses
  IniFiles;


constructor TGeoTif.Create;
begin
  inherited;
  NullValue:=-9999;
end;

//---------------------------------------------------------------
function TGeoTif.FindByBL(aB, aL: double; var aValue: SmallInt): Boolean;
//--------------------------------------------------------------
var
  b: boolean;
  eStep: Double;
  iCol: Integer;
  iRow: Integer;
begin
  Result:=False;

  eStep:=FInfo.PixelSize;

  if (BLRect_WGS.BottomRight.B < aB) and (aB < BLRect_WGS.TopLeft.B) and
     (BLRect_WGS.TopLeft.L     < aL) and (aL < BLRect_WGS.BottomRight.L) then
  begin
    iRow := Integer(Round((BLRect_WGS.TopLeft.B - aB) / eStep));
    iCol := Integer(Round((aL - BLRect_WGS.TopLeft.L) / eStep));

    Result:=(iRow>=0) and (iCol>=0) and
            (iRow<=FInfo.Rows-1) and (iCol<=FInfo.Columns-1);

    if Result then
    begin
      aValue := buff_[iRow][iCol];

   //   if aValue>0 then
    //    ShowMessage('');


    end;

//      Result:=true;
  end;
// end;

end;

// ---------------------------------------------------------------
procedure TGeoTif.Open(aFileName: string);
// ---------------------------------------------------------------

var
  i: Integer;

  oDSpGeoTIFF: TDSpGeoTIFF;

  iRow: Integer;
  oSList: TStringList;
  iCol: Integer;
  s: string;


  eMin : Double;
  eNorth: Double;
  eWest: Double;
  eEast: Double;
  eSouth: Double;


begin
  Assert(FileExists(aFileName));


  FFileName := aFileName;

  oDSpGeoTIFF:=TDSpGeoTIFF.Create;
  oDSpGeoTIFF.Open(aFileName, true);

  oDSpGeoTIFF.RasterInfo(0,FInfo);


  FArr:=oDSpGeoTIFF.Extent(0);


  SetLength(buff_, FInfo.Rows);
  for iRow := 0 to FInfo.Rows - 1 do
    SetLength(buff_[iRow], FInfo.Columns);


  for iRow := 0 to FInfo.Rows - 1 do
    oDSpGeoTIFF.ReadData(0,0,  iRow,0, 1,FInfo.Columns, buff_[iRow]);


(*
  oSList := TStringList.Create;


  for iRow := 0 to FInfo.Rows - 1 do
  begin
//    oDSpGeoTIFF.ReadData(0,0,  iRow,0, 1,FInfo.Columns, buff_[iRow]);

    s:='';
    for iCol := 0 to FInfo.Columns - 1 do
      s:=s+ Format('%d ',[buff_[iRow,iCol]]);

   oSList.Add(s);

  end;


  oSList.SaveToFile( ChangeFileExt(Params.FileName, '.txt'));


  FreeAndNil(oSList);
*)




  eNorth := FArr[dspNorth];
  eSouth := FArr[dspSouth];
  eWest  := FArr[dspWest];
  eEast  := FArr[dspEast];


  BLRect_WGS.TopLeft.B     := eNorth;
  BLRect_WGS.BottomRight.B := eSouth;

  BLRect_WGS.TopLeft.L     := eWest;
  BLRect_WGS.BottomRight.L := eEast;


  FreeAndNil(oDSpGeoTIFF);



  WGS_zone := geo_Get6ZoneL(BLRect_WGS.TopLeft.L);


 // FDEM.OpenFile(Params.DEM_FileName);


end;

// ---------------------------------------------------------------
procedure TGeoTif.SaveToBin;
// ---------------------------------------------------------------
var
  c: Integer;
  e: Double;
  I: Integer;
  oFileStream: TFileStream;
  oIni: TMemIniFile;
  r: Integer;
  s: string;

  sFile_bin: string;
  sFile_ini: string;

begin

  sFile_ini:=ChangeFileExt(FFileName, '.temp.ini');
  sFile_bin:=ChangeFileExt(FFileName, '.temp.bin');


  if FileExists(sFile_ini) and FileExists(sFile_bin) then
    Exit;


  oIni:=TMemIniFile.Create ( sFile_ini);


  e:=FInfo.PixelSize * FInfo.Columns;
  e:=FInfo.PixelSize * FInfo.Rows;

  s:= FloatToStr(FInfo.PixelSize);


  oIni.WriteFloat('main','step',FInfo.PixelSize);
  oIni.WriteFloat('main','bytes_per_cell', SizeOf(SmallInt));
  oIni.WriteInteger('main','NullValue',NullValue);

  oIni.WriteInteger('main','Rows',FInfo.Rows);
  oIni.WriteInteger('main','Columns',FInfo.Columns);

  oIni.WriteFloat('bounds_WGS','max_lat',BLRect_WGS.BottomRight.B);
  oIni.WriteFloat('bounds_WGS','min_lat',BLRect_WGS.TopLeft.B);

  oIni.WriteFloat('bounds_WGS','min_lon',BLRect_WGS.TopLeft.L);
  oIni.WriteFloat('bounds_WGS','max_lon',BLRect_WGS.BottomRight.L);

(*
       := eNorth;
  BLRect_WGS.BottomRight.B := eSouth;

  BLRect_WGS.TopLeft.L     := eWest;
  BLRect_WGS.BottomRight.L := eEast;
*)

  oIni.UpdateFile;
  FreeAndNil(oIni);

  // ---------------------------------------------------------------
  oFileStream:=TFileStream.Create ( sFile_bin, fmCreate	);
  oFileStream.Size := FInfo.Rows *  FInfo.Columns * 2;


  for r := 0 to FInfo.Rows - 1 do
    for c := 0 to FInfo.Columns - 1 do
      oFileStream.WriteBuffer(buff_[r][c], SizeOf(Smallint));

  FreeAndNil(oFileStream);



  (*  begin

    end;

  SetLength(buff_, FInfo.Rows);
  for iRow := 0 to FInfo.Rows - 1 do
    SetLength(buff_[iRow], FInfo.Columns);
*)


(*
  SetLength(buff_, FInfo.Rows);
  for iRow := 0 to FInfo.Rows - 1 do
    SetLength(buff_[iRow], FInfo.Columns);
*)
end;



end.





