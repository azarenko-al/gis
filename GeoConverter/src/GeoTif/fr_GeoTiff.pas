unit fr_GeoTiff;

interface

uses
  Classes, Controls, Forms, StdCtrls, rxToolEdit,
  ExtCtrls, SysUtils, IniFiles,

  u_const,

//  u_GeoTif_to_RLF,
                

  ComCtrls,

  frame_RLF,
  fr_Custom_Form,

  Mask, ActnList, System.Actions

  ;

type
  Tframe_GeoTiff = class(Tfrm_Custom_Form)
    frm_RLF1: Tframe_RLF_;
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
//    FGeoTif_to_RLF: TGeoTif_to_RLF;
    FTerminated : Boolean;


    FIniFileName : string;

  private
    procedure LoadFromIniFile(aFileName: String);
    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run; override;
  public
  end;

var
  frame_GeoTiff: Tframe_GeoTiff;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'geo_tif';




procedure Tframe_GeoTiff.FormCreate(Sender: TObject);
begin
  inherited;

//  FGeoTif_to_RLF := TGeoTif_to_RLF.Create();
//  FGeoTif_to_RLF.OnProgress :=DoOnProgress;

  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);

  frm_RLF1.Init;
           

  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;

end;


procedure Tframe_GeoTiff.FormDestroy(Sender: TObject);
begin
  SaveToIniFile(FIniFileName);

//  FreeAndNil(FGeoTif_to_RLF);
end;


// ---------------------------------------------------------------
procedure Tframe_GeoTiff.Run;
// ---------------------------------------------------------------
begin
  FTerminated := False;

  //btn_Run.Action := act_Stop;

(*  case cb_Proj.ItemIndex of
    0: FAsc_to_RLF.Projection :=ptUTM;
   // 1: FAsc_to_RLF.Projection :=ptGauss;
  end;
*)

 // FAsc_to_RLF.Params.UTM_zone:= AsInteger(ed_UTM_Zone.Text);

//  FGeoTif_to_RLF.Params.FileName := ed_DEM.fileName;
//
//
//  FGeoTif_to_RLF.Params.Rlf_FileName  := frm_RLF1.ed_RlF.FileName;
//  FGeoTif_to_RLF.Params.Rlf_Step      := frm_RLF1.GetStepM;
//
//  FGeoTif_to_RLF.Run;


//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;

// ---------------------------------------------------------------
procedure Tframe_GeoTiff.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);


  with oIniFile do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

   // WriteString(DEF_SECTION, ed_UTM_Zone.Name, ed_UTM_Zone.Text);

(*
    WriteString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    WriteString(DEF_SECTION, ed_Clutter_Heights.Name, ed_Clutter_Heights.FileName);
    WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);
*)
   // WriteString(DEF_SECTION, ed_RLF.Name, ed_RLF.FileName);
  //  WriteString(DEF_SECTION, ed_Step.Name, ed_Step.text);


 //   WriteBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
  //  WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);
    

  //  Free;
  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
 // frm_Clu.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);


  FreeAndNil(oIniFile);

end;


// ---------------------------------------------------------------
procedure Tframe_GeoTiff.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do
  begin
    ed_DEM.FileName := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

  //  ed_UTM_Zone.Text := ReadString(DEF_SECTION, ed_UTM_Zone.Name, ed_UTM_Zone.Text);

(*    ed_Clutter_Classes.FileName := ReadString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    ed_Clutter_Heights.FileName := ReadString(DEF_SECTION, ed_Clutter_Heights.Name,ed_Clutter_Heights.FileName);
    ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);

    cb_Clutter_Classes.checked:= ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_Heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);
*)
  //  ed_RLF.FileName := ReadString(DEF_SECTION, ed_RLF.Name,ed_RLF.FileName);
  //  ed_Step.text    := ReadString(DEF_SECTION, ed_Step.Name,ed_Step.text);

  //xxx  Free;
  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
 // frm_Clu.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;

end.
