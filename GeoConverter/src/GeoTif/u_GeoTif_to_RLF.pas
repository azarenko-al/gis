unit u_GeoTif_to_RLF;

interface
uses Classes, Sysutils, 

//  u_GeoTif,

  u_files,

  u_geo_convert_new,

  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,

  u_CustomTask   ;

 // u_ASC_File,




type
  TGeoTif_to_RLF = class(TCustomTask)
  private
    FGeoTif: TGeoTif;  
  public

//    Projection: (ptGauss, ptUTM);
//    Projection: (ptUTM);

    Params: record
    //  UTM_zone : Integer;

      FileName : string;

      RLF_FileName : string;
      Rlf_Step : Integer;
    end;

    constructor Create;

    destructor Destroy; override;
    procedure OpenFiles;

    procedure Run;

  end;


implementation


constructor TGeoTif_to_RLF.Create;
begin
  inherited;

  FGeoTif := TGeoTif.Create();
end;


destructor TGeoTif_to_RLF.Destroy;
begin
  FreeAndNil(FGeoTif);
  
  inherited;
end;

// ---------------------------------------------------------------
procedure TGeoTif_to_RLF.OpenFiles;
// ---------------------------------------------------------------
begin
  Assert(FileExists(Params.FileName));

  FGeoTif.Open(Params.FileName);
  FGeoTif.SaveToBin();

end;



// ---------------------------------------------------------------
procedure TGeoTif_to_RLF.Run;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_GK: TXYRect;

  xy : TXYPoint;

  bl_WGS: TBLPoint;
  bl_CK42: TBLPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
  iGK_zone: integer;
  iColCount: integer;
  iRowCount: integer;
  iStep: integer;
  iRow: integer;
  iCol: integer;

  rlf_rec: Trel_Record;
 // iValue: integer;

 // iIntValue: Smallint;

  oRlfFileStream: TFileStream;
  s: string;

  s2: string;
  s1: string;

  eMin : Double;
  eNorth: Double;
  eWest: Double;
  eEast: Double;
  eSouth: Double;

  blPointsArr_wgs: TBLPointArrayF;

  blPointsArr_CK42: TBLPointArrayF;
  xyPointsArr_CK42: TXYPointArrayF;

  iValue: SmallInt;
  oMatrix: TrlfMatrix;

  iGroundMinH, iGroundMaxH : integer;

  //rXYRect_GK: TxyRect;

begin
  OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;


  iGroundMinH:=100000;
  iGroundMaxH:=0;


  geo_BLRectToBLPointsF(FGeoTif.BLRect_WGS, blPointsArr_wgs);

  iGK_zone := geo_Get6ZoneL(FGeoTif.BLRect_WGS.TopLeft.L);

  Assert(iGK_zone>0);


  blPointsArr_CK42.Count :=4;
  xyPointsArr_CK42.Count :=4;


  for i:=0 to blPointsArr_wgs.Count-1 do
  begin
    bl_WGS :=blPointsArr_wgs.Items[i];

  //  bl_CK42 := geo_WGS84_to_Pulkovo42(bl_WGS);

 //   blPointsArr_CK42.Items[i] := bl_CK42;

    xyPointsArr_CK42.Items[i] := geo_Pulkovo42_to_XY(bl_WGS, iGK_zone);

  end;


  rXYRect_GK := geo_RoundXYPointsToXYRect(xyPointsArr_CK42);


//  FDEM.OpenFile(FFileName_DEM);
  // ---------------------------------------------------------------


  if Terminated then
    Exit;


  if Params.RLF_Step>0 then
   iStep :=Params.RLF_Step
  else
   iStep := 50;// round(FDEM.Cellsize);


  //iStep :=round(FDEM.Cellsize);

  Assert(iStep>0, 'Value <=0');

  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);

  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=rXYRect_GK;

  ForceDirByFileName(Params.RLF_FileName);

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

 // xy := rXYRect_GK.TopLeft;

  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


 //     if Projection=ptUTM then

      bl_CK42 := geo_GK_XY_to_Pulkovo42_BL(xy, iGK_zone);
      bl_wgs  := geo_Pulkovo42_to_WGS84(bl_CK42);


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

//      if FindCellByXY(bl_wgs.B, bl_wgs.L, wValue) then
      if FGeoTif.FindByBL(bl_wgs.B, bl_wgs.L, iValue) then
        if iValue>0 then
      begin
      ///////////  Assert(iValue>-100);

        rlf_rec.Rel_H :=iValue;


        if iValue < iGroundMinH then
          iGroundMinH:=iValue;

        if iValue > iGroundMaxH then
          iGroundMaxH:=iValue;

      end;

     // Randomize;

     // rlf_rec.Rel_H :=Random(100);


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);



  oMatrix:=TrlfMatrix.Create;
  oMatrix.UpdateMinMaxH(Params.RLF_FileName, iGroundMinH, iGroundMaxH);
  FreeAndNil(oMatrix);



  if not Terminated then
    ExportToClutterBMP(Params.RLF_FileName);
  
end;



end.


