unit frame_RLF;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms, Dialogs,
  IniFiles,
  ExtCtrls, StdCtrls, rxToolEdit,

 //  MapXLib_TLB,


   
   
 //  u_MapX_lib,

  u_const, Mask

  ;

type
  Tframe_RLF_ = class(TFrame)
    lb_File: TLabel;
    ed_RLF: TFilenameEdit;
    ed_Step1: TEdit;
    lb_Step: TLabel;
    Bevel2: TBevel;
    procedure ed_RLFExit(Sender: TObject);
    procedure FrameClick(Sender: TObject);

  private
//    function GetBorderFileName: string;
    { Private declarations }
  public
    function GetStepM: Integer;
 //   function GetBorderPoints(var aBLPointArray: TBLPointArray): Boolean;

    procedure Init;

    //
    procedure LoadFromIniFile(aIniFile: TIniFile; aSection, aFileName: String);
    procedure SaveToIniFile(aIniFile: TIniFile; aSection, aFileName: String);

  end;

implementation
{$R *.dfm}


procedure Tframe_RLF_.ed_RLFExit(Sender: TObject);
begin
  ed_RLF.FileName:= ChangeFileExt(ed_RLF.FileName, '.rlf');



 // ShowMessage('ed_RLFExit');
end;

procedure Tframe_RLF_.FrameClick(Sender: TObject);
begin

end;

//
//
//procedure Tframe_RLF_.Button1Click(Sender: TObject);
//(*var
//  BLPointArray: TBLPointArray;*)
//begin
//
// // mapx_GetFirstFeaturePoints('D:\______\Boundaries.Tab', BLPointArray);
//
//end;

//procedure Tframe_RLF_.FrameClick(Sender: TObject);
//begin
//
//end;

//// ---------------------------------------------------------------
//function Tframe_RLF_.GetBorderFileName: string;
//// ---------------------------------------------------------------
//begin
//  if cb_UseBorder.Checked then
//    Result := ed_Border.FileName
//  else
//    Result := '';
//
//end;

//// ---------------------------------------------------------------
//function Tframe_RLF_.GetBorderPoints(var aBLPointArray: TBLPointArray): Boolean;
//// ---------------------------------------------------------------
//begin
//  Result := False;
//  SetLength(aBLPointArray,0);
//
//  if cb_UseBorder.Checked then
//    if FileExists(ed_Border.FileName) then
//       Result :=  mapx_GetFirstFeaturePoints(ed_Border.FileName, aBLPointArray);
//end;


function Tframe_RLF_.GetStepM: Integer;
begin
  result :=  StrToIntDef(Trim(ed_Step1.Text), 0);

end;


procedure Tframe_RLF_.Init;
begin
 ////// Height := 90;
  Height := 80;

  lb_File.Caption := STR_RLF;

  lb_Step.Caption := STR_Step;

end;

// ---------------------------------------------------------------
procedure Tframe_RLF_.LoadFromIniFile(aIniFile: TIniFile; aSection, aFileName:
    String);
// ---------------------------------------------------------------
begin
//  with TIniFile.Create(aFileName) do
  with aIniFile do
  begin
    ed_RLF.FileName := ReadString(aSection, ed_RLF.Name,ed_RLF.text);
    ed_Step1.text   := ReadString(aSection, ed_Step1.Name, ed_Step1.text);

//    cb_UseBorder.Checked := ReadBool(aSection, cb_UseBorder.Name, cb_UseBorder.Checked);
//    ed_Border.FileName   := ReadString(aSection, ed_Border.Name, ed_Border.text);

//    cb_3D.Checked       := ReadBool(aSection, cb_3D.Name, cb_3D.Checked);
//    cb_Clutters1.Checked := ReadBool(aSection, cb_Clutters1.Name, cb_Clutters1.Checked);
                           
//    if cb_UseBorder.Checked then
  //    if FileExists(ed_Border.FileName) then

   // Free;
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_RLF_.SaveToIniFile(aIniFile: TIniFile; aSection, aFileName:
    String);
// ---------------------------------------------------------------
begin
  with aIniFile do
  begin
    WriteString(aSection, ed_RLF.Name, ed_RLF.text);
    WriteString(aSection, ed_Step1.Name, ed_Step1.text);
            
//    WriteBool(aSection, cb_UseBorder.Name, cb_UseBorder.Checked);
//    WriteString(aSection, ed_Border.Name, ed_Border.text);

//    WriteBool(aSection, cb_3D.Name, cb_3D.Checked);
//    WriteBool(aSection, cb_Clutters1.Name, cb_Clutters1.Checked);
              
  //  Free;
  end;
end;

end.


{




function mapx_GetFirstFeaturePoints(aFileName: string; var aBLPointArray:
    TBLPointArray): Boolean;
var
  oMiFile: TmiMap;
  i: Integer;
  iCount: Integer;

  vSelection: CMapXSelection;
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;

  miObjectRec: TmiObjectRec;


begin
  Result := False;

  oMiFile:=TmiMap.Create;


  if oMiFile.OpenFile (aFileName) then
  begin
    vFeatures := oMiFile.vLayer.AllFeatures;
    iCount:=vFeatures.Count;

    for i:=1 to  iCount do
    begin
//      vFeature:=vFeatures.Item(i);
      vFeature:=vFeatures.Item[i];


      oMiFile.ExtractObjectRec(vFeature, miObjectRec);

      aBLPointArray :=  miObjectRec.Parts[0].Points;

      Result := True;

    end;


  end;
  ;

  FreeAndNil(oMiFile);

end;

