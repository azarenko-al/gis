unit u_classes_projection_TXT;

interface

uses
  Classes, SysUtils,   IOUtils,  Types, StrUtils,

  u_func
  ;

type
  TProjection_Txt_file = class(TObject)
  private
  public     
    Zone_UTM : Integer;

    PROJCS: string;
    
    function LoadFromDir(aDir: string): Boolean;
    procedure LoadFromFile(aFileName: string);
  end;


implementation

uses
  System.IniFiles;

// ---------------------------------------------------------------
function TProjection_Txt_file.LoadFromDir(aDir: string): Boolean;
// ---------------------------------------------------------------
var
  s1: string;
  s2: string;
begin
  s1:=aDir + '\Projection.Txt';
  s2:=ExtractFilePath(aDir) + 'Projection.Txt';

  if FileExists(s1) then
    LoadFromFile(s1)
  else  
  
  if FileExists(s2) then
    LoadFromFile(s2);
  
  Result:=Zone_UTM>0;

  assert (Result);
  
//  sFileDir := ExtractFilePath(aFileName);

end;



//7
//
//Starting with Delphi XE 2010 you can use the IOUtils.TFile.ReadAllLines function to read the content of text file in a single line of code.
//
//class function ReadAllLines(const Path: string): TStringDynArray;
//class function ReadAllLines(const Path: string;  const Encoding: TEncoding): TStringDynArray; overload; static;
//shareimprove this answer
//


// ---------------------------------------------------------------
procedure TProjection_Txt_file.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
(*

WGS 84
40
UTM
0.0 57.0 500000.0 0.0
*)


var 
  arr: TStringDynArray;
  ini: TIniFile;
  s: string;
begin
   s:= LowerCase(ExtractFileExt(aFileName));


  if s='.ini' then
  begin
    ini:=TIniFile.Create(aFileName);

    Zone_UTM  :=ini.ReadInteger('main','zone',0);
    PROJCS:=ini.ReadString('main','PROJCS','');

    FreeAndNil(ini);

    Exit;
  end;      
   
//
//  s:=TFile.ReadAllText(aFileName);
//  if LeftStr(s,6)='PROJCS' then
//  begin
//    PROJCS:=s;
//    Exit;
//  //PROJCS["Equidistant_Conic_Yakutiya",GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Equidistant_Conic"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],PARAMETER["Longitude_Of_Center",132],PARAMETER["Standard_Parallel_1",49.4],PARAMETER["Standard_Parallel_2",67.8],PARAMETER["Latitude_Of_Center",64],UNIT["Meter",1],AUTHORITY["EPSG","54027"]]
//
//  end;  

  
  arr:=TFile.ReadAllLines(aFileName);
 
  if Length(arr)>=2 then 
    Zone_UTM :=AsInteger(arr[1]);
  

end;

end.


