object frm_Main_dem: Tfrm_Main_dem
  Left = 1437
  Top = 208
  ActiveControl = PageControl1
  BorderIcons = [biSystemMenu, biMinimize]
  BorderWidth = 5
  Caption = 'ASC,BIL,GRD -> Onega relief'
  ClientHeight = 592
  ClientWidth = 760
  Color = clBtnFace
  Constraints.MinHeight = 584
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 760
    Height = 393
    ActivePage = TabSheet_ATDI
    Align = alTop
    TabOrder = 0
    object TabSheet_ASC: TTabSheet
      Caption = 'ASC'
      ImageIndex = 5
    end
    object TabSheet_Bil: TTabSheet
      Caption = 'BIL'
    end
    object TabSheet_Bil_index: TTabSheet
      Caption = 'BIL index.txt'
      ImageIndex = 9
    end
    object TabSheet_GRD_GRC: TTabSheet
      Caption = 'Mapinfo GRD,GRC'
      ImageIndex = 6
    end
    object TabSheet_ATDI: TTabSheet
      Caption = 'ATDI'
      ImageIndex = 4
    end
    object TabSheet_ASC_and_GRD: TTabSheet
      Caption = 'ASC+GRD,GRC'
      ImageIndex = 5
    end
    object TabSheet_GeoTiff: TTabSheet
      Caption = 'GeoTiff'
      ImageIndex = 6
    end
    object TabSheet_RLF_cut: TTabSheet
      Caption = 'RLF cut'
      ImageIndex = 7
    end
    object TabSheet_RLF_to_UTM: TTabSheet
      Caption = 'RLF_to_UTM'
      ImageIndex = 8
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 24
    Top = 472
  end
  object ActionList1: TActionList
    Left = 16
    Top = 408
    object act_Setup: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnExecute = act_SetupExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 192
    Top = 408
    object N1: TMenuItem
      Caption = #1057#1077#1088#1074#1080#1089
      object MenuTXT1: TMenuItem
        Action = act_Setup
      end
    end
  end
end
