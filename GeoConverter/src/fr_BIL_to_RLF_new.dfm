inherited frame_BIL_to_RLF_new: Tframe_BIL_to_RLF_new
  Left = 1225
  Top = 621
  Caption = 'frame_BIL_to_RLF_new'
  ClientHeight = 579
  ClientWidth = 756
  OnDestroy = FormDestroy
  ExplicitLeft = 1225
  ExplicitTop = 621
  ExplicitWidth = 764
  ExplicitHeight = 607
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 563
    Width = 756
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 687
    ExplicitWidth = 748
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 105
    Width = 756
    Height = 240
    Align = alTop
    TabOrder = 1
    ExplicitWidth = 748
    DesignSize = (
      756
      240)
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 12
      Width = 300
      Height = 17
      Caption = 'Clutter_Classes'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object ed_Clutter_Classes: TFilenameEdit
      Left = 8
      Top = 31
      Width = 740
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'Index (*.txt)|*.txt'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = 'S:\-- tasks --\bil\Town\Clutter\index.txt'
      ExplicitWidth = 732
    end
    object cb_Clutter_Heights: TCheckBox
      Left = 8
      Top = 61
      Width = 300
      Height = 17
      Caption = 'Clutter_Heights'
      TabOrder = 2
    end
    object ed_Clutter_Heights: TFilenameEdit
      Left = 8
      Top = 79
      Width = 740
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'BIL (*.bil)|*.bil'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 3
      Text = ''
      ExplicitWidth = 732
    end
    object ed_Build_Heights: TFilenameEdit
      Left = 8
      Top = 131
      Width = 740
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'Index (*.txt)|*.txt'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 4
      Text = 'S:\-- tasks --\bil\Town\Heights\index.txt'
      ExplicitWidth = 732
    end
    object cb_Clutter_building_heights: TCheckBox
      Left = 8
      Top = 111
      Width = 300
      Height = 17
      Caption = #1042#1099#1089#1086#1090#1099' '#1089#1090#1088#1086#1077#1085#1080#1081
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    inline frame_Clutter_1: Tframe_Clutter_
      Left = 2
      Top = 188
      Width = 752
      Height = 50
      Align = alBottom
      Constraints.MaxHeight = 50
      TabOrder = 6
      ExplicitLeft = 2
      ExplicitTop = 188
      ExplicitWidth = 752
      ExplicitHeight = 50
      inherited Label1: TLabel
        Width = 144
        ExplicitWidth = 144
      end
      inherited Bevel2: TBevel
        Width = 752
        ExplicitWidth = 745
      end
      inherited Button1: TButton
        OnClick = frame_Clutter_1Button1Click
      end
    end
  end
  inline frm_RLF1: Tframe_RLF_ [2]
    Left = 0
    Top = 477
    Width = 756
    Height = 86
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 560
    ExplicitWidth = 748
    ExplicitHeight = 86
    DesignSize = (
      756
      86)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 756
      ExplicitWidth = 748
    end
    inherited ed_RLF: TFilenameEdit
      Width = 744
      ExplicitWidth = 736
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 756
    Height = 105
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 748
    DesignSize = (
      756
      105)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 99
      Height = 13
      Caption = 'DEM '#1088#1077#1083#1100#1077#1092' (index)'
    end
    object lb_Projection_file: TLabel
      Left = 8
      Top = 56
      Width = 63
      Height = 13
      Caption = 'Projection file'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 739
      Height = 21
      DefaultExt = '*.txt'
      Filter = 'Index (*.txt)|*.txt'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'S:\-- tasks --\bil\Town\Heights\index.txt'
      ExplicitWidth = 731
    end
    object ed_Projection_txt: TFilenameEdit
      Left = 8
      Top = 72
      Width = 739
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = '"S:\-- tasks --\bil\Town\projection.txt"'
      ExplicitWidth = 731
    end
  end
  object cb_Set_Clutter_Height_011111111: TCheckBox
    Left = 8
    Top = 358
    Width = 404
    Height = 17
    Caption = #1045#1089#1083#1080' '#1074#1099#1089#1086#1090#1072' '#1079#1076#1072#1085#1080#1103' ONE_BUILD=0 -> '#1091#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1080#1087' = OPEN'
    TabOrder = 4
    Visible = False
  end
  inherited ActionList_custom: TActionList
    Top = 400
  end
  object ActionList1: TActionList
    Left = 160
    Top = 400
    object FileOpen_DEM: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      BeforeExecute = FileOpen_DEMBeforeExecute
    end
    object FileOpen_RLF: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
    object FileOpen_Borders: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
    object FileOpen_Projection: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
  end
end
