unit u_data_classes111111111111;

interface

uses
   SysUtils, classes,

   u_geo
   ;

type
(*
  IMatrix = interface(IInterface)
  ['{35D688CB-8F5B-42C8-BF09-39186676E898}']
   // function FindValueByXY(aX, aY: double; var aValue: double): Boolean;

    function GetValueByLatLon(aLat, aLon: Double; var aValue: Double): Boolean;

  end;
  *)


  // ---------------------------------------------------------------
  TMatrixDataSourceBase = class
  // ---------------------------------------------------------------
  public
    XYBounds_MinMax: TXYBounds_MinMax;

    function GetXYBounds_MinMax(): TXYBounds_MinMax; virtual; abstract;
    function GetXYBounds_UTM(): TXYRect; virtual; abstract;

    function GetValueByLatLon(aLat, aLon: Double; var aValue: Double): Boolean;
      virtual; abstract;

    function GetMinCellSize: Double; virtual; abstract;
    function GetZoneUTM: Integer; virtual; abstract;
    function GetFileName: string; virtual; abstract;

    // eStepY := aMatrixDataSource.GetMinCellSize();



  //  function FindValueByXY(aX, aY: double; var aValue: double): Boolean; virtual;
   //     abstract;
  end;

  // ---------------------------------------------------------------
  TMatrixDataSourceList = class(TList)
  // ---------------------------------------------------------------
  private
    InterfaceList: TInterfaceList;

    function GetItem(Index: integer): TMatrixDataSourceBase;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear; override;
    procedure AddItem(aMatrix: TMatrixDataSourceBase);

  //TInterfaceList
//    function FindValueByXY(aX, aY: double; var aValue: double): Boolean;
  //   GetValueByLatLon

    function GetValueByLatLon(aLat, aLon: Double; var aValue: Double): Boolean;


    property Items[Index: integer]: TMatrixDataSourceBase read GetItem; default;
  end;


implementation

constructor TMatrixDataSourceList.Create;
begin
  inherited Create;
  InterfaceList := TInterfaceList.Create();
end;

destructor TMatrixDataSourceList.Destroy;
begin
  FreeAndNil(InterfaceList);
  inherited Destroy;
end;

procedure TMatrixDataSourceList.AddItem(aMatrix: TMatrixDataSourceBase);
//var
//  v: IMatrix;
begin
  Add(aMatrix);

(*
  if aMatrix.GetInterface(IMatrix, v) then
    InterfaceList.Add(v)
  else
    raise Exception.Create('');
*)
end;

procedure TMatrixDataSourceList.Clear;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[i].Free;

  inherited;
end;

function TMatrixDataSourceList.GetItem(Index: integer): TMatrixDataSourceBase;
begin
   Result:=TMatrixDataSourceBase(inherited Items[Index]);
end;

// ---------------------------------------------------------------
function TMatrixDataSourceList.GetValueByLatLon(aLat, aLon: Double; var aValue:
    Double): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    Result := Items[i].GetValueByLatLon(aLat, aLon, aValue);
    if Result then
      Exit;
  end;

  Result := False;
end;



end.


