object dlg_clutter_schema_change: Tdlg_clutter_schema_change
  Left = 2232
  Top = 1149
  BorderStyle = bsDialog
  Caption = 'dlg_clutter_schema_change'
  ClientHeight = 170
  ClientWidth = 320
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LabeledEdit_Src: TLabeledEdit
    Left = 8
    Top = 32
    Width = 300
    Height = 21
    EditLabel.Width = 99
    EditLabel.Height = 13
    EditLabel.Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
    TabOrder = 0
  end
  object LabeledEdit_Dest: TLabeledEdit
    Left = 8
    Top = 88
    Width = 300
    Height = 21
    EditLabel.Width = 81
    EditLabel.Height = 13
    EditLabel.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
    TabOrder = 1
  end
  object Button1: TButton
    Left = 144
    Top = 134
    Width = 75
    Height = 25
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button2: TButton
    Left = 233
    Top = 134
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 3
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'LabeledEdit_Dest.Text'
      'LabeledEdit_Src.Text')
    StoredValues = <>
    Left = 29
    Top = 115
  end
end
