unit f_Main_dem;

interface

uses
  SysUtils, Classes, Controls, Forms, dialogs,
  StdCtrls, ExtCtrls, ComCtrls, rxPlacemnt, ActnList,

  u_const,

  //d_Config,

  f_Clutters,

  u_func,

  fr_GeoTiff,

  fr_BIL_to_RLF_new,
  fr_RLF_cut,
  fr_RLF_to_UTM,
  fr_Asc_to_RLF,
  fr_BIL_to_RLF,
  fr_ATDI_to_RLF,
  fr_Asset_to_RLF,
  fr_ASC_and_GRD_to_RLF,
  fr_VM_GRD_to_RLF, Menus, System.Actions;

type
  Tfrm_Main_dem = class(TForm)
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    PageControl1: TPageControl;
    TabSheet_Bil: TTabSheet;
    TabSheet_GRD_GRC: TTabSheet;
    TabSheet_ASC: TTabSheet;
    TabSheet_ATDI: TTabSheet;
    TabSheet_ASC_and_GRD: TTabSheet;
    TabSheet_GeoTiff: TTabSheet;
    TabSheet_RLF_cut: TTabSheet;
    TabSheet_RLF_to_UTM: TTabSheet;
    TabSheet_Bil_index: TTabSheet;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    MenuTXT1: TMenuItem;
    act_Setup: TAction;

    procedure act_SetupExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure Panel1Click(Sender: TObject);
  private
  public

  end;

var
  frm_Main_dem: Tfrm_Main_dem;

implementation

uses dm_Clutters;


{$R *.dfm}


// ---------------------------------------------------------------
procedure Tfrm_Main_dem.act_SetupExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Setup then
    Tfrm_Clutters.ExecDlg_NoResult();

end;


procedure Tfrm_Main_dem.Button1Click(Sender: TObject);
var
  s: string;
begin
 // Tfrm_Clutters.ExecDlg(s);
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_dem.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption := STR_GeoConverter + GetAppVersionStr;

  PageControl1.Align:=alClient;

  CreateChildForm_(Tframe_ATDI_to_RLF,    frame_ATDI_to_RLF, TabSheet_ATDI);
  CreateChildForm_(Tframe_VM_GRD_to_RLF,  frame_VM_GRD_to_RLF, TabSheet_GRD_GRC);
  CreateChildForm_(Tframe_Asset_to_RLF,    frame_Asset_to_RLF, TabSheet_Asset);
  CreateChildForm_(Tframe_Asc_to_RLF,    frame_Asc_to_RLF, TabSheet_ASC);

  CreateChildForm_(Tframe_BIL_to_RLF,        frame_BIL_to_RLF, TabSheet_Bil);
  CreateChildForm_(Tframe_BIL_to_RLF_new,    frame_BIL_to_RLF_new, TabSheet_Bil_index);



  CreateChildForm_(Tframe_ASC_and_GRD_to_RLF,    frame_ASC_and_GRD_to_RLF, TabSheet_ASC_and_GRD);
  CreateChildForm_(Tframe_GeoTiff,               frame_GeoTiff, TabSheet_GeoTiff);
  CreateChildForm_(Tframe_RLF_cut,               frame_RLF_cut, TabSheet_RLF_cut);
  CreateChildForm_(Tframe_RLF_to_UTM,            frame_RLF_to_UTM, TabSheet_RLF_to_UTM);

  dmClutters();

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_dem.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(frame_VM_GRD_to_RLF);
  FreeAndNil(frame_Asset_to_RLF);
  FreeAndNil(frame_Asc_to_RLF);

  FreeAndNil(frame_BIL_to_RLF);
  FreeAndNil(frame_BIL_to_RLF_new);

  FreeAndNil(frame_ATDI_to_RLF);
  FreeAndNil(frame_ASC_and_GRD_to_RLF);
  FreeAndNil(frame_GeoTiff);
  FreeAndNil(frame_RLF_cut);
  FreeAndNil(frame_RLF_to_UTM);

end;


end.



