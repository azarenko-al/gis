unit u_bil;

interface
uses
  Classes, SysUtils, Graphics,

  u_Bmp_file,

  u_img,

  u_func,

  u_BufferedFileStream,

  //u_bil_classes,

  u_geo;


type
  TBil_HDR_file = class(TObject)
  public
    ulxmap        : double; //right
    ulymap        : double; //top
    ncols         : Integer;
    nrows         : Integer;
    nbits         : Integer;
    nbands        : Integer;
    ByteOrder     : string;
    bandrowbytes  : Integer;
    totalrowbytes : Integer;
    skipbytes     : Integer;
    datatype      : string;
    nodatavalue   : Integer;

    xdim          : Double;
    ydim          : Double;

    function LoadFromFile(aFileName: string): Boolean;
  end;

type

  // ---------------------------------------------------------------
  TBil_File = class(TObject)
  // ---------------------------------------------------------------
  private
    FByteCountPerCell : Integer;

    FNodataValue : Integer;

//    FStream: TFileStream;
    FStream: TReadOnlyCachedFileStream;

    function FindCellByXY(aX, aY: double; var aRow, aCol: integer): boolean;
    function GetItem(aRow, aCol: integer; var aValue: smallint): Boolean;
    procedure LoadFromHeaderFile(aFileName: string);

    procedure OpenBinaryFile(aFileName: string);
//    procedure PrepareClutterIni(aClutterIniFile: TClutterIniFile);

  public
    Header: TBil_HDR_file;

 // public

    LatLonMinMax: TLatLonMinMax;

    XYBounds: TXYRect;

    StepX    : Double;
    StepY    : Double;
    RowCount : Integer;
    ColCount : Integer;

  //  FileName : string;

    constructor Create;
    destructor Destroy; override;

    function FindValueByXY(aX, aY: double; var aValue: smallint): Boolean;

    procedure CloseBinaryFile;
    procedure OpenFile(aFileName: string);

    procedure SaveToBmp(aFileName: string);

  end;



implementation



constructor TBil_File.Create;
begin
  inherited Create;
  Header := TBil_HDR_file.Create();


  FByteCountPerCell:=2;
  FNodataValue:=-9999;
  
end;

destructor TBil_File.Destroy;
begin
  FreeAndNil(Header);
  inherited Destroy;
end;

//---------------------------------------------------------------
function TBil_File.FindCellByXY(aX, aY: double; var aRow, aCol: integer):
    boolean;
//--------------------------------------------------------------
var
  b: boolean;
begin
  Assert(XYBounds.TopLeft.x <> 0);
  Assert(XYBounds.BottomRight.x <> 0);


  Result:=False;

  aRow:=0; aCol:=0;

//  if not (Active) then
 //   Exit;
//  assert( XYBounds.BottomRight.x>0);

//  if MatrixType=mtXY_ then
//  begin

  b:=(aX < XYBounds.TopLeft.x);
  b:=(XYBounds.BottomRight.x < aX);

  b:=(XYBounds.TopLeft.y < aY);
  b:= aY < XYBounds.BottomRight.y;


  if (XYBounds.BottomRight.x < aX) and (aX < XYBounds.TopLeft.x) and
     (XYBounds.TopLeft.y     < aY) and (aY < XYBounds.BottomRight.y) then
  begin
    aRow := Integer(Round((XYBounds.TopLeft.x - aX) / StepX));
    aCol := Integer(Round((aY - XYBounds.TopLeft.y) / StepY));

    Result:=(aRow>=0) and (aCol>=0) and (aRow<=RowCount-1) and (aCol<=ColCount-1);

//      Result:=true;
  end;
// end;

end;



// ---------------------------------------------------------------
function TBil_File.GetItem(aRow, aCol: integer; var aValue: smallint): Boolean;
// ---------------------------------------------------------------

    function MacWordToPC(Value : Smallint): Smallint;
    asm xchg Al,Ah
    end;


var
  iOffset: Integer;

iByteValue: Byte;
  iIntValue: Smallint;

  iIntValue1: Smallint;  
begin

//  if Result and Assigned(FStream) then
 // begin
//    iOffset :=(Header.ncols*iRow + iCol) * FByteCountPerCell;
  iOffset :=(ColCount*aRow + aCol) * FByteCountPerCell;

  FStream.Seek (iOffset,  soFromBeginning);

  case FByteCountPerCell of
    1: begin
         FStream.read(iByteValue, 1);
//           Result:=iByteValue<>Header.nodatavalue;
         Result:=iByteValue<>Fnodatavalue;
         aValue :=iByteValue;
       end;
         
    2: begin
         FStream.read(iIntValue, 2);

         iIntValue1 := MacWordToPC(iIntValue);

         if (Header.ByteOrder='') or (Header.ByteOrder='M') then
           iIntValue := MacWordToPC(iIntValue);

       //  iIntValue :=-9999;

//           Result:=iIntValue<>Header.nodatavalue;
         Result:=iIntValue<>Fnodatavalue;
           
         aValue :=iIntValue;
       end;
  end;

 // end;


end;



// ---------------------------------------------------------------
function TBil_File.FindValueByXY(aX, aY: double; var aValue: smallint): Boolean;
// ---------------------------------------------------------------

    function MacWordToPC(Value : Smallint): Smallint;
    asm xchg Al,Ah
    end;

var
  iRow,iCol: Integer;
  iOffset: integer;
//  iByteCount: integer;

  iByteValue: Byte;
  iIntValue: Smallint;

  iIntValue1: Smallint;
  
//  iIntValue: word;

begin
//  Assert(nodatavalue<>0, 'Value <>0');

 // iByteCount := (Header.nbits div 8);


 // Assert(iByteCount>0, 'Value <=0');

  Result := FindCellByXY(aX, aY, iRow,iCol);

  if Result and Assigned(FStream) then
    Result:=GetItem(iRow,iCol, aValue);

(*
  begin
//    iOffset :=(Header.ncols*iRow + iCol) * FByteCountPerCell;
    iOffset :=(ColCount*iRow + iCol) * FByteCountPerCell;

    FStream.Seek (iOffset,  soFromBeginning);

    case FByteCountPerCell of
      1: begin
           FStream.read(iByteValue, 1);
//           Result:=iByteValue<>Header.nodatavalue;
           Result:=iByteValue<>Fnodatavalue;
           aValue :=iByteValue;
         end;

      2: begin
           FStream.read(iIntValue, 2);

           iIntValue1 := MacWordToPC(iIntValue);

           if (Header.ByteOrder='') or (Header.ByteOrder='M') then
             iIntValue := MacWordToPC(iIntValue);

         //  iIntValue :=-9999;

//           Result:=iIntValue<>Header.nodatavalue;
           Result:=iIntValue<>Fnodatavalue;

           aValue :=iIntValue;
         end;
    end;

  end;
*)


end;


// ---------------------------------------------------------------
procedure TBil_File.LoadFromHeaderFile(aFileName: string);
// ---------------------------------------------------------------
begin
  if not Header.LoadFromFile(aFileName) then
    Exit;


  with Header do
  begin
    XYBounds.TopLeft.X := ulymap;
    XYBounds.TopLeft.Y := ulxmap;
    XYBounds.BottomRight.X := ulymap - ydim*nrows;
    XYBounds.BottomRight.Y := ulxmap + xdim*ncols;

    StepX := ydim;
    StepY := xdim;

    RowCount :=nrows;
    ColCount :=ncols;

  end;

  FByteCountPerCell := (Header.nbits div 8);

end;

// ---------------------------------------------------------------
procedure TBil_File.OpenBinaryFile(aFileName: string);
// ---------------------------------------------------------------
begin
  Assert(FileExists(aFileName), 'FileExists: ' + aFileName);

 // if aFileName='' then
  //  aFileName := FileName;

//  aFileName :=ChangeFileExt(aFileName, '.bil');

//  FStream:=TFileStream.Create(aFileName, fmShareDenyWrite);

  FStream:=TReadOnlyCachedFileStream.Create(aFileName);


 // TReadOnlyCachedFileStream

end;


procedure TBil_File.CloseBinaryFile;
begin
  FreeAndNil(FStream);
end;

procedure TBil_File.OpenFile(aFileName: string);
begin
  LoadFromHeaderFile(afileName);
  OpenBinaryFile    (afileName);

end;

// ---------------------------------------------------------------
procedure TBil_File.SaveToBmp(aFileName: string);
// ---------------------------------------------------------------
var
  c: Integer;
  I: Integer;
  iColor: Integer;
  oBmpFile: TBmpFile;
  r: Integer;
 iValue: smallint  ;
begin
  oBmpFile:=TBmpFile.Create_Width_Height(aFileName, ColCount, RowCount);

  for r := 0 to RowCount - 1 do
  begin
    for c := 0 to RowCount - 1 do
    begin
      if GetItem(r,c,iValue) then
        iColor:=  img_MakeGradientColor(iValue, 0, 500, clred, clNavy)
      else
        iColor:=clBlack ;


      oBmpFile.Write(iColor);

    end;


    oBmpFile.WriteLn;
  end;

  FreeAndNil(oBmpFile);
end;


function TBil_HDR_file.LoadFromFile(aFileName: string): Boolean;
var
  I: Integer;
  oStrList: TStringList;
  s: string;
  iPos: integer;
  sParam: string;
  sValue: string;
begin
  Result := False;

  aFileName :=ChangeFileExt(aFileName, '.hdr');

  if not FileExists(aFileName) then
    Exit;


 // FileName := aFileName;

  oStrList:=TStringList.Create;
  oStrList.LoadFromFile (aFileName);

  s :=oStrList.Text;


  for I := 0 to oStrList.Count - 1 do
  begin
    s:=LowerCase(Trim(oStrList[i]));
    iPos :=Pos(' ',s);

    if iPos >0 then
    begin
      sParam :=LowerCase(Trim(Copy (s, 1, iPos)));
      sValue :=Trim(Copy (s, iPos, 100));


      if sParam = 'ulxmap' then ulxmap :=AsFloat(sValue) else
      if sParam = 'ulymap' then ulymap :=AsFloat(sValue) else

      if sParam = 'xdim' then xdim :=AsFloat(sValue)  else
      if sParam = 'ydim' then ydim :=AsFloat(sValue) else

      if sParam = 'nbits' then nbits :=AsInteger(sValue) else

      if sParam = 'ncols' then ncols :=AsInteger(sValue) else
      if sParam = 'nrows' then nrows :=AsInteger(sValue) else

      if sParam = 'nodata'      then nodatavalue :=AsInteger(sValue) else
      if sParam = 'nodatavalue' then nodatavalue :=AsInteger(sValue) else

      if sParam = 'byteorder' then ByteOrder :=UpperCase(sValue) else

      if sParam = 'datatype' then DataType :=UpperCase(sValue) else
      ;

    end;
  end;

//  Assert(nodatavalue<>0, 'Value <>0');


  oStrList.Free;

  Result := True;



//  if DataType='I16' then



(*ulxmap 405390.500000
ulymap 6180492.500000

  x := 5 080 000;
  y := 7 700 000;

(*

  x := 6180492;
  y := 405390;


*)

end;


end.


