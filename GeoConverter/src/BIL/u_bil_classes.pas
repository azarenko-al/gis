unit u_bil_classes;

interface
uses
  Classes, SysUtils,

  u_geo,

  u_bil,

  u_func_arr,

  u_func

  ;


type


  TBil_Projection_Txt_file = class(TObject)
  public
    Projection : string;
    Zone : Integer;

    procedure LoadFromFile(aFileName: string);
  end;


  TBil_Menu_Txt_file = class(TObject)
  public
    Items : array of record
                      Code : Word;
                      Name : string;

                      Onega_Code   : Byte;
                      Onega_Height : Word;
                    end;

    procedure LoadFromFile(aFileName: string);
  end;

type
  // ---------------------------------------------------------------
  TBil_index_file_item = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    FileName : string;

    Bounds: TLatLonMinMax;

    CellSize : double;

    Data_File: TBil_File;

  end;


  // ---------------------------------------------------------------
  TBil_index_file = class(TCollection)
  // ---------------------------------------------------------------
  private
    procedure GetBounds(var aRect: TLatLonMinMax);

    function GetItem(Index: integer): TBil_index_file_item;

(*  private
    Items: array of record
                      FileName : string;

                      Bounds: TLatLonMinMax;

                      CellSize : double;

                      Data_File: TBil_File;

                    end;
*)
 public
    Bounds: TLatLonMinMax;
    CellSize : double;

    XYBounds: TXYRect;

    constructor Create;
    function AddItem(aFileName: string): TBil_index_file_item;

    function FindValueByXY(aX, aY: double; var aValue: smallint): Boolean;
//    Count : Integer;

//    function GetCount: Integer;

    procedure LoadFromFile(aFileName: string);

    property Items[Index: integer]: TBil_index_file_item read GetItem; default;

  end;

function LatLonMinMaxToXYRect(aLatLon: TLatLonMinMax): TXYRect;

function LatLonPointInRect(aLat, aLon: double; aRect: TLatLonMinMax): Boolean;


implementation




 
// ---------------------------------------------------------------
procedure TBil_Projection_Txt_file.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
(*

WGS 84
40
UTM
0.0 57.0 500000.0 0.0
*)


var
  I: Integer;
  oStrList: TStringList;
  s: string;
begin
  oStrList:=TStringList.Create;
  oStrList.LoadFromFile (aFileName);

  for I := 0 to oStrList.Count - 1 do
  begin
    s:=LowerCase(Trim(oStrList[i]));

    case i of
      0: Projection :=s;
      1: Zone       :=AsInteger(s);
    end;

  end;

  FreeAndNil(oStrList);

end;

// ---------------------------------------------------------------
procedure TBil_Menu_Txt_file.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iPos: Integer;
  oStrList: TStringList;
  s,s1,s2: string;
begin
  oStrList:=TStringList.Create;
  oStrList.LoadFromFile (aFileName);

  SetLength(Items, oStrList.Count);

  for I := 0 to oStrList.Count - 1 do
  begin
    s:=LowerCase(Trim(oStrList[i]));

    iPos:=Pos(' ',s);

    s1:=Trim(Copy(s,1,iPos));
    s2:=Trim(Copy(s,iPos+1,100));

    Items[i].Code := AsInteger(s1);
    Items[i].Name := s2;
  end;

  FreeAndNil(oStrList);

end;

// ---------------------------------------------------------------
procedure TBil_index_file.GetBounds(var aRect: TLatLonMinMax);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Assert(Count>0);

  aRect := Items[0]. Bounds;

  for I := 1 to Count-1 do
  begin
    if Items[i].Bounds.Lat_min < aRect.Lat_min then aRect.Lat_min:=Items[i].Bounds.Lat_min;
    if Items[i].Bounds.Lon_min < aRect.Lon_min then aRect.Lon_min:=Items[i].Bounds.Lon_min;

    if Items[i].Bounds.Lat_max > aRect.Lat_max then aRect.Lat_max:=Items[i].Bounds.Lat_max;
    if Items[i].Bounds.Lon_max > aRect.Lon_max then aRect.Lon_max:=Items[i].Bounds.Lon_max;

  end;

end;
(*
function TBil_index_file.GetCount: Integer;
begin
  Result := Length(Items);
end;*)

// ---------------------------------------------------------------
procedure TBil_index_file.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  eCellSize: double;
  k: Integer;
  oFile: TBil_File;
  oStrList: TStringList;
  sFileDir: string;
  strArr: TStrArray;

  rBounds: TLatLonMinMax;

  oItem: TBil_index_file_item;
  sFileName: string;

begin
  Clear;

  oStrList := TStringList.Create;
  oStrList.LoadFromFile(aFileName);

  sFileDir := IncludeTrailingBackslash( ExtractFileDir(aFileName));


//  SetLength(Items, 0);

  for I := 0 to oStrList.Count - 1 do
    if oStrList[i]<>'' then
  begin
    strArr :=StringToStrArray(oStrList[i], ' ');

    sFileName := sFileDir + strArr[0];

    if not FileExists(sFileName) then
      Continue;
      

//    oItem.FileName := sFileDir + strArr[0];


    oItem:=AddItem(sFileName);

 //   SetLength(Items, Length(Items)+1);

   // k:=High(Items);


    Assert(FileExists(oItem.FileName), oItem.FileName);


    //TADJIKISTAN_clut_1_1.bin 355150.0 605150.0 4296550.0 4546550.0 50.0
    rBounds.Lon_min := AsFloat(strArr[1]);
    rBounds.Lon_max := AsFloat(strArr[2]);
    rBounds.Lat_min := AsFloat(strArr[3]);
    rBounds.Lat_max := AsFloat(strArr[4]);

    Assert(rBounds.Lon_min>0);
    Assert(rBounds.Lon_max>0);
    Assert(rBounds.Lat_min>0);
    Assert(rBounds.Lat_max>0);

    
    Items[k].Bounds:=rBounds;

//
//    Items[i].rBounds.Lon_min := AsFloat(strArr[1]);
//    Items[i].rBounds.Lon_max := AsFloat(strArr[2]);
//    Items[i].rBounds.Lat_min := AsFloat(strArr[3]);
//    Items[i].rBounds.Lat_max := AsFloat(strArr[4]);

    eCellSize:= AsFloat(strArr[5]);
    Assert(eCellSize>0);

    oItem.CellSize:= eCellSize;


    // ---------------------------------

    oFile := TBil_File.Create;
    oItem.Data_File := oFile;

    oFile.LatLonMinMax := rBounds;
    oFile.XYBounds := LatLonMinMaxToXYRect(rBounds);
    oFile.StepX  := eCellSize;
    oFile.StepY  := eCellSize;




    oFile.RowCount :=  Round ((rBounds.Lat_max -rBounds.Lat_min) / eCellSize);
    oFile.ColCount :=  Round ((rBounds.Lon_max -rBounds.Lon_min) / eCellSize);

//    oFile.CellSize := Items[i].CellSize;
//    oFile.Prepare;

    oFile.OpenFile(Items[i].FileName);

  /////////////  oFile.SaveToBmp(Items[i].FileName);

  end;

 // Count:= oStrList.Count;

  FreeAndNil(oStrList);

  GetBounds(Bounds);

  CellSize := Items[0].CellSize;

  XYBounds := LatLonMinMaxToXYRect(Bounds);

(*
  Bounds.TopLeft.X := rBounds.Lat_max;
  Bounds.TopLeft.Y := rBounds.Lon_min;
  Bounds.BottomRight.X := rBounds.Lat_min;
  Bounds.BottomRight.Y := rBounds.Lon_max;
*)
end;



// ---------------------------------------------------------------
function LatLonPointInRect(aLat, aLon: double; aRect: TLatLonMinMax): Boolean;
// ---------------------------------------------------------------
begin
  Assert(aRect.Lat_max <> 0);

  Assert(aRect.Lat_min < aRect.Lat_max);
  Assert(aRect.Lon_min < aRect.Lon_max);

  Result :=
     (aLat <= aRect.Lat_max) and
     (aLon <= aRect.Lon_max) and
     (aLat >= aRect.Lat_min) and
     (aLon >= aRect.Lon_min);
end;             

// ---------------------------------------------------------------
function LatLonMinMaxToXYRect(aLatLon: TLatLonMinMax): TXYRect;
// ---------------------------------------------------------------
begin
  Assert(aLatLon.Lat_max <> 0);    
  Assert(aLatLon.Lat_min < aLatLon.Lat_max);
  Assert(aLatLon.Lon_min < aLatLon.Lon_max);

  Result.TopLeft.X := aLatLon.Lat_max;
  Result.TopLeft.Y := aLatLon.Lon_min;
  Result.BottomRight.X := aLatLon.Lat_min;
  Result.BottomRight.Y := aLatLon.Lon_max;


//   :=
//     (aLat <= aRect.Lat_max) and
//     (aLon <= aRect.Lon_max) and
//     (aLat >= aRect.Lat_min) and
//     (aLon >= aRect.Lon_min);

end;


constructor TBil_index_file.Create;
begin
  inherited Create(TBil_index_file_item);
end;

function TBil_index_file.AddItem(aFileName: string): TBil_index_file_item;
begin
  Result := TBil_index_file_item(Add);

  Result.FileName :=aFileName;
end;

// ---------------------------------------------------------------
function TBil_index_file.FindValueByXY(aX, aY: double; var aValue: smallint):
    Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Assert(aX>0);
  Assert(aY>0);


  Result := False;

  for I := 0 to Count-1 do
  begin
    Assert(Items[i].Bounds.Lat_max <> 0);


    if LatLonPointInRect(aX, aY, Items[i].Bounds) then
    begin
     // raise Exception.Create('');

      Result := Items[i].Data_File.FindValueByXY(aX, aY, aValue);
      Exit;
    end;

  end;
 // Result := ;
end;

function TBil_index_file.GetItem(Index: integer): TBil_index_file_item;
begin
  Result := TBil_index_file_item(inherited Items[Index]);
end;




begin
//  o:=TBil_Passport_file1111111.Create;
//  o.LoadFromFile('S:\-- tasks --\Pskovs\Heights\passport.txt');
 // o.LoadFromFile('S:\passport.txt');


end.



