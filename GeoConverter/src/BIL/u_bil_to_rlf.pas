unit u_bil_to_rlf;

interface
uses Classes, Sysutils, 

  u_geo_convert_new1,

  dm_Clutters,

  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,

  u_CustomTask,

  u_bil,
  u_clutter_classes,

  u_bil_classes;


type

  TBil_to_RLF = class(TCustomTask)
  private

    FGK_zone: Integer;

    FBil_DEM: TBil_File;
    FBil_Clutter_Classes: TBil_File;
    FBil_Clutter_Heights: TBil_File;
    FBil_Build_Heights: TBil_File;

    FClutterInfo: TClutterInfo;

    FProjection_Txt_file: TBil_Projection_Txt_file;


    FUTM_zone : Integer;

    procedure CloseFiles;
    procedure OpenFiles;


  public
    Params: record
      UTM_zone1 : Integer;


      Dem_FileName : string;
      Clutter_Classes_FileName : string;
      Clutter_Heights_fileName : string;
      Build_Heights_FileName   : string;

      Projection_Txt_fileName : string;

//      Clutters_ini_fileName : string;
      Clutters_section : string;


      RLF_FileName : string;
      Rlf_Step : Integer;
    end;


    constructor Create;
    destructor Destroy; override;

    procedure Run;

  end;


implementation


constructor TBil_to_RLF.Create;
begin
  inherited;

  FBil_DEM:=TBil_File.Create;
  FBil_Clutter_Classes:=TBil_File.Create;
  FBil_Clutter_Heights:=TBil_File.Create;
  FBil_Build_Heights := TBil_File.Create();

  FMemStream:=TMemoryStream.Create;

  FClutterInfo := TClutterInfo.Create();
  FProjection_Txt_file := TBil_Projection_Txt_file.Create();

end;


destructor TBil_to_RLF.Destroy;
begin
  FreeAndNil(FBil_Build_Heights);

  FreeAndNil(FProjection_Txt_file);
  FreeAndNil(FBil_DEM);
  FreeAndNil(FBil_Clutter_Classes);
  FreeAndNil(FBil_Clutter_Heights);

  FreeAndNil(FMemStream);
  FreeAndNil(FClutterInfo);

  inherited;
end;

procedure TBil_to_RLF.CloseFiles;
begin

end;


// ---------------------------------------------------------------
procedure TBil_to_RLF.OpenFiles;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin

//  if FileExists(params.Clutters_ini_fileName) then
 // begin
 // FClutterInfo.LoadCluttersFromINI('bil');
 // FClutterInfo.LoadCluttersFromDB(params.Clutters_section);

 //   FClutterInfo.LoadCluttersFromIni(params.Clutters_ini_fileName, 'ATDI');

(*
    with FMenu_Txt_file do
      for I := 0 to High(Items) do
      begin
        FClutterInfo.GetOnegaCode(Items[i].Name, Items[i].Onega_Code, Items[i].Onega_Height);
      end;
*)

//  end;



  if FileExists(params.Projection_Txt_fileName) then
     FProjection_Txt_file.LoadFromFile(params.Projection_Txt_fileName);

  if FProjection_Txt_file.Zone>0 then
    FUTM_zone :=FProjection_Txt_file.Zone
  else
    FUTM_zone :=Params.UTM_zone1;


  FGK_zone := FUTM_zone - 30;


  FBil_DEM.OpenFile(params.DEM_fileName);

  if FileExists(params.Clutter_Classes_fileName) then
  begin
    FBil_Clutter_Classes.OpenFile (params.Clutter_Classes_fileName);

   // s:= IncludeTrailingBackslash(ExtractFileDir(params.Clutter_Classes_fileName)) + 'Menu.Txt';
  //  FMenu_Txt_file.LoadFromFile(s);


    Assert(Params.Clutters_section<>'');

    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);


 //   FClutterInfo.LoadCluttersFromDB(Params.Clutters_section);

  ///  with FMenu_Txt_file do
   //   for I := 0 to High(Items) do
    //    FClutterInfo.SetIndex(Items[i].Name, Items[i].Code);


  end;


  if FileExists(params.Clutter_Heights_fileName) then
    FBil_Clutter_Heights.OpenFile(params.Clutter_Heights_fileName);

  if FileExists(params.Build_Heights_fileName) then
    FBil_Build_Heights.OpenFile (params.Build_Heights_fileName);

end;

// ---------------------------------------------------------------
procedure TBil_to_RLF.Run;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_UTM: TXYRect;

  rXYRect_GK: TXYRect;

  xy_CK1,xy_CK2,
  xy_UTM1,xy_UTM2, xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
//  bTerminated: Boolean;

//  iUTM_zone: integer;

  iRows: Integer;
  iCols: integer;
  iColCount: integer;
  iStep: integer;
  iRowCount: integer;
  iRow: integer;
  iCol: integer;
 // iColCount: integer;

  rlf_rec: Trel_Record;
  iValue: integer;

  iIntValue: Smallint;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
  iCluH: smallint;
  k: Integer;
  s2: string;
  s1: string;

  rClutter: TClutterRec;

begin
  Terminated := False;

  OpenFiles();


  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := FBil_DEM.XYBounds;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);

  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];


    xy_gk := geo_UTM_to_GK(xy, FUTM_zone);

    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;

  rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);


  if Params.Rlf_Step>0 then
    iStep := Params.Rlf_Step
  else
    iStep :=round(FBil_DEM.Header.xdim);

  Assert(iStep>0, 'Value <=0');

  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;


  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=rXYRect_GK;

  oRlfFileStream:=TFileStream.Create(params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

  for iRow := 0 to iRowCount - 1 do
    if not Terminated then
  begin
    DoOnProgress(iRow, iRowCount, Terminated);


    for iCol := 0 to iColCount - 1 do
      if not Terminated then
    begin
      if Terminated then
       Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;

      xy_UTM := geo_GK_to_UTM(xy, FGK_zone);


(*      if Projection=ptUTM_ then
        xy_UTM := geo_GK_to_UTM(xy, FGK_zone)
      else
        xy_UTM := xy;
*)

      FillChar(rlf_rec, SizeOf(rlf_rec), 0);


      if FBil_DEM.FindValueByXY(xy_UTM.x, xy_UTM.y, iIntValue) then
      begin
        rlf_rec.Rel_H :=iIntValue;


        if FBil_Clutter_Classes.FindValueByXY(xy_UTM.x, xy_UTM.y, iCluCode) then
          if iCluCode>0 then
        begin

          if FClutterInfo.GetOnegaCode(iCluCode, rClutter) then
          begin
            rlf_rec.Clutter_Code := rClutter.Onega_Code;
            rlf_rec.Clutter_H    := rClutter.Onega_Height;
          end;



          if rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD, DEF_CLU_CITY] then
            if FBil_Build_Heights.FindValueByXY(xy_UTM.x, xy_UTM.y, iCluH) then
//            if iCluH>0 then
          begin

            if iCluH>0 then
            begin
             // rlf_rec.Clutter_Code :=DEF_CLU_ONE_BUILD;
              rlf_rec.Clutter_H    :=iCluH;
            end else
            begin
              rlf_rec.Clutter_Code :=0;
              rlf_rec.Clutter_H :=0;
            end;

          //  if iCluCode=22 then
           //   ShowMessage('');

            // ����� + ������ =0
(*

            if Params.Is_Set_Clutter_Height_0 then
            begin
              if (rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD])
                  and (rlf_rec.Clutter_H=0)
              then
                rlf_rec.Clutter_Code :=0;
            end;

            try
              if iCluH>250 then
                iCluH:=250;

              if iCluH>0 then
                rlf_rec.Clutter_H  := iCluH;
            except
              on E: Exception do
                ShowMessage( Format('code:%d; h:%d', [iCluCode,iCluH]));
            end;
*)

          end;
        end;

      end
      else
        rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  oRlfFileStream.Free;

 // ProgressBar1.Position :=0;

  FBil_DEM.CloseBinaryFile;
  FBil_Clutter_Classes.CloseBinaryFile;
  FBil_Clutter_Heights.CloseBinaryFile;

//  btn_Run.Action := act_Run;

end;

end.
