unit fr_BIL_to_RLF;

interface

uses
  Classes, Controls, Forms, StdCtrls, rxToolEdit,
  ExtCtrls, SysUtils, IniFiles,

  u_const,                             

  u_bil_to_RLF,

  ComCtrls, 

  frame_RLF,
  frame_Clutter,

  fr_Custom_Form, Mask, ActnList, StdActns, System.Actions

  ;

type
  Tframe_BIL_to_RLF = class(Tfrm_Custom_Form)
    GroupBox1: TGroupBox;
    cb_Clutter_Classes: TCheckBox;
    ed_Clutter_Classes: TFilenameEdit;
    cb_Clutter_Heights: TCheckBox;
    ed_Clutter_Heights: TFilenameEdit;
    frm_RLF1: Tframe_RLF_;
    ed_Build_Heights: TFilenameEdit;
    cb_Clutter_building_heights: TCheckBox;
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    frame_Clutter_1: Tframe_Clutter_;
    cb_Set_Clutter_Height_011111111: TCheckBox;
    ed_Projection_txt: TFilenameEdit;
    lb_Projection_file: TLabel;
    FileOpen1: TFileOpen;
//    procedure act_StopExecute(Sender: TObject);
//    procedure act_RunExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Fbil_to_RLF: Tbil_to_RLF;
   // FTerminated : Boolean;


    FIniFileName : string;

    procedure Run; override;

  private
    procedure LoadFromIniFile(aFileName: String);
    procedure SaveToIniFile(aFileName: String);

  public
  end;



var
  frame_BIL_to_RLF: Tframe_BIL_to_RLF;
  

implementation
{$R *.dfm}

const
  DEF_SECTION = 'BIL_to_RLF';


// ---------------------------------------------------------------
procedure Tframe_BIL_to_RLF.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  Fbil_to_RLF := Tbil_to_RLF.Create();
  Fbil_to_RLF.OnProgress :=DoOnProgress;

  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);

  frm_RLF1.Init;
  frame_Clutter_1.Init;


  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;
  cb_Clutter_Classes.Caption  := STR_Clutter_Classes;
  cb_Clutter_Heights.Caption  := STR_Clutter_Heights;

  lb_Projection_file.Caption  := STR_Projection_file;

  cb_Clutter_building_heights.Caption  := STR_BUILDING_Heights;

end;


procedure Tframe_BIL_to_RLF.FormDestroy(Sender: TObject);
begin
  SaveToIniFile(FIniFileName);

  FreeAndNil(Fbil_to_RLF);
end;

// ---------------------------------------------------------------
procedure Tframe_BIL_to_RLF.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------

var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do

 // with TIniFile.Create(FIniFileName) do
  begin
    ed_DEM.FileName := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);
    ed_Clutter_Classes.FileName := ReadString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    ed_Clutter_Heights.FileName := ReadString(DEF_SECTION, ed_Clutter_Heights.Name,ed_Clutter_Heights.FileName);

    ed_Build_Heights.FileName := ReadString(DEF_SECTION, ed_Build_Heights.Name,ed_Build_Heights.FileName);


   // ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);

    ed_Projection_txt.FileName    := ReadString(DEF_SECTION, ed_Projection_txt.Name, ed_Projection_txt.FileName);

 //   cb_Set_Clutter_Height_0.Checked   := ReadBool(DEF_SECTION, cb_Set_Clutter_Height_0.Name, cb_Set_Clutter_Height_0.Checked);

   // ed_RLF.FileName  := ReadString(DEF_SECTION, ed_RLF.Name,ed_RLF.FileName);

    cb_Clutter_Classes.checked:= ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_Heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);
                                           
    cb_Clutter_building_heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_building_heights.Name, cb_Clutter_building_heights.checked);


   // Free;
  end;


  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);


  FreeAndNil(oIniFile);

end;


// ---------------------------------------------------------------
procedure Tframe_BIL_to_RLF.Run;
// ---------------------------------------------------------------
begin

  FTerminated := False;

  //btn_Run.Action := act_Stop;

(*  case cb_Proj.ItemIndex of
    0: Fbil_to_RLF.Projection :=ptUTM;
    1: Fbil_to_RLF.Projection :=ptGauss;
  end;
*)

 // Fbil_to_RLF.Params.UTM_zone1:= AsInteger(ed_UTM_Zone.Text);

  Fbil_to_RLF.Params.Projection_Txt_fileName := ed_Projection_txt.FileName;


  Fbil_to_RLF.Params.Dem_FileName             := ed_DEM.fileName;
  Fbil_to_RLF.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;
  Fbil_to_RLF.Params.Clutter_Heights_FileName := ed_Clutter_Heights.FileName;
  Fbil_to_RLF.Params.Build_Heights_FileName   := ed_Build_Heights.FileName;

  Fbil_to_RLF.Params.Clutters_section    := frame_Clutter_1.GetSection();

//  Fbil_to_RLF.Params.Is_Set_Clutter_Height_0 := cb_Set_Clutter_Height_0.checked;

  Fbil_to_RLF.Params.Rlf_FileName         := frm_RLF1.ed_RlF.FileName;
  Fbil_to_RLF.Params.Rlf_Step             := frm_RLF1.GetStepM ;

  Fbil_to_RLF.Run;


//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;

// ---------------------------------------------------------------
procedure Tframe_BIL_to_RLF.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);
    WriteString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    WriteString(DEF_SECTION, ed_Clutter_Heights.Name, ed_Clutter_Heights.FileName);
    WriteString(DEF_SECTION, ed_Build_Heights.Name, ed_Build_Heights.FileName);

  //  WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);

    WriteString(DEF_SECTION, ed_Projection_txt.Name, ed_Projection_txt.FileName);

 ////   WriteBool(DEF_SECTION, cb_Set_Clutter_Height_0.Name, cb_Set_Clutter_Height_0.Checked);

 //   WriteString(DEF_SECTION, ed_RLF.Name, ed_RLF.FileName);

    WriteBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);


    WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);

    WriteBool(DEF_SECTION, cb_Clutter_building_heights.Name, cb_Clutter_building_heights.checked);


   // Free;
  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;






end.


