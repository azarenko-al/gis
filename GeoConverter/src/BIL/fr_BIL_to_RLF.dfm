inherited frame_BIL_to_RLF: Tframe_BIL_to_RLF
  Left = 1559
  Top = 345
  Caption = 'frame_BIL_to_RLF  index'
  ClientHeight = 612
  ClientWidth = 585
  OnDestroy = FormDestroy
  ExplicitLeft = 1559
  ExplicitTop = 345
  ExplicitWidth = 593
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 596
    Width = 585
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 596
    ExplicitWidth = 585
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 105
    Width = 585
    Height = 216
    Align = alTop
    TabOrder = 1
    DesignSize = (
      585
      216)
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 12
      Width = 300
      Height = 17
      Caption = 'Clutter_Classes'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object ed_Clutter_Classes: TFilenameEdit
      Left = 8
      Top = 31
      Width = 569
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'BIL (*.bil)|*.bil'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = '"S:\-- tasks --\bil\Town\Clutter\kl5.bil"'
    end
    object cb_Clutter_Heights: TCheckBox
      Left = 8
      Top = 61
      Width = 300
      Height = 17
      Caption = 'Clutter_Heights'
      TabOrder = 2
    end
    object ed_Clutter_Heights: TFilenameEdit
      Left = 8
      Top = 79
      Width = 569
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'BIL (*.bil)|*.bil'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 3
      Text = ''
    end
    object ed_Build_Heights: TFilenameEdit
      Left = 8
      Top = 131
      Width = 569
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'BIL (*.bil)|*.bil'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 4
      Text = '"S:\-- tasks --\bil\Town\Heights\hh5.bil"'
    end
    object cb_Clutter_building_heights: TCheckBox
      Left = 8
      Top = 111
      Width = 300
      Height = 17
      Caption = #1042#1099#1089#1086#1090#1099' '#1089#1090#1088#1086#1077#1085#1080#1081
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    inline frame_Clutter_1: Tframe_Clutter_
      Left = 2
      Top = 164
      Width = 581
      Height = 50
      Align = alBottom
      Constraints.MaxHeight = 50
      TabOrder = 6
      ExplicitLeft = 2
      ExplicitTop = 164
      ExplicitWidth = 581
      ExplicitHeight = 50
      inherited Label1: TLabel
        Width = 144
        ExplicitWidth = 144
      end
      inherited Bevel2: TBevel
        Width = 581
        ExplicitWidth = 582
      end
    end
  end
  inline frm_RLF1: Tframe_RLF_ [2]
    Left = 0
    Top = 521
    Width = 585
    Height = 75
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 480
    ExplicitWidth = 585
    ExplicitHeight = 75
    DesignSize = (
      585
      75)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 585
      ExplicitWidth = 585
    end
    inherited ed_RLF: TFilenameEdit
      Width = 573
      ExplicitWidth = 573
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 585
    Height = 105
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    DesignSize = (
      585
      105)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 65
      Height = 13
      Caption = 'DEM '#1088#1077#1083#1100#1077#1092
    end
    object lb_Projection_file: TLabel
      Left = 8
      Top = 56
      Width = 63
      Height = 13
      Caption = 'Projection file'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 568
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'BIL (*.bil)|*.bil'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = '"S:\-- tasks --\bil\Town\Heights\hh5.bil"'
    end
    object ed_Projection_txt: TFilenameEdit
      Left = 8
      Top = 72
      Width = 568
      Height = 21
      Filter = 'INI (*.txt)|*.txt'
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = '"S:\-- tasks --\bil\Town\projection.txt"'
    end
  end
  object cb_Set_Clutter_Height_011111111: TCheckBox
    Left = 8
    Top = 334
    Width = 404
    Height = 17
    Caption = #1045#1089#1083#1080' '#1074#1099#1089#1086#1090#1072' '#1079#1076#1072#1085#1080#1103' ONE_BUILD=0 -> '#1091#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1080#1087' = OPEN'
    TabOrder = 3
    Visible = False
  end
  inherited ActionList_custom: TActionList
    Left = 448
    Top = 328
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
  end
end
