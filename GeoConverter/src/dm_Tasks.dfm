object dmTasks: TdmTasks
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1551
  Top = 217
  Height = 268
  Width = 414
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\GI' +
      'S\GeoConverter_XE\bin\GeoConverter.tasks.mdb;Mode=Share Deny Non' +
      'e;Persist Security Info=False;Jet OLEDB:System database="";Jet O' +
      'LEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:E' +
      'ngine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global ' +
      'Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLED' +
      'B:New Database Password="";Jet OLEDB:Create System Database=Fals' +
      'e;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Copy Locale o' +
      'n Compact=False;Jet OLEDB:Compact Without Replica Repair=False;J' +
      'et OLEDB:SFP=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 48
    Top = 12
  end
  object t_Task: TADOTable
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableDirect = True
    TableName = 'Task'
    Left = 136
    Top = 80
  end
  object ds_Tasks: TDataSource
    DataSet = t_Task
    Left = 136
    Top = 136
  end
  object cxShellBrowserDialog1: TcxShellBrowserDialog
    Left = 216
    Top = 8
  end
  object t_Project: TADOTable
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableDirect = True
    TableName = 'Project'
    Left = 45
    Top = 82
  end
  object ds_Project: TDataSource
    DataSet = t_Project
    Left = 45
    Top = 138
  end
  object q_Tasks_checked: TADOQuery
    Connection = ADOConnection1
    DataSource = ds_Project
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select * from Task where project_id=:id')
    Left = 227
    Top = 80
  end
end
