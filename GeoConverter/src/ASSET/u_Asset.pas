unit u_Asset;

interface
uses

  System.Generics.Defaults,  Dialogs, System.Generics.Collections,
  Classes,SysUtils,IniFiles, Forms, StrUtils, IOUtils,  CodeSiteLogging,

//  u_config_ini,

  u_GDAL_classes,
  u_GDAL_bat,

  u_files,
  u_func,
  u_func_arr
  ;

type
  TMinMaxRect= record
                  Lat_min: Double;
                  Lat_max: double;

                  Lon_min: Double;
                  Lon_max: double;
               end;

  //-----------------------------------------------------
  TAsset_index_record = class
  //-----------------------------------------------------
  private
   protected
    RawString  : string;
    GroupIndex: Integer;

  public
    FileName      : string;
    ShortFileName : string;

    Bounds    : TMinMaxRect;
    CellSize  : integer;

    procedure LoadFromString(aValue: string);
    procedure SaveHDR(aFileName, aPROJCS: string; aZone_UTM: integer; aNull_value:
        smallint);
  end;


  //-----------------------------------------------------
  TAsset_index_file = class(TObjectList<TAsset_index_record>)
  //-----------------------------------------------------
  private
    FFileName: string;
    FZone_UTM: integer;
    FGroups: TStringList;

    Bounds: TMinMaxRect;

//    procedure Create_Empty_File(aFileName: string; aRect: TMinMaxRect; aCellSize:   Integer);
  public

  type
    TSaveToBAT_rec = record
      FileName: string;
      PROJCS:   string;
      Zone:     Integer;
      NullValue:  Smallint;
    end;

  public

    Groups_ex: TStringList;

    constructor Create;
    destructor Destroy; override;

    function AddItem: TAsset_index_record;

    procedure LoadFromFile(aFileName: string);
    procedure ParseToFiles;

    procedure SaveToBAT(aRec: TSaveToBAT_rec);
    procedure SaveToBAT_part1(aRec: TSaveToBAT_rec);
    procedure SaveToBAT_for_bounds(aRec: TSaveToBAT_rec);
  end;


  //-----------------------------------------------------
  TAsset_index_record_list = class(TObjectList<TAsset_index_record>)
  //-----------------------------------------------------
  private
    CellSize: Integer;
  
    procedure SaveToFiles_ByFileSize(aDir, aGroupName: string);

  public
    Groups: TStringList;

    constructor Create;
    destructor Destroy; override;

    procedure AddItem(aValue: string);

  end;



   procedure TAsset_index_file_Test;



implementation



// ---------------------------------------------------------------
procedure TAsset_index_file.SaveToBAT(aRec: TSaveToBAT_rec);
// ---------------------------------------------------------------
//const
//  MyArray: TArray<String> = ['First','Second','Third'];

var
  I: Integer;

  oBAT: TGDAL_Bat;

  sFileDir: string;
  strArr: TStrArray;

  rect: TMinMaxRect;

  oItem: TAsset_index_record;
//  sFileName: string;
  sParam: string;

  iEPSG: integer;

  s: string;
  sFile: string;

begin
  if aRec.Zone>0 then
    FZone_UTM:=aRec.Zone;



  Assert(FZone_UTM>0, 'FZone_UTM>0');

  iEPSG:=28400 + FZone_UTM - 30;

//--EPSG:28408

  oBAT:=TGDAL_Bat.Create;
  oBAT.Init_Header;

//
//  oBAT.Add('del all_*');
//  oBAT.Add('del *.envi');
//  oBAT.Add('del *.tif');
//  oBAT.Add('del *.json');


{
      if g_Config.Debug.IsMakeJSON then
      begin
        sFileNoExt:=  ExtractFileNameNoExt( oItem.ShortFileName);

        sParam:= Format('gdal_translate.exe  %s   %s.tif ', [  oItem.ShortFileName, sFileNoExt ]);
        oBAT.Add(sParam);


        sParam:= Format('gdalinfo.exe -json  %s >  %s.json ', [ oItem.ShortFileName, sFileNoExt ]);
        oBAT.Add(sParam);

      end;
   }

  for I := 0 to Count - 1 do
  begin
    oBAT.Add('');
    oItem:=Items[i];
    oItem.SaveHDR (oItem.FileName, aRec.PROJCS, FZone_UTM, aRec.NullValue);

    sFile:= ExtractShortPathName( oItem.ShortFileName);
    sFile:= ExtractFileNameNoExt ( oItem.ShortFileName);

    //-of envi

    sParam:= Format('gdalinfo.exe -json  %s >  %s.json ', [ oItem.ShortFileName, sFile ]);
    oBAT.Add(sParam);

  end;



  oBAT.Add('');
  //  oBAT.Add('del *.hdr');
  //  oBAT.Add('del *.envi');
 // oBAT.Add('');



  oBAT.Add(Format('gdalbuildvrt.exe  -vrtnodata %d  -overwrite  all_wgs.vrt  *.envi ', [aRec.NullValue]));

//  oBAT.Add('gdalbuildvrt.exe   -vrtnodata -3624  -overwrite  all_wgs.vrt  *.envi   ');
  oBAT.Add('gdalinfo.exe -json all_wgs.vrt > all_wgs.vrt.json');

  //oBAT.Add( 'gdalbuildvrt.exe  -overwrite  all_wgs.vrt  *.envi   150m/*.envi   17m/*.envi ');
  //oBAT.Add('gdalbuildvrt  -overwrite  all_wgs.vrt  *_wgs.tif');

  oBAT.Add('gdalwarp.exe  all_wgs.vrt all_wgs_.tif');


  oBAT.Add('gdalwarp.exe '+ Format('-t_srs EPSG:%d  all_wgs_.tif all_pulkovo.tif', [iEPSG]));
  oBAT.Add('gdal_translate.exe  -of envi   all_pulkovo.tif all_pulkovo.envi');
  oBAT.Add('gdalinfo.exe -json all_pulkovo.envi > all_pulkovo.json');


  oBAT.SaveToFile(ChangeFileExt(aRec.FileName,'.bat') , TEncoding.GetEncoding(866));

  FreeAndNil(oBAT);

// gdal_translate.exe   -a_srs  EPSG:3395  -of GTiff   -a_ullr 298297 318092 6661044 6685834  "1.envi" "2222.tif"


end;


// ---------------------------------------------------------------
procedure TAsset_index_file.SaveToBAT_part1(aRec: TSaveToBAT_rec);
// ---------------------------------------------------------------
//const
//  MyArray: TArray<String> = ['First','Second','Third'];

var
  I: Integer;

  oBAT: TGDAL_Bat;

  oSList: TStringList;

  sFileDir: string;
  strArr: TStrArray;

  rect: TMinMaxRect;

  oItem: TAsset_index_record;
  sFileName: string;
  sParam: string;

  iEPSG: integer;

  s: string;
 // sDest: string;
  sFile: string;
  sFileNoExt: string;

begin
  oBAT:=TGDAL_Bat.Create;
  oBAT.Init_Header;

  oSList:=TStringList.Create;


  if aRec.Zone>0 then
    FZone_UTM:=aRec.Zone;

  Assert(FZone_UTM>0, 'FZone_UTM>0');

  iEPSG:=28400 + FZone_UTM - 30;



//  oBAT.Add('del all.tif');

//  oBAT.Add('del *.envi');
//  oBAT.Add('del *.tif');
//  oBAT.Add('del *.json');


  for I := 0 to Count - 1 do
  begin
   // oBAT.Add('');

    oItem:=Items[i];
    oItem.SaveHDR (oItem.FileName, aRec.PROJCS, FZone_UTM, aRec.NullValue);

    oSList.Add (oItem.ShortFileName);

    // ----------------------------------------------------------------------------
//    sDest:=ChangeFileExt(oItem.ShortFileName, '.envi');
//    sDest:=oItem.ShortFileName;


//    if not FileExists(sDest) then
//    begin
//*.envi   /K /D
    //  s:=  oItem.ShortFileName;

//      sParam:= Format('xcopy  %s  *.envi /K /D', [ oItem.ShortFileName ]);
//      oBAT.Add(sParam);

{
      if g_Config.Debug.IsMakeJSON then
      begin
        sFileNoExt:=  ExtractFileNameNoExt( oItem.ShortFileName);

        sParam:= Format('gdal_translate.exe  %s   %s.tif ', [  oItem.ShortFileName, sFileNoExt ]);
        oBAT.Add(sParam);


        sParam:= Format('gdalinfo.exe -json  %s >  %s.json ', [ oItem.ShortFileName, sFileNoExt ]);
        oBAT.Add(sParam);

      end;
 }


 //   end;

  end;



  oBAT.Add('');
//  oBAT.Add('del *.hdr');
//  oBAT.Add('del *.envi');
  oBAT.Add('');

//gdalbuildvrt.exe   -vrtnodata -3624  -input_file_list index_108.files   -overwrite  all.vrt
//gdalinfo.exe -json all.vrt > all.vrt.json

                  //     oBAT.SaveToFile(ChangeFileExt(aFileName,'.bat') , TEncoding.GetEncoding(866));

oSList.SaveToFile(ExtractFilePath(aRec.FileName) + 'all.files' , TEncoding.GetEncoding(866) );

//     -r {nearest (default),bilinear,cubic,cubicspline,lanczos,average,mode}
//--Select a resampling algorithm.

  oBAT.Add(Format('gdalbuildvrt.exe   -vrtnodata %d  -overwrite  all.vrt  -input_file_list  all.files ',[arec.NullValue])  );
  //oBAT.Add('gdalbuildvrt.exe   -vrtnodata -3624  -overwrite  all.vrt  -input_file_list  all.files '  );
  oBAT.Add('gdalinfo.exe -json all.vrt > all.vrt.json');

  oBAT.Add('rem gdal_translate.exe all.vrt all.tif');



//oBAT.Add( 'gdalbuildvrt.exe  -overwrite  all_wgs.vrt  *.envi   150m/*.envi   17m/*.envi ');
//oBAT.Add('gdalbuildvrt  -overwrite  all_wgs.vrt  *_wgs.tif');


{
oBAT.Add('gdalwarp.exe  all_wgs.vrt all_wgs_.tif');


oBAT.Add('gdalwarp.exe '+ Format('-t_srs EPSG:%d  all_wgs_.tif all_pulkovo.tif',[iEPSG]));
oBAT.Add('gdal_translate.exe  -of envi   all_pulkovo.tif all_pulkovo.envi');
oBAT.Add('gdalinfo.exe -json all_pulkovo.envi > all_pulkovo.json');
}


  oBAT.SaveToFile(ChangeFileExt(aRec.FileName,'.bat') , TEncoding.GetEncoding(866));

  FreeAndNil(oBAT);

// gdal_translate.exe   -a_srs  EPSG:3395  -of GTiff   -a_ullr 298297 318092 6661044 6685834  "1.envi" "2222.tif"


end;



// ---------------------------------------------------------------
procedure TAsset_index_file.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCellSize: Integer;
  oStrList: TStringList;


  sFileDir: string;
  strArr: TStrArray;

  rect: TMinMaxRect;

  oItem: TAsset_index_record;
  sFileName: string;

//  arr: TArray<String>;

begin
  Clear;

  sFileDir := ExtractFilePath(aFileName);

   
// gdal_translate.exe   -a_srs  EPSG:3395  -of GTiff   -a_ullr 298297 318092 6661044 6685834  "1.envi" "2222.tif" 
   
  FFileName:=aFileName;
   

  oStrList := TStringList.Create;
  oStrList.LoadFromFile(aFileName);

/////////  oStrList.Sort;


  for I := 0 to oStrList.Count - 1 do
  begin
    if tRIM(oStrList[i])='' then
      Continue;

  
    strArr :=StringToStrArray(oStrList[i], ' ');

    sFileName := sFileDir + strArr[0];


//    if not FileExists(sFileName) then
//      Continue;


//    if not FileExists(sFileName) then
//      Continue;

    oItem:=AddItem();

    oItem.RawString    :=oStrList[i];
    oItem.FileName     :=sFileName;
    oItem.ShortFileName:=strArr[0];

//    oItem.FileName := sFileDir + strArr[0];

    //TADJIKISTAN_clut_1_1.bin 355150.0 605150.0 4296550.0 4546550.0 50.0
    oItem.Bounds.Lon_min := AsFloat(strArr[1]);
    oItem.Bounds.Lon_max := AsFloat(strArr[2]);
    
    oItem.Bounds.Lat_min := AsFloat(strArr[3]);
    oItem.Bounds.Lat_max := AsFloat(strArr[4]);

    oItem.CellSize:= AsInteger(strArr[5]);

  end;


  FreeAndNil(oStrList);

end;



function TAsset_index_file.AddItem: TAsset_index_record;
begin
  Result := TAsset_index_record.Create;

  Add(Result);
end;


// ---------------------------------------------------------------
procedure TAsset_index_record.LoadFromString(aValue: string);
// ---------------------------------------------------------------
var
  strArr: TStrArray;

begin
  strArr :=StringToStrArray(aValue, ' ');

  RawString    :=aValue;
  ShortFileName:=strArr[0];

  //TADJIKISTAN_clut_1_1.bin 355150.0 605150.0 4296550.0 4546550.0 50.0
  Bounds.Lon_min := AsFloat(strArr[1]);
  Bounds.Lon_max := AsFloat(strArr[2]);
  Bounds.Lat_min := AsFloat(strArr[3]);
  Bounds.Lat_max := AsFloat(strArr[4]);

  CellSize:= AsInteger(strArr[5]);                       
  
end;

// ---------------------------------------------------------------
procedure TAsset_index_record.SaveHDR(aFileName, aPROJCS: string; aZone_UTM:  integer; aNull_value: smallint);
// ---------------------------------------------------------------
var
  iColCount: word;
  iRowCount: word;
  s: string;

  rEnvi_HDR: TGDAL_Envi_HDR;

  sAll: string;
begin
//  if FileExists(ChangeFileExt(aFileName,'.hdr')) then
//    Exit;


  CodeSite.Send('procedure TAsset_index_record.SaveHDR( - '+ aFileName);


  iRowCount:=Trunc((Bounds.Lat_max - Bounds.Lat_min) / CellSize);
  iColCount:=Trunc((Bounds.Lon_max - Bounds.Lon_min) / CellSize);

  Assert (iRowCount>0);
  Assert (iColCount>0);


  rEnvi_HDR.ColCount:=iColCount;
  rEnvi_HDR.RowCount:=iRowCount;
  rEnvi_HDR.DataType:=2;

  rEnvi_HDR.Lon_min:=Bounds.Lon_min;
  rEnvi_HDR.Lat_max:=Bounds.Lat_max;

  rEnvi_HDR.CellSize:=CellSize;
  rEnvi_HDR.Zone_UTM    :=aZone_UTM;

  rEnvi_HDR.PROJCS:=aPROJCS;
  rEnvi_HDR.Null_value :=aNull_value;

  rEnvi_HDR.SaveToFile(ChangeFileExt(aFileName,'.hdr'));

end;


constructor TAsset_index_file.Create;
begin
  inherited Create;
  FGroups := TStringList.Create();
  Groups_ex := TStringList.Create();
  
end;


destructor TAsset_index_file.Destroy;
begin
  FreeAndNil(FGroups);
  FreeAndNil(Groups_ex);
  inherited Destroy;
end;


// ---------------------------------------------------------------
procedure TAsset_index_file.ParseToFiles;
// ---------------------------------------------------------------
var
  I: Integer;
  iInd: Integer;
  j: Integer;
  k: Integer;
  oRecordList: TAsset_index_record_list;
  s: string;
  sDir: string;
  strArr: TArray<string>;
  sName: string;

begin

  Assert(FFileName<>'');

  oRecordList:= TAsset_index_record_list.Create;
 
 
  FGroups.Clear;
  Groups_ex.Clear;
 
  
//  oSList:=TStringList.Create;

  
  s   :=ExtractFileDir(FFileName);
  sDir:=ExtractFileName(s);


//  s.spl
  
  //-----------------------------------
  
  for I := 0 to Count-1 do
  begin
     s:=ChangeFileExt( Items[i].ShortFileName, '');
     s:=s.Replace('__','_');
     strArr := s.Split(['_']);
      
    sName:='';
    
    
    for j := 0 to High(strArr) do
    begin
      s:=LowerCase(strArr[j]);

     // s.CompareTo()
      
      if Eq(s, sDir) then
        Continue;
      
      if Eq(s, 'dlu') or Eq(s, 'DTM') or Eq(s, 'DHM') then
        Continue;

      
//DHM_05m_Iskitim_01-01.bin 642390 645000 6060000 6063905 5

      if (s.Length=2 ) and (s[1] in ['a'..'z'])
                       and (s[2] in ['0'..'9']) 
        then Break;

      if (s.Length=1 ) and (s[1] in ['a'..'z'])                    
        then Break;
       
      if (s.Length=1 ) and (s[1] in ['0'..'9'])                    
        then Break;


     try
      
      if ((RightStr(s,1)='m') and (s[1] in ['0'..'9']) )
          or
         (s[1] in ['a'..'z'])
      then
        begin
          sName:=sName+ IIF(sName<>'', '_'+ s, s);
        
        
        end else
          Break;

     
     except
       ShowMessage(s) ;

     end;
          
    end;
                      
    
    // ----------------------------
    iInd:=FGroups.IndexOf(sName);
    if iInd<0 then
      iInd:=FGroups.AddObject(sName, TObject(Items[i].CellSize));
        
    Items[i].GroupIndex:=iInd;
            
  end;

  //------------------------------------------------

  
  for I := 0 to FGroups.Count-1 do
  begin
    oRecordList.Clear;
    oRecordList.Groups.Clear;


    for j := 0 to Count-1 do
      if Items[j].GroupIndex = i then 
      begin
  Assert(Items[j].RawString<>'');
  
        oRecordList.AddItem(Items[j].RawString);

      end;

    oRecordList.SaveToFiles_ByFileSize (ExtractFilePath(FFileName), FGroups[i]);

    Groups_ex.AddStrings (oRecordList.Groups);

  end;

  FreeAndNil(oRecordList);

   
end;

// ---------------------------------------------------------------
procedure TAsset_index_file.SaveToBAT_for_bounds(aRec: TSaveToBAT_rec);
// ---------------------------------------------------------------
var
  I: Integer;
  oBAT: TGDAL_Bat;
  strArr: TStrArray;

  oItem: TAsset_index_record;

  s: string;
  sFileNoExt: string;

begin
  if aRec.Zone>0 then
    FZone_UTM:=aRec.Zone;

  Assert(FZone_UTM>0, 'FZone_UTM>0');


  oBAT:=TGDAL_Bat.Create;
//  oBAT.Init_Header;


  for I := 0 to Count - 1 do
  begin
    oItem:=Items[i];
    oItem.SaveHDR (oItem.FileName, aRec.PROJCS, FZone_UTM, aRec.NullValue);

    sFileNoExt:= ExtractFileNameNoExt ( oItem.ShortFileName);

    s:= Format('gdalinfo.exe -json  %s >  %s.json ', [ oItem.ShortFileName, sFileNoExt ]);
    oBAT.Add(s);


    s:= Format('gdal_translate.exe -co compress=lzw   %s   %s.tif ', [  oItem.ShortFileName, sFileNoExt ]);
    oBAT.Add(s);

  end;


  oBAT.SaveToFile(ChangeFileExt(aRec.FileName,'.bat') , TEncoding.GetEncoding(866));

  FreeAndNil(oBAT);

end;



//=========================================================================================
procedure TAsset_index_file_Test;
//=========================================================================================
//const
//  DEF_INDEX_FILE = 'W:\GIS\GeoConverter_XE\Data\megafon\dal\index.txt';
  
var
  oFile: TAsset_index_file;
  s: string;
  sFile: string;
begin
//  s:=ExtractFileDir('P:\_mts\test1 ������� ����\DHM\index.txt');

  //s:=ExtractFileName(s);


//'P:\_mts\test1 ������� ����\DHM\index.txt'

  oFile:=TAsset_index_file.Create;


                                                        
//  oFile.LoadFromFile('P:\_megafon\test_Novosib\clutter\index.txt');
  
  oFile.LoadFromFile('P:\_megafon\test_Mos\height\index.txt');
//  oFile.LoadFromFile('P:\_megafon\test_Mos\clutter\index.txt');

 
  
//  oFile.LoadFromFile('W:\GIS\GeoConverter_XE\Data\megafon\dal\index.txt');
  oFile.ParseToFiles;

  FreeAndNil(oFile);

  
 // oFile.SaveToBAT('W:\GIS\GeoConverter_XE\Data\megafon\dal\index.bat');
  
//  oFile.LoadFromFile('P:\_megafon\NENETZ\Height\4.txt');

//  sFile:='W:\GIS\GeoConverter_XE\Data\megafon\dal\index.bat';
//  sFile:='P:\_megafon\NENETZ\Height\4.bat';
                   
//  RunApp (sFile,'');  
//
//
//  
//  oFile.LoadFromFile('P:\_mts\test1 ������� ����\DHM\index.txt');
//  oFile.ParseToFiles;
//  
//  oFile.SaveToBAT('P:\_mts\test1 ������� ����\DHM\index.bat');
//
//  
//  FreeAndNil(oFile);
  
end;


constructor TAsset_index_record_list.Create;
begin
  inherited Create;
  Groups := TStringList.Create();
end;


destructor TAsset_index_record_list.Destroy;
begin
  FreeAndNil(Groups);
  inherited Destroy;
end;


// ---------------------------------------------------------------
procedure TAsset_index_record_list.AddItem(aValue: string);
// ---------------------------------------------------------------
var
  r: TAsset_index_record;
begin
  Assert(aValue<>'');

  r := TAsset_index_record.Create;
  r.LoadFromString(aValue);

  Add(r);

end;




// ---------------------------------------------------------------
procedure TAsset_index_record_list.SaveToFiles_ByFileSize(aDir, aGroupName:
    string);
// ---------------------------------------------------------------
var
  i: Integer;
  iCellSize: Integer;
  iMax: Integer;

  oSList: TStringList;
  
  r: TMinMaxRect;
  s: string;
 
  iPart : word;  
  sGroupName: string;
begin
  //Sort_;

  Assert(Count>0);

  iPart := 0;

  iCellSize:= Items[0].CellSize;


  oSList:=TStringList.Create;

  //--------------------

//  oSList.Add(GetItem[0].RawString);

  while Count > 0 do
  begin
    oSList.Clear;

    oSList.Add(Items[0].RawString);
//    iSizeSum:=GetSizeSum(0);

//CodeSite.Send(Items[0].RawString);
//CodeSite.Send(Format('%n',[1.0*iSizeSum]) );

    iMax:=1;  //!!!

    for i := 1 to Count-1 do
    begin

  //    iSizeSum:=GetSizeSum(i);

//CodeSite.Send(Items[i].RawString);
//CodeSite.Send(Format('%n',[1.0*iSizeSum]) );

{
      assert (g_Config.MaxSize > 0);


      if iSizeSum > g_Config.MaxSize then // 2000000000 then //1 000 000 000
        break;
 }

      oSList.Add(Items[i].RawString);
      Inc(iMax);  
    end;                

  //  Assert(iMaxIndex<=Count);

    for i:=0 to iMax-1  do
        Delete(0);


    if (iPart>0) or (Count>0)  then
      sGroupName:=aGroupName + Format('_part_%d',[iPart+1])
    else
      sGroupName:=aGroupName;


//    if iPart=0 then
//      s:=aDir + 'index_'+ aGroupName +'.txt'
//    else

    s:=aDir + 'index_'+ sGroupName + '.txt';

//




//CodeSite.Send(s);
//CodeSite.Send(Format('%n',[1.0*iSizeSum]) );

//CodeSite.Send(IntToStr(iSizeSum) );
//CodeSite.Send('');

    oSList.Sort;
    oSList.SaveToFile(s);

    Groups.AddObject (sGroupName, Pointer(iCellSize));

    Inc (iPart);
  end;


//  s:=aDir + 'index_'+ aGroupName +'.txt';

  //--------------------

  FreeAndNil(oSList);


//  clutter_berdsk_5m.pla 630320 639420 6065720 6074900 5
//clutter_iskitim_5m.pla 645385 651605 6045960 6059940 5
//clutter_novosibirsk_5m_dop_A1.bil 603627 638412 6103623 6118578 5
//clutter_novosibirsk_5m_dop_B1.bil 603627 638412 6088668 6103623 5
//clutter_novosibirsk_5m_dop_C1.bil 603627 638412 6073713 6088668 5
//clutter_novosibirskaya_obl_25m_E2.bil 179168 308868 5909312 6000537 25
//clutter_novosibirskaya_obl_25m_B1.bil 49468 179168 6182987 6274212 25
//clutter_novosibirskaya_obl_25m_D6.bil 697968 827668 6000537 6091762 25
//clutter_novosibirskaya_obl_25m_B3.bil 308868 438568 6182987 6274212 25
//clutter_novosibirskaya_obl_25m_E3.bil 308868 438568 5909312 6000537 25


end;





end.




  {
  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile));

  // ---------------------------------------------------------------

  oIni:=TIniFile.Create(sFile);
  sGDAL_rel_Path:=oIni.ReadString('main','path','');
  FreeAndNil(oIni);

  // ---------------------------------------------------------------
//  sPath:=IncludeTrailingBackslash(sPath);

  sPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sGDAL_rel_Path );
  sPath_bin:=sPath + 'bin\';


  if aZone>0 then
    FZone:=aZone;

  Assert(FZone>0, 'FZone>0');

  iEPSG:=28400 + FZone - 30;


//--EPSG:28408

  oBAT:=TStringList.Create;

  //set path=W:\GIS\GeoConverter_XE\bin\OSGeo4W\bin\

//s:='set path='+ sPath_bin + ';'+ ExtractFilePath(aFileName);

  oBAT.Add('set path='+ sPath_bin + ';'+ ExtractFilePath(aFileName));

  oBAT.Add(Format('set GDAL_DATA=%s',[sPath])+ 'share\epsg_csv');
  oBAT.Add('set GDAL_FILENAME_IS_UTF8=NO');

  }

 {


// ---------------------------------------------------------------
procedure TAsset_index_record_list.Sort_;
// ---------------------------------------------------------------
var
  i: Integer;
begin

    Sort( TComparer<TAsset_index_record>.Construct(
            function(const aLeft, aRight: TAsset_index_record): Integer
            begin
              Result := 0;

              Assert (Assigned(aLeft));
              Assert (Assigned(aRight));



              if (aLeft.Bounds.Lon_min = aRight.Bounds.Lon_min) and
                 (aLeft.Bounds.Lat_max <= aRight.Bounds.Lat_max)
              then
                Exit;

              if (aLeft.Bounds.Lon_min < aRight.Bounds.Lon_min) //and
//                 (aLeft.Bounds.Lat_max <= aRight.Bounds.Lat_max)
              then
                Result := -1
              else
         //       if (aLeft.Bounds.Lat_min < aRight.Bounds.Lat_min) then
          //        Result := -1
           //     else
                  Result := 1

               // aLeft.Bounds.Lon_min - aRight.Bounds.Lon_min;
            end
          ) );

end;


// ---------------------------------------------------------------
function TAsset_index_record_list.GetSizeSum(aMaxIndex: Integer): int64;
// ---------------------------------------------------------------
var
  I: Integer;
  iCellSize: Integer;
  iCols: Integer;
  iRows: Integer;
  oSList: TStringList;

  r: TMinMaxRect;
begin
  Assert(Count>0);


  iCellSize:= Items[0].CellSize;
  r:=Items[0].Bounds;


  for I := 1 to aMaxIndex do
  begin
    if r.Lat_min > Items[i].Bounds.Lat_min then  r.Lat_min := Items[i].Bounds.Lat_min;
    if r.Lat_max < Items[i].Bounds.Lat_max then  r.Lat_max := Items[i].Bounds.Lat_max;

    if r.Lon_min > Items[i].Bounds.Lon_min then  r.Lon_min := Items[i].Bounds.Lon_min;
    if r.Lon_max < Items[i].Bounds.Lon_max then  r.Lon_max := Items[i].Bounds.Lon_max;

  end;

  iRows:= Trunc( (r.Lat_max - r.Lat_min ) / iCellSize);
  iCols:= Trunc( (r.Lon_max - r.Lon_min ) / iCellSize);


  try
    Result:= int64(1) * iRows * iCols * 4;
  except
  end;

end;



// ---------------------------------------------------------------
procedure TAsset_index_file.Create_Empty_File(aFileName: string; aRect:  TMinMaxRect; aCellSize: Integer);
// ---------------------------------------------------------------

    function MacWordToPC(Value : word): word;
      asm xchg Al,Ah
    end;

var
  c: Integer;
  iCols: Integer;
  iRows: Integer;
  r: Integer;
  tf: TFileStream;
  oStream: TMemoryStream;
  w: word;


const
  DEF_BORDER = 5;
  DEF_BODY = 1;

 // DEF_OFFSET = 5;

//  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
//  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
//  DEF_CLU_WATER     = 2;  // 3- ���
//  DEF_CLU_COUNTRY   = 3;  // 3- ���
//  DEF_CLU_ROAD      = 4;
//  DEF_CLU_RailROAD  = 5;
//  DEF_CLU_BOLOTO_NEPROHODIMOE  = 6; // 6 - ������������ ������
//  DEF_CLU_CITY      = 7;  // 7- �����
//  DEF_CLU_BOLOTO    = 8; // ���������� ������
//  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
//  DEF_CLU_ONE_HOUSE = 31; // 31- ���
//

begin
  oStream:=TMemoryStream.Create;



  iCols:= Trunc(aRect.Lon_max - aRect.Lon_min) div aCellSize;
  iRows:= Trunc(aRect.Lat_max - aRect.Lat_min) div aCellSize;

  oStream.Size:=iRows * iCols * 2;
  Assert(oStream.Position=0);



  for r := 0  to iRows-1 do
  begin
    w:= MacWordToPC (DEF_BORDER);
    for c := 0 to iCols-1  do  oStream.Write(w, 2);
  end;



  Assert (oStream.Size = iRows * iCols * 2);


//  tf.Size := [desired value];
  ForceDirectories (ExtractFilePath(aFileName));


  tf := TFileStream.Create(aFileName, fmCreate or fmShareExclusive);

  oStream.SaveToStream(tf);
//  tf.CopyFrom(oStream, oStream.Size);

  tf.Free;

  oStream.Free;
end;

