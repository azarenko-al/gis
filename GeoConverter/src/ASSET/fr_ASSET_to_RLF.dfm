inherited frame_ASSET_to_RLF: Tframe_ASSET_to_RLF
  Left = 1186
  Top = 169
  Caption = 'frame_ASSET_to_RLF'
  ClientHeight = 992
  ClientWidth = 965
  OnDestroy = FormDestroy
  ExplicitLeft = 1186
  ExplicitTop = 169
  ExplicitWidth = 973
  ExplicitHeight = 1020
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Top = 935
    Width = 965
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 632
    ExplicitWidth = 634
    inherited Bevel1: TBevel
      Width = 965
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ExplicitWidth = 663
    end
    inherited btn_Run: TButton
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
    end
  end
  inherited ProgressBar1: TProgressBar
    Top = 976
    Width = 965
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 666
    ExplicitWidth = 634
  end
  inline frm_RLF1: Tframe_RLF_ [2]
    Left = 0
    Top = 854
    Width = 965
    Height = 81
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 544
    ExplicitWidth = 634
    ExplicitHeight = 81
    DesignSize = (
      965
      81)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 965
      ExplicitWidth = 663
    end
    inherited ed_RLF: TFilenameEdit
      Width = 947
      ExplicitWidth = 616
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 137
    Width = 965
    Height = 208
    Align = alTop
    TabOrder = 3
    ExplicitWidth = 634
    DesignSize = (
      965
      208)
    object ed_Clutter_Classes: TFilenameEdit
      Left = 7
      Top = 39
      Width = 948
      Height = 21
      Filter = 'Txt (*.txt)|*.txt'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'S:\__TASKS\TADJIKISTAN_asset\clutter\index.txt'
      ExplicitWidth = 617
    end
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 16
      Width = 400
      Height = 17
      Caption = 'Clutter_Classes'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    inline frame_Clutter_1: Tframe_Clutter_
      Left = 2
      Top = 156
      Width = 961
      Height = 50
      Align = alBottom
      Constraints.MaxHeight = 50
      TabOrder = 2
      ExplicitLeft = 2
      ExplicitTop = 212
      ExplicitWidth = 630
      inherited Label1: TLabel
        Width = 144
        ExplicitWidth = 144
      end
      inherited Bevel2: TBevel
        Width = 961
        ExplicitWidth = 660
      end
      inherited Edit1: TEdit
        Text = 'ASSET'
      end
    end
    object ed_Clutter_H: TFilenameEdit
      Left = 8
      Top = 115
      Width = 948
      Height = 21
      Filter = 'Txt (*.txt)|*.txt'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 3
      Text = 'S:\__TASKS\TADJIKISTAN_asset\clutter\index.txt'
      ExplicitWidth = 617
    end
    object cb_Clutter_H: TCheckBox
      Left = 8
      Top = 96
      Width = 209
      Height = 17
      Caption = 'Clutter_H (txt)'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 137
    Align = alTop
    BevelOuter = bvNone
    PopupMenu = PopupMenu1
    TabOrder = 4
    ExplicitWidth = 634
    DesignSize = (
      965
      137)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 65
      Height = 13
      Caption = 'DEM '#1088#1077#1083#1100#1077#1092
    end
    object lb_Projection_file: TLabel
      Left = 8
      Top = 80
      Width = 63
      Height = 13
      Caption = 'Projection file'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 953
      Height = 21
      Filter = 'Txt (*.txt)|*.txt'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'S:\__TASKS\TADJIKISTAN_asset\DTM\index.txt'
      ExplicitWidth = 622
    end
    object ed_Projection_txt: TFilenameEdit
      Left = 8
      Top = 96
      Width = 952
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = '"S:\-- tasks --\bil\Town\projection.txt"'
      ExplicitWidth = 621
    end
  end
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 560
    Top = 349
    Width = 385
    Height = 276
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    OptionsView.RowHeaderWidth = 139
    TabOrder = 5
    Version = 1
    object cxVerticalGrid1CategoryRow5: TcxCategoryRow
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow5: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      Properties.Caption = #1088#1077#1083#1100#1077#1092
      ID = 2
      ParentID = -1
      Index = 1
      Version = 1
    end
    object row_DEM: TcxEditorRow
      Properties.Caption = 'DEM '#1088#1077#1083#1100#1077#1092
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.ValueType = 'String'
      Properties.Options.ShowEditButtons = eisbAlways
      Properties.Value = 'S:\-- tasks --\bil\Town\Heights\index.txt'
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1CategoryRow2: TcxCategoryRow
      ID = 4
      ParentID = -1
      Index = 2
      Version = 1
    end
    object row_Projection_txt: TcxEditorRow
      Properties.Caption = 'Projection file'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '"S:\-- tasks --\bil\Town\projection.txt"'
      ID = 5
      ParentID = 4
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow1: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 6
      ParentID = 4
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1CategoryRow3: TcxCategoryRow
      Properties.Caption = 'Clutters'
      ID = 7
      ParentID = -1
      Index = 3
      Version = 1
    end
    object row_Clutter_Classes: TcxEditorRow
      Properties.Caption = 'row_Clutter_Classes'
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '"S:\-- tasks --\bil\Town\Clutter\kl5.bil"'
      ID = 8
      ParentID = 7
      Index = 0
      Version = 1
    end
    object row_Clutter_Classes_Use: TcxEditorRow
      Properties.Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.DataBinding.ValueType = 'Boolean'
      Properties.Value = Null
      ID = 9
      ParentID = 7
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1CategoryRow4: TcxCategoryRow
      Properties.Caption = #1042#1099#1089#1086#1090#1099' '#1089#1090#1088#1086#1077#1085#1080#1081
      ID = 10
      ParentID = -1
      Index = 4
      Version = 1
    end
    object row_Build_Heights: TcxEditorRow
      Properties.Caption = 'row_Build_Heights'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = '"S:\-- tasks --\bil\Town\Heights\hh5.bil"'
      ID = 11
      ParentID = 10
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow6: TcxEditorRow
      Properties.Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 12
      ParentID = 10
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1EditorRow3: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 13
      ParentID = -1
      Index = 5
      Version = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 24
    Top = 568
    Width = 449
    Height = 257
    TabOrder = 6
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Tasks
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGrid1DBTableView1checked: TcxGridDBColumn
        DataBinding.FieldName = 'checked'
      end
      object cxGrid1DBTableView1filename_DTM: TcxGridDBColumn
        DataBinding.FieldName = 'filename_DTM'
      end
      object cxGrid1DBTableView1filename_CLU: TcxGridDBColumn
        DataBinding.FieldName = 'filename_CLU'
      end
      object cxGrid1DBTableView1filename_CLU_H: TcxGridDBColumn
        DataBinding.FieldName = 'filename_CLU_H'
      end
      object cxGrid1DBTableView1filename_projection_txt: TcxGridDBColumn
        DataBinding.FieldName = 'filename_projection_txt'
      end
      object cxGrid1DBTableView1is_use_clutter: TcxGridDBColumn
        DataBinding.FieldName = 'is_use_clutter'
      end
      object cxGrid1DBTableView1is_use_Clutter_Heights: TcxGridDBColumn
        DataBinding.FieldName = 'is_use_Clutter_Heights'
      end
      object cxGrid1DBTableView1UTM_zone: TcxGridDBColumn
        DataBinding.FieldName = 'UTM_zone'
      end
      object cxGrid1DBTableView1Source_directory: TcxGridDBColumn
        DataBinding.FieldName = 'Source_directory'
      end
      object cxGrid1DBTableView1task_type: TcxGridDBColumn
        DataBinding.FieldName = 'task_type'
      end
      object cxGrid1DBTableView1filename_onega_rlf: TcxGridDBColumn
        DataBinding.FieldName = 'filename_onega_rlf'
      end
      object cxGrid1DBTableView1step: TcxGridDBColumn
        DataBinding.FieldName = 'step'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 520
    Top = 568
    Width = 329
    Height = 273
    OptionsView.RowHeaderWidth = 148
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 7
    DataController.DataSource = ds_Tasks
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1checked: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.DataBinding.FieldName = 'checked'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1filename_DTM: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.FieldName = 'filename_DTM'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1filename_CLU: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.FieldName = 'filename_CLU'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1filename_CLU_H: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.FieldName = 'filename_CLU_H'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1filename_projection_txt: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
      Properties.EditProperties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.DataBinding.FieldName = 'filename_projection_txt'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1is_use_clutter: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.DataBinding.FieldName = 'is_use_clutter'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1is_use_Clutter_Heights: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.DataBinding.FieldName = 'is_use_Clutter_Heights'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1UTM_zone: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'UTM_zone'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1Source_directory: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Source_directory'
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1task_type: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'task_type'
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1filename_onega_rlf: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'filename_onega_rlf'
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1step: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'step'
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
  end
  inherited ActionList_custom: TActionList
    Left = 64
    Top = 360
  end
  object PopupMenu1: TPopupMenu
    Left = 64
    Top = 408
    object actSelectfolder1: TMenuItem
    end
  end
  object cxShellBrowserDialog1111: TcxShellBrowserDialog
    Left = 64
    Top = 480
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\GI' +
      'S\GeoConverter_XE\bin\GeoConverter.tasks.mdb;Mode=Share Deny Non' +
      'e;Persist Security Info=False;Jet OLEDB:System database="";Jet O' +
      'LEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:E' +
      'ngine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global ' +
      'Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLED' +
      'B:New Database Password="";Jet OLEDB:Create System Database=Fals' +
      'e;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Copy Locale o' +
      'n Compact=False;Jet OLEDB:Compact Without Replica Repair=False;J' +
      'et OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 304
    Top = 364
  end
  object t_Task: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorLocation = clUseServer
    TableDirect = True
    TableName = 'Task'
    Left = 304
    Top = 432
  end
  object ds_Tasks: TDataSource
    DataSet = t_Task
    Left = 304
    Top = 488
  end
end
