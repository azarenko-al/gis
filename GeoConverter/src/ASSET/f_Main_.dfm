inherited frame_ASSET_to_RLF: Tframe_ASSET_to_RLF
  Left = 743
  Top = 501
  Caption = 'frame_ASSET_to_RLF'
  ClientHeight = 685
  ClientWidth = 1341
  Menu = MainMenu1
  OnDestroy = FormDestroy
  OnShow = FormShow
  ExplicitWidth = 1357
  ExplicitHeight = 744
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 650
    Width = 1341
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 650
    ExplicitWidth = 1341
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 28
    Width = 945
    Height = 622
    ActivePage = TabSheet1_Tasks
    Align = alLeft
    TabOrder = 5
    object TabSheet1_Tasks: TTabSheet
      Caption = #1047#1072#1076#1072#1085#1080#1103
      object cxGrid_tasks: TcxGrid
        Left = 0
        Top = 0
        Width = 937
        Height = 185
        Align = alTop
        TabOrder = 0
        object cxGridDBTableView1_tasks: TcxGridDBTableView
          PopupMenu = PopupMenu1
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          FilterBox.Visible = fvNever
          DataController.DataSource = ds_Tasks
          DataController.KeyFieldNames = 'id'
          DataController.Options = []
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = cxGridDBColumn3
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGridDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 40
          end
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 48
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'Dir'
            Width = 47
          end
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'filename_DTM'
            SortIndex = 0
            SortOrder = soAscending
            Width = 224
          end
          object col_is_use_clutter: TcxGridDBColumn
            DataBinding.FieldName = 'is_use_clutter'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Properties.UseAlignmentWhenInplace = True
            Width = 45
          end
          object cxGridDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'filename_CLU'
            Width = 141
          end
          object cxGridDBColumn7: TcxGridDBColumn
            DataBinding.FieldName = 'is_use_Clutter_Heights'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Properties.UseAlignmentWhenInplace = True
            Width = 53
          end
          object cxGridDBColumn8: TcxGridDBColumn
            DataBinding.FieldName = 'filename_CLU_H'
            Width = 136
          end
          object cxGridDBColumn9: TcxGridDBColumn
            DataBinding.FieldName = 'Clutter_schema'
            Width = 85
          end
          object cxGridDBColumn10: TcxGridDBColumn
            DataBinding.FieldName = 'filename_projection_txt'
            Width = 139
          end
          object cxGridDBColumn11: TcxGridDBColumn
            DataBinding.FieldName = 'UTM_zone'
          end
          object cxGridDBColumn12: TcxGridDBColumn
            DataBinding.FieldName = 'Source_directory'
            Visible = False
            Width = 98
          end
          object cxGridDBColumn13: TcxGridDBColumn
            DataBinding.FieldName = 'task_type'
            Visible = False
            Width = 52
          end
          object cxGridDBColumn14: TcxGridDBColumn
            DataBinding.FieldName = 'filename_onega_rlf'
            Width = 158
          end
          object cxGridDBColumn15: TcxGridDBColumn
            DataBinding.FieldName = 'step'
            Width = 51
          end
          object cxGridDBTableView1_tasksColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_rlf_exists'
          end
        end
        object cxGridLevel1: TcxGridLevel
          Caption = #1047#1072#1076#1072#1085#1080#1103
          GridView = cxGridDBTableView1_tasks
        end
      end
      object cxSplitter2: TcxSplitter
        Left = 0
        Top = 208
        Width = 937
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salBottom
        Control = pn_bottom
      end
      object pn_bottom: TPanel
        Left = 0
        Top = 216
        Width = 937
        Height = 378
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object cxDBVerticalGrid1: TcxDBVerticalGrid
          Left = 0
          Top = 0
          Width = 713
          Height = 378
          Align = alLeft
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          OptionsView.ScrollBars = ssVertical
          OptionsView.RowHeaderWidth = 161
          OptionsData.Appending = False
          Navigator.Buttons.CustomButtons = <>
          ParentFont = False
          TabOrder = 0
          DataController.DataSource = ds_Tasks
          Version = 1
          object cxDBVerticalGrid1id: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.DataBinding.FieldName = 'id'
            Visible = False
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1checked: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.DataBinding.FieldName = 'checked'
            Visible = False
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1Source_directory: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'Source_directory'
            Visible = False
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1DBEditorRow3EditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'Dir'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
            Visible = False
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
            Properties.Caption = #1056#1077#1083#1100#1077#1092
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_DTM: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_DTMEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_DTM'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 6
            ParentID = 5
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1filename_projection_txt: TcxDBEditorRow
            Properties.Caption = 'projection_txt'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_projection_txtEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_projection_txt'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 7
            ParentID = 5
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1UTM_zone: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.DataBinding.FieldName = 'UTM_zone'
            ID = 8
            ParentID = 5
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
            Properties.Caption = 'Clutters'
            ID = 9
            ParentID = -1
            Index = 6
            Version = 1
          end
          object cxDBVerticalGrid1is_use_clutter: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.NullStyle = nssUnchecked
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.DataBinding.FieldName = 'is_use_clutter'
            ID = 10
            ParentID = 9
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1filename_CLU: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_CLUEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_CLU'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 11
            ParentID = 9
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'clutter_schema'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 12
            ParentID = 9
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.NullStyle = nssUnchecked
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.DataBinding.FieldName = 'is_use_clutter_bounds'
            Visible = False
            ID = 13
            ParentID = 9
            Index = 3
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow3: TcxCategoryRow
            Properties.Caption = #1042#1099#1089#1086#1090#1099' '#1089#1090#1088#1086#1077#1085#1080#1081
            ID = 14
            ParentID = -1
            Index = 7
            Version = 1
          end
          object cxDBVerticalGrid1is_use_Clutter_Heights: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.NullStyle = nssUnchecked
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.DataBinding.FieldName = 'is_use_Clutter_Heights'
            ID = 15
            ParentID = 14
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1filename_CLU_H: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_CLU_HEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_CLU_H'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 16
            ParentID = 14
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1task_type: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'task_type'
            Visible = False
            ID = 17
            ParentID = -1
            Index = 8
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow4: TcxCategoryRow
            Properties.Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' - '#1052#1072#1090#1088#1080#1094#1072' '#1074#1099#1089#1086#1090' rlf'
            ID = 18
            ParentID = -1
            Index = 9
            Version = 1
          end
          object cxDBVerticalGrid1filename_onega_rlf: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = cxDBVerticalGrid1filename_onega_rlfEditPropertiesButtonClick
            Properties.DataBinding.FieldName = 'filename_onega_rlf'
            Properties.Options.ShowEditButtons = eisbAlways
            ID = 19
            ParentID = 18
            Index = 0
            Version = 1
          end
          object cxDBVerticalGrid1step: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.DataBinding.FieldName = 'step'
            ID = 20
            ParentID = 18
            Index = 1
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.DataBinding.FieldName = 'is_rlf_exists'
            Properties.Options.Editing = False
            ID = 21
            ParentID = 18
            Index = 2
            Version = 1
          end
          object cxDBVerticalGrid1CategoryRow5: TcxCategoryRow
            Properties.Caption = 'Null value'
            ID = 22
            ParentID = -1
            Index = 10
            Version = 1
          end
          object cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.DataBinding.FieldName = 'Null_value'
            ID = 23
            ParentID = 22
            Index = 0
            Version = 1
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1088#1086#1077#1082#1090#1099
      ImageIndex = 1
      object cxGrid1_projects: TcxGrid
        Left = 0
        Top = 0
        Width = 505
        Height = 594
        Align = alLeft
        TabOrder = 0
        RootLevelOptions.TabsForEmptyDetails = False
        object cxGrid1_projectsDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          DataController.DataSource = ds_Project
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1_projectsDBTableView2id_: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
          end
          object cxGrid1_projectsDBTableView2name_: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'name'
            SortIndex = 0
            SortOrder = soAscending
            Width = 126
          end
        end
        object cxGrid1_projectsLevel2: TcxGridLevel
          Caption = #1055#1088#1086#1077#1082#1090#1099
          GridView = cxGrid1_projectsDBTableView2
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      ImageIndex = 2
      object DBGrid1: TDBGrid
        Left = 16
        Top = 3
        Width = 761
        Height = 574
        DataSource = ds_Tasks
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object StatusBar1: TStatusBar [2]
    Left = 0
    Top = 666
    Width = 1341
    Height = 19
    Panels = <>
  end
  inherited ActionList_custom: TActionList
    Left = 1008
    Top = 136
  end
  object PopupMenu1: TPopupMenu
    Left = 1008
    Top = 184
    object Run1: TMenuItem
      Action = act_Run
    end
    object actRunall1: TMenuItem
      Action = act_Run_all
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object actSelectfolder1: TMenuItem
      Action = act_Load_From_Dir
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Delete1: TMenuItem
      Action = DatasetDelete1
    end
  end
  object cxShellBrowserDialog1: TcxShellBrowserDialog
    Left = 1008
    Top = 464
  end
  object ADOConnection_Tasks: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\GI' +
      'S\GeoConverter\bin\GeoConverter.tasks.mdb;Mode=Share Deny None;P' +
      'ersist Security Info=False;Jet OLEDB:System database="";Jet OLED' +
      'B:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engi' +
      'ne Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Par' +
      'tial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:N' +
      'ew Database Password="";Jet OLEDB:Create System Database=False;J' +
      'et OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Copy Locale on C' +
      'ompact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet ' +
      'OLEDB:SFP=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 1176
    Top = 44
  end
  object t_Task: TADOTable
    Connection = ADOConnection_Tasks
    CursorType = ctStatic
    OnCalcFields = t_TaskCalcFields
    IndexFieldNames = 'project_id'
    MasterFields = 'id'
    MasterSource = ds_Project
    TableDirect = True
    TableName = 'Task'
    Left = 1216
    Top = 136
    object t_Taskid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object t_Taskproject_id: TIntegerField
      FieldName = 'project_id'
    end
    object t_Taskchecked: TBooleanField
      FieldName = 'checked'
    end
    object t_Taskfilename_DTM: TWideStringField
      FieldName = 'filename_DTM'
      Size = 255
    end
    object t_Taskfilename_CLU: TWideStringField
      FieldName = 'filename_CLU'
      Size = 255
    end
    object t_Taskfilename_CLU_H: TWideStringField
      FieldName = 'filename_CLU_H'
      Size = 255
    end
    object t_Taskfilename_projection_txt: TWideStringField
      FieldName = 'filename_projection_txt'
      Size = 255
    end
    object t_Taskis_use_clutter: TBooleanField
      FieldName = 'is_use_clutter'
    end
    object t_TaskIs_Use_Clutter_bounds: TBooleanField
      FieldName = 'Is_Use_Clutter_bounds'
    end
    object t_Taskis_use_Clutter_Heights: TBooleanField
      FieldName = 'is_use_Clutter_Heights'
    end
    object t_TaskUTM_zone: TIntegerField
      FieldName = 'UTM_zone'
    end
    object t_Taskclutter_schema: TWideStringField
      FieldName = 'clutter_schema'
      Size = 50
    end
    object t_Taskfilename_onega_rlf: TWideStringField
      FieldName = 'filename_onega_rlf'
      Size = 250
    end
    object t_Taskstep: TSmallintField
      FieldName = 'step'
    end
    object t_TaskDir: TWideStringField
      FieldName = 'Dir'
      Size = 255
    end
    object t_Taskis_rlf_exists: TBooleanField
      DisplayLabel = #1052#1072#1090#1088#1080#1094#1072' '#1089#1086#1079#1076#1072#1085#1072
      FieldKind = fkCalculated
      FieldName = 'is_rlf_exists'
      Calculated = True
    end
    object t_TaskNull_Value: TIntegerField
      FieldName = 'Null_Value'
    end
  end
  object ds_Tasks: TDataSource
    DataSet = t_Task
    Left = 1216
    Top = 192
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 1008
    Top = 256
    object FileOpen_DEM: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.FileName = 'index.txt'
      Dialog.Filter = 'Txt (*.txt)|*.txt|ASC|*.asc'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_DEMAccept
    end
    object FileOpen_CLU: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.FileName = 'index.txt'
      Dialog.Filter = 'Txt (*.txt)|*.txt|ASC|*.asc|Mapinfo GRC|*.grc'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_CLUAccept
    end
    object FileOpen_CLU_H: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.FileName = 'index.txt'
      Dialog.Filter = 'Txt (*.txt)|*.txt|ASC|*.asc'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_CLU_HAccept
    end
    object FileOpen_proj: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.FileName = 'index.txt'
      Dialog.Filter = 'Txt (*.txt)|*.txt|Ini (*.ini)|*.ini'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_projAccept
    end
    object FileOpen_relief: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.FileName = 'index.txt'
      Dialog.Filter = 'Rlf (*.rlf)|*.rlf'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_reliefAccept
    end
    object act_clu_schema: TAction
      Category = 'File'
      Caption = 'act_clu_schema'
      OnExecute = act_clu_schemaExecute
    end
    object act_Load_From_Dir: TAction
      Category = 'File'
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1087#1072#1087#1082#1080
      OnExecute = act_Load_From_DirExecute
    end
    object act_Run_all: TAction
      Category = 'File'
      Caption = 'act_Run_all'
      OnExecute = act_Run_allExecute
    end
    object act_clu_schema_setup: TAction
      Category = 'File'
      Caption = #1062#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072
      OnExecute = act_clu_schemaExecute
    end
    object BrowseForFolder1: TBrowseForFolder
      Category = 'File'
      Caption = 'BrowseForFolder1'
      DialogCaption = 'BrowseForFolder1'
      BrowseOptions = []
      BrowseOptionsEx = []
      OnAccept = BrowseForFolder1Accept
    end
    object DatasetDelete1: TDataSetDelete
      Category = 'Dataset'
      Caption = '&'#1059#1076#1072#1083#1080#1090#1100
      Hint = 'Delete'
      ImageIndex = 5
      DataSource = ds_Tasks
    end
    object act_Project_add: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = act_Project_addExecute
    end
    object act_clu_schema_change_in_tasks: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1093#1077#1084#1091' '#1074' '#1079#1072#1076#1072#1095#1072#1093
      OnExecute = act_clu_schema_change_in_tasksExecute
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'dxBarLookupCombo1.Text'
      'cxShellBrowserDialog1.Path'
      'pn_bottom.Height'
      'PageControl1.TabIndex')
    StoredValues = <>
    Left = 1152
    Top = 440
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    Left = 1008
    Top = 536
  end
  object MainMenu1: TMainMenu
    Left = 1008
    Top = 72
    object N2: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object N3: TMenuItem
        Action = act_Load_From_Dir
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Run2: TMenuItem
        Action = act_Run
      end
      object actRunall2: TMenuItem
        Action = act_Run_all
      end
    end
    object Yfcnhjqrf1: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      object N5: TMenuItem
        Action = act_clu_schema_setup
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Action = act_clu_schema_change_in_tasks
      end
    end
  end
  object ds_Project: TDataSource
    DataSet = t_Project
    Left = 1144
    Top = 192
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.NativeStyle = False
    NotDocking = [dsNone]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 1016
    Top = 320
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 234
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 584
      FloatTop = 331
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 244
          Visible = True
          ItemName = 'dxBarLookupCombo1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 569
      FloatTop = 313
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLookupCombo1: TdxBarLookupCombo
      Caption = #1055#1088#1086#1077#1082#1090
      Category = 0
      Hint = #1055#1088#1086#1077#1082#1090
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FFFFFF00FFFFFF00
        FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00
        FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FFFF000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FFFF000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FFFF000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FFFF000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FFFF000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FFFF000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FFFF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFF000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFF}
      ShowCaption = True
      ShowEditor = False
      ImmediateDropDown = True
      KeyField = 'id'
      ListField = 'name'
      ListSource = ds_Project
      RowCount = 30
    end
    object dxBarButton1: TdxBarButton
      Action = act_Run_all
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Run
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Load_From_Dir
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = act_Project_add
      Category = 0
    end
    object dxBarControlContainerItem1: TdxBarControlContainerItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownAutoSize = True
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 50
      Properties.KeyFieldNames = 'id'
      Properties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.ListSource = ds_Project
    end
  end
  object q_Tasks_checked: TADOQuery
    Connection = ADOConnection_Tasks
    DataSource = ds_Project
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      '  select * '
      'from Task '
      'where project_id=:id and checked=true'
      ''
      'order by Filename_DTM')
    Left = 1144
    Top = 304
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 1152
    Top = 384
  end
  object t_Project: TADOTable
    Connection = ADOConnection_Tasks
    CursorType = ctStatic
    OnCalcFields = t_TaskCalcFields
    TableName = 'Project'
    Left = 1144
    Top = 136
  end
end
