object dmAsset_to_RLF: TdmAsset_to_RLF
  OldCreateOrder = False
  Height = 182
  Width = 244
  object t_Task: TADOTable
    CursorLocation = clUseServer
    IndexName = 'ProjectTask'
    MasterFields = 'id'
    MasterSource = ds_Project
    TableDirect = True
    TableName = 'Task'
    Left = 64
    Top = 40
    object t_Taskid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object t_Taskproject_id: TIntegerField
      FieldName = 'project_id'
    end
    object t_Taskchecked: TBooleanField
      FieldName = 'checked'
    end
    object t_Taskfilename_DTM: TWideStringField
      FieldName = 'filename_DTM'
      Size = 255
    end
    object t_Taskfilename_CLU: TWideStringField
      FieldName = 'filename_CLU'
      Size = 255
    end
    object t_Taskfilename_CLU_H: TWideStringField
      FieldName = 'filename_CLU_H'
      Size = 255
    end
    object t_Taskfilename_projection_txt: TWideStringField
      FieldName = 'filename_projection_txt'
      Size = 255
    end
    object t_Taskis_use_clutter: TBooleanField
      FieldName = 'is_use_clutter'
    end
    object t_TaskIs_Use_Clutter_bounds: TBooleanField
      FieldName = 'Is_Use_Clutter_bounds'
    end
    object t_Taskis_use_Clutter_Heights: TBooleanField
      FieldName = 'is_use_Clutter_Heights'
    end
    object t_TaskUTM_zone: TIntegerField
      FieldName = 'UTM_zone'
    end
    object t_Taskclutter_schema: TWideStringField
      FieldName = 'clutter_schema'
      Size = 50
    end
    object t_Taskfilename_onega_rlf: TWideStringField
      FieldName = 'filename_onega_rlf'
      Size = 250
    end
    object t_Taskstep: TSmallintField
      FieldName = 'step'
    end
    object t_TaskDir: TWideStringField
      FieldName = 'Dir'
      Size = 255
    end
    object t_Taskis_rlf_exists: TBooleanField
      DisplayLabel = #1052#1072#1090#1088#1080#1094#1072' '#1089#1086#1079#1076#1072#1085#1072
      FieldKind = fkCalculated
      FieldName = 'is_rlf_exists'
      Calculated = True
    end
  end
  object ds_Tasks: TDataSource
    DataSet = t_Task
    Left = 64
    Top = 96
  end
  object ds_Project: TDataSource
    DataSet = q_Project
    Left = 128
    Top = 96
  end
  object q_Project: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      '  select * '
      'from Project'
      ''
      'order by name')
    Left = 128
    Top = 40
  end
end
