unit fr_ASSET_to_RLF;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls,
  IniFiles,

  u_const,

  u_Asset_to_RLF,

  
//  u_VM_GRD_to_rlf,

 // u_cx_vgrid,

  rxToolEdit,


 // u_Asset_to_RLF,

  frame_RLF,
  frame_Clutter,

  fr_Custom_Form,

  Mask, ActnList, Menus, cxShellBrowserDialog, cxClasses, System.Actions,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxEdit, cxButtonEdit, cxCheckBox, cxVGrid, cxInplaceContainer, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxDBVGrid,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Data.Win.ADODB,
  cxGridLevel, cxGridCustomView, cxGrid;

type
  Tframe_ASSET_to_RLF = class(Tfrm_Custom_Form)
    frm_RLF1: Tframe_RLF_;
    GroupBox1: TGroupBox;
    ed_Clutter_Classes: TFilenameEdit;
    cb_Clutter_Classes: TCheckBox;
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    frame_Clutter_1: Tframe_Clutter_;
    lb_Projection_file: TLabel;
    ed_Projection_txt: TFilenameEdit;
    ed_Clutter_H: TFilenameEdit;
    cb_Clutter_H: TCheckBox;
    PopupMenu1: TPopupMenu;
    actSelectfolder1: TMenuItem;
    cxShellBrowserDialog1111: TcxShellBrowserDialog;
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    cxVerticalGrid1EditorRow5: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_DEM: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Projection_txt: TcxEditorRow;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Clutter_Classes: TcxEditorRow;
    row_Clutter_Classes_Use: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_Build_Heights: TcxEditorRow;
    cxVerticalGrid1EditorRow6: TcxEditorRow;
    cxVerticalGrid1EditorRow3: TcxEditorRow;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ADOConnection1: TADOConnection;
    t_Task: TADOTable;
    ds_Tasks: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1checked: TcxGridDBColumn;
    cxGrid1DBTableView1filename_DTM: TcxGridDBColumn;
    cxGrid1DBTableView1filename_CLU: TcxGridDBColumn;
    cxGrid1DBTableView1filename_CLU_H: TcxGridDBColumn;
    cxGrid1DBTableView1filename_projection_txt: TcxGridDBColumn;
    cxGrid1DBTableView1is_use_clutter: TcxGridDBColumn;
    cxGrid1DBTableView1is_use_Clutter_Heights: TcxGridDBColumn;
    cxGrid1DBTableView1UTM_zone: TcxGridDBColumn;
    cxGrid1DBTableView1Source_directory: TcxGridDBColumn;
    cxGrid1DBTableView1task_type: TcxGridDBColumn;
    cxGrid1DBTableView1filename_onega_rlf: TcxGridDBColumn;
    cxGrid1DBTableView1step: TcxGridDBColumn;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1checked: TcxDBEditorRow;
    cxDBVerticalGrid1filename_DTM: TcxDBEditorRow;
    cxDBVerticalGrid1filename_CLU: TcxDBEditorRow;
    cxDBVerticalGrid1filename_CLU_H: TcxDBEditorRow;
    cxDBVerticalGrid1filename_projection_txt: TcxDBEditorRow;
    cxDBVerticalGrid1is_use_clutter: TcxDBEditorRow;
    cxDBVerticalGrid1is_use_Clutter_Heights: TcxDBEditorRow;
    cxDBVerticalGrid1UTM_zone: TcxDBEditorRow;
    cxDBVerticalGrid1Source_directory: TcxDBEditorRow;
    cxDBVerticalGrid1task_type: TcxDBEditorRow;
    cxDBVerticalGrid1filename_onega_rlf: TcxDBEditorRow;
    cxDBVerticalGrid1step: TcxDBEditorRow;
    procedure act_Select_folderExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
  //  FTerminated : Boolean;

    FIniFileName: string;


    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run; override;
  public

    procedure LoadFromIniFile(aFileName: String);

  end;

var
  frame_ASSET_to_RLF: Tframe_ASSET_to_RLF;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'ASSET_to_RLF';


procedure Tframe_ASSET_to_RLF.act_Select_folderExecute(Sender: TObject);
var
  sDir: string;
begin
  inherited;

  {
  if PBFolderDialog1.Execute then
  begin
    sDir:= IncludeTrailingBackslash( PBFolderDialog1.Folder);

    ed_DEM.FileName:=sDir + 'Height\';
    ed_Clutter_H.FileName:=sDir + 'obstacle\';
    ed_Clutter_Classes.FileName:=sDir + 'Clutter\';

  end;
    }
    
end;

procedure Tframe_ASSET_to_RLF.Button1Click(Sender: TObject);
begin
  //cxShellBrowserDialog1.Execute;
end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;


  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);

  frm_RLF1.Init;                          
  frame_Clutter_1.Init;

  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;
  cb_Clutter_Classes.Caption  := STR_Clutter_Classes;
  //cb_Clutter_Heights.Caption  := STR_Clutter_Heights;

  lb_Projection_file.Caption  := STR_Projection_file;

 // lb_Menu_txt.Caption  := STR_Menu_txt;


end;

procedure Tframe_ASSET_to_RLF.FormDestroy(Sender: TObject);
begin
  SaveToIniFile(FIniFileName);

 
end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.Run;
// ---------------------------------------------------------------

var
  s: string;

  FASSET_to_rlf: TASSET_to_rlf;
  
begin
  FASSET_to_rlf := TASSET_to_rlf.Create();
  FASSET_to_rlf.OnProgress :=DoOnProgress;


  FTerminated := False;

  btn_Run.Action := act_Stop;

  FASSET_to_rlf.Params.Dem_Index_FileName := ed_DEM.FileName;

  FASSET_to_rlf.Params.IsUse_Clutter_Classes          := cb_Clutter_Classes.Checked;
  FASSET_to_rlf.Params.Clutter_Classes_Index_FileName := ed_Clutter_Classes.FileName;

  FASSET_to_rlf.Params.IsUse_Clutter_H          := cb_Clutter_H.Checked;  
  FASSET_to_rlf.Params.Clutter_H_Index_FileName := ed_Clutter_H.FileName;

  FASSET_to_rlf.Params.Projection_Txt_fileName := ed_Projection_txt.FileName;


//  FASSET_to_rlf.Params.Passport_FileName := ed_Passport.FileName;
 // FASSET_to_rlf.Params.Menu_Txt_FileName := ed_Menu_TXT1.FileName;

  
//  if cb_Clutter_H.Checked then
    

//    ed_Clutter_Classes
//    cb_Clutter_H
    

//  FASSET_to_rlf.Params.IsUse_Build_Heights    := cb_building_heights.Checked;
//  if cb_building_heights.Checked then
//    FASSET_to_rlf.Params.Build_Heights_FileName := ed_Build_Heights.FileName;



//  FASSET_to_rlf.Params.UseDifferentFiles := cb_UseDifferentFiles.Checked;


 // FASSET_to_rlf.Params.Dem_FileName := ed_DEM.FileName;
 // FASSET_to_rlf.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;
 // FASSET_to_rlf.Clutter_Heights_FileName := ed_Clutter_Heights.FileName;

  FASSET_to_rlf.Params.Clutters_section   := frame_Clutter_1.GetSection();
 // FASSET_to_rlf.Params.Clutters_ini_fileName    := ed_Clutters_ini.FileName;

  FASSET_to_rlf.Params.RLF_FileName := frm_RLF1.ed_RLF.FileName;

  FASSET_to_rlf.Params.Rlf_Step     := frm_RLF1.GetStepM;

  
try 
  FASSET_to_rlf.Run;  
except

end;

 
  FreeAndNil(FASSET_to_rlf);  


  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;
  
end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------


var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do

 // with TIniFile.Create(FIniFileName) do
  begin
    ed_DEM.FileName := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

    ed_Clutter_Classes.FileName := ReadString(DEF_SECTION, ed_Clutter_Classes.Name, ed_Clutter_Classes.FileName);
    ed_Clutter_H.FileName       := ReadString(DEF_SECTION, ed_Clutter_H.Name, ed_Clutter_H.FileName);

  //  ed_Clutter_Heights.FileName := ReadString(DEF_SECTION, ed_Clutter_Heights.Name,ed_Clutter_Heights.FileName);
   // ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);

  //  ed_Passport.FileName    := ReadString(DEF_SECTION, ed_Passport.Name, ed_Passport.FileName);

    ed_Projection_txt.FileName    := ReadString(DEF_SECTION, ed_Projection_txt.Name, ed_Projection_txt.FileName);

//    ed_Menu_TXT1.FileName    := ReadString(DEF_SECTION, ed_Menu_TXT1.Name, ed_Menu_TXT1.FileName);

    //ed_RLF1.FileName  := ReadString(DEF_SECTION, ed_RLF1.Name, ed_RLF1.FileName);

    cb_Clutter_Classes.Checked := ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_H.Checked       := ReadBool(DEF_SECTION, cb_Clutter_H.Name, cb_Clutter_H.checked);


 //   cb_Building_Heights.Checked := ReadBool  (DEF_SECTION, cb_Building_Heights.Name, cb_Building_Heights.checked);
//    ed_Build_Heights.FileName   := ReadString(DEF_SECTION, ed_Build_Heights.Name, ed_Build_Heights.FileName);


    //cb_UseDifferentFiles.Checked      := ReadBool(DEF_SECTION, cb_UseDifferentFiles.Name, cb_UseDifferentFiles.checked);

 //   Free;
  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  s: string;
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do  

 // with TIniFile.Create(FIniFileName) do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

    WriteBool  (DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    WriteString(DEF_SECTION, ed_Clutter_Classes.Name, ed_Clutter_Classes.FileName);



    WriteBool  (DEF_SECTION, cb_Clutter_H.Name,       cb_Clutter_H.checked);
    WriteString(DEF_SECTION, ed_Clutter_H.Name, ed_Clutter_H.FileName);
  //  WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);

  //  WriteString(DEF_SECTION, ed_Passport.Name, ed_Passport.FileName);

    WriteString(DEF_SECTION, ed_Projection_txt.Name, ed_Projection_txt.FileName);

//    WriteBool  (DEF_SECTION, cb_Building_Heights.Name, cb_Building_Heights.checked);
//    WriteString(DEF_SECTION, ed_Build_Heights.Name, ed_Build_Heights.FileName);



 //   WriteString(DEF_SECTION, ed_Menu_TXT1.Name, ed_Menu_TXT1.FileName);

 //   WriteString(DEF_SECTION, ed_RLF1.Name, ed_RLF1.FileName);




  //  WriteBool(DEF_SECTION, cb_UseDifferentFiles.Name,       cb_UseDifferentFiles.checked);

  //  WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);


   // Free;
  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;
 

end.
