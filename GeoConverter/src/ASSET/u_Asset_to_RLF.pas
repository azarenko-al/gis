unit u_Asset_to_RLF;


interface

uses Classes, Sysutils, Dialogs,forms,

  u_GDAL_envi,
  u_GDAL_classes,

  u_Rel_to_Clutter_map,

  u_GDAL_Bat,

  u_CustomTask,

//  u_Rel_to_clutter_map,

  //T_Rel_to_Clutter_map.Exec(aFileName: string);

  dm_Clutters,

  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,

  u_Run,

  u_classes_projection_TXT,

  u_clutter_classes,
  u_config_ini,

  u_Asset;


type
  TAsset_to_RLF = class(TCustomTask)
  private
 //   FUTM_zone : Integer;
    FZone_GK : Integer;

    // -------------------------
    // index_file
    // -------------------------
    FDEM_index_file      : TAsset_index_file;
    FClutter_index_file  : TAsset_index_file;
    FClutter_H_index_file: TAsset_index_file;


    FDEM_Envi_File: TEnvi_File;
    FClutter_Envi_File: TEnvi_File;
    FClutter_H_Envi_File: TEnvi_File;


    FFiles: TStringList;


    FClutterInfo: TClutterModel;

    FProjection_Txt_file: TProjection_Txt_file;

    procedure Run;
    procedure RunFile(aRLF_FileName: string);
   protected
    procedure CloseFiles; override;

   public

     Params: record

            Dem_Index_FileName : string;

            Projection_Txt_FileName : string;

            // -----------------------------------
            IsUse_Clutter_Classes : Boolean;
            Clutter_Classes_Index_FileName : string;
//            Is_Use_Clutter_bounds : Boolean;

            // -----------------------------------
            IsUse_Clutter_H       : Boolean;
            Clutter_H_Index_FileName : string;

            Clutters_section : string;

            RLF_FileName : string;

            NullValue : smallint;
//            CLU_Null_value : smallint;
//            CLU_H_Null_value : smallint;

            Rlf_Step  : Integer;
        end;

    constructor Create;

    destructor Destroy; override;

    procedure Run_new;

  end;



implementation

// ---------------------------------------------------------------
constructor TAsset_to_RLF.Create;
// ---------------------------------------------------------------
begin
  inherited;

  FDEM_index_file    :=TAsset_index_file.Create;
  FClutter_index_file:=TAsset_index_file.Create;
  FClutter_H_index_file:=TAsset_index_file.Create;

  FDEM_Envi_File:=TEnvi_File.Create;
  FClutter_Envi_File:=TEnvi_File.Create;
  FClutter_H_Envi_File:=TEnvi_File.Create;


  FClutterInfo := TClutterModel.Create();
  FProjection_Txt_file := TProjection_Txt_file.Create();

  FFiles:=TStringList.Create;

end;


// ---------------------------------------------------------------
destructor TAsset_to_RLF.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FFiles);
  FreeAndNil(FProjection_Txt_file);
  FreeAndNil(FClutterInfo);
  

  FreeAndNil(FDEM_index_file);
  FreeAndNil(FClutter_index_file);
  FreeAndNil(FClutter_H_index_file);
 
  FreeAndNil(FDEM_Envi_File);
  FreeAndNil(FClutter_Envi_File);
  FreeAndNil(FClutter_H_Envi_File);  
          
  inherited;
end;


procedure TAsset_to_RLF.CloseFiles;
begin
  FDEM_Envi_File.CloseFile;
  FClutter_Envi_File.CloseFile;
  FClutter_H_Envi_File.CloseFile;
end;


// ---------------------------------------------------------------
procedure TAsset_to_RLF.Run_new;
// ---------------------------------------------------------------

//const
//  DEF_MAX_ROWS = 4000;
//  DEF_MAX_COLS = 4000;


//  "size":[
//    3082,
//    1029
//  ],


var
  dX_por_row: Integer;
  dY_por_col: Integer;
  //e: Double;
  I: Integer;
  iColPart: Integer;
  iEPSG_GK: word;
  iRowPart: Integer;
  k: Integer;
  oBAT: TGDAL_Bat;
  sDir: string;

  sFile: string;

  rec: TGDAL_json_rec;
  s: string;
  sFileName: string;

  xyRect: TXYRect;

  rec_save: TAsset_index_file.TSaveToBAT_rec;

begin
  Terminated := False;


  // -------------------------------------------

  assert(Params.RLF_FileName<>'');

  assert (FileExists(params.Dem_Index_FileName) );
  assert (FileExists(params.Projection_Txt_FileName) );

  params.IsUse_Clutter_Classes:=params.IsUse_Clutter_Classes and FileExists(params.Clutter_Classes_Index_FileName);
  params.IsUse_Clutter_H      :=params.IsUse_Clutter_H       and FileExists(params.Clutter_H_Index_FileName);

  // -------------------------------------------

  oBAT:=TGDAL_Bat.Create;
  oBAT.Init_Header;


  if Params.IsUse_Clutter_Classes then
    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);


  FProjection_Txt_file.LoadFromFile(Params.Projection_Txt_FileName);
 // FUTM_zone :=FProjection_Txt_file.Zone_UTM;

 // else
  //  FUTM_zone :=Params.UTM_zone1;


  FZone_GK := FProjection_Txt_file.Zone_UTM - 30;

  //-------------------------------------------
  FillChar(rec_save, SizeOf(rec_save), 0);
  rec_save.PROJCS   :=FProjection_Txt_file.PROJCS;
  rec_save.Zone     :=FProjection_Txt_file.Zone_UTM;
  rec_save.NullValue:=Params.NullValue;
  //-------------------------------------------



//  Result := False;


  // ---------------------------------------------------------------
//  if FileExists(params.Dem_Index_FileName) then
//  begin
  FDEM_index_file.LoadFromFile(params.Dem_Index_FileName);
//  end;

  // ---------------------------------------------------------------
 // if FileExists(params.Projection_Txt_FileName) then
//  begin

  sFile:= ChangeFileExt(params.Dem_Index_FileName,'.bat');




  //aRec: TSaveToBAT_rec
  rec_save.FileName:=sFile;

//  k:=FProjection_Txt_file.Zone_UTM;
  //    FDEM_index_file.SaveToBAT(sFile, FProjection_Txt_file.PROJCS, FProjection_Txt_file.Zone);

  FDEM_index_file.SaveToBAT_part1(rec_save); //, sFile, FProjection_Txt_file.PROJCS, FProjection_Txt_file.Zone_UTM);

  sDir:=ExtractFilePath(sFile);

  SetCurrentDir(sDir);

  RunApp (sFile,'');


  s:=sDir + 'all.vrt.json';
  rec.LoadFromFile(s);


 //   k:=DEF_MAX_ROWS * DEF_MAX_COLS * 4;



    //ShowMessage(Format('Number  = %n', [k]));

    dX_por_row:=trunc (-(rec.CornerCoordinates.upperLeft.x - rec.CornerCoordinates.lowerRight.x) / rec.Size.rows );
    dY_por_col:=trunc (-(rec.CornerCoordinates.upperLeft.y - rec.CornerCoordinates.lowerRight.y) / rec.Size.cols );


    k:=g_Config.MaxRows;

  //  k:=rec.Size.rows div DEF_MAX_ROWS;
//    k:=rec.Size.cols div DEF_MAX_COLS;

    //  "size":[
//    3082,
//    1029
//  ],



  /////////////////////////
  oBAT.Add('del all.tif');
  oBAT.Add('gdal_translate -co compress=lzw  all.vrt all.tif');

  Assert (g_Config.MaxRows>0);
  Assert (g_Config.MaxCols>0);

    for iRowPart :=0 to rec.Size.rows div g_Config.MaxRows do
    for iColPart :=0 to rec.Size.cols div g_Config.MaxCols do
    begin
      s:= Format ('rem row:%d col:%d', [ iRowPart, iColPart  ]);
      oBAT.Add(s);
      oBAT.Add('');


      xyRect.TopLeft.y:=  rec.CornerCoordinates.upperLeft.y + dY_por_col * (iColPart) * g_Config.MaxCols; //DEF_MAX_COLS;
      xyRect.TopLeft.X:=  rec.CornerCoordinates.upperLeft.x + dX_por_row * (iRowPart) * g_Config.MaxRows; //DEF_MAX_ROWS;

      xyRect.BottomRight.Y:=  rec.CornerCoordinates.upperLeft.y + dY_por_col * (iColPart+1) * g_Config.MaxCols;//DEF_MAX_COLS;
      xyRect.BottomRight.X:=  rec.CornerCoordinates.upperLeft.x + dX_por_row * (iRowPart+1) * g_Config.MaxRows;//DEF_MAX_ROWS;

      if xyRect.BottomRight.X < rec.CornerCoordinates.lowerRight.x then
        xyRect.BottomRight.X := rec.CornerCoordinates.lowerRight.x;

      if xyRect.BottomRight.Y > rec.CornerCoordinates.lowerRight.y then
        xyRect.BottomRight.Y := rec.CornerCoordinates.lowerRight.y;


//  "cornerCoordinates":{
//    "upperLeft":[
//      -355050.0,
//      -1025000.0
//    ],
//    "lowerLeft":[
//      -355050.0,
//      -1275000.0
//    ],
//    "lowerRight":[
//      -105050.0,
//      -1275000.0
//    ],
//    "upperRight":[
//      -105050.0,
//      -1025000.0
//    ],



      s:= Format ('%1.1f %1.1f %1.1f %1.1f', [
        xyRect.TopLeft.y,
        xyRect.TopLeft.X,

        xyRect.BottomRight.Y,
        xyRect.BottomRight.X

        ]);



     // FFiles.Add(Format('all_%d_%d.tif', [iRowPart, iColPart]));

      sFileName:= Format('all_%d_%d', [iRowPart, iColPart]);
      FFiles.Add(sFileName);

      oBAT.Add( Format('gdal_translate -projwin  %s -of GTiff all.vrt %s.tif', [s, sFileName ]));
//      oBAT.Add( Format('gdal_translate -projwin  %s -of ENVI all.vrt all_%d_%d.envi', [s, iRowPart, iColPart]));
      oBAT.Add( Format('gdalinfo.exe -json %s.tif  > %s.json', [ sFileName, sFileName ]));



          iEPSG_GK:=28400 + FProjection_Txt_file.Zone_UTM - 30;


oBAT.Add(Format('del %s_pulkovo.tif',[sFileName]));

oBAT.Add(Format('gdalwarp.exe -t_srs EPSG:%d  %s.tif %s_pulkovo.tif',[iEPSG_GK, sFileName, sFileName]));
oBAT.Add(Format('gdal_translate.exe  -of envi   %s_pulkovo.tif %s_pulkovo.envi',[sFileName, sFileName]));
oBAT.Add(Format('gdalinfo.exe -json %s_pulkovo.envi > %s_pulkovo.json',[sFileName, sFileName]));



    end;

    sFile:= ExtractFilePath(params.Dem_Index_FileName) + '_2.bat';
    oBAT.SaveToFile(sFile);



    RunApp (sFile,'');


//gdal_translate -projwin  -951025   927500  -950000  926000 -of GTiff all_wgs.vrt  _new.tif

//  "size":[
//    3082,
//    1029
//  ],

//  Params.IsUse_Clutter_Classes:=false;
//  Params.IsUse_Clutter_H:=false;

//
//
//    exit;
//
//    sFile:= sDir + 'all_pulkovo.envi';
//
//



//  end;


  if params.IsUse_Clutter_Classes then
    if FileExists(params.Clutter_Classes_Index_FileName) then
    begin
      FClutter_index_file.LoadFromFile(params.Clutter_Classes_Index_FileName);

      sFile:= ChangeFileExt(params.Clutter_Classes_Index_FileName,'.bat');

//      FClutter_index_file.SaveToBAT(sFile, FProjection_Txt_file.PROJCS,  FProjection_Txt_file.Zone);
      rec_save.FileName:=sFile;

      FClutter_index_file.SaveToBAT_part1(rec_save); //, sFile, FProjection_Txt_file.PROJCS,  FProjection_Txt_file.Zone_UTM);

      sDir:=ExtractFilePath(sFile);

      SetCurrentDir(sDir);
      
      RunApp (sFile,'');

      sFile:= ExtractFilePath(params.Clutter_Classes_Index_FileName) + '_2.bat';
      oBAT.SaveToFile(sFile);

      RunApp (sFile,'');

//      sFile:= sDir + 'all_pulkovo.envi';
//      FClutter_Envi_File.OpenFile(sFile);

    end ;




  if params.IsUse_Clutter_H then
    if FileExists(params.Clutter_H_Index_FileName) then
    begin
      FClutter_H_index_file.LoadFromFile(params.Clutter_H_Index_FileName);

      sFile:= ChangeFileExt(params.Clutter_H_Index_FileName,'.bat');

      rec_save.FileName:=sFile;
      FClutter_H_index_file.SaveToBAT_part1(rec_save); //, sFile, FProjection_Txt_file.PROJCS,  FProjection_Txt_file.Zone_UTM);

      sDir:=ExtractFilePath(sFile);

      SetCurrentDir(sDir);

      RunApp (sFile,'');

      sFile:= ExtractFilePath(params.Clutter_H_Index_FileName) + '_2.bat';
      oBAT.SaveToFile(sFile);

      RunApp (sFile,'');

//      sFile:= sDir + 'all_pulkovo.envi';
//      FClutter_H_Envi_File.OpenFile(sFile);
    end;



    for I := 0 to FFiles.Count-1 do
    begin
      sDir:=ExtractFilePath(params.Dem_Index_FileName);
      s:=sDir + FFiles[i] + '_pulkovo.envi';
      FDEM_Envi_File.OpenFile(s);

      if params.IsUse_Clutter_Classes then
      begin
        sDir:=ExtractFilePath(params.Clutter_Classes_Index_FileName);
        s:=sDir + FFiles[i] + '_pulkovo.envi';
        FClutter_Envi_File.OpenFile(s);

      end;


      if params.IsUse_Clutter_H then
      begin
        sDir:=ExtractFilePath(params.Clutter_H_Index_FileName);
        s:=sDir + FFiles[i] + '_pulkovo.envi';
        FClutter_H_Envi_File.OpenFile(s);

      end;



      if FFiles.Count=1 then
        s:=Params.RLF_FileName
      else
        s:=ChangeFileExt(Params.RLF_FileName, '_' +Copy(FFiles[i],5, 999) + '.rlf');

      RunFile(s);

    end;


end;


// ---------------------------------------------------------------
procedure TAsset_to_RLF.Run;
// ---------------------------------------------------------------


//  "size":[
//    3082,
//    1029
//  ],


var
   //e: Double;
  I: Integer;
  iColPart: Integer;
  iEPSG: word;
  iRowPart: Integer;
  k: Integer;
  oBAT: TGDAL_Bat;
  sDir: string;

  sFile: string;

  rec: TGDAL_json_rec;
  s: string;
  sFileName: string;

  xyRect: TXYRect;

  rec_save: TAsset_index_file.TSaveToBAT_rec;

begin



  assert (FileExists(params.Dem_Index_FileName) );
  assert (FileExists(params.Projection_Txt_FileName) );
  // -----------------------------------------------------

  FProjection_Txt_file.LoadFromFile(params.Projection_Txt_FileName);
  FZone_GK := FProjection_Txt_file.Zone_UTM - 30;

  if Params.IsUse_Clutter_Classes then
    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);

  // -----------------------------------------------------
  FillChar(rec_save, SizeOf(rec_save), 0);
  rec_save.PROJCS:=FProjection_Txt_file.PROJCS;
  rec_save.Zone  :=FProjection_Txt_file.Zone_UTM;
  rec_save.NullValue:=Params.NullValue;
  // -----------------------------------------------------

//  rec_save.NullValue :=Params.;



  oBAT:=TGDAL_Bat.Create;
  oBAT.Init_Header;



  Terminated := False;

//  Result := False;


  // ---------------------------------------------------------------
//  if FileExists(params.Dem_Index_FileName) then
//  begin
  FDEM_index_file.LoadFromFile(params.Dem_Index_FileName);
//  end;

  // ---------------------------------------------------------------
 // if FileExists(params.Projection_Txt_FileName) then
  begin

    sFile:= ChangeFileExt(params.Dem_Index_FileName,'.bat');

//    FDEM_index_file.SaveToBAT(sFile, FProjection_Txt_file.PROJCS, FProjection_Txt_file.Zone);
    rec_save.FileName:=sFile;

    FDEM_index_file.SaveToBAT(rec_save); //, sFile, FProjection_Txt_file.PROJCS, FProjection_Txt_file.Zone_UTM);

    sDir:=ExtractFilePath(sFile);

    SetCurrentDir(sDir);

    RunApp (sFile,'');

    sFile:= sDir + 'all_pulkovo.envi';

    FDEM_Envi_File.OpenFile(sFile);
  end;

  // ---------------------------------------------------------------

  if params.IsUse_Clutter_Classes then
    if FileExists(params.Clutter_Classes_Index_FileName) then
    begin
      FClutter_index_file.LoadFromFile(params.Clutter_Classes_Index_FileName);

      sFile:= ChangeFileExt(params.Clutter_Classes_Index_FileName,'.bat');

//      FClutter_index_file.SaveToBAT(sFile, FProjection_Txt_file.PROJCS,  FProjection_Txt_file.Zone);
      rec_save.FileName:=sFile;
      FClutter_index_file.SaveToBAT(rec_save); //, sFile, FProjection_Txt_file.PROJCS,  FProjection_Txt_file.Zone_UTM);

      sDir:=ExtractFilePath(sFile);

      SetCurrentDir(sDir);

      RunApp (sFile,'');

      sFile:= sDir + 'all_pulkovo.envi';
      FClutter_Envi_File.OpenFile(sFile);

    end;

  // ---------------------------------------------------------------


  if params.IsUse_Clutter_H then
    if FileExists(params.Clutter_H_Index_FileName) then
    begin
      FClutter_H_index_file.LoadFromFile(params.Clutter_H_Index_FileName);

      sFile:= ChangeFileExt(params.Clutter_H_Index_FileName,'.bat');

      rec_save.FileName:=sFile;

      FClutter_H_index_file.SaveToBAT(rec_save); //, sFile, FProjection_Txt_file.PROJCS,  FProjection_Txt_file.Zone_UTM);

      sDir:=ExtractFilePath(sFile);

      SetCurrentDir(sDir);

      RunApp (sFile,'');

      sFile:= sDir + 'all_pulkovo.envi';
      FClutter_H_Envi_File.OpenFile(sFile);
    end;


  //FUTM_zone :=;

 // else
  //  FUTM_zone :=Params.UTM_zone1;


///////  FDEM_file.OpenFile(params.DEM_fileName);

 // FDEM_file.LoadPassport(params.Passport_FileName);



  RunFile(Params.RLF_FileName); //(Params.RLF_FileName);


end;




// ---------------------------------------------------------------
procedure TAsset_to_RLF.RunFile(aRLF_FileName: string);
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  rXYRect_GK: TXYRect;

  xy_CK1,xy_CK2,
  xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
  eHeight: Double;

  iColCount: longword;
  iRowCount: longword;
  iStep: integer;
  iRow: longword;
  iCol: longword;


  rlf_rec: Trel_Record;
  iValue: integer;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;

  k: Integer;
  s2: string;
  s1: string;
  iW: Integer;
  wCluCode: Integer;

  rClutter: TClutterRec;
  sDir: string;


  oFile: TEnvi_File;

  oMatrix: TrlfMatrix;
//  sRLF_FileName: string;

begin
  aRLF_FileName:=ChangeFileExt(aRLF_FileName, '.rlf');


//   ExportToClutterTab(Params.RLF_FileName);
//   Exit;
//------------------------------------------------



  if Terminated then
    Exit;


//   if Params.Is_Use_Clutter_bounds then
//     oFile:=FClutter_Envi_File
//   else

   oFile:=FDEM_Envi_File;



  if Params.Rlf_Step=0 then
    iStep :=Round(oFile.CellSize_lat)
  else
    iStep := Params.Rlf_Step;


 // iStep :=Round(FDEM_index_file.CellSize);

  Assert(iStep>0, 'Value <=0');

  iRowCount := oFile.RowCount;
  iColCount := oFile.ColCount;


  iRowCount := Round((oFile.bounds.lat_max - oFile.bounds.lat_min)  / iStep);
  iColCount := Round((oFile.bounds.lon_max - oFile.bounds.lon_min)  / iStep);


  rXYRect_GK.TopLeft.X:=oFile.bounds.lat_max;
  rXYRect_GK.TopLeft.Y:=oFile.bounds.lon_min;

  rXYRect_GK.BottomRight.X:=rXYRect_GK.TopLeft.X - iRowCount * iStep;
  rXYRect_GK.BottomRight.Y:=rXYRect_GK.TopLeft.Y + iColCount * iStep;


 //   header_Rec.ZoneNum_GK:=FGK_zone;
  FDEM_Envi_File.GK_zone:=FZone_GK;

  FDEM_Envi_File.SaveToIniFile (ChangeFileExt(Params.RLF_FileName,'.ini') );
//  oFile.SaveToXmlFile (ChangeFileExt(Params.RLF_FileName,'.xml') );


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;
  header_Rec.ZoneNum_GK:=FZone_GK;

  header_Rec.XYBounds:=rXYRect_GK;


  sDir:=ExtractFilePath(aRLF_FileName);
//  sDir:=ExtractFilePath(Params.RLF_FileName);
  ForceDirectories(sDir);


//  sRLF_FileName:=ChangeFileExt(Params.RLF_FileName, '.rlf');

  oRlfFileStream:=TFileStream.Create(aRLF_FileName, fmCreate);
//  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

  FDEM_Envi_File.GetDataByIndex(0);

//  if Params.Null_value <> 0 then
//  begin
//    FDEM_Envi_File.NullValue      :=Params.Null_value;
//    FClutter_Envi_File.NullValue  :=Params.Null_value;
//    FClutter_H_Envi_File.NullValue:=Params.Null_value;
//
//  end;
//


  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);

    //

    
    for iCol := 0 to iColCount - 1 do
    begin
    
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

      if FDEM_Envi_File.FindValueByXY(xy.x, xy.y, iW) then
      begin
       // if iW > High(Smallint) then
        //  iW := High(Smallint);

        rlf_rec.Rel_H :=iW;

        if Params.IsUse_Clutter_Classes then
          if FClutter_Envi_File.FindValueByXY(xy.x, xy.y, wCluCode) then
        begin
          if (wCluCode>=1) and (wCluCode<100) then
          begin
            if FClutterInfo.GetOnegaCode(wCluCode,  rClutter) then
            begin
              rlf_rec.Clutter_Code := rClutter.Onega_Code;
              rlf_rec.Clutter_H    := rClutter.Onega_Height;
            end;




          //--------------------------------
          if Params.IsUse_Clutter_H then
            if FClutter_H_Envi_File.FindValueByXY(xy.x, xy.y, iW) then
            begin
             // if w>250 then
              //   w:=0;//250;

              try
                if iW>250 then
                  iW:=255;

                rlf_rec.Clutter_H := iW;              
              except
                rlf_rec.Clutter_H :=0;
              
//CodeSite.SendError(IntToStr(iW));

              end;

              if rlf_rec.Clutter_Code in [0,2,4,5] then //������
                rlf_rec.Clutter_H := 0;
              
              
            end;

        end;

        end;

      end
      else
        rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;



  FreeAndNil(oRlfFileStream);


  // ---------------------------------------------------------------
//  oMatrix:=TrlfMatrix.Create;
//  oMatrix.LoadHeaderFromFile (Params.RLF_FileName);
//  FreeAndNil(oMatrix);

  // ---------------------------------------------------------------



  if not Terminated then
    T_Rel_to_Clutter_map.Exec(aRLF_FileName);
//    T_Rel_to_Clutter_map.Exec(aFileName: string);
//    ExportToClutterTab(aRLF_FileName);


end;



begin


end.





