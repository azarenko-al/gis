﻿unit f_Main_;

interface

uses
  u_envi_to_rlf,


  d_clutter_schema_change,
//  dm_Asset_to_RLF,

  u_cx_ini,

  u_config_ini,

  Vcl.Dialogs,

//  CodeSiteLogging,

  d_Clutters,

  u_Asset_to_RLF,

  u_db_mdb,
  u_db,

  u_classes_projection_TXT,

  u_Asset,

  u_func,
  u_files,

  fr_Custom_Form,



  Mask, ActnList, Menus, cxShellBrowserDialog,
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls,
  IniFiles,   IOUtils, Variants, cxLookAndFeels, cxVGrid,
  Data.DB, cxDBVGrid,  cxGridDBTableView, Data.Win.ADODB,
  cxGridLevel, cxGridCustomView, cxGrid, Vcl.StdActns, cxSplitter,
  RxPlacemnt,  Vcl.AppEvnts,  cxBarEditItem, dxBarExtItems, dxBar, dxBarExtDBItems, Vcl.DBActns,

  cxGridCustomTableView, cxGraphics, cxControls, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxCheckBox, cxTextEdit, cxButtonEdit,
  cxDBLookupComboBox, cxClasses, cxInplaceContainer, cxGridTableView,
  System.Actions, Vcl.Grids, Vcl.DBGrids, cxSpinEdit;



type
  Tframe_ASSET_to_RLF = class(Tfrm_Custom_Form)
    PopupMenu1: TPopupMenu;
    actSelectfolder1: TMenuItem;
    cxShellBrowserDialog1: TcxShellBrowserDialog;
    ADOConnection_Tasks: TADOConnection;
    t_Task: TADOTable;
    ds_Tasks: TDataSource;
    ActionList1: TActionList;
    FileOpen_DEM: TFileOpen;
    FileOpen_CLU: TFileOpen;
    FileOpen_CLU_H: TFileOpen;
    FileOpen_proj: TFileOpen;
    FileOpen_relief: TFileOpen;
    act_clu_schema: TAction;
    FormStorage1: TFormStorage;
    act_Load_From_Dir: TAction;
    cxLookAndFeelController1: TcxLookAndFeelController;
    act_Run_all: TAction;
    N1: TMenuItem;
    actRunall1: TMenuItem;
    Run1: TMenuItem;
    MainMenu1: TMainMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Run2: TMenuItem;
    actRunall2: TMenuItem;
    Yfcnhjqrf1: TMenuItem;
    act_clu_schema_setup: TAction;
    N5: TMenuItem;
    ds_Project: TDataSource;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLookupCombo1: TdxBarLookupCombo;
    q_Tasks_checked: TADOQuery;
    BrowseForFolder1: TBrowseForFolder;
    dxBarButton1: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    PageControl1: TPageControl;
    TabSheet1_Tasks: TTabSheet;
    cxGrid_tasks: TcxGrid;
    cxGridDBTableView1_tasks: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    col_is_use_clutter: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxSplitter2: TcxSplitter;
    TabSheet2: TTabSheet;
    cxGrid1_projects: TcxGrid;
    cxGrid1_projectsDBTableView2: TcxGridDBTableView;
    cxGrid1_projectsDBTableView2id_: TcxGridDBColumn;
    cxGrid1_projectsDBTableView2name_: TcxGridDBColumn;
    cxGrid1_projectsLevel2: TcxGridLevel;
    DatasetDelete1: TDataSetDelete;
    Delete1: TMenuItem;
    N6: TMenuItem;
    dxBarButton4: TdxBarButton;
    act_Project_add: TAction;
    dxBarControlContainerItem1: TdxBarControlContainerItem;
    cxBarEditItem1: TcxBarEditItem;
    pn_bottom: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1checked: TcxDBEditorRow;
    cxDBVerticalGrid1Source_directory: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_DTM: TcxDBEditorRow;
    cxDBVerticalGrid1filename_projection_txt: TcxDBEditorRow;
    cxDBVerticalGrid1UTM_zone: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid1is_use_clutter: TcxDBEditorRow;
    cxDBVerticalGrid1filename_CLU: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow3: TcxCategoryRow;
    cxDBVerticalGrid1is_use_Clutter_Heights: TcxDBEditorRow;
    cxDBVerticalGrid1filename_CLU_H: TcxDBEditorRow;
    cxDBVerticalGrid1task_type: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow4: TcxCategoryRow;
    cxDBVerticalGrid1filename_onega_rlf: TcxDBEditorRow;
    cxDBVerticalGrid1step: TcxDBEditorRow;
    ApplicationEvents1: TApplicationEvents;
    act_clu_schema_change_in_tasks: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    t_Taskid: TAutoIncField;
    t_Taskproject_id: TIntegerField;
    t_Taskchecked: TBooleanField;
    t_Taskfilename_DTM: TWideStringField;
    t_Taskfilename_CLU: TWideStringField;
    t_Taskfilename_CLU_H: TWideStringField;
    t_Taskfilename_projection_txt: TWideStringField;
    t_Taskis_use_clutter: TBooleanField;
    t_TaskIs_Use_Clutter_bounds: TBooleanField;
    t_Taskis_use_Clutter_Heights: TBooleanField;
    t_TaskUTM_zone: TIntegerField;
    t_Taskclutter_schema: TWideStringField;
    t_Taskfilename_onega_rlf: TWideStringField;
    t_Taskstep: TSmallintField;
    t_TaskDir: TWideStringField;
    t_Taskis_rlf_exists: TBooleanField;
    cxGridDBTableView1_tasksColumn1: TcxGridDBColumn;
    cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow;
    t_Project: TADOTable;
    StatusBar1: TStatusBar;
    cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    cxDBVerticalGrid1CategoryRow5: TcxCategoryRow;
    t_TaskNull_Value: TIntegerField;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure act_clu_schemaExecute(Sender: TObject);
    procedure act_clu_schema_change_in_tasksExecute(Sender: TObject);
    procedure act_Load_From_DirExecute(Sender: TObject);
    procedure act_Project_addExecute(Sender: TObject);
    procedure act_Run_allExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure BrowseForFolder1Accept(Sender: TObject);
//    procedure act_StopExecute(Sender: TObject);
//    procedure act_Select_folderExecute(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBVerticalGrid1filename_DTMEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FileOpen_DEMAccept(Sender: TObject);
    procedure cxDBVerticalGrid1filename_CLU_HEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1filename_onega_rlfEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1filename_CLUEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FileOpen_reliefAccept(Sender: TObject);
    procedure cxDBVerticalGrid1filename_projection_txtEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FileOpen_CLUAccept(Sender: TObject);
    procedure FileOpen_CLU_HAccept(Sender: TObject);
    procedure FileOpen_projAccept(Sender: TObject);
//    procedure Yfcnhjqrf1Click(Sender: TObject);
    procedure cxDBVerticalGrid1DBEditorRow3EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FormShow(Sender: TObject);
    procedure t_TaskCalcFields(DataSet: TDataSet);
//    procedure t_TaskNewRecord(DataSet: TDataSet);
//    procedure frame_Clutter_1Button1Click(Sender: TObject);

  private
  //  FTerminated : Boolean;
    FRegPath: string;
  
 //   FIniFileName: string;


    function Find_Index_txt(aDir, aPath: string; var aFileName: string): Boolean;
//    FDataset: TDataset;
    
    procedure Process_Dir(aDir: string; aClutter_schema: string = '');
    procedure Run_All;
    procedure Run_asset(aDataset: TDataset);
    procedure Run_ENVI(aDataset: TDataset);


//    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run(aDataset: TDataset); override;
  public

//    procedure LoadFromIniFile(aFileName: String);

  end;
  
//
var
  frame_ASSET_to_RLF: Tframe_ASSET_to_RLF;


implementation


{$R *.dfm}

//const
//  DEF_SECTION = 'ASSET_to_RLF';


const
  FLD_Filename_onega_rlf      = 'filename_onega_rlf';
  FLD_Filename_CLU_H          = 'filename_CLU_H';
  FLD_Filename_CLU            = 'filename_CLU';
  FLD_Is_Use_Clutter_bounds   = 'Is_Use_Clutter_bounds';
  FLD_Is_use_Clutter_Heights  = 'is_use_Clutter_Heights';
  FLD_Is_use_clutter          = 'is_use_clutter';
  FLD_Filename_DTM            = 'filename_DTM';
  FLD_filename_projection_txt = 'filename_projection_txt';  //--filename_projection_txt
  FLD_clutter_schema          = 'clutter_schema';
  FLD_step                    = 'step';
  FLD_UTM_zone                = 'UTM_zone';

  FLD_DIR = 'Dir';
  

  

procedure Tframe_ASSET_to_RLF.ActionList1Update(Action: TBasicAction; var
    Handled: Boolean);
begin
  act_Run.Enabled:=not t_Task.IsEmpty;
  act_Run_all.Enabled:=not t_Task.IsEmpty;
end;


procedure Tframe_ASSET_to_RLF.FormDestroy(Sender: TObject);
begin
  cxGridDBTableView1_tasks.StoreToRegistry ( FRegPath +  cxGridDBTableView1_tasks.Name);


//  cxGrid1DBTableView1.StoreTo (cxPropertiesStore1.Name);

end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
//  CodeSite.Send('procedure Tframe_ASSET_to_RLF.FormCreate(Sender: TObject);');

  inherited;

//  Confi
  
//  ShowMessage('procedure Tframe_ASSET_to_RLF.FormCreate(Sender: TObject);');

  Caption := 'GeoConverter' + '| создан: ' + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));
//  Caption := 'Asset -> Onega rlf ' + '| создан:' GetAppVersionStr;

//    Caption:= 'Ïîñòðîåíèå LOS | '+  'ñîçäàí: '  + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));



  if ADOConnection_Tasks.Connected then
    ShowMessage('ADOConnection1.Connected');
 
  

  FRegPath := 'Software\Onega\ASSET_to_RLF\v2\';


  PageControl1.Align:=alClient;
  cxDBVerticalGrid1.Align:=alClient;

  cxGrid_tasks.Align:=alClient;
  cxGrid1_projects.Align:=alClient;
  
  //  FRegPath := REGISTRY_COMMON_FORMS + ClassName +'\';


//..  FRegPath := g_Storage.GetPathByClass(ClassName)+ '1';

  
//s:=  FormStorage1.RegIniFile.FileName;
  

//  cxDBVerticalGrid1.Align:=alLeft;


  act_Run_all.Caption:='Построить все';

  
  //cxShellBrowserDialog1.Path:='P:\_megafon\83_NW_NenetskyAO_UTM40\';
  
//  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

 // LoadFromIniFile(FIniFileName);

 ADOConnection_Tasks.Close;

  if not mdb_OpenConnection(ADOConnection_Tasks, GetApplicationDir() + 'GeoConverter.tasks.mdb') then
    Halt;


//ShowMessage('111');

  t_Task.Open;
  t_Project.Open;

//  t_Project.Locate('name', dxBarLookupCombo1.Text, []);
     
 
  FDataset:=t_Task;   


  cxGridDBTableView1_tasks.RestoreFromRegistry ( FRegPath +  cxGridDBTableView1_tasks.Name);


  cx_LoadColumnCaptions_FromIni(cxGridDBTableView1_tasks);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);


  cxGridDBTableView1_tasks.Navigator.Visible:=True;

//  cxGrid1DBTableView1.re 

end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.act_clu_schemaExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  if Sender=act_clu_schema then
  begin

    s:=FDataset.FieldByName(FLD_clutter_schema).AsString;
 
    if Tdlg_Clutters.ExecDlg(s) then
      db_UpdateRecord_NoPost(FDataset, [FLD_clutter_schema, s]);
  end else


  if Sender=act_clu_schema_setup then
    Tdlg_Clutters.ExecDlg(s);

  
end;

procedure Tframe_ASSET_to_RLF.act_clu_schema_change_in_tasksExecute(Sender:
    TObject);
begin
  if Tdlg_clutter_schema_change.ExecDlg(ADOConnection_Tasks) then
  begin  
    t_Task.Refresh;

  end;

end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.act_Load_From_DirExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  sClutter_schema: string;
begin
 if cxShellBrowserDialog1.Execute then 
   if Tdlg_Clutters.ExecDlg(sClutter_schema) then

//     dmAsset_to_RLF.Process_Dir (IncludeTrailingBackslash(cxShellBrowserDialog1.Path), FDataset, sClutter_schema );
     Process_Dir (IncludeTrailingBackslash(cxShellBrowserDialog1.Path), sClutter_schema );

end;

// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.act_Project_addExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  sName: string;
begin

  if InputQuery('Добавить проект', 'Введите название', sName) then
  begin
    if not t_Project.Locate('name', sName, []) then
      db_AddRecord_(t_Project, [FLD_NAME, sName] );

    t_Project.Locate('name', sName, []);


  end;

end;


// ---------------------------------------------------------------
function Tframe_ASSET_to_RLF.Find_Index_txt(aDir, aPath: string; var aFileName:  string): Boolean;
// ---------------------------------------------------------------

var
  s: string;
  Arr: TArray<string>;

begin
  arr:= aPath.Split([',']);

  for s in arr  do
  begin
    aFileName:=IncludeTrailingBackslash(aDir) + IncludeTrailingBackslash(s)   + 'index.txt';

    if FileExists ( aFileName ) then
    begin
     // aFileName:= sFile

      Result:=True;
      Exit;
    end;

  end;


  Result:= False;
end;


// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.Process_Dir(aDir: string; aClutter_schema: string  = '');
// ---------------------------------------------------------------
var
  k: Integer;
  oAsset_CLU: TAsset_index_file;
  oAsset_CLU_H: TAsset_index_file;

  sDir_Clutter_Classes: string;
  sDir_Clutter_H: string;
  sDir_DTM: string;


  oAsset_DTM: TAsset_index_file;
  I: Integer;
  iInd: Integer;
  iStep: Integer;
//  iZone: Integer;
  oProjection_file: TProjection_Txt_file;
  sCLU_H_index_txt: string;
  sCLU_index_txt: string;

  sDEM: string;
  sDEM_index_txt: string;
  sDir_CLU: string;
  sDir_CLU_H: string;
  sDir_DEM: string;
  sFile_rlf: string;
  sName: string;
  sName_CLU: string;
  sName_CLU_H: string;
  sName_DTM: string;
  sPath_proj: string;

  sPath: string;

begin
  

  aDir:= IncludeTrailingBackslash(aDir);

 // if cxShellBrowserDialog1.Execute then 

    if aClutter_schema='' then
      aClutter_schema:= FDataset.FieldByName(FLD_clutter_schema).AsString;

  
    oAsset_DTM:=TAsset_index_file.Create;
    oAsset_CLU:=TAsset_index_file.Create;
    oAsset_CLU_H:=TAsset_index_file.Create;
    oProjection_file:=TProjection_Txt_file.Create;



   
 //   if oProjection_file.PROJCS  <> '' then    
      for sPath_proj in TDirectory.GetFiles (aDir, 'proj*.ini')  do
      begin
        oProjection_file.LoadFromFile(sPath_proj);
  //      iZone:=oProjection_file.ZOne; 
      end;  

      if sPath_proj='' then      
        for sPath_proj in TDirectory.GetFiles (aDir, 'proj*.txt')  do
        begin
          oProjection_file.LoadFromFile(sPath_proj);
    //      iZone:=oProjection_file.ZOne; 
        end;  
      
 
 //   sDir_DEM:=aDir + 'Height\';
    sPath:=g_Config.relief; //.   oIni.ReadString('main','relief','');
  //  Assert(sPath<>'');

 
    if Find_Index_txt (aDir,sPath, sDEM_index_txt) then
//      if FileExists(sDir_DEM + 'index.txt') then
    begin
      oAsset_DTM.LoadFromFile(sDEM_index_txt); //  sDir_DEM + 'index.txt');
      oAsset_DTM.ParseToFiles;
    end;  

    //================================================
    
    sPath:=g_Config.clutter; //:=oIni.ReadString('main','clutter','');
//    Assert(sPath<>'');
    
    if Find_Index_txt (aDir,sPath, sCLU_index_txt) then
                                       
//    sDir_CLU:=aDir + 'Clutter\';
//    if FileExists(sDir_CLU + 'index.txt') then
    begin    
      oAsset_CLU.LoadFromFile (sCLU_index_txt); //sDir_CLU + 'index.txt');
      oAsset_CLU.ParseToFiles;
    end;

    //================================================
    
    //
 //   sDir_CLU_H:=aDir + 'obstacle\';
    sPath:=g_Config.clutter_h; //  :=oIni.ReadString('main','clutter_h','');
 //   Assert(sPath<>'');

    if Find_Index_txt (aDir, sPath, sCLU_H_index_txt) then
//    if FileExists(sDir_CLU_H + 'index.txt') then
    begin
      oAsset_CLU_H.LoadFromFile (sCLU_H_index_txt); //sDir_CLU_H + 'index.txt');
      oAsset_CLU_H.ParseToFiles;
    end;  

    //================================================

    FDataset.DisableControls;
    
    
    for I := 0 to oAsset_DTM.Groups_ex.Count-1 do
    begin
      sName:=oAsset_DTM.Groups_ex[i];
      
//CodeSite.Send('oAsset_DTM.Groups_ex[i] -' + sName);

//Continue;


      iStep:=Integer(oAsset_DTM.Groups_ex.Objects[i]);

//      sName_DTM:=  sDir_DEM + 'index_'+ sName +'.txt';
      sName_DTM:=  ExtractFilePath(sDEM_index_txt) + 'index_'+ sName +'.txt';


      
      //-------------------------------------------   
      iInd:=oAsset_CLU.Groups_ex.IndexOf(sName);
      if iInd>=0 then
         sName_CLU:=  ExtractFilePath(sCLU_index_txt) + 'index_'+ oAsset_CLU.Groups_ex[iInd]+'.txt'
      else
         sName_CLU:='';

      //-------------------------------------------   
      iInd:=oAsset_CLU_H.Groups_ex.IndexOf(sName);
      if iInd>=0 then
         sName_CLU_H:= ExtractFilePath(sCLU_H_index_txt) + 'index_'+ oAsset_CLU_H.Groups_ex[iInd]+'.txt'
      else
         sName_CLU_H:='';

      //-------------------------------------------   
      sFile_rlf:= aDir + GetParentFolderName_v1 (aDir) +'_'+ sName + '.rlf';

      
         
     if FDataset.Locate(FLD_PROJECT_ID+';'+FLD_Filename_DTM, VarArrayOf([t_Project [FLD_ID],  sName_DTM]), []) then
        FDataset.Delete;


     db_AddRecord_(FDataset, [

        FLD_DIR                    , aDir,

        FLD_PROJECT_ID,              t_Project [FLD_ID],

     
        FLD_Filename_DTM            , sName_DTM,
        FLD_filename_projection_txt , sPath_proj, //'filename_projection_txt',  
        FLD_UTM_zone                , oProjection_file.Zone_UTM,      // 'UTM_zone',
     

        FLD_Is_use_clutter          , IIF(sName_CLU='', False, True),
        FLD_Filename_CLU            , IIF(sName_CLU='', '', sName_CLU),

        FLD_Is_use_Clutter_Heights  , IIF(sName_CLU_H='', False, True),
        FLD_Filename_CLU_H          , IIF(sName_CLU_H='', '', sName_CLU_H),
      
        FLD_clutter_schema          , aClutter_schema,

        FLD_Filename_onega_rlf      , sFile_rlf, //aDir + GetParentFolderName_v1 + sName + '.rlf',
        FLD_step                    , iStep
 

       ]);
   
    end;  

    FDataset.EnableControls;
                      

    FreeAndNil(oAsset_DTM);
    FreeAndNil(oAsset_CLU);
    FreeAndNil(oAsset_CLU_H);
    FreeAndNil(oProjection_file);

    //  FreeAndNil(oIni);
      
 // end;

    
end;

 
procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  act_clu_schema.Execute;

end;

procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1DBEditorRow3EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  BrowseForFolder1.Folder:= FDataset.FieldByName(FLD_DIR).AsString;
  BrowseForFolder1.Execute;


end;

procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1filename_CLUEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin   
  FileOpen_CLU.Dialog.FileName := FDataset.FieldByName(FLD_filename_CLU).AsString;
  FileOpen_CLU.Execute;

end;

procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1filename_CLU_HEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  FileOpen_CLU_H.Dialog.FileName := FDataset.FieldByName(FLD_filename_CLU_H).AsString;
  FileOpen_CLU_H.Dialog.InitialDir:= ExtractFilePath(FileOpen_CLU_H.Dialog.FileName);
  FileOpen_CLU_H.Execute;
end;


procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1filename_DTMEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
begin
  FileOpen_DEM.Dialog.FileName := FDataset.FieldByName(FLD_filename_DTM).AsString;
  FileOpen_DEM.Dialog.InitialDir:= ExtractFilePath(FileOpen_DEM.Dialog.FileName);
  FileOpen_DEM.Execute;
end;


procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1filename_onega_rlfEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  FileOpen_relief.Dialog.FileName := FDataset.FieldByName(FLD_filename_onega_rlf).AsString;
  FileOpen_relief.Dialog.InitialDir:= ExtractFilePath(FileOpen_relief.Dialog.FileName);
  FileOpen_relief.Execute;

end;


procedure Tframe_ASSET_to_RLF.cxDBVerticalGrid1filename_projection_txtEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  s: string;
begin
//  s:=FDataset.FieldByName(FLD_filename_projection_txt).AsString;

  FileOpen_proj.Dialog.FileName := FDataset.FieldByName(FLD_filename_projection_txt).AsString;
  FileOpen_proj.Dialog.InitialDir:= ExtractFilePath(FileOpen_proj.Dialog.FileName);

  FileOpen_proj.Execute;

end;

procedure Tframe_ASSET_to_RLF.FileOpen_CLUAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_clu, TFileOpen (Sender).Dialog.FileName]);

end;

procedure Tframe_ASSET_to_RLF.FileOpen_CLU_HAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_clu_h, TFileOpen (Sender).Dialog.FileName]);

end;

procedure Tframe_ASSET_to_RLF.FileOpen_DEMAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_DTM, TFileOpen (Sender).Dialog.FileName]);
end;

procedure Tframe_ASSET_to_RLF.FileOpen_projAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_projection_txt, TFileOpen (Sender).Dialog.FileName]);
end;


procedure Tframe_ASSET_to_RLF.BrowseForFolder1Accept(Sender: TObject);
begin

  db_UpdateRecord_NoPost(FDataset, [FLD_DIR, TBrowseForFolder (Sender).Folder]);
  
end;

procedure Tframe_ASSET_to_RLF.FileOpen_reliefAccept(Sender: TObject);
begin
  db_UpdateRecord_NoPost(FDataset, [FLD_filename_onega_rlf, TFileOpen (Sender).Dialog.FileName]);
  
end;

 
// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.Run(aDataset: TDataset);
// ---------------------------------------------------------------
var
  sExt: string;
  sFile: string;
begin
  sFile:=aDataset.FieldByName(FLD_Filename_DTM).AsString;

  sExt:=LowerCase( ExtractFileExt(sFile) );

  if sExt='.txt' then
    Run_asset(aDataset)
  else
    Run_ENVI(aDataset);


end;


// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.Run_asset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  FASSET_to_rlf: TASSET_to_rlf;
  sFile: string;

begin
//  sFile:=aDataset.FieldByName(FLD_Filename_DTM).AsString;

  //----------------------------------------------------------------
  FASSET_to_rlf := TASSET_to_rlf.Create();
  FASSET_to_rlf.OnProgress :=DoOnProgress;


  FTerminated := False;

  //btn_Run.Action := act_Stop;

  FASSET_to_rlf.Params.Dem_Index_FileName             := aDataset.FieldByName(FLD_Filename_DTM).AsString;
  FASSET_to_rlf.Params.Projection_Txt_fileName        := aDataset.FieldByName(FLD_filename_projection_txt).AsString;

  FASSET_to_rlf.Params.IsUse_Clutter_Classes          := aDataset.FieldByName(FLD_Is_use_clutter).AsBoolean;
  FASSET_to_rlf.Params.Clutter_Classes_Index_FileName := aDataset.FieldByName(FLD_filename_CLU).AsString;
//  FASSET_to_rlf.Params.Is_Use_Clutter_bounds          := aDataset.FieldByName(FLD_Is_Use_Clutter_bounds).AsBoolean;



  FASSET_to_rlf.Params.IsUse_Clutter_H                := aDataset.FieldByName(FLD_Is_use_Clutter_Heights).AsBoolean;
  FASSET_to_rlf.Params.Clutter_H_Index_FileName       := aDataset.FieldByName(FLD_Filename_CLU_H).AsString;


  FASSET_to_rlf.Params.Clutters_section   :=aDataset.FieldByName(FLD_clutter_schema).AsString;


  FASSET_to_rlf.Params.RLF_FileName :=aDataset.FieldByName(FLD_filename_onega_rlf).AsString;

  FASSET_to_rlf.Params.Rlf_Step     :=aDataset.FieldByName(FLD_step).AsInteger;


  FASSET_to_rlf.Params.NullValue   :=aDataset.FieldByName('Null_value').AsInteger;



  try
    FASSET_to_rlf.Run_new;
  except

  end;


  FreeAndNil(FASSET_to_rlf);


//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;


// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.Run_ENVI(aDataset: TDataset);
// ---------------------------------------------------------------
var
  FENVI_to_rlf: TENVI_to_rlf;

begin


  FENVI_to_rlf := TENVI_to_rlf.Create();
  FENVI_to_rlf.OnProgress :=DoOnProgress;


  FTerminated := False;

  //btn_Run.Action := act_Stop;

  FENVI_to_rlf.Params.FileName_Dem    := aDataset.FieldByName(FLD_Filename_DTM).AsString;
  FENVI_to_rlf.Params.Zone_UTM        := aDataset.FieldByName(FLD_UTM_zone).AsInteger;

  FENVI_to_rlf.Params.IsUse_Clutter    := aDataset.FieldByName(FLD_Is_use_clutter).AsBoolean;
  FENVI_to_rlf.Params.FileName_Clutter := aDataset.FieldByName(FLD_filename_CLU).AsString;
//  FASSET_to_rlf.Params.Is_Use_Clutter_bounds          := aDataset.FieldByName(FLD_Is_Use_Clutter_bounds).AsBoolean;



  FENVI_to_rlf.Params.IsUse_Clutter_H          := aDataset.FieldByName(FLD_Is_use_Clutter_Heights).AsBoolean;
  FENVI_to_rlf.Params.FileName_Clutter_H := aDataset.FieldByName(FLD_Filename_CLU_H).AsString;


  FENVI_to_rlf.Params.Clutters_section   :=aDataset.FieldByName(FLD_clutter_schema).AsString;


  FENVI_to_rlf.Params.RLF_FileName :=aDataset.FieldByName(FLD_filename_onega_rlf).AsString;
  FENVI_to_rlf.Params.Rlf_Step     :=aDataset.FieldByName(FLD_step).AsInteger;

  FENVI_to_rlf.Params.Null_value   :=aDataset.FieldByName('Null_value').AsInteger;



  try
    FENVI_to_rlf.Execute;
  except

  end;


  FreeAndNil(FENVI_to_rlf);


//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;



procedure Tframe_ASSET_to_RLF.act_Run_allExecute(Sender: TObject);
begin
  Run_All;
end;


procedure Tframe_ASSET_to_RLF.ApplicationEvents1Idle(Sender: TObject; var Done:
    Boolean);
var
  s: string;
begin
  if not t_Project.IsEmpty then
  begin
    s:=t_Project.FieldByName('name').AsString;
  
    TabSheet1_Tasks.Caption:=Format('Задания (%s)',[s]);
  end;
      
end;


procedure Tframe_ASSET_to_RLF.FormShow(Sender: TObject);
begin
  t_Project.Locate('name', dxBarLookupCombo1.Text, []);
  
end;


// ---------------------------------------------------------------
procedure Tframe_ASSET_to_RLF.Run_All;
// ---------------------------------------------------------------
begin
  q_Tasks_checked.Close;
  q_Tasks_checked.Open;

  {
'
  select * 
from Task 
where project_id=:id and checked=true

order by Filename_DTM
'
}



 // db_View(q_Tasks_checked);
  
  FTerminated:=False;
  
  with q_Tasks_checked do 
    while not EOF and not FTerminated do
    begin
      Run (q_Tasks_checked) ;
    
      Next;
    end;  
       
  
end;

procedure Tframe_ASSET_to_RLF.t_TaskCalcFields(DataSet: TDataSet);
var
  s: string;
begin
  s:=DataSet.FieldByName('filename_onega_rlf').AsString;
  DataSet['is_rlf_exists']:=FileExists(s);
  
end;



//var 
//  k: Integer;

begin
// k:= StrToInt('$A1');

//ShowMessage(    GetParentFolderName_v1( 'd:\aaaa\dddd.txt')  );


end.

 
