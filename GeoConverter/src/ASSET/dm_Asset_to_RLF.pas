unit dm_Asset_to_RLF;

interface

uses
  System.SysUtils, System.Classes,  IOUtils, DB,  Variants,

//  CodeSiteLogging,
  
  u_classes_projection_TXT,

  u_db,
  u_func, 
  u_files,
  
  u_Config_ini,
  
  u_Asset, Data.Win.ADODB
  ;

type
  TdmAsset_to_RLF = class(TDataModule)
    t_Task: TADOTable;
    t_Taskid: TAutoIncField;
    t_Taskproject_id: TIntegerField;
    t_Taskchecked: TBooleanField;
    t_Taskfilename_DTM: TWideStringField;
    t_Taskfilename_CLU: TWideStringField;
    t_Taskfilename_CLU_H: TWideStringField;
    t_Taskfilename_projection_txt: TWideStringField;
    t_Taskis_use_clutter: TBooleanField;
    t_TaskIs_Use_Clutter_bounds: TBooleanField;
    t_Taskis_use_Clutter_Heights: TBooleanField;
    t_TaskUTM_zone: TIntegerField;
    t_Taskclutter_schema: TWideStringField;
    t_Taskfilename_onega_rlf: TWideStringField;
    t_Taskstep: TSmallintField;
    t_TaskDir: TWideStringField;
    t_Taskis_rlf_exists: TBooleanField;
    ds_Tasks: TDataSource;
    ds_Project: TDataSource;
    q_Project: TADOQuery;
  private
    function Find_Index_txt(aDir, aPath: string; var aFileName: string): Boolean;
//    aDataset: TDataset;

    procedure Process_Dir(aDir: string; aDataset: TDataSet; aClutter_schema: string
        = '');
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmAsset_to_RLF: TdmAsset_to_RLF;

const
  FLD_Filename_onega_rlf      = 'filename_onega_rlf';
  FLD_Filename_CLU_H          = 'filename_CLU_H';
  FLD_Filename_CLU            = 'filename_CLU';
  FLD_Is_Use_Clutter_bounds   = 'Is_Use_Clutter_bounds';
  FLD_Is_use_Clutter_Heights  = 'is_use_Clutter_Heights';
  FLD_Is_use_clutter          = 'is_use_clutter';
  FLD_Filename_DTM            = 'filename_DTM';
  FLD_filename_projection_txt = 'filename_projection_txt';  //--filename_projection_txt
  FLD_clutter_schema          = 'clutter_schema';
  FLD_step                    = 'step';
  FLD_UTM_zone                = 'UTM_zone';

  FLD_DIR = 'Dir';
  

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

// ---------------------------------------------------------------
function TdmAsset_to_RLF.Find_Index_txt(aDir, aPath: string; var aFileName:  string): Boolean;
// ---------------------------------------------------------------

var
  s: string;
  Arr: TArray<string>;

begin
  arr:= aPath.Split([',']);

  for s in arr  do
  begin
    aFileName:=IncludeTrailingBackslash(aDir) + IncludeTrailingBackslash(s)   + 'index.txt';

    if FileExists ( aFileName ) then
    begin
     // aFileName:= sFile

      Result:=True;
      Exit;
    end;  
    
  end;
  
    
  Result:= False; 
end;

// ---------------------------------------------------------------
procedure TdmAsset_to_RLF.Process_Dir(aDir: string; aDataset: TDataSet;
    aClutter_schema: string = '');
// ---------------------------------------------------------------
var
  k: Integer;
  oAsset_CLU: TAsset_index_file;
  oAsset_CLU_H: TAsset_index_file;
  oAsset_DTM: TAsset_index_file;
  
//  sDir: string;
  sDir_Clutter_Classes: string;
  sDir_Clutter_H: string;
  sDir_DTM: string;


  I: Integer;
  iInd: Integer;
  iStep: Integer;
  iZone: Integer;
  oProjection_file: TProjection_Txt_file;
  sCLU_H_index_txt: string;
  sCLU_index_txt: string;
//  sClutter_schema: string;
  sDEM: string;
  sDEM_index_txt: string;
  sDir_CLU: string;
  sDir_CLU_H: string;
  sDir_DEM: string;
  sFile_rlf: string;
  sName: string;
  sName_CLU: string;
  sName_CLU_H: string;
  sName_DTM: string;
  sPath_proj: string;

//  oIni: TIniFile;
  sPath: string;

begin

//  oIni:=TIniFile.create ( ChangeFileExt (Application.ExeName, '.ini'));
  

  aDir:= IncludeTrailingBackslash(aDir);

 // if cxShellBrowserDialog1.Execute then 

    if aClutter_schema='' then
      aClutter_schema:= aDataset.FieldByName(FLD_clutter_schema).AsString;

  
    oAsset_DTM:=TAsset_index_file.Create;
    oAsset_CLU:=TAsset_index_file.Create;
    oAsset_CLU_H:=TAsset_index_file.Create;
    oProjection_file:=TProjection_Txt_file.Create;

    

  //  sDir:= IncludeTrailingBackslash(cxShellBrowserDialog1.Path);

//    sDir:= 'P:\_megafon\83_NW_NenetskyAO_UTM40\';
    
    
//    sDir_DTM            :=sDir + 'Height\';
//
    iZone:=0;
           

    for sPath_proj in TDirectory.GetFiles (aDir, '*.prj')  do
    begin
      oProjection_file.PROJCS:=IOUtils.TFile.ReadAllText(sPath_proj );
    end;  
   
    if oProjection_file.PROJCS  <> '' then    
      for sPath_proj in TDirectory.GetFiles (aDir, 'proj*.txt')  do
      begin
        oProjection_file.LoadFromFile(sPath_proj);
        iZone:=oProjection_file.Zone_UTM;
      end;  

 
 //   sDir_DEM:=aDir + 'Height\';
    sPath:=g_Config.relief; //.   oIni.ReadString('main','relief','');
  //  Assert(sPath<>'');

 
    if Find_Index_txt (aDir,sPath, sDEM_index_txt) then
//      if FileExists(sDir_DEM + 'index.txt') then
    begin
      oAsset_DTM.LoadFromFile(sDEM_index_txt); //  sDir_DEM + 'index.txt');
      oAsset_DTM.ParseToFiles;
    end;  

    //================================================
    
    sPath:=g_Config.clutter; //:=oIni.ReadString('main','clutter','');
//    Assert(sPath<>'');
    
    if Find_Index_txt (aDir,sPath, sCLU_index_txt) then
                                       
//    sDir_CLU:=aDir + 'Clutter\';
//    if FileExists(sDir_CLU + 'index.txt') then
    begin    
      oAsset_CLU.LoadFromFile (sCLU_index_txt); //sDir_CLU + 'index.txt');
      oAsset_CLU.ParseToFiles;
    end;

    //================================================
    
    //
 //   sDir_CLU_H:=aDir + 'obstacle\';
    sPath:=g_Config.clutter_h; //  :=oIni.ReadString('main','clutter_h','');
 //   Assert(sPath<>'');

    if Find_Index_txt (aDir, sPath, sCLU_H_index_txt) then
//    if FileExists(sDir_CLU_H + 'index.txt') then
    begin
      oAsset_CLU_H.LoadFromFile (sCLU_H_index_txt); //sDir_CLU_H + 'index.txt');
      oAsset_CLU_H.ParseToFiles;
    end;  

    //================================================

    aDataset.DisableControls;

    
    for I := 0 to oAsset_DTM.Groups_ex.Count-1 do
    begin
      sName:=oAsset_DTM.Groups_ex[i];
      
//CodeSite.Send('oAsset_DTM.Groups_ex[i] -' + sName);

//Continue;


      iStep:=Integer(oAsset_DTM.Groups_ex.Objects[i]);

//      sName_DTM:=  sDir_DEM + 'index_'+ sName +'.txt';
      sName_DTM:=  ExtractFilePath(sDEM_index_txt) + 'index_'+ sName +'.txt';

    
      
      //-------------------------------------------   
      iInd:=oAsset_CLU.Groups_ex.IndexOf(sName);
      if iInd>=0 then
         sName_CLU:=  ExtractFilePath(sCLU_index_txt) + 'index_'+ oAsset_CLU.Groups_ex[iInd]+'.txt'
      else
         sName_CLU:='';

      //-------------------------------------------   
      iInd:=oAsset_CLU_H.Groups_ex.IndexOf(sName);
      if iInd>=0 then
         sName_CLU_H:= ExtractFilePath(sCLU_H_index_txt) + 'index_'+ oAsset_CLU_H.Groups_ex[iInd]+'.txt'
      else
         sName_CLU_H:='';

      //-------------------------------------------   
      sFile_rlf:= aDir + GetParentFolderName_v1 (aDir) +'_'+ sName + '.rlf';

      
         
     if aDataset.Locate(FLD_PROJECT_ID+';'+FLD_Filename_DTM, VarArrayOf([q_Project [FLD_ID],  sName_DTM]), []) then
        aDataset.Delete;

         
     db_AddRecord_(aDataset, [

        FLD_DIR                    , aDir,

        FLD_PROJECT_ID,              q_Project [FLD_ID],

     
        FLD_Filename_DTM            , sName_DTM,
        FLD_filename_projection_txt , sPath_proj, //'filename_projection_txt',  
        FLD_UTM_zone                , iZone,      // 'UTM_zone',
     

        FLD_Is_use_clutter          , IIF(sName_CLU='', False, True),
        FLD_Filename_CLU            , IIF(sName_CLU='', '', sName_CLU),

        FLD_Is_use_Clutter_Heights  , IIF(sName_CLU_H='', False, True),
        FLD_Filename_CLU_H          , IIF(sName_CLU_H='', '', sName_CLU_H),
      
        FLD_clutter_schema          , aClutter_schema,

        FLD_Filename_onega_rlf      , sFile_rlf, //aDir + GetParentFolderName_v1 + sName + '.rlf',
        FLD_step                    , iStep
 

       ]);
   
    end;  

    aDataset.EnableControls;


    FreeAndNil(oAsset_DTM);
    FreeAndNil(oAsset_CLU);
    FreeAndNil(oAsset_CLU_H);
    FreeAndNil(oProjection_file);

    //  FreeAndNil(oIni);
      
 // end;

    
end;

end.
