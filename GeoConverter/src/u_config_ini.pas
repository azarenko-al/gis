unit u_config_ini;

interface

uses SysUtils, Forms, IniFiles;


type
  TConfigIniRec= record

     relief: string;
     clutter: string;
     clutter_h: string;

     MaxRows: word;
     MaxCols: word;

     Debug: record
       IsMakeJSON : boolean;
     end;

     procedure Load;
  end;


var
  g_Config: TConfigIniRec;
  

implementation


// ---------------------------------------------------------------
procedure TConfigIniRec.Load;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  sFile: string;

begin
  sFile:=ExtractFilePath(Application.ExeName) + 'config.ini';

  if not FileExists (sFile) then
    Exit;


  oIni:=TIniFile.create (sFile);

  relief    :=oIni.ReadString('dir','relief','Height,DTM');
  clutter   :=oIni.ReadString('dir','clutter','Clutter,DLU');
  clutter_h :=oIni.ReadString('dir','clutter_h','obstacle,DHM');


  MaxRows:=oIni.ReadInteger('main','MaxRows',0);
  MaxCols:=oIni.ReadInteger('main','MaxCols',0);

  Assert (MaxRows>0);
  Assert (MaxCols>0);


  Debug.IsMakeJSON :=oIni.ReadBool('Debug','IsMakeJSON',false);


  FreeAndNil(oIni);

end;


begin
  g_Config.Load;

end.
