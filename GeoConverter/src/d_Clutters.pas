unit d_Clutters;

interface

uses
  Variants, Classes, Controls, Forms,
  Dialogs, ExtCtrls, rxPlacemnt, StdCtrls, ActnList,
  Data.Win.ADODB, cxSplitter,

//  u_const,
  dm_Clutters,

  Menus,  cxGridDBTableView,
  cxGridLevel, cxGrid, cxGridCustomTableView,
  StdActns, DBCtrls,

  Data.DB, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxCheckBox, cxDBLookupComboBox, System.Actions,
  cxGridTableView, cxClasses, cxGridCustomView;


type
  Tdlg_Clutters = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    OpenDialog1: TOpenDialog;
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    act_Load_Menu_TXT: TAction;
    PopupMenu1: TPopupMenu;
    actLoadMenuTXT1: TMenuItem;
    Button3: TButton;
    cxGrid_left: TcxGrid;
    cxGridDBTableView1_group: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    Button4: TButton;
    act_FileOpen_GRC: TFileOpen;
    MainMenu1: TMainMenu;
    MenuTXT1: TMenuItem;
    N1: TMenuItem;
    OpenMapinfoGRC1: TMenuItem;
    col_group_name: TcxGridDBColumn;
    t_schema: TADOTable;
    t_onega_clutters: TADOTable;
    ds_schema: TDataSource;
    ds_schema_clutters: TDataSource;
    ds_onega_clutters: TDataSource;
    cxGrid_main: TcxGrid;
    cxGrid_mainDBTableView1_items: TcxGridDBTableView;
    cxGrid_mainDBTableView1_itemsschema_name: TcxGridDBColumn;
    cxGrid_mainDBTableView1_itemsColumn1: TcxGridDBColumn;
    col_enabled: TcxGridDBColumn;
    col_code: TcxGridDBColumn;
    cxGrid_mainDBTableView1_itemsname: TcxGridDBColumn;
    col_onega_name: TcxGridDBColumn;
    cxGrid_mainDBTableView1_itemsheight: TcxGridDBColumn;
    col_comment: TcxGridDBColumn;
    cxGrid_mainLevel1: TcxGridLevel;
    cxSplitter1: TcxSplitter;
    cxGridDBTableView1_groupColumn1: TcxGridDBColumn;
    q_schema_clutters: TADOQuery;
    col_onega_code: TcxGridDBColumn;
    OpenMapinfoGRC2: TMenuItem;
    procedure act_FileOpen_GRCAccept(Sender: TObject);
    procedure act_FileOpen_GRCBeforeExecute(Sender: TObject);
    procedure act_Load_Menu_TXTExecute(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private 
    FRegPath: string;
         
  
    { Private declarations }
  public
    class function ExecDlg(var aName: string): Boolean;
    class procedure ExecDlg_NoResult;


  end;

var
  dlg_Clutters: Tdlg_Clutters;

implementation

{$R *.dfm}


procedure Tdlg_Clutters.act_FileOpen_GRCAccept(Sender: TObject);
begin
//  dmClutters.LoadFromFile_Mapinfo_GRC(act_FileOpen_GRC.Dialog.FileName);

end;

procedure Tdlg_Clutters.act_FileOpen_GRCBeforeExecute(Sender: TObject);
begin
   //
end;


procedure Tdlg_Clutters.act_Load_Menu_TXTExecute(Sender: TObject);
begin
  if Sender=act_Load_Menu_TXT then
    if openDialog1.Execute then
    begin
      dmClutters.LoadFromFile_Menu_TXT( t_schema [FLD_ID],  openDialog1.FileName);

      q_schema_clutters.Close;
      q_schema_clutters.Open;
    end;
end;

procedure Tdlg_Clutters.Button4Click(Sender: TObject);
begin

end;

// ---------------------------------------------------------------
class procedure Tdlg_Clutters.ExecDlg_NoResult;
// ---------------------------------------------------------------
begin
   with Tdlg_Clutters.Create(Application) do
   begin
//     dmClutters.t_schema.Locate(FLD_NAME, aName, []);
   
     ShowModal;

  //   if Result then
    //   aName := dmClutters.t_schema[FLD_Name];
   
     Free;
   end;

end;


// ---------------------------------------------------------------
class function Tdlg_Clutters.ExecDlg(var aName: string): Boolean;
// ---------------------------------------------------------------
begin
   with Tdlg_Clutters.Create(Application) do
   begin
     t_schema.Locate(FLD_NAME, aName, []);


     Result := ShowModal=mrOk;

     if Result then
       aName := t_schema[FLD_Name];


     Free;
   end;

end;

procedure Tdlg_Clutters.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

// ---------------------------------------------------------------
procedure Tdlg_Clutters.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  FRegPath := 'Software\Onega\ASSET_to_RLF\' + Tdlg_Clutters.ClassName + '\' ;


//  dmClutters();

 // DBGrid2.Align:=alClient;

//  dbgrid1.Columns[0].Title.Caption := '�����';
(*
  dbgrid2.Columns[0].Title.Caption := '���';
  dbgrid2.Columns[1].Title.Caption := '��������';
  dbgrid2.Columns[2].Title.Caption := 'ONEGA ���';
  dbgrid2.Columns[3].Title.Caption := '������ [m]';
  dbgrid2.Columns[4].Title.Caption := '����������';
*)
  Caption := '��������� ���������';


  cxGrid_main.Align:=alClient;
  //pn_Main.Align:=alClient;

  col_group_name.Caption:='��������';
//  col_group_comment.Caption:='����������';
  col_comment.Caption:='����������';


  t_schema.Open;
  t_onega_clutters.Open;

  q_schema_clutters.Open;

  

(*  cx_CheckDataFields (cxGridDBTableView1_group);
  cx_CheckDataFields (cxGrid1DBTableView1_items);
*)
end;


end.
