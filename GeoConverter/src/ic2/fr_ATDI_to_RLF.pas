unit fr_ATDI_to_RLF;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls,
  IniFiles,rxToolEdit, Dialogs,

  u_const,

  fr_Custom_Form,

  u_ATDI_to_RLF,

  frame_RLF,
  frame_Clutter,


   Menus, rxPlacemnt, Mask, ActnList, System.Actions

  ;

type
  Tframe_ATDI_to_RLF = class(Tfrm_Custom_Form)
    GroupBox2: TGroupBox;
    ed_Clutter_Classes: TFilenameEdit;
    cb_Clutter_Classes: TCheckBox;
    ed_Clutter_H: TFilenameEdit;
    cb_Clutter_heights: TCheckBox;
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    FormStorage1: TFormStorage;
    PopupMenu1: TPopupMenu;
    actSelectfolder1: TMenuItem;
    ActionList1: TActionList;
    act_Select_folder1: TAction;
    procedure act_Select_folderExecute(Sender: TObject);
   // procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  //  procedure FormStorage1RestorePlacement(Sender: TObject);
 //   procedure FormStorage1SavePlacement(Sender: TObject);
    procedure frame_Clutter_1Button1Click(Sender: TObject);
  //  procedure JvFormStorage1RestorePlacement(Sender: TObject);
 //   procedure JvFormStorage1SavePlacement(Sender: TObject);

  private


    FIniFileName: string;

    FATDI_to_RLF: TATDI_to_RLF;

    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run; override;
  public

    procedure LoadFromIniFile(aFileName: String);


  end;

var
  frame_ATDI_to_RLF: Tframe_ATDI_to_RLF;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'ATDI_to_RLF';



procedure Tframe_ATDI_to_RLF.act_Select_folderExecute(Sender: TObject);
var
  sDir: string;
begin
  inherited;

//  if JvBrowseForFolderDialog1.Execute then
 {
  if PBFolderDialog11.Execute then
  begin

//    sDir:= IncludeTrailingBackslash( JvBrowseForFolderDialog1.Directory);

    sDir:= IncludeTrailingBackslash( PBFolderDialog11.Folder);

    ed_DEM.FileName             :=sDir + 'Height\';
    ed_Clutter_H.FileName       :=sDir + 'obstacle\';
    ed_Clutter_Classes.FileName :=sDir + 'Clutter\';

  end;  }
end;


// ---------------------------------------------------------------
procedure Tframe_ATDI_to_RLF.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  FATDI_to_RLF := TATDI_to_RLF.Create();
  FATDI_to_RLF.OnProgress :=DoOnProgress;

  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);

  frm_RLF1.Init;
  frame_Clutter_1.Init;

  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;
  cb_Clutter_Classes.Caption  := STR_Clutter_Classes;
  cb_Clutter_Heights.Caption  := STR_Clutter_Heights;


 // lb_Menu_txt.Caption  := STR_Menu_txt;


end;

procedure Tframe_ATDI_to_RLF.FormDestroy(Sender: TObject);
begin
  FormStorage1.SaveFormPlacement;

  SaveToIniFile(FIniFileName);

  FreeAndNil(FATDI_to_RLF);
end;




procedure Tframe_ATDI_to_RLF.frame_Clutter_1Button1Click(Sender: TObject);
begin
  inherited;
  frame_Clutter_1.Button1Click(Sender);
end;


// ---------------------------------------------------------------
procedure Tframe_ATDI_to_RLF.Run;
// ---------------------------------------------------------------
begin
  inherited;

  FTerminated := False;

  //btn_Run.Action := act_Stop;

  FATDI_to_RLF.Params.Dem_FileName := ed_DEM.FileName;

  FATDI_to_RLF.Params.IsUse_Clutter_Classes    := cb_Clutter_Classes.Checked;
  FATDI_to_RLF.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;

  FATDI_to_RLF.Params.IsUse_Clutter_Height     := cb_Clutter_heights.Checked;
  FATDI_to_RLF.Params.Clutter_Heights_fileName := ed_Clutter_H.FileName;


//  cb_Clutter_height

//  FIC2_to_RLF.Params.Passport_FileName := ed_Passport.FileName;
//  FIC2_to_RLF.Params.Menu_Txt_FileName := ed_Menu_TXT1.FileName;

//  frm_RLF1.GetBorderPoints(FATDI_to_RLF.Params.BorderBLPoints);

//  Border_FileName := frm_RLF1.GetBorderFileName();


//  FIC2_to_RLF.Params.


//  Border_FileName


 // FIC2_to_RLF.Params.Dem_FileName := ed_DEM.FileName;
 // FIC2_to_RLF.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;
 // FIC2_to_RLF.Clutter_Heights_FileName := ed_Clutter_Heights.FileName;

  FATDI_to_RLF.Params.Clutters_Section    := frame_Clutter_1.GetSection();
 // FIC2_to_RLF.Params.Clutters_ini_fileName    := ed_Clutters_ini.FileName;

  FATDI_to_RLF.Params.Rlf_FileName := frm_RLF1.ed_RLF.FileName;
  FATDI_to_RLF.Params.Rlf_Step     := frm_RLF1.GetStepM ;


  FATDI_to_RLF.Run;

//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;

// ---------------------------------------------------------------
procedure Tframe_ATDI_to_RLF.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do
 // with TIniFile.Create(FIniFileName) do
  begin
    ed_DEM.FileName := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

    ed_Clutter_Classes.FileName := ReadString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    ed_Clutter_H.FileName       := ReadString(DEF_SECTION, ed_Clutter_H.Name,ed_Clutter_H.FileName);

//    ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);

 //   ed_Passport.FileName    := ReadString(DEF_SECTION, ed_Passport.Name, ed_Passport.FileName);

 //   ed_Menu_TXT1.FileName    := ReadString(DEF_SECTION, ed_Menu_TXT1.Name, ed_Menu_TXT1.FileName);

  //  ed_RLF1.FileName  := ReadString(DEF_SECTION, ed_RLF1.Name, ed_RLF1.FileName);

    cb_Clutter_Classes.checked:= ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_heights.Name, cb_Clutter_heights.checked);

  //  cb_Clutter_Heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);

   // Free;
  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;

// ---------------------------------------------------------------
procedure Tframe_ATDI_to_RLF.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  s: string;

var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do

  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

    WriteString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    WriteString(DEF_SECTION, ed_Clutter_H.Name, ed_Clutter_H.FileName);
  //  WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);

  //  WriteString(DEF_SECTION, ed_Passport.Name, ed_Passport.FileName);

 ///   WriteString(DEF_SECTION, ed_Menu_TXT1.Name, ed_Menu_TXT1.FileName);

 //   WriteString(DEF_SECTION, ed_RLF1.Name, ed_RLF1.FileName);


    WriteBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    WriteBool(DEF_SECTION, cb_Clutter_heights.Name, cb_Clutter_heights.checked);

  //  WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);

(*

    WriteString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    WriteString(DEF_SECTION, ed_Clutter_Heights.Name, ed_Clutter_Heights.FileName);
    WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);


*)
  //  Free;
  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);


  FreeAndNil(oIniFile);

end;


end.
