unit u_IC2;

interface
uses
  Classes,SysUtils ,

  u_BufferedFileStream,

  u_Geo
  ;

type
  // ---------------------------------------------------------------
  TATDI_File = class(TObject)
  // ---------------------------------------------------------------
  private
    FStream: TReadOnlyCachedFileStream;

    function ByteArrayToStr(aArr: array of byte): string;
    procedure LoadHeader;

  public
  //  GK_zone : Integer;
    UTM_zone : Integer;

    ByteCountPerCell : Integer;

    XYBounds: TXYRect;

    StepX : Double;
    StepY : Double;

    RowCount : Integer;
    ColCount : Integer;

    NullValue : Word;

 //   ByteOrder: (bmNone,bmReverse);

  //  FileName : string;

    constructor Create;
    destructor Destroy; override;

    procedure CloseFile;

    function FindValueByXY(aX, aY: double; var aValue: word): Boolean;
    function FindCellByXY(aX, aY: double; var aRow, aCol: integer): boolean;

    function GetDataByIndex(aIndex: Integer): word;
    function GetValueByRowCol(aRow,aCol: Integer; var aValue: word): Boolean;

    procedure OpenFile(aFileName: string);

  end;



implementation



constructor TATDI_File.Create;
begin
  ByteCountPerCell :=2;

  NullValue :=$D8F1;
end;

destructor TATDI_File.Destroy;
begin
end;

//---------------------------------------------------------------
function TATDI_File.FindCellByXY(aX, aY: double; var aRow, aCol: integer):
    boolean;
//--------------------------------------------------------------
var
  b: boolean;
begin
  Result:=False;

  aRow:=0; aCol:=0;

//  if not (Active) then
 //   Exit;
//  assert( XYBounds.BottomRight.x>0);

//  if MatrixType=mtXY_ then
//  begin

  b:=(aX < XYBounds.TopLeft.x);
  b:=(XYBounds.BottomRight.x < aX);

  b:=(XYBounds.TopLeft.y < aY);
  b:= aY < XYBounds.BottomRight.y;


  if (XYBounds.BottomRight.x < aX) and (aX < XYBounds.TopLeft.x) and
     (XYBounds.TopLeft.y     < aY) and (aY < XYBounds.BottomRight.y) then
  begin
    aRow := Integer(Round((XYBounds.TopLeft.x - aX) / StepX));
    aCol := Integer(Round((aY - XYBounds.TopLeft.y) / StepY));

    Result:=(aRow>=0) and (aCol>=0) and (aRow<=RowCount-1) and (aCol<=ColCount-1);

//      Result:=true;
  end;
// end;

end;

// ---------------------------------------------------------------
function TATDI_File.FindValueByXY(aX, aY: double; var aValue: word): Boolean;
// ---------------------------------------------------------------
var
  iRow,iCol: Integer;
begin

  Result := FindCellByXY(aX, aY, iRow,iCol);

  if Result and Assigned(FStream) then
  begin
//    iOffset :=(ColCount*iRow + iCol) * iByteCount;

    aValue:=GetDataByIndex(ColCount*iRow + iCol);

//    aValue :=iIntValue;

//    if aValue=$FFFF then
    if aValue=Nullvalue then
      Result:=False;



//    FStream.Seek (iOffset,  soFromBeginning);
//
//    case iByteCount of
//(*      1: begin
//           FStream.read(iByteValue, 1);
//           Result:=iByteValue<>NullValue;
//           aValue :=iByteValue;
//         end;*)
//      2: begin
//           FStream.read(iIntValue, 2);
//
//           if ByteOrder=bmReverse then
//             iIntValue := MacWordToPC(iIntValue);
//
//         //  if Header.ByteOrder='M' then
//           //  iIntValue := MacWordToPC(iIntValue);
//
//         //  iIntValue :=-9999;
//
//           Result:=iIntValue<>NullValue;
//           aValue :=iIntValue;
//         end;
//    end;

  end;

end;

// ---------------------------------------------------------------
procedure TATDI_File.OpenFile(aFileName: string);
// ---------------------------------------------------------------
begin
  if Assigned(FStream) then
    FreeAndNil(FStream);

 // Assert(not Assigned(FStream), 'Value not assigned');

 // aFileName :=ChangeFileExt(aFileName, '.bil');
  FStream:=TReadOnlyCachedFileStream.Create(aFileName);

  LoadHeader();
end;


procedure TATDI_File.CloseFile;
begin
  FreeAndNil(FStream);
end;


//---------------------------------------------------------------------
function TATDI_File.GetValueByRowCol(aRow,aCol: Integer; var aValue: word):
    Boolean;
//---------------------------------------------------------------------
begin
  aValue := GetDataByIndex(aRow * ColCount + aCol);

  Result := True;
end;

// ---------------------------------------------------------------
function TATDI_File.GetDataByIndex(aIndex: Integer): word;
// ---------------------------------------------------------------
(*
//    function MacWordToPC(Value : Smallint): Smallint;
    function MacWordToPC(Value : word): word;
    asm xchg Al,Ah
    end;*)

var
//  iByteCountPerCell: Integer;
 // iIntValue: Integer;
  iOffset: Integer;
  bt: Byte;
  w: word;
  w1: word;
begin
//  iByteCountPerCell:=2;

  iOffset := aIndex*ByteCountPerCell; //(ColCount*iRow + iCol) * iByteCount;


  FStream.Seek (1010 + iOffset,  soFromBeginning);

 // FStream.Read(Result, ByteCountPerCell);
     // D8F1

  case ByteCountPerCell of

    1: begin
         FStream.Read(bt, ByteCountPerCell);
         Result:=bt;
       end;

    2: begin
         FStream.Read(w, ByteCountPerCell);

         if w<>NullValue then
           if w>20000 then
             begin
               w:=NullValue;
               //w1 := MacWordToPC(w);
             end;

           


        // if w=$#D8F1 then


      //   w1 := MacWordToPC(w);

        // FStream.read(iIntValue, 2);

      //   if ByteOrder=bmReverse then
       //    iIntValue := MacWordToPC(iIntValue);

       //  if Header.ByteOrder='M' then
         //  iIntValue := MacWordToPC(iIntValue);

       //  iIntValue :=-9999;

         Result:=w;
//         iIntValue<>NullValue;
 //        aValue :=iIntValue;

       end;

  end;


                        // :=
//    case iByteCount of
//(*      1: begin
//           FStream.read(iByteValue, 1);
//           Result:=iByteValue<>NullValue;
//           aValue :=iByteValue;
//         end;*)
//      2: begin
//           FStream.read(iIntValue, 2);
//
//           if ByteOrder=bmReverse then
//             iIntValue := MacWordToPC(iIntValue);
//
//         //  if Header.ByteOrder='M' then
//           //  iIntValue := MacWordToPC(iIntValue);
//
//         //  iIntValue :=-9999;
//
//           Result:=iIntValue<>NullValue;
//           aValue :=iIntValue;
//         end;
//    end;
//


//  Result := ;
end;

// ---------------------------------------------------------------
procedure TATDI_File.LoadHeader;
// ---------------------------------------------------------------

type
  TIC2_file_header = record
    NotDef1: array[1..16*10] of byte;

    Numbers: array[1..8] of
      record
        V: array[1..15] of byte;
      end;

    CellX,CellY: array[1..10] of byte;
    NotDef2: array[1..4+16] of byte;

    ColCount1,RowCount1: array[1..10] of byte;

    UTN: array[0..30] of byte;
  end;


  TIC2_header_new = record
    Number_str: array[1..8] of string;
    Numbers:    array[1..8] of Double;

    //    Number_str: array[1..8] of string;

    CellX_str,
    CellY_str: string;

    CellX,
    CellY: Double;

    RowCount_str,
    ColCount_str: string;

    RowCount,
    ColCount: Integer;

    UTN: string;
  end;

var
//  fs: TFileStream;
  r: TIC2_file_header;
  r1: TIC2_header_new;

  I: Integer;
  iPos: Integer;
  //s: string;
begin
 // FillChar(r1, SizeOf(r1),0);


//  fs:=TFileStream.create('S:\-- tasks --\sfera\ics_telecom\clutter\clutter_2m.SOL',fmOpenRead);
  FStream.Position :=0;
  FStream.Read(r, SizeOf(r));


  for I := 1 to 8 do
    r1.Number_str[i]:=ByteArrayToStr(r.Numbers[i].V);


  r1.UTN:=ByteArrayToStr(r.UTN);

  iPos:=Pos('UTN',r1.UTN);
  if iPos>0 then
  begin
    UTM_zone:=StrToInt(Copy(r1.UTN, iPos+3, 100));
  end;


  r1.RowCount_str:=ByteArrayToStr(r.RowCount1);
  r1.ColCount_str:=ByteArrayToStr(r.ColCount1);

  r1.CellX_str:=ByteArrayToStr(r.CellX);
  r1.CellY_str:=ByteArrayToStr(r.CellY);

  FormatSettings.DecimalSeparator:='.';

  for I := 1 to 8 do
    r1.Numbers[i]:=StrToFloat(r1.Number_str[i]);


  RowCount:=StrToInt(r1.RowCount_str);
  ColCount:=StrToInt(r1.ColCount_str);

  StepX:=StrToFloat(r1.CellX_str);
  StepY:=StrToFloat(r1.CellY_str);

  XYBounds.TopLeft.Y:=r1.Numbers[1];
  XYBounds.TopLeft.X:=r1.Numbers[2];

  XYBounds.BottomRight.Y:=XYBounds.TopLeft.Y + ColCount*StepY;
  XYBounds.BottomRight.X:=XYBounds.TopLeft.X - RowCount*StepX;


//  UTM_zone :=36;

//  freeandnil(fs);
end;


// ---------------------------------------------------------------
function TATDI_File.ByteArrayToStr(aArr: array of byte): string;
// ---------------------------------------------------------------
var
  j: Integer;
  b: byte;
begin
  Result:='';

  for j := 0 to SizeOf(aArr)-1 do
  begin
    b:=aArr[j];
    if b=0 then
    begin
      b:=0;
      Break;
    end;

    Result:=Result + Char(b);
  end;

end;



var

  o: TATDI_File;

  i,iVal : Integer;
begin
(*
  o:=TATDI_File.Create;


//  o.OpenFile('S:\-- tasks --\sfera\ics_telecom\clutter\clutter_2m.SOL');
  o.OpenFile('O:\_alex\Kaliningrad_12102012\isc_telecom\clutter\clutter_kaliningrad_5m.SOL');

*)

end.





