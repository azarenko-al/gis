inherited frame_ATDI_to_RLF: Tframe_ATDI_to_RLF
  Left = 1406
  Top = 256
  Caption = 'frame_ATDI_to_RLF'
  ClientHeight = 560
  ClientWidth = 592
  OnDestroy = FormDestroy
  ExplicitLeft = 1406
  ExplicitTop = 256
  ExplicitWidth = 600
  ExplicitHeight = 588
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 544
    Width = 592
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 544
    ExplicitWidth = 592
  end
  object GroupBox2: TGroupBox [1]
    Left = 0
    Top = 57
    Width = 592
    Height = 168
    Align = alTop
    TabOrder = 1
    DesignSize = (
      592
      168)
    object ed_Clutter_Classes: TFilenameEdit
      Left = 8
      Top = 36
      Width = 576
      Height = 21
      Filter = 'SOL (*.sol)|*.sol'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = '"S:\-- tasks --\sfera\ics_telecom\clutter\clutter_2m.SOL"'
    end
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 16
      Width = 400
      Height = 17
      Caption = 'Clutter_Classes'
      TabOrder = 1
    end
    object ed_Clutter_H: TFilenameEdit
      Left = 8
      Top = 81
      Width = 576
      Height = 21
      Filter = 'BLG (*.BLG)|*.BLG'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 2
      Text = '"S:\-- tasks --\sfera\ics_telecom\obstacle\obstacle_2m.BLG"'
    end
    object cb_Clutter_heights: TCheckBox
      Left = 8
      Top = 64
      Width = 400
      Height = 17
      Caption = 'Clutter height file'
      TabOrder = 3
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 0
    Width = 592
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    PopupMenu = PopupMenu1
    TabOrder = 2
    DesignSize = (
      592
      57)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 65
      Height = 13
      Caption = 'DEM '#1088#1077#1083#1100#1077#1092
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 574
      Height = 21
      Filter = 'geo (*.geo)|*.geo'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = '"S:\-- tasks --\sfera\ics_telecom\height\height_2m.GEO"'
    end
  end
  inherited ActionList_custom: TActionList
    Left = 272
    Top = 248
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = []
    UseRegistry = True
    StoredValues = <>
    Left = 160
    Top = 248
  end
  object PopupMenu1: TPopupMenu
    Left = 464
    Top = 248
    object actSelectfolder1: TMenuItem
      Action = act_Select_folder1
    end
  end
  object ActionList1: TActionList
    Left = 392
    Top = 248
    object act_Select_folder1: TAction
      Caption = 'act_Select_folder1'
      OnExecute = act_Select_folderExecute
    end
  end
end
