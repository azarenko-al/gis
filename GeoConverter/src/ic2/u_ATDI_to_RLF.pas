unit u_ATDI_to_RLF;


interface
uses Classes, Sysutils, 

  u_geo_convert_new1,

  dm_Clutters,

//  u_Rel_to_Clutter_map,


//  dm_Test,

  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,
  u_geo_poly,  

  u_CustomTask,

  u_clutter_classes,

//  u_bil_classes,
  u_IC2;



type
  TATDI_to_RLF = class(TCustomTask)
  private
    FUTM_zone : Integer;
    FGK_zone : Integer;

   // FDEM_index_file: TAsset_index_file;
//    FClutter_index_file: TAsset_index_file;

   //z FMenu_Txt_file: TClutter_Menu_Txt_file;

    FDEM:             TATDI_File;
    FClutter_Classes: TATDI_File;
    FClutter_Heights: TATDI_File;

//    FBil_Build_Heights: TBil_File;



    FClutterIniFile: TClutterInfo;

    procedure CloseFiles;
   protected
    function OpenFiles: Boolean; override;
   public

  Params: record
              Dem_FileName : string;

              BorderBLPoints: TBLPointArray;


              IsUse_Clutter_Classes : Boolean;
              Clutter_Classes_FileName : string;

              IsUse_Clutter_Height : Boolean;
              Clutter_Heights_fileName : string;

              Clutters_Section : string;

              RLF_FileName : string;
              Rlf_Step : Integer;
            end;

    constructor Create;

    destructor Destroy; override;
    procedure Run; override;
  end;


implementation

// ---------------------------------------------------------------
constructor TATDI_to_RLF.Create;
// ---------------------------------------------------------------
begin
  inherited;


//  FDEM_index_file:=TAsset_index_file.Create;
 // FClutter_index_file:=TAsset_index_file.Create;

  FDEM:=TATDI_File.Create;

  FClutter_Classes:=TATDI_File.Create;
  FClutter_Heights:=TATDI_File.Create;

  FClutterIniFile := TClutterInfo.Create();

//zzzz  FMenu_Txt_file := TClutter_Menu_Txt_file.Create();
end;


// ---------------------------------------------------------------
destructor TATDI_to_RLF.Destroy;
// ---------------------------------------------------------------
begin
 ///// FreeAndNil(FMenu_Txt_file);

  FreeAndNil(FClutterIniFile);

  FreeAndNil(FDEM);
  FreeAndNil(FClutter_Classes);
  FreeAndNil(FClutter_Heights);

 // FreeAndNil(FMenu_Txt_file);

  inherited;
end;


// ---------------------------------------------------------------
procedure TATDI_to_RLF.CloseFiles;
// ---------------------------------------------------------------
begin
  FDEM.CloseFile;
end;             


// ---------------------------------------------------------------
function TATDI_to_RLF.OpenFiles: Boolean;
// ---------------------------------------------------------------
//var
 // I: Integer;
 // s: string;
var
  I: Integer;
begin
  Result := False;

 (* if FileExists(params.Dem_IndexFileName) then
  begin
    FDEM_index_file.LoadFromFile(params.Dem_IndexFileName);
    if FDEM_index_file.Count>0 then
      params.DEM_fileName:= FDEM_index_file.Files[0].FileName;
  end;


  if FileExists(params.Clutter_Classes_IndexFileName) then
  begin
    FClutter_index_file.LoadFromFile(params.Clutter_Classes_IndexFileName);
    if FClutter_index_file.Count>0 then
      params.Clutter_Classes_FileName:= FClutter_index_file.Files[0].FileName;
  end;

*)
(*
  if FileExists(params.Passport_FileName) then
     FPassport_file.LoadFromFile(params.Passport_FileName);


  if FileExists(params.Projection_FileName) then
     FPassport_file.LoadFromFile(params.Projection_FileName);
*)

 // if FPassport_file.UtmZone>0 then
 // FUTM_zone :=FPassport_file.UtmZone;

 // else
  //  FUTM_zone :=Params.UTM_zone1;



  if Params.IsUse_Clutter_Classes then
  begin
    FClutter_Classes.OpenFile(params.Clutter_Classes_FileName);
    FClutter_Classes.ByteCountPerCell :=1;
   // FClutter_Classes.ByteOrder :=bmReverse;

   // FClutter_Classes.LoadPassport(params.Passport_FileName);


    if Params.IsUse_Clutter_Height then
      if FileExists(params.Clutter_Heights_fileName) then
      begin
        FClutter_Heights.OpenFile(params.Clutter_Heights_fileName);
        FClutter_Heights.ByteCountPerCell :=1;
      end;

   // if FileExists(params.Menu_Txt_FileName) then
   //   FMenu_Txt_file.LoadFromFile(params.Menu_Txt_FileName);


   // if FileExists(params.Clutters_ini_fileName) then
   // begin
    //  FClutterIniFile.LoadCluttersFromINI('ATDI');

  dmClutters.LoadCluttersFromDB(FClutterIniFile, Params.Clutters_Section);

   //   FClutterIniFile.LoadCluttersFromDB(params.Clutters_Section);

   //   with FMenu_Txt_file do
    //    for I := 0 to High(Items) do
      //    FClutterIniFile.SetIndex(Items[i].Name, Items[i].Code);

//          FClutterIniFile.GetOnegaCode(Items[i].Name, Items[i].Onega_Code, Items[i].Onega_Height);

   // end;

  end;
           

  FDEM.OpenFile(params.DEM_fileName);
  FDEM.ByteCountPerCell :=2;

  FUTM_zone :=FDEM.Utm_Zone;

  FGK_zone := FUTM_zone - 30;


(*  if FileExists(params.Build_Heights_fileName) then
    FBil_Build_Heights.OpenFile (params.Build_Heights_fileName);

*)

(*  if FileExists(params.Clutter_Classes_fileName) then
  begin
    FClutter_Classes.OpenFile (params.Clutter_Classes_fileName);

  //  s:= IncludeTrailingBackslash(ExtractFileDir(params.Clutter_Classes_fileName)) + 'Menu.Txt';
  //  FMenu_Txt_file.LoadFromFile(s);
  end;*)

(*
  if FileExists(params.Clutter_Heights_fileName) then
    FBil_Clutter_Heights.OpenFile(params.Clutter_Heights_fileName);

  if FileExists(params.Build_Heights_fileName) then
    FBil_Build_Heights.OpenFile (params.Build_Heights_fileName);
*)


(*
  with FMenu_Txt_file do
    for I := 0 to High(Items) do
    begin
      FClutterIniFile.GetOnegaCode(Items[i].Name, Items[i].Onega_Code, Items[i].Onega_Height);
    end;

*)
 // end;

end;


// ---------------------------------------------------------------
procedure TATDI_to_RLF.Run;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;
  b: Boolean;

  rXYRect_UTM: TXYRect;

  rXYRect_GK: TXYRect;

  xy_CK1,xy_CK2,
  xy_UTM1,xy_UTM2, xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;

  iColCount: integer;
  iRowCount: integer;
  iStep: integer;
  iRow: integer;
  iCol: Integer;

  rlf_rec: Trel_Record;
  iValue: integer;

  w: Word;

  oRlfFileStream: TFileStream;

  s: string;
  iCluCode: Word;
  iCluH: Word;
  k: Integer;
  oRlf: TrlfMatrix;
  s2: string;
  s1: string;

  rClutter: TClutterRec;

  xyBorderXYPoints: TXYPointArray;


begin
 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;

 // btn_Run.Action := act_Stop;
(*
  if Params.Clutters_ini_FileName<>'' then
    FClutterIniFile.LoadCluttersFromIni(Params.Clutters_ini_fileName);
*)

//  CloseFiles;

  OpenFiles;


//  Border_FileName


 // iUTM_zone := AsInteger(ed_UTM_Zone.Text);
////////////  iGK_zone := UTM_zone - 30;

 // FDEM.OpenFile(DEM_FileName);

  if Terminated then
    Exit;


//  geo_IsXYPointInXYPolygon();


  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := FDEM.XYBounds;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);


  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;

  // -------------------------
  // border
  // -------------------------
  SetLength(xyBorderXYPoints, Length(Params.BorderBLPoints));

  for i:=0 to Length(Params.BorderBLPoints)-1 do
  begin
    bl:=Params.BorderBLPoints[i];

    xy := geo_Pulkovo42_to_XY(bl, FGK_zone );

    xyBorderXYPoints[i] := xy;
  end;




  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

    xy_gk := geo_UTM_to_GK(xy, FUTM_zone);

    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;


  if Length(xyBorderXYPoints)>0 then
    rXYRect_GK :=geo_RoundXYPointsToXYRectNew(xyBorderXYPoints)
  else
    rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);



//  FDEM.OpenFile(FFileName_DEM);
  // ---------------------------------------------------------------

(*
  if Clutter_Classes_FileName<>'' then
  begin
    FClutter_Classes.OpenFile(Clutter_Classes_FileName);
    FClutter_Classes.PrepareClutterIni(FClutterIniFile);
  end;*)

  if Terminated then
    Exit;


(*  // ---------------------------------------------------------------
  if Clutter_Heights_fileName<>'' then
    FClutter_Heights.OpenFile(Clutter_Heights_fileName);

  // ---------------------------------------------------------------
          *)

  if Params.Rlf_Step=0 then
    iStep :=Round(FDEM.StepX)
  else
    iStep := Params.Rlf_Step;


  Assert(iStep>0, 'Value <=0');

  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=rXYRect_GK;

 // FMemStream.Clear;

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------
              
//  TdmTest.Init;

 // xy := rXYRect_GK.TopLeft;

  for iRow := 0 to iRowCount - 1 do
  begin
 //   FMemStream.Position:=0;

    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


(*
function geo_IsXYPointInXYPolygon(aXYPoint: TXYPoint; var aXYPolygon:
    TXYPointArray): boolean;


*)
      b:=True;

      if Length(xyBorderXYPoints)>0 then
        if not geo_IsXYPointInXYPolygon(xy, xyBorderXYPoints) then
          Continue;


      // if Projection=ptUTM then
      xy_UTM := geo_GK_to_UTM(xy, FGK_zone);


//      u_geo_poly


    //  bl := geo_UTM_to_WGS84(xy_UTM, FUTM_zone);


    //  dmTest.Add(bl.b, bl.l, 0);
    //

      /// else
      //    xy_UTM := xy;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);


      if FDEM.FindValueByXY(xy_UTM.x, xy_UTM.y, w) then
      begin

        try
          rlf_rec.Rel_H :=w;
        except
        //  on E: Exception do ShowMessage('');
        end;

(*
        if Params.IsUse_Clutter_Height then
          if FClutter_Heights.FindValueByXY(xy_UTM.x, xy_UTM.y, iCluH) then
            if iCluH>0 then
            begin
              if iCluH>250 then iCluH:=250;

              rlf_rec.Clutter_H := iCluH;
            end;

*)

        if Params.IsUse_Clutter_Classes then
          if FClutter_Classes.FindValueByXY(xy_UTM.x, xy_UTM.y, iCluCode) then
            if (iCluCode>=1) and (iCluCode<100) then
            begin

              if FClutterIniFile.GetOnegaCode(iCluCode,  rClutter) then
              begin
                rlf_rec.Clutter_Code := rClutter.Onega_Code;
                rlf_rec.Clutter_H    := rClutter.Onega_Height;
              end;

(*const
  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
  DEF_CLU_WATER     = 2;  // 3- ���
  DEF_CLU_COUNTRY   = 3;  // 3- ���
  DEF_CLU_ROAD      = 4;
  DEF_CLU_RailROAD  = 5;
  DEF_CLU_CITY      = 7;  // 7- �����
  DEF_CLU_BOLOTO    = 8; // 31- ���
  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
  DEF_CLU_ONE_HOUSE = 31; // 31- ���

  //--------------------------------
          if Params.IsUse_Clutter_H then
            if FClutter_H_index_file.FindValueByXY(xy_UTM.x, xy_UTM.y, w) then
            begin
              if w>250 then 
                 w:=0;//250;

              rlf_rec.Clutter_H := w;

              if rlf_rec.Clutter_Code in [0,2,4,5] then //������
                rlf_rec.Clutter_H := 0;
              
              
            end;

  
*)



           //   rlf_rec.Clutter_Code := FClutterIniFile.Clutters[iCluCode].Onega_Code;
           //   rlf_rec.Clutter_H    := FClutterIniFile.Clutters[iCluCode].Onega_Height;


            //  if rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD, DEF_CLU_CITY] then
            
                if Params.IsUse_Clutter_Height then
                  if FClutter_Heights.FindValueByXY(xy_UTM.x, xy_UTM.y, iCluH) then
                  begin
                    if iCluH>250 then 
                       iCluH:=0;

                    rlf_rec.Clutter_H := iCluH;

                    if rlf_rec.Clutter_Code in [0,2,4,5] then //������
                      rlf_rec.Clutter_H := 0;
              
                    
                  end;

            //  if Params.Is_Set_Clutter_Height_0 then
            //  begin
                if (rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD])
                    and (rlf_rec.Clutter_H=0)
                then
                  rlf_rec.Clutter_Code :=0;
            end;

      end
      else
        rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;


    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);


  if not Terminated then
    ExportToClutterTab(Params.RLF_FileName);

end;

end.
