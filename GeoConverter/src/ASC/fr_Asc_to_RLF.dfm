inherited frame_Asc_to_RLF: Tframe_Asc_to_RLF
  Left = 1355
  Top = 204
  Caption = 'frame_Asc_to_RLF'
  ClientHeight = 447
  ClientWidth = 645
  Visible = True
  OnDestroy = FormDestroy
  ExplicitLeft = 1355
  ExplicitTop = 204
  ExplicitWidth = 653
  ExplicitHeight = 475
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 431
    Width = 645
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    TabOrder = 3
    ExplicitTop = 431
    ExplicitWidth = 645
  end
  inline frm_RLF1: Tframe_RLF_ [1]
    Left = 0
    Top = 355
    Width = 645
    Height = 76
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 355
    ExplicitWidth = 645
    ExplicitHeight = 76
    DesignSize = (
      645
      76)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 645
      ExplicitWidth = 645
    end
    inherited ed_RLF: TFilenameEdit
      Width = 630
      ExplicitWidth = 630
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 76
    Width = 645
    Height = 165
    Align = alTop
    TabOrder = 1
    DesignSize = (
      645
      165)
    object ed_Clutter_Classes: TFilenameEdit
      Left = 8
      Top = 28
      Width = 627
      Height = 21
      Filter = 'BIL (*.txt)|*.txt|ASC (*.asc)|*.asc'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'Moscow_Clatter_Classes_5m_1.asc'
    end
    object ed_Clutter_Heights: TFilenameEdit
      Left = 8
      Top = 76
      Width = 628
      Height = 21
      Filter = 'BIL (*.txt)|*.txt|ASC (*.asc)|*.asc'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = 'Moscow_Clatter_Heights_5m.asc'
    end
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 10
      Width = 300
      Height = 17
      Caption = 'Clutter_Classes'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object cb_Clutter_Heights: TCheckBox
      Left = 8
      Top = 58
      Width = 300
      Height = 17
      Caption = 'Clutter_Heights'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    inline frame_Clutter_1: Tframe_Clutter_
      Left = 2
      Top = 113
      Width = 641
      Height = 50
      Align = alBottom
      Constraints.MaxHeight = 50
      TabOrder = 4
      ExplicitLeft = 2
      ExplicitTop = 113
      ExplicitWidth = 641
      ExplicitHeight = 50
      inherited Label1: TLabel
        Width = 144
        ExplicitWidth = 144
      end
      inherited Bevel2: TBevel
        Width = 641
        ExplicitWidth = 642
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 645
    Height = 76
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      645
      76)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 24
      Height = 13
      Caption = 'DEM'
    end
    object lb_UTM_Zone: TLabel
      Left = 8
      Top = 56
      Width = 52
      Height = 13
      Caption = 'UTM Zone'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 627
      Height = 21
      Filter = 'ASC (*.asc)|*.asc'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'Moscow_DEM_5m.asc'
    end
    object ed_UTM_Zone: TEdit
      Left = 71
      Top = 51
      Width = 57
      Height = 21
      TabOrder = 1
      Text = '37'
    end
  end
  inherited ActionList_custom: TActionList
    Left = 48
    Top = 264
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 208
    Top = 264
  end
end
