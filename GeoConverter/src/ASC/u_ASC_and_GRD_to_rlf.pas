unit u_ASC_and_GRD_to_rlf;

interface
uses Classes, Sysutils, Math,

dm_Clutters,

  u_geo_convert_new1,

  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,

  u_CustomTask,

  u_clutter_classes,

  VM_Grids,


  u_ASC_File;


type
  TOnProgressEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean) of object;


  TASC_and_GRD_to_rlf = class(TCustomTask)
  private

    FDEM: TAscTextFile;
    FClutter_Classes: TvmGRID;
    FClutter_Heights: TvmGRID;

    FBuild_Heights: TvmGRID;


    FClutterInfo: TClutterInfo;

    procedure CloseFiles;

    procedure DoProgress(aProgress, aMax: integer; var aTerminated: Boolean);

    procedure PrepareClutterInfoFromGRD(aClutterInfo: TClutterInfo);

  protected
    function OpenFiles: Boolean; override;
  public

    Params: record
              UTM_zone : Integer;

              Border_FileName : string;


              IsUse_Clutter_Classes : Boolean;
              IsUse_Clutter_Heights : Boolean;

              Dem_FileName : string;
              Clutter_Classes_FileName : string;
              Clutter_Heights_fileName : string;

              Build_Heights_FileName : string;


              //Clutters_ini_fileName : string;
              Clutters_section : string;

              RLF_FileName : string;
              RLF_Step     : Integer;
            end;


    constructor Create;
    destructor Destroy; override;

    procedure Run;

  end;


implementation

// ---------------------------------------------------------------
constructor TASC_and_GRD_to_rlf.Create;
// ---------------------------------------------------------------
begin
  inherited;

  FDEM:=TAscTextFile.Create;
  FClutter_Classes:=TvmGRID.Create;
  FClutter_Heights:=TvmGRID.Create;
  FBuild_Heights:=TvmGRID.Create;

  FClutterInfo := TClutterInfo.Create();
end;


destructor TASC_and_GRD_to_rlf.Destroy;
begin
  FreeAndNil(FClutterInfo);

  FreeAndNil(FDEM);
  FreeAndNil(FClutter_Classes);
  FreeAndNil(FClutter_Heights);
  FreeAndNil(FBuild_Heights);

  inherited;
end;


procedure TASC_and_GRD_to_rlf.DoProgress(aProgress, aMax: integer; var
    aTerminated: Boolean);
begin
  DoOnProgress(aProgress,aMax, aTerminated);
end;


procedure TASC_and_GRD_to_rlf.CloseFiles;
begin
  FDEM.Close();
  FClutter_Classes.CloseFile();
  FClutter_Heights.CloseFile();
end;



// ---------------------------------------------------------------
function TASC_and_GRD_to_rlf.OpenFiles: Boolean;
// ---------------------------------------------------------------
begin
  Result := False;

  if not FileExists(Params.DEM_fileName) then
    Exit;

  FDEM.OnProgress := DoProgress;


//       procedure DoProgress(aProgress,aMax: integer);



  if not FDEM.OpenFile(Params.DEM_fileName) then
    Exit;

  if Params.IsUse_Clutter_Classes then
  begin
  //  Assert(FileExists(Params.Clutters_ini_fileName));

//    if FileExists(Params.Clutters_ini_fileName) then
  //  FClutterInfo.LoadCluttersFromIni('asc');

      dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);


  //  FClutterInfo.LoadCluttersFromDB(Params.Clutters_section);// rs_ini_fileName, 'asc');


//    FClutterInfo.LoadCluttersFromIni(Params.Clutters_ini_fileName, 'grd');

    if FileExists(Params.Clutter_Classes_FileName) then
    begin
      FClutter_Classes.OpenFile(Params.Clutter_Classes_FileName);
      PrepareClutterInfoFromGRD(FClutterInfo);
    end;

    if Params.IsUse_Clutter_Heights then
      if FileExists(Params.Clutter_Heights_fileName) then
        FClutter_Heights.OpenFile(Params.Clutter_Heights_fileName);
  end;

  Result := True;

end;

// ---------------------------------------------------------------
procedure TASC_and_GRD_to_rlf.PrepareClutterInfoFromGRD(aClutterInfo:
    TClutterInfo);
// ---------------------------------------------------------------
var
  b: Boolean;
  I: Integer;
  iNum: Integer;
  s: string;
begin
//  for I := 1 to High(FClutter_Classes.ClassificatorArr) do
  for I := 0 to High(FClutter_Classes.ClassificatorArr) do
  begin
    s    := FClutter_Classes.ClassificatorArr[i].ClsName;
    iNum := FClutter_Classes.ClassificatorArr[i].ClsNum;

  ///////////
    b:=aClutterInfo.SetCodeByName(s, iNum);
  end;
end;


// ---------------------------------------------------------------
procedure TASC_and_GRD_to_rlf.Run;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_UTM: TXYRect;
  rXYRect_GK: TXYRect;

  xy_CK1, xy_CK2,
  xy_UTM1, xy_UTM2,
  xy, xy_gk, xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  c: Integer;
  eValue: Double;
  iGK_zone: integer;
  iColCount: integer;

  iRowCount: integer;
  iRow: integer;
  iCol: Integer;

  rlf_rec: Trel_Record;
  iValue: integer;

  iIntValue: Smallint;

  oRlfFileStream: TFileStream;

  s: string;
  iCluCode: smallint;
  iCluH: smallint;
  iIndex: Integer;
  iStep: Integer;
  iUTM_zone: Integer;
  k: Integer;
  r: Integer;
  s2: string;
  s1: string;

  xyBounds_UTM: TXYRect;  
  rClutter: TClutterRec;

begin
  Terminated := False;

  if not OpenFiles() then
    Exit;

  iUTM_zone:= Params.UTM_zone;
  iGK_zone := Params.UTM_zone - 30;

//  iUTM_zone :=FDEM.UTM_zone;
//  iGK_zone := iUTM_zone - 30;

  iGK_zone := geo_UTM_to_GK_zone(iUTM_zone);

 // FDEM.LoadFromHeaderFile(DEM_fileName);

  xyBounds_UTM := FDEM.XYBounds_UTM;

  if Params.RLF_Step>0 then
    iStep :=Params.RLF_Step
  else
    iStep :=round(FDEM.Cellsize);

//  FClutter_Classes.XYBounds

  if Params.IsUse_Clutter_Classes then
  begin
    xyBounds_UTM.TopLeft.X     := FClutter_Classes.XYBounds_MinMax.MaxLat;
    xyBounds_UTM.TopLeft.Y     := FClutter_Classes.XYBounds_MinMax.MinLon;

    xyBounds_UTM.BottomRight.X := FClutter_Classes.XYBounds_MinMax.MinLat;
    xyBounds_UTM.BottomRight.Y := FClutter_Classes.XYBounds_MinMax.MaxLon;


//    eStepX :=FDEM.Cellsize;
  //  eStepY :=FDEM.Cellsize;
   // iStep :=round(Min(FClutter_Classes.StepX, FClutter_Classes.StepY));

  end;  


  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := xyBounds_UTM; //FDEM.xyBounds_UTM;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);

  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

    xy_gk := geo_UTM_to_GK(xy, iUTM_zone);

    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;

  rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);



  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

 // Assert(FDEM.MaxValue>0, 'Value <=0');

  header_Rec.GroundMinH:=FDEM.MinValue;
  header_Rec.GroundMaxH:=FDEM.MaxValue;


  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.xyBounds:=rXYRect_GK;


  Params.RLF_FileName:=ChangeFileExt(Params.RLF_FileName, '.rlf');


  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

  for iRow := 0 to iRowCount - 1 do
  begin
    DoOnProgress(iRow,iRowCount, Terminated);
    if Terminated then
      Break;
      
    for iCol := 0 to iColCount - 1 do
      if not Terminated then
    begin
      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;

      xy_UTM := geo_GK_to_UTM(xy, iGK_zone);

      FillChar(rlf_rec, SizeOf(rlf_rec), 0);
      rlf_rec.Rel_H :=EMPTY_HEIGHT;


      if FDEM.FindValue(xy_UTM.x, xy_UTM.y, iIntValue) then
      begin
        rlf_rec.Rel_H :=iIntValue;

          // -------------------------
          if Params.IsUse_Clutter_Classes then
            if FClutter_Classes.GetIndexByCoordLatLon(xy_UTM.x, xy_UTM.y, r,c) then
            begin     
           //   eValue := FClutter_Classes.GetValueFromFile(iIndex);

              //tEst
           //   rlf_rec.Clutter_Code := 0;
           //   rlf_rec.Clutter_H    := 0;

              if FClutter_Classes.GetValueByRowCol(r,c, eValue) then
                if eValue <> FClutter_Classes.NullValue then
              begin
                iCluCode := round(eValue);

                if (iCluCode>=0) and (iCluCode<100) then
                begin

                  if FClutterInfo.GetOnegaCode(iCluCode, rClutter) then
                  begin
                    rlf_rec.Clutter_Code := rClutter.Onega_Code;
                    rlf_rec.Clutter_H    := rClutter.Onega_Height;
                  end;
                end;
              end;


              if Params.IsUse_Clutter_Heights and (rlf_rec.Clutter_Code>0) then
                if FClutter_Heights.GetIndexByCoordLatLon(xy_UTM.x, xy_UTM.y,  r,c) then
                begin
                  if FClutter_Heights.GetValueByRowCol(r,c, eValue) then
                   if eValue <> FClutter_Heights.NullValue then
                     if eValue>0 then
                     begin
                       if eValue>255 then
                         eValue:=255;
                         
                       rlf_rec.Clutter_H  := Round(eValue);
                     end;

                end;
            end;
          // -------------------------
       // end;

      end;

      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);

  CloseFiles();
              

  if not Terminated then
    ExportToClutterTab(Params.RLF_FileName);


end;

end.


