unit u_Asset_old;


interface
uses

{$DEFINE AUTO_CREATE}

//  System.Generics.Defaults,  Dialogs, System.Generics.Collections,  
Classes,SysUtils,IniFiles, Forms, StrUtils,

  CodeSiteLogging,    
  
  
  u_func,
  u_func_arr
  ;

type
  TMinMaxRect= record
                  Lat_min: Double;
                  Lat_max: double;
                  
                  Lon_min: Double;
                  Lon_max: double;
               end;



  //-----------------------------------------------------
  TAsset_index_record = class  
  //-----------------------------------------------------
  protected
    RawString  : string;
    GroupIndex: Integer;
                  
  //  function GetXYRect: TXYRect;
  
  public    
    FileName      : string;
    ShortFileName : string;

    Bounds    : TMinMaxRect;
    CellSize  : integer;          
    
    procedure LoadFromString(aValue: string);
    procedure SaveHDR(aFileName: string; aZone: integer);
  end;

{
  //-----------------------------------------------------
  TAsset_index_record_short = record
  //-----------------------------------------------------
//  protected
 //   GroupIndex: Integer;
                  
  //  function GetXYRect: TXYRect;
  
  public    
//    FileName      : string;
    RawString  : string;

    ShortFileName : string;

    Bounds    : TMinMaxRect;
    CellSize  : integer;          
    
    procedure LoadFromString(aValue: string);
  //  procedure SaveHDR(aFileName: string; aZone: integer);
  end;

 } 
  
//
//  TAsset_group_item = record
//  public
//    Name : string; 
//    Step : Integer; 
//  end;


 // TAsset_group_dict = TDictionary<string, TAsset_group_item>;
  
//  public
 //   Name : string; 
 //   Step : Integer; 
  //end;

  
//  TAsset_index_group = class(TStringList) 
//  public
// //   Name : string; 
// //   Step : Integer; 
//  end;

//  TAsset_index_group = class(TList<TAsset_index_group_item>)  ;
                                                              

  
  //-----------------------------------------------------
  TAsset_index_file = class(TList) //<TAsset_index_record>)  
  //-----------------------------------------------------
  private
    FFileName: string;
    FZone : integer;
    FGroups: TStringList;

    procedure Create_Empty_File(aFileName: string; aRect: TMinMaxRect; aCellSize:   Integer);


    function SaveToBAT_each_file(aFileName: string): Boolean;
    
//    procedure Sort_;

  public
    Bounds: TMinMaxRect;
                  
    Groups_ex: TStringList;
                 
    constructor Create;
    destructor Destroy; override;
    
    function AddItem: TAsset_index_record;

    procedure LoadFromFile(aFileName: string);
    procedure ParseToFiles;

    function GetItem(aIndex: Integer): TAsset_index_record;

   
    procedure SaveToBAT(aFileName: string; aZone: Integer = 0);

    property Items[Index: Integer]: TAsset_index_record read GetItem; default;
    
  end;


  //-----------------------------------------------------
  TAsset_index_record_list = class(TList) //<TAsset_index_record_short>)  
  //-----------------------------------------------------
  private
    CellSize: Integer;                   
  
    function GetSizeSum(aMaxIndex: Integer): int64;

  public
    Groups: TStringList;              

    constructor Create;
    destructor Destroy; override;
    
    procedure Sort_;
    
    procedure AddItem(aValue: string);

    function GetItem(aIndex: Integer): TAsset_index_record;

    procedure SaveToFiles_ByFileSize(aDir, aGroupName: string);

    property Items[Index: Integer]: TAsset_index_record read GetItem; default;
  end;
                   


   procedure TAsset_index_file_Test__11111111;

  

implementation


function LatLonPointInRect(aLat, aLon: double; aRect: TMinMaxRect): Boolean;
begin
  Result :=
     (aLat <= aRect.Lat_max) and
     (aLon <= aRect.Lon_max) and
     (aLat >= aRect.Lat_min) and
     (aLon >= aRect.Lon_min);
end;


// ---------------------------------------------------------------
procedure TAsset_index_file.SaveToBAT(aFileName: string; aZone: Integer = 0);
// ---------------------------------------------------------------
//const
//  MyArray: TArray<String> = ['First','Second','Third'];

var

  I: Integer;

  oStrList: TStringList;

  oBAT: TStringList;
  
  
  sFileDir: string;
  strArr: TStrArray;

  rect: TMinMaxRect;

  oItem: TAsset_index_record;
  sFileName: string;
  sParam: string;
  
  iEPSG: integer;

  oIni: TIniFile;
  s: string;
  sDest: string;
  sFile: string;
  sGDAL_rel_Path: string;
  sPath: string;
  sPath_bin: string;

begin
  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile));

  // ---------------------------------------------------------------
  
  oIni:=TIniFile.Create(sFile);
  sGDAL_rel_Path:=oIni.ReadString('main','path','');
  FreeAndNil(oIni);
  
  // ---------------------------------------------------------------
//  sPath:=IncludeTrailingBackslash(sPath);
  
  sPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sGDAL_rel_Path );
  sPath_bin:=sPath + 'bin\';
  

  if aZone>0 then
    FZone:=aZone;
  
  Assert(FZone>0);

  iEPSG:=28400 + FZone - 30;


//--EPSG:28408

  oBAT:=TStringList.Create;

  //set path=W:\GIS\GeoConverter_XE\bin\OSGeo4W\bin\

//s:='set path='+ sPath_bin + ';'+ ExtractFilePath(aFileName);    
  
  oBAT.Add('set path='+ sPath_bin + ';'+ ExtractFilePath(aFileName));     

  oBAT.Add(Format('set GDAL_DATA=%s',[sPath])+ 'share\epsg_csv');
  oBAT.Add('set GDAL_FILENAME_IS_UTF8=NO');
  
  
  oBAT.Add('del all_*');                  
  oBAT.Add('del *.envi');                  
  oBAT.Add('del *.tif');                  
  oBAT.Add('del *.json');                  

  
  for I := 0 to Count - 1 do
  begin
    oBAT.Add('');   
    
    oItem:=Items[i];

    oItem.SaveHDR (oItem.FileName, FZone);    
    
    
    // ----------------------------------------------------------------------------
    sDest:=ChangeFileExt(oItem.ShortFileName, '.envi');

s:=  oItem.ShortFileName;

//    if not FileExists(sDest) then
//    begin
      sParam:= Format('copy  %s  %s ', [ oItem.ShortFileName, sDest ]); 
      oBAT.Add(sParam);                  
                  
      sParam:= Format('gdalinfo.exe -json  %s >  %s ', [ sDest, ChangeFileExt(sDest, '.json') ]); 
      oBAT.Add(sParam);                  


 //   end; 

  end;

 

  oBAT.Add('');    
//  oBAT.Add('del *.hdr');                  
//  oBAT.Add('del *.envi');                  
  oBAT.Add('');    


oBAT.Add( 'gdalbuildvrt.exe  -overwrite  all_wgs.vrt  *.envi');
//oBAT.Add('gdalbuildvrt  -overwrite  all_wgs.vrt  *_wgs.tif');
          
oBAT.Add('gdalwarp.exe  all_wgs.vrt all_wgs_.tif');


oBAT.Add('gdalwarp.exe '+ Format('-t_srs EPSG:%d  all_wgs_.tif all_pulkovo.tif',[iEPSG]));
oBAT.Add('gdal_translate.exe  -of envi   all_pulkovo.tif all_pulkovo.envi'); 
oBAT.Add('gdalinfo.exe -json all_pulkovo.envi > all_pulkovo.json');


  
  oBAT.SaveToFile(ChangeFileExt(aFileName,'.bat') , TEncoding.GetEncoding(866));
  
  FreeAndNil(oBAT);

// gdal_translate.exe   -a_srs  EPSG:3395  -of GTiff   -a_ullr 298297 318092 6661044 6685834  "1.envi" "2222.tif" 
     

end;   


// ---------------------------------------------------------------
function TAsset_index_file.SaveToBAT_each_file(aFileName: string): Boolean;
// ---------------------------------------------------------------
//const
//  MyArray: TArray<String> = ['First','Second','Third'];

var

  I: Integer;

  oStrList: TStringList;

  oBAT: TStringList;
 
  
  sFileDir: string;
  strArr: TStrArray;

  rect: TMinMaxRect;

  oItem: TAsset_index_record;
  sFileName: string;
  sParam: string;
  
  iEPSG: integer;

  oIni: TIniFile;
  s: string;
  sDest: string;
  sFile: string;
  sGDAL_rel_Path: string;
  sPath: string;
  sPath_bin: string;

begin
  sFile:=ExtractFilePath(Application.ExeName) + 'gdal.ini';
  Assert(FileExists(sFile));

  // ---------------------------------------------------------------
  
  oIni:=TIniFile.Create(sFile);
  sGDAL_rel_Path:=oIni.ReadString('main','path','');
  FreeAndNil(oIni);
  
  // ---------------------------------------------------------------
//  sPath:=IncludeTrailingBackslash(sPath);
  
  sPath:= IncludeTrailingBackslash ( ExtractFilePath(Application.ExeName) +  sGDAL_rel_Path );
  sPath_bin:=sPath + 'bin\';
  


  Assert(FZone>0);

  iEPSG:=28400 + FZone - 30;


//--EPSG:28408

  oBAT:=TStringList.Create;

  //set path=W:\GIS\GeoConverter_XE\bin\OSGeo4W\bin\
  
  oBAT.Add('set path='+ sPath_bin + ';'+ ExtractFilePath(aFileName));     
  
//  oBAT.Add('set path='+ sGDAL_rel_Path);     

  oBAT.Add(Format('set GDAL_DATA=%s',[sPath])+ 'share\epsg_csv');
  oBAT.Add('set GDAL_FILENAME_IS_UTF8=NO');
  
  oBAT.Add('del all_*.*');                  
  oBAT.Add('del *.json');                    
  oBAT.Add('del *.envi');                  
  oBAT.Add('del *.tif');                  

  
  for I := 0 to Count - 1 do
  begin
 //   oBAT.Add('');   
    
    oItem:=Items[i];

    oItem.SaveHDR (oItem.FileName, FZone);    
    
//    oItem.Asset_File.SaveHDR (oItem.FileName, aZone);    
    
    // ----------------------------------------------------------------------------
    sDest:=ChangeFileExt(oItem.ShortFileName, '.envi');

    if not FileExists(sDest) then
    begin
      sParam:= Format('copy %s %s ',
                   [
                    oItem.ShortFileName,
                    sDest

//                    DoubleQuotedStr(oItem.FileName),
//                    DoubleQuotedStr(sDest)

                   ]); 
      oBAT.Add(sParam);                  
                  
    end;  
        
  

  end;


  Result:= oBAT.Count>3;

 

  oBAT.SaveToFile(ChangeFileExt(aFileName,'.bat') , TEncoding.GetEncoding(866));
  
  FreeAndNil(oBAT);

// gdal_translate.exe   -a_srs  EPSG:3395  -of GTiff   -a_ullr 298297 318092 6661044 6685834  "1.envi" "2222.tif" 
     

end;   



// ---------------------------------------------------------------
procedure TAsset_index_file.Create_Empty_File(aFileName: string; aRect:  TMinMaxRect; aCellSize: Integer);
// ---------------------------------------------------------------

    function MacWordToPC(Value : word): word;
      asm xchg Al,Ah
    end;

var 
  c: Integer;
  iCols: Integer;
  iRows: Integer;
  r: Integer;
  tf: TFileStream;
  oStream: TMemoryStream;
  w: word;


const
  DEF_BORDER = 5;
  DEF_BODY = 1;

 // DEF_OFFSET = 5;
  
//  DEF_CLU_OPEN_AREA = 0;  // 1- ��� (1)
//  DEF_CLU_FOREST    = 1;  // 1- ��� (1)
//  DEF_CLU_WATER     = 2;  // 3- ���
//  DEF_CLU_COUNTRY   = 3;  // 3- ���
//  DEF_CLU_ROAD      = 4;
//  DEF_CLU_RailROAD  = 5;
//  DEF_CLU_BOLOTO_NEPROHODIMOE  = 6; // 6 - ������������ ������
//  DEF_CLU_CITY      = 7;  // 7- �����
//  DEF_CLU_BOLOTO    = 8; // ���������� ������
//  DEF_CLU_ONE_BUILD = 73; // 73- ���.����
//  DEF_CLU_ONE_HOUSE = 31; // 31- ���
//
  
begin
  oStream:=TMemoryStream.Create;
               


  iCols:= Trunc(aRect.Lon_max - aRect.Lon_min) div aCellSize;
  iRows:= Trunc(aRect.Lat_max - aRect.Lat_min) div aCellSize;

  oStream.Size:=iRows * iCols * 2;
  Assert(oStream.Position=0);
  
//  oStream.size:=iCols * iRows * 2;
//  oStream.Position:=0;
  

  w:=MacWordToPC (DEF_BORDER);
  for r := 0  to 2  do
    for c := 0  to iCols-1  do  oStream.Write(w, 2);


  for r := 1+2  to iRows-2 -2 do
  begin
    w:= MacWordToPC (DEF_BORDER);
    for c := 0 to 2 do    oStream.Write(w, 2);
    
    w:=MacWordToPC (DEF_BODY);
    for c := 1+2 to iCols-1-1 -2  do   oStream.Write(w, 2);

    w:=MacWordToPC (DEF_BORDER);
    for c := 0 to 2 do    oStream.Write(w, 2);
 
  end;  


  w:=MacWordToPC (DEF_BORDER);
  for r := 0 to 1  do
    for c := 0 to iCols-1 do   oStream.Write(w, 2);


  Assert (oStream.Size = iRows * iCols * 2);

  
//  tf.Size := [desired value];
  tf := TFileStream.Create(aFileName, fmCreate or fmShareExclusive);

  oStream.SaveToStream(tf);
//  tf.CopyFrom(oStream, oStream.Size);

  tf.Free;

  oStream.Free;
end;


// ---------------------------------------------------------------
procedure TAsset_index_file.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCellSize: Integer;
  oStrList: TStringList;
  
  
  sFileDir: string;
  strArr: TStrArray;

  rect: TMinMaxRect;

  oItem: TAsset_index_record;
  sFileName: string;

 // oProjection_file: TProjection_Txt_file;
begin
  Clear;


//  
//  oProjection_file:=TProjection_Txt_file.Create;
//
//  oProjection_file.LoadFromDir(ExtractFileDir(aFileName));
//  FZone:=oProjection_file.ZOne; 
//  
//  Assert(FZone > 0);
   

  sFileDir := ExtractFilePath(aFileName);
  
//  oBAT:=TStringList.Create;
   
// gdal_translate.exe   -a_srs  EPSG:3395  -of GTiff   -a_ullr 298297 318092 6661044 6685834  "1.envi" "2222.tif" 
   
  FFileName:=aFileName;
   

  oStrList := TStringList.Create;
  oStrList.LoadFromFile(aFileName);

 // sFileDir := IncludeTrailingBackslash( ExtractFileDir(aFileName));


//  SetLength(Items, oStrList.Count);

  for I := 0 to oStrList.Count - 1 do
  begin
    if tRIM(oStrList[i])='' then
      Continue;

  
    strArr :=StringToStrArray(oStrList[i], ' ');

    sFileName := sFileDir + strArr[0];

    {$IFDEF AUTO_CREATE}    
    
  { TODO : remove }
  
    if not FileExists(sFileName) then
    begin
      rect.Lon_min := AsFloat(strArr[1]);
      rect.Lon_max := AsFloat(strArr[2]);
      rect.Lat_min := AsFloat(strArr[3]);
      rect.Lat_max := AsFloat(strArr[4]);
      iCellSize    := AsInteger(strArr[5]);         

      Create_Empty_File(sFileName, rect, iCellSize);
    end;  

    {$ENDIF}

    if not FileExists(sFileName) then  
      Continue;
    
    
//    if not FileExists(sFileName) then
//      Continue;

    oItem:=AddItem();

    oItem.RawString    :=oStrList[i];
    oItem.FileName     :=sFileName;
    oItem.ShortFileName:=strArr[0];

//    oItem.FileName := sFileDir + strArr[0];

    //TADJIKISTAN_clut_1_1.bin 355150.0 605150.0 4296550.0 4546550.0 50.0
    oItem.Bounds.Lon_min := AsFloat(strArr[1]);
    oItem.Bounds.Lon_max := AsFloat(strArr[2]);
    oItem.Bounds.Lat_min := AsFloat(strArr[3]);
    oItem.Bounds.Lat_max := AsFloat(strArr[4]);

    oItem.CellSize:= AsInteger(strArr[5]);


  end;


  FreeAndNil(oStrList);


end;





function TAsset_index_file.AddItem: TAsset_index_record;
begin
  Result := TAsset_index_record.Create;

  Add(Result);
end;


// ---------------------------------------------------------------
procedure TAsset_index_record.LoadFromString(aValue: string);
// ---------------------------------------------------------------
var
  iCellSize: Integer;
 // sFileName: string;
  strArr: TStrArray;
  
begin
  strArr :=StringToStrArray(aValue, ' ');

//  sFileName := aDir + strArr[0];

  RawString    :=aValue;
//  FileName     :=sFileName;
  ShortFileName:=strArr[0];

  //TADJIKISTAN_clut_1_1.bin 355150.0 605150.0 4296550.0 4546550.0 50.0
  Bounds.Lon_min := AsFloat(strArr[1]);
  Bounds.Lon_max := AsFloat(strArr[2]);
  Bounds.Lat_min := AsFloat(strArr[3]);
  Bounds.Lat_max := AsFloat(strArr[4]);

  CellSize:= AsInteger(strArr[5]);                       
  
end;

// ---------------------------------------------------------------
procedure TAsset_index_record.SaveHDR(aFileName: string; aZone: integer);
// ---------------------------------------------------------------
const
  DEF =
      'ENVI'+ LF+ 
      'samples = %d'+ LF+ 
      'lines   = %d'+ LF+ 
      'bands   = 1 '+ LF+ 
      'header offset = 0  '+ LF+ 
      'file type = ENVI Standard '+ LF+ 
      'data type = 2 '+ LF+ 
      'interleave = bsq '+ LF+ 
      'byte order = 0 '+ LF+ 
      'map info = {UTM, 1, 1, %d, %d, %d, %d, %d, North,WGS-84}' ;
 
 //map info = {UTM, 1, 1, 298297, 6685834, 5, 5, 36, North,WGS-84}
    
// --    procedure SaveHDR(aFileName: string);

var
  iColCount: Integer;
  iRowCount: Integer;
  s: string;
begin
  
  iRowCount:=Trunc((Bounds.Lat_max - Bounds.Lat_min) / CellSize);
  iColCount:=Trunc((Bounds.Lon_max - Bounds.Lon_min) / CellSize);

  Assert (iRowCount>0);
  Assert (iColCount>0);
  


  s:= Format(DEF,[iColCount, iRowCount,

    Trunc(Bounds.Lon_min),
    Trunc(Bounds.Lat_max),    

    Trunc(CellSize),
    Trunc(CellSize),

    aZone

   ]);


  StrToTextFile(ChangeFileExt(aFileName,'.hdr'), s  );


end;



constructor TAsset_index_file.Create;
begin
  inherited Create;
  FGroups := TStringList.Create();
  Groups_ex := TStringList.Create();
  
end;


destructor TAsset_index_file.Destroy;
begin
  FreeAndNil(FGroups);
  FreeAndNil(Groups_ex);
  inherited Destroy;
end;

function TAsset_index_file.GetItem(aIndex: Integer): TAsset_index_record;
begin
  Result := TAsset_index_record (inherited Items[aIndex] );

//  Result := ;
end;

// ---------------------------------------------------------------
procedure TAsset_index_file.ParseToFiles;
// ---------------------------------------------------------------
var
//  oGroups: TAsset_index_group;
  I: Integer;
  iInd: Integer;
  j: Integer;
  k: Integer;
  oRecordList: TAsset_index_record_list;
  s: string;
  sDir: string;

//  strArr: TStrArray;
  strArr: TArray<string>;

//  oSList: TStringList;
  sName: string;

//  oGroup_list: TAsset_index_group;

begin
// Sort_;


  Assert(FFileName<>'');

 // oGroup_list:=TAsset_index_group.Create;// ;//= class(TList<TAsset_index_record>) //  class(TCollection)

  oRecordList:= TAsset_index_record_list.Create;
 
 
  FGroups.Clear;
  Groups_ex.Clear;
 
  
//  oSList:=TStringList.Create;

  
  s   :=ExtractFileDir(FFileName);
  sDir:=ExtractFileName(s);


//  s.spl
  
  //-----------------------------------
  
  for I := 0 to Count-1 do
  begin
     s:=ChangeFileExt( Items[i].ShortFileName, '');
     strArr := s.Split(['_']);
     
      
    sName:='';
    
    
    for j := 1 to High(strArr) do
    begin
      s:=LowerCase(strArr[j]);

//DHM_05m_Iskitim_01-01.bin 642390 645000 6060000 6063905 5

      if (s.Length=2 ) and (s[1] in ['a'..'z'])
                       and (s[2] in ['0'..'9']) 
        then Break;

      if (s.Length=1 ) and (s[1] in ['a'..'z'])                    
        then Break;
       
      

      if ((RightStr(s,1)='m') and (s[1] in ['0'..'9']) ) 
          or 
         (s[1] in ['a'..'z'])
      then
        begin
          sName:=sName+ IIF(sName<>'', '_'+ s, s);
        
        
        end else
          Break;
    end;
                      
    
    // ----------------------------
    iInd:=FGroups.IndexOf(sName);
    if iInd<0 then
      iInd:=FGroups.AddObject(sName, TObject(Items[i].CellSize));
        
    Items[i].GroupIndex:=iInd;
            
  end;

  //------------------------------------------------
  
 // ShowMessage(oGroup_list.Text);
  
  
  
  for I := 0 to FGroups.Count-1 do
  begin
//    oSList.Clear;              
    oRecordList.Clear;


    for j := 0 to Count-1 do
      if Items[j].GroupIndex = i then 
      begin
  //      oSList.Add(Items[j].RawString);

  Assert(Items[j].RawString<>'');
  
        oRecordList.AddItem(Items[j].RawString);

      end;  

 //   s:=ExtractFilePath(FFileName) + 'index_'+ Groups[i] +'.txt';
      
//    oRecordList.Sort_;
    oRecordList.SaveToFiles_ByFileSize (ExtractFilePath(FFileName), FGroups[i]);
      
//    for j := 0 to oRecordList.Groups.Count-1 do
 //     Groups_ex.AddObject (oRecordList.Groups[j],  )
      
    Groups_ex.AddStrings (oRecordList.Groups);
    
//    oSList.SaveToFile (s) ;

  end;
    

//  Result:=Groups.Count;


  FreeAndNil(oRecordList);

  
//DHM_05m_Iskitim_01-01.bin 642390 645000 6060000 6063905 5
//DHM_05m_Iskitim_01-02.bin 645000 650000 6060000 6063905 5
//DHM_05m_Iskitim_01-03.bin 650000 652680 6060000 6063905 5
//DHM_05m_Iskitim_02-01.bin 642390 645000 6055000 6060000 5
//DHM_05m_Iskitim_02-02.bin 645000 650000 6055000 6060000 5
//DHM_05m_Iskitim_02-03.bin 650000 652680 6055000 6060000 5
//DHM_05m_Iskitim_03-01.bin 642390 645000 6050000 6055000 5


   
end;



//=========================================================================================
procedure TAsset_index_file_Test__11111111;
//=========================================================================================
const
  DEF_INDEX_FILE = 'W:\GIS\GeoConverter_XE\Data\megafon\dal\index.txt';
  
var
  oFile: TAsset_index_file;
  s: string;
  sFile: string;
begin
//  s:=ExtractFileDir('P:\_mts\test1 ������� ����\DHM\index.txt');

  //s:=ExtractFileName(s);


//'P:\_mts\test1 ������� ����\DHM\index.txt'

  oFile:=TAsset_index_file.Create;


                                                        
  oFile.LoadFromFile('P:\_megafon\test_Novosib\clutter\index2.txt');
  
//  oFile.LoadFromFile('W:\GIS\GeoConverter_XE\Data\megafon\dal\index.txt');
  oFile.ParseToFiles;

  FreeAndNil(oFile);
  
  
 // oFile.SaveToBAT('W:\GIS\GeoConverter_XE\Data\megafon\dal\index.bat');
  
//  oFile.LoadFromFile('P:\_megafon\NENETZ\Height\4.txt');

//  sFile:='W:\GIS\GeoConverter_XE\Data\megafon\dal\index.bat';
//  sFile:='P:\_megafon\NENETZ\Height\4.bat';
                   
//  RunApp (sFile,'');  
//
//
//  
//  oFile.LoadFromFile('P:\_mts\test1 ������� ����\DHM\index.txt');
//  oFile.ParseToFiles;
//  
//  oFile.SaveToBAT('P:\_mts\test1 ������� ����\DHM\index.bat');
//
//  
//  FreeAndNil(oFile);
  
  
end;


constructor TAsset_index_record_list.Create;
begin
  inherited Create;
  Groups := TStringList.Create();
end;


destructor TAsset_index_record_list.Destroy;
begin
  FreeAndNil(Groups);
  inherited Destroy;
end;


// ---------------------------------------------------------------
procedure TAsset_index_record_list.AddItem(aValue: string);
// ---------------------------------------------------------------
var 
  r: TAsset_index_record;
begin
  Assert(aValue<>'');


  r := TAsset_index_record.Create;
  
  r.LoadFromString(aValue);
  
//  Result := TAsset_index_record_s.Create;

  Add(r);
  
end;


// ---------------------------------------------------------------
function TAsset_index_record_list.GetSizeSum(aMaxIndex: Integer): int64;
// ---------------------------------------------------------------
var
  I: Integer;
  iCellSize: Integer;
  iCols: Integer;
  iRows: Integer;
  oSList: TStringList;
  
  r: TMinMaxRect;
//  r11: TMinMaxRect;
  
begin
  Assert(Count>0);
    
    
    
  iCellSize:= Items[0].CellSize;

  r:=Items[0].Bounds;

//  for I := 1 to Count-1 do
//    r11:=Items[i].Bounds;


  
  for I := 1 to Count-1 do
  begin
    if r.Lat_min > Items[i].Bounds.Lat_min then  r.Lat_min := Items[i].Bounds.Lat_min;
    if r.Lat_max < Items[i].Bounds.Lat_max then  r.Lat_max := Items[i].Bounds.Lat_max;

    if r.Lon_min > Items[i].Bounds.Lon_min then  r.Lon_min := Items[i].Bounds.Lon_min;
    if r.Lon_max < Items[i].Bounds.Lon_max then  r.Lon_max := Items[i].Bounds.Lon_max;

  end;  

  iRows:= Trunc( (r.Lat_max - r.Lat_min ) / iCellSize);
  iCols:= Trunc( (r.Lon_max - r.Lon_min ) / iCellSize);


    try
    
  Result:= int64(1) * iRows * iCols * 4;

    except

    
    end;

  
end;

function TAsset_index_record_list.GetItem(aIndex: Integer): TAsset_index_record;
begin
  Result := TAsset_index_record (inherited Items[aIndex] );
end;


// ---------------------------------------------------------------
procedure TAsset_index_record_list.SaveToFiles_ByFileSize(aDir, aGroupName: string);
// ---------------------------------------------------------------
var
  i: Integer;
  iCellSize: Integer;
  iMaxIndex: Integer;
  oSList: TStringList;
  
  r: TMinMaxRect;
  iSizeSum: int64;
  s: string;
 
  iPart : word;  
begin
  Sort_;


  Assert(Count>0);

  iPart := 0;  
  
//  iSize:=GetSize(0);

  iCellSize:= Items[0].CellSize;


  oSList:=TStringList.Create;

  //--------------------
  
//  oSList.Add(GetItem[0].RawString);

  while Count > 0 do
  begin
    oSList.Clear;

//    oSList.Add(Items[0].RawString);
  
    for iMaxIndex := 0 to Count-1 do
    begin
      oSList.Add(Items[iMaxIndex].RawString);

      iSizeSum:=GetSizeSum(iMaxIndex);

      if iSizeSum > 2000000000 then //1 000 000 000
        break;
    end;                

  //  Assert(iMaxIndex<=Count);

    for i:=0 to iMaxIndex-1  do
     if count>0 then
        Delete(0);


    if iPart=0 then
      s:=aDir + 'index_'+ aGroupName +'.txt'
    else
      s:=aDir + 'index_'+ aGroupName + Format('_part_%d.txt',[iPart]);


      
CodeSite.Send(s);
CodeSite.Send(IntToStr(iSizeSum) );
CodeSite.Send('');

      
    oSList.SaveToFile(s);  
  
    Groups.AddObject (s, Pointer(iCellSize));
    
    Inc (iPart);
  end;
  

//  s:=aDir + 'index_'+ aGroupName +'.txt';

  //--------------------

  FreeAndNil(oSList);
  

//  clutter_berdsk_5m.pla 630320 639420 6065720 6074900 5
//clutter_iskitim_5m.pla 645385 651605 6045960 6059940 5
//clutter_novosibirsk_5m_dop_A1.bil 603627 638412 6103623 6118578 5
//clutter_novosibirsk_5m_dop_B1.bil 603627 638412 6088668 6103623 5
//clutter_novosibirsk_5m_dop_C1.bil 603627 638412 6073713 6088668 5
//clutter_novosibirskaya_obl_25m_E2.bil 179168 308868 5909312 6000537 25
//clutter_novosibirskaya_obl_25m_B1.bil 49468 179168 6182987 6274212 25
//clutter_novosibirskaya_obl_25m_D6.bil 697968 827668 6000537 6091762 25
//clutter_novosibirskaya_obl_25m_B3.bil 308868 438568 6182987 6274212 25
//clutter_novosibirskaya_obl_25m_E3.bil 308868 438568 5909312 6000537 25


  
//  for I := 0 to Groups.Count-1 do
//  begin
//    oSList.Clear;              
//    oRecordList.Clear;
//
//
//    for j := 0 to Count-1 do
//      if GetItem[j].GroupIndex = i then 
//      begin
//        oSList.Add(GetItem[j].RawString);
//
//        oRecordList.AddItem(GetItem[j].RawString);
//
//      end;  
//
//    s:=ExtractFilePath(FFileName) + 'index_'+ Groups[i] +'.txt';
//      
//    oSList.SaveToFile (s) ;
//
//  end;
//
//    

end;

// ---------------------------------------------------------------
function Compare( aLeft, aRight: Pointer): Integer;
// ---------------------------------------------------------------
var
  oLeft: TAsset_index_record;
  oRight: TAsset_index_record;

begin
  Result := 0;

  try
  

 
  oLeft:= TAsset_index_record(aLeft);
  oRight:= TAsset_index_record(aRight);

//  /
 //  TAsset_index_record           
  
//  if (not Assigned(aLeft)) or (not Assigned(aRight)) then 
//    Exit;


   Assert (Assigned(aLeft)); 
  Assert (Assigned(aRight));

    
  //  if aLeft.Bounds.Lon_min = 0 then exit;
//  if aRight.Bounds.Lon_min = 0 then exit;
              
  if oLeft.Bounds.Lon_min = oRight.Bounds.Lon_min then
    Exit;
  
          
  if oLeft.Bounds.Lon_min < oRight.Bounds.Lon_min then
    Result := -1
  else
    Result := 1

  finally

  end;
            
   // aLeft.Bounds.Lon_min - aRight.Bounds.Lon_min;
end;
 


// ---------------------------------------------------------------
procedure TAsset_index_record_list.Sort_;
// ---------------------------------------------------------------
begin
  Sort (Compare);
  
//
//  if Count > 1 then
//  
//
//  
//    Sort( TComparer<TAsset_index_record_short>.Construct(
//            function(const aLeft, aRight: TAsset_index_record_short): Integer
//            begin
//              Result := 0;
//              
////              Assert (Assigned(aLeft)); 
//  //            Assert (Assigned(aRight));
//              if aLeft.Bounds.Lon_min = 0 then exit;
//              if aRight.Bounds.Lon_min = 0 then exit;
//              
//          
//              if aLeft.Bounds.Lon_min < aRight.Bounds.Lon_min then
//                Result := 1
//              else
//                Result := -1
//            
//               // aLeft.Bounds.Lon_min - aRight.Bounds.Lon_min;
//            end
//          ) );

end;




//var
//  oAsset_group_dict: TAsset_group_dict;


//  rec: TAsset_group_item;
  
begin

//
//    s:=ExtractFilePath(FFileName) + 'index_'+ Groups[i] +'.txt';
//      
//    oSList.SaveToFile (s) ;
//

    

//  oAsset_group_dict:=TAsset_group_dict.Create;
//
//  rec.Name:='sdfsdf';
//  rec.Step:=4234;
//
//  
//  oAsset_group_dict.Add('1', rec);
//  oAsset_group_dict.Add('2', rec);
//
//
//  rec.Name:='sdfsdrwef';
//  rec.Step:=423444;
//
//  oAsset_group_dict.Add('asdfffffffffa', rec);
//
//
//  oAsset_group_dict.TryGetValue('1',rec);
  

 {$IFDEF test}

//  TAsset_index_file_Test__11111111;
 {$ENDIF}

end.


{


  TAsset_group_item = record
  public
 //   Name : string; 
 //   Step : Integer; 
  end;


  TAsset_group_dict = TDictionary<string, TAsset_group_item>;
//  public
 //   Name : string; 
 //   Step : Integer; 
  //end;


function TAsset_index_record.GetXYRect: TXYRect;
begin
  Result.TopLeft.X := Bounds.Lat_max;
  Result.TopLeft.Y := Bounds.Lon_min;

  Result.BottomRight.X := Bounds.Lat_min;
  Result.BottomRight.Y := Bounds.Lon_max;


end;



uses
  .. System.Generics.Defaults // Contains TComparer

myList.Sort(
  TComparer<TMyRecord>.Construct(
    function(const Left, Right: TMyRecord): Integer
    begin
      Result := Left.intVal - Right.intVal;
    end
  )
);