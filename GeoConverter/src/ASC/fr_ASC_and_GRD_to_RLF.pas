unit fr_ASC_and_GRD_to_RLF;

interface

uses
  SysUtils, Classes, rxToolEdit, Controls, Forms,
  StdCtrls, ExtCtrls, ComCtrls,IniFiles,

  VM_Grids,

  u_const,

  u_func,

  u_ASC_and_GRD_to_rlf,


  frame_RLF,
  frame_Clutter,

  fr_Custom_Form,

  Mask, ActnList, System.Actions
  ;

type
  Tframe_ASC_and_GRD_to_RLF = class(Tfrm_Custom_Form)
    frm_RLF1: Tframe_RLF_;
    GroupBox1: TGroupBox;
    ed_Clutter_Classes: TFilenameEdit;
    ed_Clutter_Heights: TFilenameEdit;
    cb_Clutter_Classes: TCheckBox;
    cb_Clutter_Heights: TCheckBox;
    Panel2: TPanel;
    lb_Relief: TLabel;
    lb_UTM_Zone: TLabel;
    ed_DEM: TFilenameEdit;
    ed_UTM_Zone: TEdit;
    frame_Clutter_1: Tframe_Clutter_;
    Button2: TButton;
    ActionList1: TActionList;
    act_Show_Clutter_Classes_file_Info: TAction;
    Button1: TButton;
    act_Show_Clutter_H_file_Info: TAction;
    procedure act_Show_Clutter_Classes_file_InfoExecute(Sender: TObject);
//    procedure act_StopExecute(Sender: TObject);
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
  private
  //  FTerminated : Boolean;

    FIniFileName: string;

    FASC_and_GRD_to_rlf: TASC_and_GRD_to_rlf;


    procedure Run; override;

    procedure SaveToIniFile(aFileName: String);

    
  public
    procedure LoadFromIniFile(aFileName: String);

  end;

var
  frame_ASC_and_GRD_to_RLF: Tframe_ASC_and_GRD_to_RLF;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'ASC_and_GRD_to_RLF';


// ---------------------------------------------------------------
procedure Tframe_ASC_and_GRD_to_RLF.act_Show_Clutter_Classes_file_InfoExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Show_Clutter_Classes_file_Info then
    TvmGRID.Dlg_ShowInfo(ed_Clutter_Classes.FileName) else

  if Sender=act_Show_Clutter_H_file_Info then
    TvmGRID.Dlg_ShowInfo(ed_Clutter_Heights.FileName);


end;


// ---------------------------------------------------------------
procedure Tframe_ASC_and_GRD_to_RLF.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  FASC_and_GRD_to_rlf := TASC_and_GRD_to_rlf.Create();
  FASC_and_GRD_to_rlf.OnProgress :=DoOnProgress;

  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

 // ed_DEM.Filter             := 'asc (*.asc)|*.asc|dem (*.dem)|*.dem';

  ed_Clutter_Classes.Filter := 'grc (*.grc)|*.grc';
  ed_Clutter_Heights.Filter := 'grd (*.grd)|*.grd';

 // TdmConfig.Init;

//  DBLookupComboBox1.ListSource:=dmConfig.ds_Clutter_schema;
 // DBLookupComboBox1.KeyValue  :=dmConfig.ds_Clutter_schema.DataSet.FieldByName('Name').AsString;

  LoadFromIniFile(FIniFileName);

  frm_RLF1.Init;
  frame_Clutter_1.Init;

  lb_Relief.Caption  := STR_RELIEf;
  cb_Clutter_Classes.Caption  := STR_Clutter_Classes;
  cb_Clutter_Heights.Caption  := STR_Clutter_Heights;

 lb_UTM_Zone.Caption  := STR_UTM_Zone;

 // act_Run.caption := STR_RUN;

end;

// ---------------------------------------------------------------
procedure Tframe_ASC_and_GRD_to_RLF.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  SaveToIniFile(FIniFileName);

  FreeAndNil(FASC_and_GRD_to_rlf);
end;

procedure Tframe_ASC_and_GRD_to_RLF.GroupBox1Click(Sender: TObject);
begin
  inherited;
end;


// ---------------------------------------------------------------
procedure Tframe_ASC_and_GRD_to_RLF.Run;
// ---------------------------------------------------------------
var
  iValue: Integer;
begin
  FTerminated := False;

  //btn_Run.Action := act_Stop;

  FASC_and_GRD_to_rlf.Params.UTM_zone:= AsInteger(ed_UTM_Zone.Text);

  FASC_and_GRD_to_rlf.Params.Dem_FileName := ed_DEM.FileName;

//  if cb_Clutter_Classes.Checked then
  FASC_and_GRD_to_rlf.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;
//  else
 //   FVM_GRD_to_rlf.Params.Clutter_Classes_FileName := '';

  FASC_and_GRD_to_rlf.Params.IsUse_Clutter_Classes := cb_Clutter_Classes.Checked;
  FASC_and_GRD_to_rlf.Params.IsUse_Clutter_Heights := cb_Clutter_heights.Checked;

//  if cb_Clutter_Heights.Checked then
  FASC_and_GRD_to_rlf.Params.Clutter_Heights_FileName := ed_Clutter_Heights.FileName;
//  else
 //   FVM_GRD_to_rlf.Params.Clutter_Heights_FileName :='';

  FASC_and_GRD_to_rlf.Params.Clutters_section := frame_Clutter_1.GetSection;

  FASC_and_GRD_to_rlf.Params.Rlf_FileName := frm_RLF1.ed_RLF.FileName;
  FASC_and_GRD_to_rlf.Params.Rlf_Step :=  frm_RLF1.GetStepM;


//  FASC_and_GRD_to_rlf.Params.Border_FileName := frm_RLF1.GetBorderFileName;

//  GetBorderFileName



  FASC_and_GRD_to_rlf.Run;

//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;



// ---------------------------------------------------------------
procedure Tframe_ASC_and_GRD_to_RLF.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do
  begin
    ed_DEM.Text  := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.Text);

    ed_UTM_Zone.Text := ReadString(DEF_SECTION, ed_UTM_Zone.Name, ed_UTM_Zone.Text);


    ed_Clutter_Classes.Text := ReadString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.Text);
    ed_Clutter_Heights.Text := ReadString(DEF_SECTION, ed_Clutter_Heights.Name,ed_Clutter_Heights.Text);
   // ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);


    cb_Clutter_Classes.checked:= ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_Heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);

  //  ed_RLF.FileName  := ReadString(DEF_SECTION, ed_RLF.Name,ed_RLF.FileName);
  //  ed_Step.text := ReadString(DEF_SECTION, ed_Step.Name,ed_Step.text);


  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;


// ---------------------------------------------------------------
procedure Tframe_ASC_and_GRD_to_RLF.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.Text);
    WriteString(DEF_SECTION, ed_UTM_Zone.Name, ed_UTM_Zone.Text);

    WriteString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.Text);
    WriteString(DEF_SECTION, ed_Clutter_Heights.Name, ed_Clutter_Heights.Text);

  //  WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);


    WriteBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);


 //   WriteString(DEF_SECTION, ed_RLF.Name,  ed_RLF.FileName);
   // WriteString(DEF_SECTION, ed_Step.Name, ed_Step.text);

   // Free;
  end;


  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);
end;


end.

