unit fr_Asc_to_RLF;

interface

uses
  Classes, Controls, Forms, StdCtrls, rxToolEdit,
  ExtCtrls, SysUtils, IniFiles,

  u_const,

  u_Asc_to_RLF,
  u_func,

  ComCtrls,

  frame_RLF,
  frame_Clutter,

  fr_Custom_Form, 

  Mask, ActnList, System.Actions, RxPlacemnt

  ;

type
  Tframe_Asc_to_RLF = class(Tfrm_Custom_Form)
    frm_RLF1: Tframe_RLF_;
    GroupBox1: TGroupBox;
    ed_Clutter_Classes: TFilenameEdit;
    ed_Clutter_Heights: TFilenameEdit;
    cb_Clutter_Classes: TCheckBox;
    cb_Clutter_Heights: TCheckBox;
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    lb_UTM_Zone: TLabel;
    ed_UTM_Zone: TEdit;
    frame_Clutter_1: Tframe_Clutter_;
    FormStorage1: TFormStorage;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    FIniFileName : string;

  private
    procedure LoadFromIniFile(aFileName: String);
    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run; override;
  public
  end;

var
  frame_Asc_to_RLF: Tframe_Asc_to_RLF;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'Asc_to_RLF';






procedure Tframe_Asc_to_RLF.FormCreate(Sender: TObject);
begin
  inherited;

  ed_DEM.Filter             := 'asc (*.asc)|*.asc|dem (*.dem)|*.dem';
  ed_Clutter_Classes.Filter := 'asc (*.asc)|*.asc|dem (*.dem)|*.dem'; //'grc (*.grc)|*.grc';
  ed_Clutter_Heights.Filter := 'asc (*.asc)|*.asc|dem (*.dem)|*.dem'; //'grd (*.grd)|*.grd';



  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);
                                    
  frm_RLF1.Init;
  frame_Clutter_1.Init;

  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;
  cb_Clutter_Classes.Caption  := STR_Clutter_Classes;
  cb_Clutter_Heights.Caption  := STR_Clutter_Heights;

//    act_Run.caption := STR_RUN;
 lb_UTM_Zone.Caption  := STR_UTM_Zone;

end;


procedure Tframe_Asc_to_RLF.FormDestroy(Sender: TObject);
begin
  SaveToIniFile(FIniFileName);

end;


// ---------------------------------------------------------------
procedure Tframe_Asc_to_RLF.Run;
// ---------------------------------------------------------------
var
  FAsc_to_RLF: TAsc_to_RLF;
  

begin
  FAsc_to_RLF := TAsc_to_RLF.Create();
  FAsc_to_RLF.OnProgress :=DoOnProgress;



  FTerminated := False;

  //btn_Run.Action := act_Stop;

(*  case cb_Proj.ItemIndex of
    0: FAsc_to_RLF.Projection :=ptUTM;
   // 1: FAsc_to_RLF.Projection :=ptGauss;
  end;
*)

  FAsc_to_RLF.Params.UTM_zone:= AsInteger(ed_UTM_Zone.Text);
  FAsc_to_RLF.Params.Dem_ASC_FileName := ed_DEM.fileName;

  FAsc_to_RLF.Params.IsUse_Clutter_Classes := cb_Clutter_Classes.Checked;
  FAsc_to_RLF.Params.IsUse_Clutter_Heights := cb_Clutter_heights.Checked;

  FAsc_to_RLF.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;
  FAsc_to_RLF.Params.Clutter_Heights_FileName := ed_Clutter_Heights.FileName;

 // FAsc_to_RLF.Params.Clutters_ini_fileName    := ed_Clutters_ini.FileName;
  FAsc_to_RLF.Params.ClutterSectionName   := frame_Clutter_1.GetSection();//  ed_Clutters_ini.FileName;

  FAsc_to_RLF.Params.Rlf_FileName  := frm_RLF1.ed_RlF.FileName;
  FAsc_to_RLF.Params.Rlf_Step      := frm_RLF1.GetStepM;

  FAsc_to_RLF.Run;


  //btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;


  FreeAndNil(FAsc_to_RLF);
  
end;

// ---------------------------------------------------------------
procedure Tframe_Asc_to_RLF.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do
  //
 // with TIniFile.Create(FIniFileName) do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.Text);

    WriteString(DEF_SECTION, ed_UTM_Zone.Name, ed_UTM_Zone.Text);


    WriteString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.Text);
    WriteString(DEF_SECTION, ed_Clutter_Heights.Name, ed_Clutter_Heights.Text);
   // WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);

  //  WriteString(DEF_SECTION, ed_RLF.Name, ed_RLF.FileName);
 //   WriteString(DEF_SECTION, ed_Step.Name, ed_Step.text);


    WriteBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    WriteBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);


   // Free;
  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);


  FreeAndNil(oIniFile);

end;


// ---------------------------------------------------------------
procedure Tframe_Asc_to_RLF.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(FIniFileName);

  with oIniFile do
  begin
    ed_DEM.Text := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.Text);

    ed_UTM_Zone.Text := ReadString(DEF_SECTION, ed_UTM_Zone.Name, ed_UTM_Zone.Text);

    ed_Clutter_Classes.Text := ReadString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.Text);
    ed_Clutter_Heights.Text := ReadString(DEF_SECTION, ed_Clutter_Heights.Name,ed_Clutter_Heights.Text);
   // ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);

    cb_Clutter_Classes.checked:= ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_Heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);

  //  ed_RLF.FileName := ReadString(DEF_SECTION, ed_RLF.Name,ed_RLF.FileName);
  //  ed_Step.text    := ReadString(DEF_SECTION, ed_Step.Name,ed_Step.text);

  //  Free;
  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;


end.
