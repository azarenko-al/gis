unit u_ASC_File;

interface

uses SysUtils,Dialogs,Classes,IniFiles,
//  u_progress,

    u_BufferedFileStream,

    u_clutter_classes,

    u_Geo,
    
    u_func_arr,
    u_func;



type
  TOnProgressEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean) of object;
            

type

  TAscTextFile = class
  private
    FOnProgress: TOnProgressEvent;
   // FTextFile: TextFile;

//    FStream: TFileStream;
    FStream: TReadOnlyCachedFileStream;

  private
    FClutterIniFile: TClutterInfo;

//    procedure DoProgress(aProgress,aMax: integer);
    procedure DoProgress(aProgress,aMax: integer; var aTerminated: Boolean);

    function GetIndexByXY(aX, aY: double; var aIndex: integer): boolean;
//     function LoadFromBin____________(aTxtFileName: string): Boolean;
//     procedure SaveToBin111111111(aTxtFileName: string);

    function TextFileToBin(aTxtFileName, aBinFileName: string): Boolean;

  private
     Xllcorner: Double;   //bottom left corner
     Yllcorner: Double;

     xllcenter: Double;
     yllcenter: Double;

  public
    NClassesCount: Integer;
    NClasses: array[1..100] of record
               //  Index: integer;   //1 "open" 255 255 179
                 Name: string;

               //  Onega_Code: integer;
               end;

     NCols: word;
     NRows: word;


     Cellsize: Double;
     NODATA_value: SmallInt;

     MinValue : SmallInt;
     MaxValue : SmallInt;

//     Items: array of
//            array of smallint;

     XYBounds: TXYRect;
//     XYBounds_UTM: TXYRect;

     {
     Bounds1 : record
              MinLat  : Double;
              MaxLat  : Double;
              MinLon  : Double;
              MaxLon  : Double;
            end;
      }

//    constructor Create;
//    destructor Destroy; override;
(*    FClutterArr: array of record
                 //  Code : Integer;
                   Name : string;
                   Onega_code : Integer;
                 end;
*)


     function FindCellXY(aX, aY: double; var aRow, aCol: integer): boolean;
     function FindValue(aX, aY: double; var aValue: smallint): Boolean;

// TODO: GetNClasses_RlfCode
//   function GetNClasses_RlfCode(aValue: Integer): integer;

//     function LoadDataFromFile(aFileName: string): boolean;
     function LoadHeader(aFileName: string): boolean;


     function OpenFile(aFileName: string): boolean;

     procedure Close;
     function GetValueByRowCol(aRow, aCol: Integer): SmallInt;

     function GetValueFromFile(aIndex: Integer): SmallInt;

     procedure LoadFromIni(aFileName: string);
     procedure SaveToIni(aFileName: string);



// TODO: GetOnegaCodeByName
//  function GetOnegaCodeByName(aName: string): Integer;

//    procedure PrepareClutterIni(aClutterIniFile: TClutterInfo);


    // procedure XYBounds1;

     property OnProgress: TOnProgressEvent read FOnProgress write FOnProgress;

   end;

//function TextFileToStringLIst(aFileName: string): TStringList;
    

implementation

(*

procedure TAscTextFile.DoProgress(aProgress,aMax: integer);
var
  bTerminated: Boolean;
begin
  if assigned(FOnProgress) then
    FOnProgress(aProgress,aMax, bTerminated);
end;*)

//---------------------------------------------------------------
function TAscTextFile.FindCellXY(aX, aY: double; var aRow, aCol: integer): boolean;
//--------------------------------------------------------------
var
  b: boolean;
begin
  Result:=False;

  aRow:=0; aCol:=0;

//  if not (Active) then
 //   Exit;
//  assert( XYBounds_UTM.BottomRight.x>0);

//  if MatrixType=mtXY_ then
//  begin

  b:=(aX < XYBounds.TopLeft.x);
  b:=(XYBounds.BottomRight.x < aX);

  b:=(XYBounds.TopLeft.y < aY);
  b:= aY < XYBounds.BottomRight.y;


  if (XYBounds.BottomRight.x < aX) and (aX < XYBounds.TopLeft.x) and
     (XYBounds.TopLeft.y     < aY) and (aY < XYBounds.BottomRight.y) then
  begin
    aRow := Integer(Round((XYBounds.TopLeft.x - aX) / Cellsize));
    aCol := Integer(Round((aY - XYBounds.TopLeft.y) / Cellsize));

    Result:=(aRow>=0) and (aCol>=0) and (aRow<=NRows-1) and (aCol<=NCols-1);

//      Result:=true;
  end;
// end;

end;

//---------------------------------------------------------------
function TAscTextFile.GetIndexByXY(aX, aY: double; var aIndex: integer):
    boolean;
//--------------------------------------------------------------
var
  b: boolean;
  iCol: Integer;
  iRow: Integer;
begin
  Result:=False;

 // aRow:=0; aIndex:=0;

//  if not (Active) then
 //   Exit;
//  assert( XYBounds_UTM.BottomRight.x>0);

//  if MatrixType=mtXY_ then
//  begin

(*  b:=(aX < XYBounds_UTM.TopLeft.x);
  b:=(XYBounds_UTM.BottomRight.x < aX);

  b:=(XYBounds_UTM.TopLeft.y < aY);
  b:= aY < XYBounds_UTM.BottomRight.y;
*)
(*
  iRow := Round((XYBounds_UTM.TopLeft.x - aX) / Cellsize);
  iCol := Round((aY - XYBounds_UTM.TopLeft.y) / Cellsize);

//  Result:=(iRow>=0) and (iCol>=0) and (aRow<=NRows-1) and (aIndex<=NCols-1);

  aIndex := Integer(Round((aY - XYBounds_UTM.TopLeft.y) / Cellsize));


  Result:=(iRow>=0) and (iCol>=0) and (aRow<=NRows-1) and (aIndex<=NCols-1);
*)

  if (XYBounds.BottomRight.x < aX) and (aX < XYBounds.TopLeft.x) and
     (XYBounds.TopLeft.y     < aY) and (aY < XYBounds.BottomRight.y) then
  begin
    iRow := Integer(Round((XYBounds.TopLeft.x - aX) / Cellsize));
    iCol := Integer(Round((aY - XYBounds.TopLeft.y) / Cellsize));

    aIndex := iRow*NCols + iCol;

    Result:=(iRow>=0) and (iCol>=0) and (iRow<=NRows-1) and (iCol<=NCols-1);

//      Result:=true;
  end else
    Result := False;
// end;

end;



// ---------------------------------------------------------------
function TAscTextFile.FindValue(aX, aY: double; var aValue: smallint): Boolean;
// ---------------------------------------------------------------
var
  iRow,iCol: Integer;
begin
  Result := FindCellXY(aX, aY, iRow,iCol);

  if Result then
  begin
  //  aValue := Items[iRow,iCol];

    aValue :=GetValueFromFile(iRow*NCols + iCol);

    Result := aValue<>NODATA_value;
  end;
end;

//---------------------------------------------------------------------
function TAscTextFile.GetValueFromFile(aIndex: Integer): SmallInt;
//---------------------------------------------------------------------
var iValue: SmallInt;
begin
  Result:=NODATA_value;

  if Assigned(FStream) And (aIndex<NCols*NRows) then
  begin
    FStream.Seek(aIndex*2, soFromBeginning);
    FStream.Read(Result, 2);
  end;

end;


//---------------------------------------------------------------------
function TAscTextFile.GetValueByRowCol(aRow, aCol: Integer): SmallInt;
//---------------------------------------------------------------------
var 
  iValue: SmallInt;
  iIndex: Integer;
begin
  iIndex:=aRow * NCols + aCol;
  
  if Assigned(FStream) And (iIndex<NCols*NRows) then
  begin
    FStream.Seek(iIndex*2, soFromBeginning);
    FStream.Read(Result, 2);
  end else
    Result:=NODATA_value;
  

end;


// ---------------------------------------------------------------
procedure TAscTextFile.LoadFromIni(aFileName: string);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
begin
  oIni:=TIniFile.Create(ChangeFileExt(aFileName, '.ini'));

  NCols := oIni.ReadInteger('main','NCols',NCols);
  NRows := oIni.ReadInteger('main','NRows',NRows);

  Xllcorner := AsFloat( oIni.ReadString('main','Xllcorner',''));
  Yllcorner := AsFloat( oIni.ReadString('main','Yllcorner',''));

  xllcenter:=AsFloat( oIni.ReadString('main','xllcenter',''));
  yllcenter:=AsFloat( oIni.ReadString('main','yllcenter',''));

  Cellsize := AsFloat( oIni.ReadString('main','Cellsize',''));

  NODATA_value := oIni.ReadInteger('main','NODATA_value',NODATA_value);

  MinValue := oIni.ReadInteger('main','MinValue',MinValue);
  MaxValue := oIni.ReadInteger('main','MaxValue',MaxValue);


  FreeAndNil(oIni);

end;

// ---------------------------------------------------------------
procedure TAscTextFile.SaveToIni(aFileName: string);
// ---------------------------------------------------------------
var
  oIni: TIniFile;
begin
  oIni:=TIniFile.Create(ChangeFileExt(aFileName, '.ini'));

  oIni.WriteInteger('main','NCols',NCols);
  oIni.WriteInteger('main','NRows',NRows);

  oIni.WriteFloat('main','Xllcorner',Xllcorner);
  oIni.WriteFloat('main','Yllcorner',Yllcorner);

  oIni.WriteFloat('main','xllcenter',xllcenter);
  oIni.WriteFloat('main','yllcenter',yllcenter);

  oIni.WriteFloat('main','Cellsize',Cellsize);

  oIni.WriteInteger('main','NODATA_value',NODATA_value);

  oIni.WriteInteger('main','MinValue',MinValue);
  oIni.WriteInteger('main','MaxValue',MaxValue);


  FreeAndNil(oIni);
end;



(*

     NCols: Integer;
     NRows: Integer;

     Xllcorner: Double;   //bottom left corner
     Yllcorner: Double;

     xllcenter: Double;
     yllcenter: Double;

     Cellsize: Double;
     NODATA_value: SmallInt;

     MinValue : SmallInt;
     MaxValue : SmallInt;
*)



//--------------------------------------------------------------
function TAscTextFile.LoadHeader(aFileName: string): boolean;
//--------------------------------------------------------------
var
  i1,i2: integer;
  deg: Double;
  iLen: integer;
  s1: string;
  c: integer;
  r: integer;
  i: integer;
  S: string;
  eValue: Double;
  strArr: TStrArray;
  iStepX,iStepY: integer;
  j,k: integer;
  iCluH: integer;
  iValue: integer;
  iIndex: Integer;     
  oTextFile: TextFile;

begin
  Result:=False;

  if not FileExists(aFileName) then
    Exit;


   AssignFile(oTextFile, aFileName);
   Reset(oTextFile);


   while not Eof(oTextFile) do
   begin
     Readln(oTextFile, s);

     if Length(s)>60 then // DATA string
       Break;

     s:=LowerCase(s);

    // Value for null pixels
     if Pos('ncols',s)>0  then
     begin
       ncols:=AsInteger(Copy(s,Length('ncols')+1, 100));
     end else

     if Pos('nrows',s)>0  then
     begin
       nrows:=AsInteger(Copy(s,Length('nrows')+1, 100));
     end else

     if Pos('xllcorner',s)>0  then
     begin
       xllcorner:=AsFloat(Copy(s,Length('xllcorner')+1, 100));
//       xllcorner:=Round(AsFloat(Copy(s,Length('xllcorner')+1, 100)));
     end else

     if Pos('yllcorner',s)>0  then
     begin
       yllcorner:=AsFloat(Copy(s,Length('yllcorner')+1, 100));
//       yllcorner:=Round(AsFloat(Copy(s,Length('yllcorner')+1, 100)));
     end else

     if Pos('yllcenter',s)>0  then
     begin
       yllcenter:=AsFloat(Copy(s,Length('yllcenter')+1, 100));
//       yllcorner:=Round(AsFloat(Copy(s,Length('yllcorner')+1, 100)));
     end else

     if Pos('xllcenter',s)>0  then
     begin
       xllcenter:=AsFloat(Copy(s,Length('xllcenter')+1, 100));
//       yllcorner:=Round(AsFloat(Copy(s,Length('yllcorner')+1, 100)));
     end else


     if Pos('cellsize',s)>0  then
     begin
       CellSize:=AsFloat(Copy(s,Length('cellsize')+1, 100));
//       cellsize:=Round(AsFloat(Copy(s,Length('cellsize')+1, 100)));
      // StepY:=StepX;
     end else


     if Pos('nodata_value',s)>0  then
     begin
       NODATA_value:=AsInteger(Copy(s,Length('nodata_value')+1, 100));
    //   Break;
    //   StepX:=AsFloat(Copy(s,Length('cellsize')+1, 100));
    //   StepY:=StepX;
     end else


     if Pos('nclasses',s)>0  then
     begin
       NClassesCount:=AsInteger(Copy(s,Length('nclasses')+1, 100));
   //    SetLength (NClasses, iLen);
       i:=0;

       while (not Eof(oTextFile)) and (i<NClassesCount) do
       begin
         Readln(oTextFile, s1);

         strArr:=StringToStrArray(S1, ' ');

         iIndex:=AsInteger(strArr[0]);

//         NClasses[i].Index:=AsInteger(strArr[0]);
         if (iIndex>=1) and (iIndex<100) then
           NClasses[iIndex].Name :=ReplaceStr(strArr[1],'"','');

         Inc(i);
       end;

       // NClasses[i].RlfCode:=iCode;
     //  ApplyRlfCodes();

       Break;   //!!!!!!!!!
     end;

  end;

  Assert(nrows>0, 'nrows>0');
//  Assert(yllcorner>0);
 // Assert(xllcorner>0);

  if (xllcorner>0) and (yllcorner>0) then
  begin
    XYBounds.TopLeft.Y     :=xllcorner;
    XYBounds.BottomRight.X :=yllcorner;

  end else

  if (xllcenter>0) and (yllcenter>0) then
  begin
    XYBounds.TopLeft.Y     :=xllcenter-Cellsize/2;
    XYBounds.BottomRight.X :=yllcenter+Cellsize/2;

  end;


  XYBounds.TopLeft.X     :=XYBounds.BottomRight.X + (Cellsize*NRows);
  XYBounds.BottomRight.Y :=XYBounds.TopLeft.Y + (Cellsize*NCols);

{  XYBounds_UTM.TopLeft.X     :=XYBounds_UTM.BottomRight.X + (Cellsize*NRows+1);
  XYBounds_UTM.BottomRight.Y :=XYBounds_UTM.TopLeft.Y + (Cellsize*NCols+1);
}

  if NClassesCount>0 then
    NODATA_value := 0;

 // if aCloseFile then
  CloseFile(oTextFile);

  Result:=True;

end;


// ---------------------------------------------------------------
function TAscTextFile.OpenFile(aFileName: string): boolean;
// ---------------------------------------------------------------
var
  iBinSize: Integer;
  iFileSize: Integer;
  iSize: Integer;
  k: Integer;
  sBinaryFileName : string;
begin
//  CursorHourGlass;

  Result := LoadHeader(aFileName);
  if not Result then
    Exit;


//  sBinaryFileName:=ChangeFileExt(aFileName, '.bin');
  sBinaryFileName:=aFileName+ '.bin';

  iBinSize:=NCols*NRows*2;

  if FileExists(sBinaryFileName) then 
  begin
    iFileSize:=GetFileSize(sBinaryFileName);
  
    if (iBinSize<>GetFileSize(sBinaryFileName)) then
      TextFileToBin (aFileName, sBinaryFileName);
  end else

//  if not (FileExists(sBinaryFileName) and (iBinSize=GetFileSize(sBinaryFileName))) then
    TextFileToBin (aFileName, sBinaryFileName);


/////////  if not FileExists(sBinaryFileName) then


 // BinaryFile_Open(sBinaryFileName);

  Result := FileExists(sBinaryFileName);

  if FileExists(sBinaryFileName) then
//    FStream:=TFileStream.Create(sBinaryFileName, fmOpenRead);
    FStream:=TReadOnlyCachedFileStream.Create(sBinaryFileName);



   // TReadOnlyCachedFileStream

//  if  then


/////////  Result := Result and LoadDataFromFile(aFileName);

 // CursorDefault;
end;




//-------------------------------------------------------------------
procedure TAscTextFile.Close;
//-------------------------------------------------------------------
begin
  FreeAndNil(FStream);
end;

//-------------------------------------------------------------------
procedure TAscTextFile.DoProgress(aProgress,aMax: integer; var aTerminated:
    Boolean);
//-------------------------------------------------------------------
begin

  if assigned(FOnProgress) then
    FOnProgress(aProgress,aMax, aTerminated);

end;


//---------------------------------------------------------------
function TAscTextFile.TextFileToBin(aTxtFileName, aBinFileName: string):
    Boolean;
//---------------------------------------------------------------
var
  S: string;
  oTxtFileStream: TFileStream;
  bt: Byte;
  bTerminated: Boolean;
  iLine: Integer;
  j: Integer;
  oBinFileStream: TFileStream;

 // oMemStream: TMemoryStream;

  siValue: SmallInt;

  strArr: TStrArray;

begin
 // Result := TStringList.create;
  bTerminated := False;
  Result := False;


  if not FileExists(aTxtFileName) then
//  begin
 //   Result:=False;
    Exit;
 // end;

  MinValue:=NODATA_value;
  MaxValue:=NODATA_value;


  oTxtFileStream:=TFileStream.Create(aTxtFileName, fmShareDenyWrite);
  oBinFileStream:=TFileStream.Create(aBinFileName, fmCreate);

 // oMemStream:=TMemoryStream.Create;


  s :='';
  iLine := 0;

  while oTxtFileStream.read(bt,1)= 1do
  begin
    if not (Bt in [$0A,$0D])  then
      s:=s+ Char(Bt)
    else
    begin
    //  if s<>'' then
      if Length(s)>60 then  // пропустить NO DATA section
      begin

        strArr:=StringToStrArray(s, ' ');

        for j := 0 to High(strArr) do
        begin
          siValue:=Round(AsFloat(strArr[j]));

          if siValue<>NODATA_value then
          begin
            if (MinValue=NODATA_value) or (MinValue>siValue) then MinValue:=siValue;
            if (MaxValue=NODATA_value) or (MaxValue<siValue) then MaxValue:=siValue;
          end;

       //   oMemStream.Write(siValue, SizeOf(siValue));

          oBinFileStream.Write(siValue, SizeOf(siValue));
        end;
(*

        if oMemStream.Size > 1000000 then
        begin
          oBinFileStream.CopyFrom (oMemStream,0);
          oMemStream.Clear;
        end;
*)
        DoProgress(iLine, NRows, bTerminated);
        Inc(iLine);

        if bTerminated then
            Break;
      //  end;
      end;

      s :='';
    end;
  end;

  // if s<>'' then
    //    Result.Add(s);

  //oBinFileStream.CopyFrom (oMemStream,0);


  FreeAndNil(oTxtFileStream);
  FreeAndNil(oBinFileStream);
 // FreeAndNil(oMemStream);

  if bTerminated then
    DeleteFile(aBinFileName);

  Result := not bTerminated;

end;




procedure Test;
var
  obj: TAscTextFile;
begin
  obj:=TAscTextFile.Create;
 // obj.LoadCluttersFromIni('s:\1111\Clutters.ini');

//   obj.OpenFile('d:\andr\code_01.txt');


//   obj.LoadHeader('s:\1111\height.asc');
//   obj.LoadData  ('s:\1111\height.asc');

{

   obj.LoadHeader('f:\clutter.txt');
   obj.LoadHeader('f:\height.txt');

   obj.LoadData('f:\clutter.txt');
   obj.LoadData('f:\height.txt');

   obj.Free;
}
end;



begin
//  Test

end.



