inherited frame_ASC_and_GRD_to_RLF: Tframe_ASC_and_GRD_to_RLF
  Left = 1517
  Top = 430
  BorderStyle = bsSingle
  Caption = 'frame_ASC_and_GRD_to_RLF'
  ClientHeight = 585
  ClientWidth = 625
  Scaled = False
  OnDestroy = FormDestroy
  ExplicitLeft = 1517
  ExplicitTop = 430
  ExplicitWidth = 633
  ExplicitHeight = 613
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 569
    Width = 625
    TabOrder = 3
    ExplicitTop = 569
    ExplicitWidth = 625
  end
  inline frm_RLF1: Tframe_RLF_ [1]
    Left = 0
    Top = 489
    Width = 625
    Height = 80
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 448
    ExplicitWidth = 625
    ExplicitHeight = 80
    DesignSize = (
      625
      80)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 625
      ExplicitWidth = 625
    end
    inherited ed_RLF: TFilenameEdit
      ExplicitWidth = 610
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 77
    Width = 625
    Height = 220
    Align = alTop
    TabOrder = 1
    OnClick = GroupBox1Click
    DesignSize = (
      625
      220)
    object ed_Clutter_Classes: TFilenameEdit
      Left = 8
      Top = 32
      Width = 535
      Height = 21
      Filter = 'grc (*.grc)|*.grc'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = ''
    end
    object ed_Clutter_Heights: TFilenameEdit
      Left = 8
      Top = 80
      Width = 535
      Height = 21
      Filter = 'grd (*.grd)|*.grd'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = ''
    end
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 14
      Width = 300
      Height = 17
      Caption = 'Clutter_Classes'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object cb_Clutter_Heights: TCheckBox
      Left = 8
      Top = 62
      Width = 300
      Height = 17
      Caption = 'Clutter_Heights'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    inline frame_Clutter_1: Tframe_Clutter_
      Left = 2
      Top = 168
      Width = 621
      Height = 50
      Align = alBottom
      Constraints.MaxHeight = 50
      TabOrder = 4
      ExplicitLeft = 2
      ExplicitTop = 168
      ExplicitWidth = 621
      ExplicitHeight = 50
      inherited Label1: TLabel
        Width = 144
        ExplicitWidth = 144
      end
      inherited Bevel2: TBevel
        Width = 621
        ExplicitWidth = 621
      end
      inherited Edit1: TEdit
        Text = 'ASC_GRD'
      end
    end
    object Button2: TButton
      Left = 552
      Top = 29
      Width = 57
      Height = 25
      Action = act_Show_Clutter_Classes_file_Info
      Anchors = [akTop, akRight]
      TabOrder = 5
    end
    object Button1: TButton
      Left = 552
      Top = 77
      Width = 57
      Height = 25
      Action = act_Show_Clutter_H_file_Info
      Anchors = [akTop, akRight]
      TabOrder = 6
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 625
    Height = 77
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      625
      77)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Relief'
    end
    object lb_UTM_Zone: TLabel
      Left = 8
      Top = 54
      Width = 52
      Height = 13
      Caption = 'UTM Zone'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 612
      Height = 21
      Filter = 'asc (*.asc); dem (*.dem)|*.asc;*.dem'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = ''
    end
    object ed_UTM_Zone: TEdit
      Left = 72
      Top = 50
      Width = 57
      Height = 21
      TabOrder = 1
      Text = '37'
    end
  end
  inherited ActionList_custom: TActionList
    Left = 384
    Top = 312
  end
  object ActionList1: TActionList
    Left = 488
    Top = 320
    object act_Show_Clutter_Classes_file_Info: TAction
      Caption = 'Info'
      OnExecute = act_Show_Clutter_Classes_file_InfoExecute
    end
    object act_Show_Clutter_H_file_Info: TAction
      Caption = 'Info'
      OnExecute = act_Show_Clutter_Classes_file_InfoExecute
    end
  end
end
