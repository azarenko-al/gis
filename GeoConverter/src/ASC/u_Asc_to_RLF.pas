unit u_Asc_to_RLF;

interface
uses Classes, Sysutils, Graphics,

  u_geo_convert_new1,
  dm_Clutters,
  I_rel_Matrix1,

        
  u_rlf_Matrix,

  u_geo,

  u_CustomTask,

  u_ASC_File,

  u_clutter_classes;


type
  TAsc_to_RLF = class(TCustomTask)
  private
    FDEM: TAscTextFile;
    FClutter_Classes: TAscTextFile;
    FClutter_Heights: TAscTextFile;

    FClutterIniFile: TClutterInfo;
    
    procedure OpenFiles;
  protected
    procedure CloseFiles; override;
  public

//    Projection: (ptGauss, ptUTM);
//    Projection: (ptUTM);

    Params: record
      UTM_zone : Integer;
      GK_zone : Integer;

      Border_FileName : string;


      IsUse_Clutter_Classes : Boolean;
      IsUse_Clutter_Heights : Boolean;

      Dem_ASC_FileName : string;
      Clutter_Classes_FileName : string;
      Clutter_Heights_fileName : string;

    //  Build_Heights_FileName : string;

//      Clutters_ini_fileName : string;

      ClutterSectionName : string;

      RLF_FileName : string;
      Rlf_Step : Integer;
    end;

    constructor Create;      
    destructor Destroy; override;

    procedure Run;
    procedure Run_clutters;

  end;


implementation

// ---------------------------------------------------------------
constructor TAsc_to_RLF.Create;
// ---------------------------------------------------------------
begin
  inherited;

  FDEM:=TAscTextFile.Create;
  FDEM.OnProgress:= DoOnProgress;

  FClutter_Classes:=TAscTextFile.Create;
  FClutter_Classes.OnProgress:= DoOnProgress;

  FClutter_Heights:=TAscTextFile.Create;
  FClutter_Heights.OnProgress:= DoOnProgress;

  FClutterIniFile := TClutterInfo.Create();

end;


// ---------------------------------------------------------------
destructor TAsc_to_RLF.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FClutterIniFile);

  FreeAndNil(FDEM);
  FreeAndNil(FClutter_Classes);
  FreeAndNil(FClutter_Heights);

  inherited;
end;


procedure TAsc_to_RLF.CloseFiles;
begin
  FDEM.Close;
  FClutter_Classes.Close;
  FClutter_Heights.Close;


end;


// ---------------------------------------------------------------
procedure TAsc_to_RLF.OpenFiles;
// ---------------------------------------------------------------
begin
 // if Params.Clutters_ini_FileName<>'' then

//  FClutterIniFile.LoadCluttersFromIni('ASC');
/////  dmClutters.LoadCluttersFromDB(FClutterIniFile, Params.ClutterSectionName);

//  FClutterIniFile.LoadCluttersFromDB(Params.ClutterSectionName);

//      IsUse_Clutter_Classes : Boolean;
//      IsUse_Clutter_Heights : Boolean;

 // if Params.IsUse_Clutter_Classes then
  if Params.Clutter_Classes_FileName<>'' then
//    begin
    FClutter_Classes.OpenFile(Params.Clutter_Classes_FileName);
    //  FClutter_Classes.PrepareClutterIni(FClutterIniFile);
//    end;

  if Params.DEM_ASC_FileName<>'' then
    FDEM.OpenFile(Params.DEM_ASC_FileName);
   

  // ---------------------------------------------------------------
//  if Params.IsUse_Clutter_Heights then
    if Params.Clutter_Heights_fileName<>'' then
      FClutter_Heights.OpenFile(Params.Clutter_Heights_fileName);

  // ---------------------------------------------------------------


end;



// ---------------------------------------------------------------
procedure TAsc_to_RLF.Run;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_UTM: TXYRect;

  rXYRect_GK: TXYRect;

  xy_CK1,xy_CK2,
  xy_UTM1,xy_UTM2, xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
 // cl: integer;
  iGK_zone: integer;
  iColCount: integer;
  iRowCount: integer;
  iStep: integer;
  iRow: integer;
  iCol: integer;

  rlf_rec: Trel_Record;
  iValue: integer;

  iIntValue: Smallint;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
  iCluH: smallint;
  k: Integer;
 // oBmpFile: TBmpFile;
  s2: string;
  s1: string;

  rClutter: TClutterRec;
//  sBmpFileName: string;

begin
  OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;

 // btn_Run.Action := act_Stop;
 

 // iUTM_zone := AsInteger(ed_UTM_Zone.Text);
  iGK_zone := Params.UTM_zone - 30;


  if Terminated then
    Exit;

  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := FDEM.XYBounds;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);


  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

//    if Projection=ptUTM then
    xy_gk := geo_UTM_to_GK(xy, Params.UTM_zone);
      
  //  else
    //  xy_gk := xy;


    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;

  rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);

//  FDEM.OpenFile(FFileName_DEM);
  // ---------------------------------------------------------------



  if Terminated then
    Exit;


  if Params.RLF_Step>0 then
   iStep :=Params.RLF_Step
  else
   iStep :=round(FDEM.Cellsize);


  //iStep :=round(FDEM.Cellsize);

  Assert(iStep>0, 'Value <=0');

  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);
               
  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  Assert(FDEM.MaxValue>0, 'Value <=0');

  header_Rec.GroundMinH:=FDEM.MinValue;
  header_Rec.GroundMaxH:=FDEM.MaxValue;


  //header_Rec.:=iColCount;
  //header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=rXYRect_GK;

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------
 (*
 sBmpFileName :=ChangeFileExt(Params.RLF_FileName, '.bmp');
*)
//  oBmpFile:=TBmpFile.Create_Width_Height (sBmpFileName, iColCount, iRowCount);

  // ---------------------------------------------------------------


 // xy := rXYRect_GK.TopLeft;

  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


 //     if Projection=ptUTM then
      xy_UTM := geo_GK_to_UTM(xy, iGK_zone);
   //   else
     //   xy_UTM := xy;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

      if FDEM.FindValue(xy_UTM.x, xy_UTM.y, iIntValue) then
      begin
        rlf_rec.Rel_H :=iIntValue;

        if Params.IsUse_Clutter_Classes then
          if FClutter_Classes.FindValue(xy_UTM.x, xy_UTM.y, iCluCode) then
           begin
            if (iCluCode>=1) and (iCluCode<100) then
            begin
              if FClutterIniFile.GetOnegaCode(iCluCode,rClutter) then
              begin
                rlf_rec.Clutter_Code := rClutter.Onega_Code;
                rlf_rec.Clutter_H    := rClutter.Onega_Height;
              end;

              //  FClutter_Classes.NClasses[iCluCode].Onega_Code;

              // ����� + ������ =0
          //    if (rlf_rec.Clutter_Code = DEF_CLU_CITY) and (iCluH=0)  then
           //     rlf_rec.Clutter_Code :=0;
            end;


            if FClutter_Heights.FindValue(xy_UTM.x, xy_UTM.y, iCluH) then
            begin
              if iCluH>250 then iCluH:=250;

              rlf_rec.Clutter_H := iCluH;
            end;
          end;

      //  cl:=clRed;
      end
      else begin
        rlf_rec.Rel_H :=EMPTY_HEIGHT;

       // cl:=clBlack;
      end;
(*
      if b then
      begin
        b:=oSrcRlfMatrix.FindPointXY (xy, 0, rlf_rec);
        if b then
          cl:=clRed
        else
          cl:=clBlack;
      end else begin
     //   rlf_rec.Rel_H := EMPTY_HEIGHT;
       // cl:=clBlack;
      end;
*)

    //  oBmpFile.Write(cl);

      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));
    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;
//    oBmpFile.Writeln;

  end;

 // oRlfFileStream.Free;
  FreeAndNil(oRlfFileStream);

  CloseFiles();


  
  if not Terminated then
    ExportToClutterTab(Params.RLF_FileName);


end;


// ---------------------------------------------------------------
procedure TAsc_to_RLF.Run_clutters;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

//  arrXYRectArray_UTM: TXYRectArray;
//  arrXYPointArrayF_new: TXYPointArrayF;
//  arrXYPointArrayF_GK: TXYPointArrayF;
//
//  rXYRect_UTM: TXYRect;
//
//  rXYRect_GK: TXYRect;
//
//  xy_CK1,xy_CK2,
  xy_UTM1,xy_UTM2, xy,xy_gk,xy_UTM: TXYPoint;

//  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
 // cl: integer;
//  iGK_zone: integer;
//  iColCount: integer;
//  iRowCount: integer;
 // iStep: integer;
  iRow: integer;
  iCol: integer;

  rlf_rec: Trel_Record;
  iValue: integer;

  iIntValue: Smallint;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
  iCluH: smallint;
  k: Integer;
 // oBmpFile: TBmpFile;
  s2: string;
  s1: string;

  rClutter: TClutterRec;
//  sBmpFileName: string;

begin
  OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;

 // btn_Run.Action := act_Stop;
 

 // iUTM_zone := AsInteger(ed_UTM_Zone.Text);
  //iGK_zone := Params.UTM_zone - 30;


  if Terminated then
    Exit;

//    
//  SetLength(arrXYRectArray_UTM, 1);
//
//  arrXYRectArray_UTM[0] := FDEM.XYBounds_UTM;
//
//  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);
//
//
//  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);
//
//  arrXYPointArrayF_GK.Count :=4;
//
//
//  for i:=0 to arrXYPointArrayF_new.Count-1 do
//  begin
//    xy :=arrXYPointArrayF_new.Items[i];
//
////    if Projection=ptUTM then
//    xy_gk := geo_UTM_to_GK(xy, Params.UTM_zone);
//      
//  //  else
//    //  xy_gk := xy;
//
//
//    arrXYPointArrayF_GK.Items[i] := xy_gk;
//  end;
//
//  rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);

//  FDEM.OpenFile(FFileName_DEM);
  // ---------------------------------------------------------------



  if Terminated then
    Exit;

//
//  if Params.RLF_Step>0 then
//   iStep :=Params.RLF_Step
//  else
//   iStep :=round(FDEM.Cellsize);


  //iStep :=round(FDEM.Cellsize);

//  Assert(iStep>0, 'Value <=0');

 

 
//  iRowCount := Round(Abs(FClutter_Classes.XYBounds.TopLeft.X - FClutter_Classes.XYBounds.BottomRight.X) / FClutter_Classes.Cellsize);
//  iColCount := Round(Abs(FClutter_Classes.XYBounds.TopLeft.Y - FClutter_Classes.XYBounds.BottomRight.Y) / FClutter_Classes.Cellsize);
               
 
  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=FClutter_Classes.NCols;
  header_Rec.RowCount:=FClutter_Classes.NRows;

//  Assert(FDEM.MaxValue>0, 'Value <=0');

  header_Rec.GroundMinH:=FDEM.MinValue;
  header_Rec.GroundMaxH:=FDEM.MaxValue;


  //header_Rec.:=iColCount;
  //header_Rec.RowCount:=iRowCount;

  header_Rec.ZoneNum_GK:=Params.GK_zone;
  
  header_Rec.StepX:=FClutter_Classes.Cellsize;
  header_Rec.StepY:=FClutter_Classes.Cellsize;

  header_Rec.XYBounds:=FClutter_Classes.XYBounds;

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------
 (*
 sBmpFileName :=ChangeFileExt(Params.RLF_FileName, '.bmp');
*)
//  oBmpFile:=TBmpFile.Create_Width_Height (sBmpFileName, iColCount, iRowCount);

  // ---------------------------------------------------------------


 // xy := rXYRect_GK.TopLeft;

  for iRow := 0 to FClutter_Classes.NRows - 1 do
  begin
    if Terminated then
      Break;

//    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to FClutter_Classes.NCols - 1 do
    begin
      if Terminated then  Break;

 //     xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
 //     xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


 //     if Projection=ptUTM then
  //    xy_UTM := geo_GK_to_UTM(xy, iGK_zone);
   //   else
     //   xy_UTM := xy;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

   //   if FDEM.FindValue(xy_UTM.x, xy_UTM.y, iIntValue) then
      begin
        rlf_rec.Rel_H :=0;

     //   if Params.IsUse_Clutter_Classes then
        iCluCode:=FClutter_Classes.GetValueByRowCol(iRow, iCol);

        if iCluCode <> FClutter_Classes.NODATA_value  then
          rlf_rec.Clutter_Code := iCluCode
        else
          rlf_rec.Rel_H :=EMPTY_HEIGHT;

        
     {
      then
           begin
            if (iCluCode>=1) and (iCluCode<100) then
            begin
              if FClutterIniFile.GetOnegaCode(iCluCode,rClutter) then
              begin
                rlf_rec.Clutter_Code := rClutter.Onega_Code;
                rlf_rec.Clutter_H    := rClutter.Onega_Height;
              end;

              //  FClutter_Classes.NClasses[iCluCode].Onega_Code;

              // ����� + ������ =0
          //    if (rlf_rec.Clutter_Code = DEF_CLU_CITY) and (iCluH=0)  then
           //     rlf_rec.Clutter_Code :=0;
            end;


            if FClutter_Heights.FindValue(xy_UTM.x, xy_UTM.y, iCluH) then
            begin
              if iCluH>250 then iCluH:=250;

              rlf_rec.Clutter_H := iCluH;
            end;
          end;

      //  cl:=clRed;
      end
      else begin
        rlf_rec.Rel_H :=EMPTY_HEIGHT;
           }
       // cl:=clBlack;
      end;
(*
      if b then
      begin
        b:=oSrcRlfMatrix.FindPointXY (xy, 0, rlf_rec);
        if b then
          cl:=clRed
        else
          cl:=clBlack;
      end else begin
     //   rlf_rec.Rel_H := EMPTY_HEIGHT;
       // cl:=clBlack;
      end;
*)

    //  oBmpFile.Write(cl);

      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));
    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;
//    oBmpFile.Writeln;

  end;

 // oRlfFileStream.Free;
  FreeAndNil(oRlfFileStream);

  CloseFiles();

  TrlfMatrix.SaveToIni (Params.RLF_FileName);
  

  if not Terminated then
    ExportToClutterTab(Params.RLF_FileName);


end;


procedure Test;
var
  obj: TAsc_to_RLF;
begin
  obj:=TAsc_to_RLF.Create;
  
  obj.Params.Clutter_Classes_FileName:='D:\andr\Code_01.txt';
  obj.Params.RLF_FileName:='D:\andr\Code_01.rlf';
  obj.Params.GK_zone:=7;

  obj.Run_clutters;
  
  
  obj.Free;
  
end;
  

begin
//  Test();


end.

