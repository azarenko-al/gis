unit fr_RLF_to_UTM;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls,
  IniFiles,rxToolEdit,

  u_const,

  fr_Custom_Form,

  u_RLF_to_UTM, Mask, ActnList, System.Actions;

type
  Tframe_RLF_to_UTM = class(Tfrm_Custom_Form)
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    Panel3: TPanel;
    Label1: TLabel;
    ed_DEM_UTM: TFilenameEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
                               
  private

    FIniFileName: string;

    FRLF_to_UTM: TRLF_to_UTM;

    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run; override;
  public

    procedure LoadFromIniFile(aFileName: String);


  end;

var
  frame_RLF_to_UTM: Tframe_RLF_to_UTM;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'RLF_to_UTM';



// ---------------------------------------------------------------
procedure Tframe_RLF_to_UTM.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  FRLF_to_UTM := TRLF_to_UTM.Create();
  FRLF_to_UTM.OnProgress :=DoOnProgress;

  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);

 // frm_RLF1.Init;

  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;

 // lb_Menu_txt.Caption  := STR_Menu_txt;


end;


procedure Tframe_RLF_to_UTM.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FRLF_to_UTM);

  SaveToIniFile(FIniFileName);

end;

// ---------------------------------------------------------------
procedure Tframe_RLF_to_UTM.Run;
// ---------------------------------------------------------------
begin
  inherited;

  FTerminated := False;

//  btn_Run.Action := act_Stop;

  FRLF_to_UTM.Params.FileName := ed_DEM.FileName;
  FRLF_to_UTM.Params.UTM_FileName := ed_DEM_UTM.FileName;

  FRLF_to_UTM.Run;

//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;

// ---------------------------------------------------------------
procedure Tframe_RLF_to_UTM.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do
  begin
    ed_DEM.FileName     := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);
    ed_DEM_UTM.FileName := ReadString(DEF_SECTION, ed_DEM_UTM.Name, ed_DEM_UTM.FileName);
  end;

  FreeAndNil(oIniFile);

end;

// ---------------------------------------------------------------
procedure Tframe_RLF_to_UTM.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name,     ed_DEM.FileName);
    WriteString(DEF_SECTION, ed_DEM_UTM.Name, ed_DEM_UTM.FileName);
  end;

  FreeAndNil(oIniFile);

end;


end.
