unit u_RLF_to_UTM;

interface

uses
  Classes, Sysutils, 

  u_CustomTask,

  u_geo_convert_new1,

  I_rel_Matrix1,

  u_geo,
 // u_geo_poly,

  u_rlf_Matrix

  ;


type
  TRLF_to_UTM = class(TCustomTask)
  private
  public
    Params: record
      FileName     : string;
      UTM_FileName : string;

    end;

    procedure Run;

  end;


implementation


// ---------------------------------------------------------------
procedure TRLF_to_UTM.Run;
// ---------------------------------------------------------------
var
  b: Boolean;
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  rXYRect_GK: TXYRect;
  xyRect_UTM: TXYRect;

  xy : TXYPoint;

  bl_CK42: TBLPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;

//  iGK_zone: integer;
  iColCount: integer;
  iRowCount: integer;

  iRow: integer;
  iCol: integer;
  iGK_ZoneNum: Integer;
  iStep: Integer;
  iUTM_ZoneNum: Integer;

  rlf_rec: Trel_Record;
//  iValue: integer;

  oRlfFileStream: TFileStream;
  oSrcRlfMatrix: TrlfMatrix;
  s: string;

  xy_GK: TXYPoint;
  xy_UTM: TXYPoint;

  xyXYPoints_GK: TXYPointArrayF;
  xyPoints_UTM: TXYPointArrayF;

 // wValue: Word;
  
begin
  oSrcRlfMatrix:=TrlfMatrix.Create;

  oSrcRlfMatrix.OpenFile(Params.FileName);

 // OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;

 // btn_Run.Action := act_Stop;

//   Info.PixelSize



  if Terminated then
    Exit;


  //iStep :=round(FDEM.Cellsize);

//  Assert(iStep>0, 'Value <=0');

  iRowCount := oSrcRlfMatrix.RowCount;
  iColCount := oSrcRlfMatrix.ColCount;
  rXYRect_GK:= oSrcRlfMatrix.XYBounds;


  // ---------------------------------------------------------------

  iGK_ZoneNum:=oSrcRlfMatrix.ZoneNum_GK;
  iUTM_ZoneNum :=iGK_ZoneNum + 30;


  geo_XYRectToXYPointsF (oSrcRlfMatrix.XYBounds, xyXYPoints_GK);


  xyPoints_UTM.Count :=4;


  // GK xy -> UTM xy
  for i:=0 to xyXYPoints_GK.Count-1 do
  begin
    xy_GK := xyXYPoints_GK.Items[i];
    xy_UTM := geo_GK_to_UTM(xy_GK, iGK_ZoneNum);

    xyPoints_UTM.Items[i] := xy_UTM;
  end;

  xyRect_UTM := geo_RoundXYPointsToXYRect(xyPoints_UTM);



  iStep := Round(oSrcRlfMatrix.StepX);

  iRowCount := Round(Abs(xyRect_UTM.TopLeft.X - xyRect_UTM.BottomRight.X) / iStep);
  iColCount := Round(Abs(xyRect_UTM.TopLeft.Y - xyRect_UTM.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=xyRect_UTM;

  header_Rec.ZoneNum_GK := iUTM_ZoneNum;// iGK_ZoneNum + 30;
  header_Rec.Projection :='UTM';

////  iGK_zone := oSrcRlfMatrix.ZoneNum;

  // ---------------------------------------------------------------

  oRlfFileStream:=TFileStream.Create(Params.UTM_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------


  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy_UTM.x := xyRect_UTM.TopLeft.X - iRow*iStep - iStep/2;
      xy_UTM.y := xyRect_UTM.TopLeft.Y + iCol*iStep + iStep/2;

      xy_GK := geo_UTM_to_GK(xy_UTM, iUTM_ZoneNum);


      if not oSrcRlfMatrix.FindPointXY(xy_GK, iGK_ZoneNum, rlf_rec) then
      begin
        FillChar(rlf_rec, SizeOf(rlf_rec), 0);
        rlf_rec.Rel_H := EMPTY_HEIGHT;
      end;

      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);
end;


(*

var
  obj: TRLF_to_UTM;


begin
  obj:=TRLF_to_UTM.Create;
  obj.Params.FileName:='s:\_UTM\nnovg_pl.rlf';
  obj.Params.UTM_FileName:='s:\_UTM\nnovg_pl.utm.rlf';
  obj.Run;
*)

end.


