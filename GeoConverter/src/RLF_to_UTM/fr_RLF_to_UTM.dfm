inherited frame_RLF_to_UTM: Tframe_RLF_to_UTM
  Left = 1222
  Top = 389
  Caption = 'frame_RLF_to_UTM'
  ClientHeight = 359
  ClientWidth = 558
  OnDestroy = FormDestroy
  ExplicitLeft = 1222
  ExplicitTop = 389
  ExplicitWidth = 566
  ExplicitHeight = 387
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 343
    Width = 558
    ExplicitTop = 337
    ExplicitWidth = 558
  end
  object Panel2: TPanel [1]
    Left = 0
    Top = 0
    Width = 558
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      558
      65)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 107
      Height = 13
      Caption = 'ONEGA rlf ('#1080#1089#1093#1086#1076#1085#1099#1081')'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 543
      Height = 21
      Filter = 'Rlf (*.rlf)|*.rlf'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'S:\__TASKS\TADJIKISTAN_dem__.rlf'
    end
  end
  object Panel3: TPanel [2]
    Left = 0
    Top = 65
    Width = 558
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      558
      65)
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 76
      Height = 13
      Caption = 'UTM ONEGA rlf'
    end
    object ed_DEM_UTM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 543
      Height = 21
      Filter = 'Rlf (*.rlf)|*.rlf'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'S:\__TASKS\TADJIKISTAN_dem__.rlf'
    end
  end
end
