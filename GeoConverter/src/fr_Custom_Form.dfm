object frm_Custom_Form: Tfrm_Custom_Form
  Left = 1168
  Top = 672
  Caption = 'frm_Custom_Form'
  ClientHeight = 251
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 235
    Width = 349
    Height = 16
    Align = alBottom
    Smooth = True
    TabOrder = 0
    ExplicitTop = 259
    ExplicitWidth = 275
  end
  object ActionList_custom: TActionList
    Left = 80
    Top = 40
    object act_Stop: TAction
      Caption = 'Stop'
      OnExecute = act_StopExecute
    end
    object act_Run: TAction
      Caption = 'Run'
      OnExecute = act_StopExecute
    end
  end
end
