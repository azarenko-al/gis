unit u_bil_to_rlf_index;

interface
uses Classes, Sysutils, 

  u_geo_convert_new1,

  dm_Clutters,

  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,

  u_CustomTask,

  
  u_clutter_classes,

  u_bil_classes;


type

  TBil_to_RLF_index = class(TCustomTask)
  private

    FGK_zone: Integer;

    FDEM_index_file      : TBil_index_file;
    FClutter_index_file  : TBil_index_file;
    FClutter_H_index_file: TBil_index_file;



//    FBil_DEM: TBil_File;
//    FBil_Clutter_Classes: TBil_File;
//    FBil_Clutter_Heights: TBil_File;
//    FBil_Build_Heights: TBil_File;

    FClutterInfo: TClutterInfo;

    FProjection_Txt_file: TBil_Projection_Txt_file;

    FUTM_zone : Integer;

    procedure CloseFiles;
    function OpenFiles: Boolean;
    procedure RunFile;


  public
    Params: record
      UTM_zone1 : Integer;
                             
      Dem_Index_FileName : string;

      IsUse_Clutter_Classes : Boolean;
      IsUse_Clutter_H       : Boolean;

      Clutter_Classes_Index_FileName : string;
      Clutter_H_Index_fileName : string;

//      Build_Heights_FileName   : string;

      Projection_Txt_fileName : string;

//      Clutters_ini_fileName : string;
      Clutters_section : string;


      RLF_FileName : string;
      Rlf_Step     : Integer;
    end;


    constructor Create;
    destructor Destroy; override;

    procedure Run;

  end;


implementation


constructor TBil_to_RLF_index.Create;
begin
  inherited;

(*  FBil_DEM:=TBil_File.Create;
  FBil_Clutter_Classes:=TBil_File.Create;
  FBil_Clutter_Heights:=TBil_File.Create;
  FBil_Build_Heights := TBil_File.Create();
*)

  FDEM_index_file      := TBil_index_file.Create;
  FClutter_index_file  := TBil_index_file.Create;
  FClutter_H_index_file:= TBil_index_file.Create;



  FMemStream:=TMemoryStream.Create;

  FClutterInfo := TClutterInfo.Create();
  FProjection_Txt_file := TBil_Projection_Txt_file.Create();

end;


destructor TBil_to_RLF_index.Destroy;
begin
  FreeAndNil(FDEM_index_file);
  FreeAndNil(FClutter_index_file);
  FreeAndNil(FClutter_H_index_file);



  FreeAndNil(FProjection_Txt_file);

(*
  FreeAndNil(FBil_Build_Heights);

  FreeAndNil(FBil_DEM);
  FreeAndNil(FBil_Clutter_Classes);
  FreeAndNil(FBil_Clutter_Heights);
*)

  FreeAndNil(FMemStream);
  FreeAndNil(FClutterInfo);

  inherited;
end;

procedure TBil_to_RLF_index.CloseFiles;
begin

end;



// ---------------------------------------------------------------
function TBil_to_RLF_index.OpenFiles: Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := False;

  Assert(FileExists(params.Dem_Index_FileName));

  if FileExists(params.Dem_Index_FileName) then
    FDEM_index_file.LoadFromFile(params.Dem_Index_FileName)
  else
    Exit;


  if params.IsUse_Clutter_Classes then
    if FileExists(params.Clutter_Classes_Index_FileName) then
    begin
      FClutter_index_file.LoadFromFile(params.Clutter_Classes_Index_FileName);
    end;


  if params.IsUse_Clutter_H then
    if FileExists(params.Clutter_H_Index_FileName) then
    begin
      FClutter_H_index_file.LoadFromFile(params.Clutter_H_Index_FileName);
    end;



  if FileExists(params.Projection_Txt_FileName) then
     FProjection_Txt_file.LoadFromFile(params.Projection_Txt_FileName);

 // if FPassport_file.UtmZone>0 then
 // FUTM_zone :=FPassport_file.UtmZone;
  FUTM_zone :=FProjection_Txt_file.Zone;

 // else
  //  FUTM_zone :=Params.UTM_zone1;


  FGK_zone := FUTM_zone - 30;


///////  FDEM_file.OpenFile(params.DEM_fileName);

 // FDEM_file.LoadPassport(params.Passport_FileName);

  if Params.IsUse_Clutter_Classes then
  begin     
    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);


   // FClutterInfo.LoadCluttersFromDB(Params.Clutters_section);
  end;

  Result := True;

end;


// ---------------------------------------------------------------
procedure TBil_to_RLF_index.Run;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Terminated := False;

  if not OpenFiles then
    Exit;

  RunFile;


end;



// ---------------------------------------------------------------
procedure TBil_to_RLF_index.RunFile;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  arrXYRectArray_UTM: TXYRectArray;
  arrXYPointArrayF_new: TXYPointArrayF;
  arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_UTM: TXYRect;

  rXYRect_GK: TXYRect;

  xy_CK1,xy_CK2,
  xy_UTM1,xy_UTM2, xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
 // iUTM_zone: integer;
//  iGK_zone: integer;
 // iRows: Integer;
//  iCols: integer;
  iColCount: integer;
  iRowCount: integer;
  iStep: integer;
  iRow: integer;
  iCol: integer;
 // iColCount: integer;

  rlf_rec: Trel_Record;
  iValue: integer;

 // iIntValue: Smallint;

//  FMemStream: TMemoryStream;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;
 // iCluH: smallint;
  k: Integer;
  s2: string;
  s1: string;
 // sFile: string;
//  sFile_DEM_file: string;
  w: smallint;
  wCluCode: smallint;

  rClutter: TClutterRec;
 

begin
 // FFileName_DEM:=ed_DEM.FileName;


 // btn_Run.Action := act_Stop;
(*
  if Params.Clutters_ini_FileName<>'' then
    FClutterInfo.LoadCluttersFromIni(Params.Clutters_ini_fileName);
*)

//  OpenFiles;


 // iUTM_zone := AsInteger(ed_UTM_Zone.Text);
////////////  iGK_zone := UTM_zone - 30;
//FDEM_index_file.Files

//  sFile_DEM_file := FDEM_index_file.Files[aIndex].FileName;
//  FDEM_file.OpenFile(sFile_DEM_file);

(*  FDEM_file.Lat_min := FDEM_index_file.Files[aIndex].Lat_min;
  FDEM_file.Lat_max := FDEM_index_file.Files[aIndex].Lat_max;
  FDEM_file.Lon_min := FDEM_index_file.Files[aIndex].Lon_min;
  FDEM_file.Lon_max := FDEM_index_file.Files[aIndex].Lon_max;
  FDEM_file.CellSize:= FDEM_index_file.Files[aIndex].CellSize;

  FDEM_file.Prepare;
*)
(*
  if Params.IsUse_Clutter_Classes then
//  if FileExists(params.Clutter_Classes_IndexFileName) then
  begin
    sFile := FClutter_index_file.Files[aIndex].FileName;
    FClutter_Classes_file.OpenFile(sFile);

    FClutter_Classes_file.Lat_min := FClutter_index_file.Files[aIndex].Lat_min;
    FClutter_Classes_file.Lat_max := FClutter_index_file.Files[aIndex].Lat_max;
    FClutter_Classes_file.Lon_min := FClutter_index_file.Files[aIndex].Lon_min;
    FClutter_Classes_file.Lon_max := FClutter_index_file.Files[aIndex].Lon_max;
    FClutter_Classes_file.CellSize:= FClutter_index_file.Files[aIndex].CellSize;

    FClutter_Classes_file.Prepare;

//    if FClutter_index_file.Count>0 then
 //     params.Clutter_Classes_FileName:= FClutter_index_file.Files[0].FileName;

  end;*)


(*
  if Terminated then
    Exit;
*)

  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := FDEM_index_file.XYBounds;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);


  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

    xy_gk := geo_UTM_to_GK(xy, FUTM_zone);

(*
    if Projection=ptUTM then
      xy_gk := geo_UTM_to_GK(xy, UTM_zone)
    else
      xy_gk := xy;
*)

    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;

  rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);

//  FDEM_file.OpenFile(FFileName_DEM);
  // ---------------------------------------------------------------

(*
  if Clutter_Classes_FileName<>'' then
  begin
    FClutter_Classes_file.OpenFile(Clutter_Classes_FileName);
    FClutter_Classes_file.PrepareClutterIni(FClutterInfo);
  end;*)

  if Terminated then
    Exit;


(*  // ---------------------------------------------------------------
  if Clutter_Heights_fileName<>'' then
    FAsc_Clutter_Heights.OpenFile(Clutter_Heights_fileName);

  // ---------------------------------------------------------------
          *)


(*
  if Params.Rlf_Step=0 then
    iStep :=Round(FDEM.StepX)
  else
    iStep := Params.Rlf_Step;
*)



  if Params.RLF_Step>0 then
   iStep :=Params.RLF_Step
  else
  iStep :=Round(FDEM_index_file.CellSize);
  // iStep :=round(FDEM.Cellsize);



  Assert(iStep>0, 'Value <=0');

  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=rXYRect_GK;

//  s:=IncludeTrailingBackslash(Params.RLF_FileDir) + ExtractFileName(sFile_DEM_file);
 // s:=IncludeTrailingBackslash(Params.RLF_FileName);
 // s:=ChangeFileExt(s, '.rlf');

  Params.RLF_FileName := ChangeFileExt(Params.RLF_FileName, '.rlf');

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

 // xy := rXYRect_GK.TopLeft;

  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);

    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


      assert(xy.y>0);
      
     // if Projection=ptUTM then
      xy_UTM := geo_GK_to_UTM(xy, FGK_zone);
     /// else
    //    xy_UTM := xy;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

      if FDEM_index_file.FindValueByXY(xy_UTM.x, xy_UTM.y, w) then
      begin
        if w > High(Smallint) then
          w := High(Smallint);

        rlf_rec.Rel_H :=w;

        if Params.IsUse_Clutter_Classes then
          if FClutter_index_file.FindValueByXY(xy_UTM.x, xy_UTM.y, wCluCode) then
        begin
        //  wCluCode:=1;

          if (wCluCode>=1) and (wCluCode<100) then
          begin
            if FClutterInfo.GetOnegaCode(wCluCode,  rClutter) then
            begin
              rlf_rec.Clutter_Code := rClutter.Onega_Code;
              rlf_rec.Clutter_H    := rClutter.Onega_Height;
            end;



          //  rlf_rec.Clutter_Code := FClutterInfo.Clutters[wCluCode].Onega_Code;
           // rlf_rec.Clutter_H    := FClutterInfo.Clutters[wCluCode].Onega_Height;

            //  FClutter_Classes_file.NClasses[iCluCode].Onega_Code;

            // ����� + ������ =0
        //    if (rlf_rec.Clutter_Code = DEF_CLU_CITY) and (iCluH=0)  then
         //     rlf_rec.Clutter_Code :=0;
      

          if Params.IsUse_Clutter_H then
            if FClutter_H_index_file.FindValueByXY(xy_UTM.x, xy_UTM.y, w) then
            begin
              if w>250 then w:=250;

              rlf_rec.Clutter_H := w;
            end;

           end;
(*
          if FAsset_Clutter_Heights.FindValue(xy_UTM.x, xy_UTM.y, iCluH) then
          begin
            if iCluH>250 then iCluH:=250;

            rlf_rec.Clutter_H := iCluH;
          end;
*)

        end;

      end
      else
        rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

//  oRlfFileStream.Free;

  FreeAndNil(oRlfFileStream);

(*  FDEM_file.CloseFile;
  FClutter_Classes_file.CloseFile;
*)



  if not Terminated then
    ExportToClutterTab(Params.RLF_FileName);


end;




end.
