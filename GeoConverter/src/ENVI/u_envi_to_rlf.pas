unit u_envi_to_rlf;


interface

uses Classes, Sysutils, Dialogs,forms,

  u_GDAL_envi,
  u_GDAL_classes,

//  u_Rel_to_Clutter_map,

  u_GDAL_Bat,

  u_CustomTask,

  u_Rel_to_clutter_map,

  //T_Rel_to_Clutter_map.Exec(aFileName: string);

  dm_Clutters,


  I_rel_Matrix1,

  u_rlf_Matrix,

  u_geo,

  u_files,
  u_Run,



  u_clutter_classes;


type
  TEnvi_to_RLF = class(TCustomTask)
  private
 //   FUTM_zone : Integer;
 //   FZone_GK : Integer;

    // -------------------------
    // index_file
    // -------------------------

    FDEM_Envi_File: TEnvi_File;
    FClutter_Envi_File: TEnvi_File;
    FClutter_H_Envi_File: TEnvi_File;

 //   FFiles: TStringList;

    FClutterInfo: TClutterModel;

    procedure Prepare_ENVI;
//    FProjection_Txt_file: TProjection_Txt_file;

    procedure RunFile;
   protected
  //  procedure CloseFiles; override;

   public

     Params: record
            FileName_Dem : string;

            Zone_UTM : byte;

  //            Projection_Txt_FileName : string;

            // -----------------------------------
            IsUse_Clutter : Boolean;
            FileName_Clutter : string;

            // -----------------------------------
            IsUse_Clutter_H       : Boolean;
            FileName_Clutter_H : string;

            Clutters_section : string;


            Null_value : smallint;


            RLF_FileName : string;
            Rlf_Step  : Integer;
        end;

    constructor Create;

    destructor Destroy; override;

    procedure Execute;
  end;



implementation

// ---------------------------------------------------------------
constructor TEnvi_to_RLF.Create;
// ---------------------------------------------------------------
begin
  inherited;

//  FDEM_index_file    :=TAsset_index_file.Create;
//  FClutter_index_file:=TAsset_index_file.Create;
//  FClutter_H_index_file:=TAsset_index_file.Create;


  FDEM_Envi_File:=TEnvi_File.Create;
  FClutter_Envi_File:=TEnvi_File.Create;
  FClutter_H_Envi_File:=TEnvi_File.Create;


  FClutterInfo := TClutterModel.Create();
  //FProjection_Txt_file := TProjection_Txt_file.Create();

//  FFiles:=TStringList.Create;

end;


// ---------------------------------------------------------------
destructor TEnvi_to_RLF.Destroy;
// ---------------------------------------------------------------
begin
//  FreeAndNil(FFiles);
//  FreeAndNil(FProjection_Txt_file);
  FreeAndNil(FClutterInfo);


//  FreeAndNil(FDEM_index_file);
//  FreeAndNil(FClutter_index_file);
//  FreeAndNil(FClutter_H_index_file);

  FreeAndNil(FDEM_Envi_File);
  FreeAndNil(FClutter_Envi_File);
  FreeAndNil(FClutter_H_Envi_File);

  inherited;
end;


//procedure TAsset_to_RLF.CloseFiles;
//begin
//  FDEM_Envi_File.CloseFile;
//  FClutter_Envi_File.CloseFile;
//  FClutter_H_Envi_File.CloseFile;
//end;


// ---------------------------------------------------------------
procedure TEnvi_to_RLF.Execute;
// ---------------------------------------------------------------

//const
//  DEF_MAX_ROWS = 4000;
//  DEF_MAX_COLS = 4000;


//  "size":[
//    3082,
//    1029
//  ],


var
//  dX_por_row: Integer;
//  dY_por_col: Integer;
  //e: Double;
  I: Integer;
  iColPart: Integer;
  iEPSG_GK: word;
  iRowPart: Integer;
  k: Integer;
  oBAT: TGDAL_Bat;
  sDir: string;

  sFile: string;

  rec: TGDAL_json_rec;
  s: string;
  sFileName: string;

  xyRect: TXYRect;

begin
  Terminated := False;

  // -------------------------------------------

  assert(Params.RLF_FileName<>'');

  assert (FileExists(params.FileName_Dem) );
//  assert (FileExists(params.Projection_Txt_FileName) );

  params.IsUse_Clutter   :=params.IsUse_Clutter    and FileExists(params.FileName_Clutter);
  params.IsUse_Clutter_H :=params.IsUse_Clutter_H  and FileExists(params.FileName_Clutter_H);

  // -------------------------------------------


  if Params.IsUse_Clutter then
    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);

  Prepare_ENVI;

  RunFile();

end;


// ---------------------------------------------------------------
procedure TEnvi_to_RLF.Prepare_ENVI;
// ---------------------------------------------------------------

var
  iEPSG_UTM: word;
  iEPSG_GK : word;

  oBAT: TGDAL_Bat;
  sDir: string;

  sFile: string;

  //rec: TGDAL_json_rec;
  s: string;
  sBat: string;
  sFileName: string;
  sFileNoExt: string;

 // xyRect: TXYRect;

begin

  assert (FileExists(params.FileName_Dem) );
  assert (params.Zone_UTM>0);

  oBAT:=TGDAL_Bat.Create;

//
//  if Params.IsUse_Clutter then
//  begin
//    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);
//
//  end;

  iEPSG_UTM :=  32600 + Params.Zone_UTM;
  iEPSG_GK  :=  28400 + Params.Zone_UTM - 30;


  // -----------------------------------------------------
  //  FileName_Dem
  // -----------------------------------------------------
  if not FileExists( ChangeFileExt(Params.FileName_Dem,'.envi'))  then
  begin
    oBAT.Init_Header;

    sFile      := ExtractFileName ( Params.FileName_Dem);
    sFileNoExt := ExtractFileNameNoExt ( Params.FileName_Dem);

//    oBAT.Add(Format('gdal_translate -co compress=lzw  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));
    oBAT.Add(Format('gdal_translate  -a_srs EPSG:%d  -of GTiff  "%s" "%s.utm.tif"', [iEPSG_UTM,  sFile, sFileNoExt ]));
    oBAT.Add(Format('gdalwarp  -overwrite -t_srs EPSG:%d   "%s.utm.tif" "%s.gk.tif"', [iEPSG_GK,  sFileNoExt, sFileNoExt ]));
    oBAT.Add('');

    oBAT.Add(Format('gdal_translate -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));
    oBAT.Add(Format('gdalinfo.exe   -json %s.envi > %s.json',[sFileNoExt, sFileNoExt]));


//    gdalwarp -t_srs EPSG:28407  "C_77_50m37_Moskowskaya_1.tif" "C_77_50m37_Moskowskaya_2.tif"


    sBat:=ChangeFileExt(Params.FileName_Dem,'.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));


    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');

  end;


  // -----------------------------------------------------
  //  FileName_Dem
  // -----------------------------------------------------
  if Params.IsUse_Clutter then
    if not FileExists( ChangeFileExt(Params.FileName_Clutter,'.envi'))  then
  begin
    oBAT.Init_Header;

    sFile      := ExtractFileName ( Params.FileName_Clutter);
    sFileNoExt := ExtractFileNameNoExt ( Params.FileName_Clutter);


//    oBAT.Add(Format('gdal_translate  -co compress=lzw  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));
    oBAT.Add(Format('gdal_translate -a_srs EPSG:%d  -of GTiff  "%s" "%s.utm.tif"', [iEPSG_UTM,  sFile, sFileNoExt ]));
    oBAT.Add(Format('gdalwarp  -overwrite  -t_srs EPSG:%d   "%s.utm.tif" "%s.gk.tif"', [iEPSG_GK,  sFileNoExt, sFileNoExt ]));

    oBAT.Add(Format('gdal_translate -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));
    oBAT.Add(Format('gdalinfo.exe   -json %s.envi > %s.json',[sFileNoExt, sFileNoExt]));


//    oBAT.Add(Format('gdal_translate  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));

//    gdal_translate -of AAIGrid  -b 1    "grd.tif" "grd_2.asc"


    sBat:=ChangeFileExt(Params.FileName_Clutter,'.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));

    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');

  end;


  // -----------------------------------------------------
  //  FileName_Dem
  // -----------------------------------------------------
  if Params.IsUse_Clutter_H then
    if not FileExists( ChangeFileExt(Params.FileName_Clutter_H,'.envi'))  then

  begin
    oBAT.Init_Header;

    sFile      := ExtractFileName ( Params.FileName_Clutter_H);
    sFileNoExt := ExtractFileNameNoExt ( Params.FileName_Clutter_H);


//    oBAT.Add(Format('gdal_translate -co compress=lzw  -a_srs EPSG:%d  -of GTiff  "%s" "%s.tif"', [iEPSG,  sFile, sFileNoExt ]));
    oBAT.Add(Format('gdal_translate -a_srs EPSG:%d  -of GTiff  "%s" "%s.utm.tif"', [iEPSG_UTM,  sFile, sFileNoExt ]));
    oBAT.Add(Format('gdalwarp   -overwrite -t_srs EPSG:%d   "%s.utm.tif" "%s.gk.tif"', [iEPSG_GK,  sFileNoExt, sFileNoExt ]));
    oBAT.Add('');

    oBAT.Add(Format('gdal_translate -of Envi   "%s.gk.tif" "%s.envi"' , [sFileNoExt, sFileNoExt ]));
    oBAT.Add(Format('gdalinfo.exe   -json %s.envi > %s.json',[sFileNoExt, sFileNoExt]));


    sBat:=ChangeFileExt(Params.FileName_Clutter_H,'.bat');
    oBAT.SaveToFile(sBat, TEncoding.GetEncoding(866));

    SetCurrentDir(ExtractFilePath(sBat));
    RunApp (sBat,'');

  end;


  FreeAndNil(oBAT);


end;



// ---------------------------------------------------------------
procedure TEnvi_to_RLF.RunFile;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  rXYRect_GK: TXYRect;


  xy,xy_gk,xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
  eHeight: Double;

  iColCount: int64;
  iRowCount: int64;
  iStep: word;
  iRow: int64;
  iCol: int64;


  rlf_rec: Trel_Record;
  iValue: integer;

  oRlfFileStream: TFileStream;
  s: string;
  iCluCode: smallint;

  k: Integer;
  s2: string;
  s1: string;
  iW: Integer;
  wCluCode: Integer;

  rClutter: TClutterRec;
//  sDir: string;


  oFile: TEnvi_File;

  oMatrix: TrlfMatrix;
//  sRLF_FileName: string;

begin
  Params.RLF_FileName :=ChangeFileExt(Params.RLF_FileName, '.rlf');


//
//  T_Rel_to_Clutter_map.Exec(Params.RLF_FileName);
//  exit;


//  FDEM_Envi_File.OpenFile('E:\Tasks\beeline\ASC\oblast\1.envi');
//  FDEM_Envi_File.GetDataByIndex(0);

  FDEM_Envi_File.OpenFile(params.FileName_Dem);
//  FDEM_Envi_File.GetDataByIndex(0);

 // FDEM_Envi_File.GetDataByIndex(54100 * 4700);

//  FDEM_Envi_File.GetDataByIndex((FDEM_Envi_File.RowCount-1) * (FDEM_Envi_File.ColCount-1));



  if params.IsUse_Clutter then
    FClutter_Envi_File.OpenFile(params.FileName_Clutter);

  if params.IsUse_Clutter_H then
    FClutter_H_Envi_File.OpenFile(params.FileName_Clutter_H);


  //  FDEM_Envi_File:=TEnvi_File.Create;
//  FClutter_Envi_File:=TEnvi_File.Create;
//  FClutter_H_Envi_File:=TEnvi_File.Create;


//   ExportToClutterTab(Params.RLF_FileName);
//   Exit;
//------------------------------------------------

//
//
//  if Terminated then
//    Exit;


//   if Params.Is_Use_Clutter_bounds then
//     oFile:=FClutter_Envi_File
//   else

  oFile:=FDEM_Envi_File;


//  FDEM_Envi_File:=TEnvi_File.Create;
//  FClutter_Envi_File:=TEnvi_File.Create;
//  FClutter_H_Envi_File:=TEnvi_File.Create;
//


  if Params.Rlf_Step=0 then
    iStep :=Round(oFile.CellSize_lat)
  else
    iStep := Params.Rlf_Step;


 // iStep :=Round(FDEM_index_file.CellSize);

  Assert(iStep>0, 'Value <=0');

  iRowCount := oFile.RowCount;
  iColCount := oFile.ColCount;


  iRowCount := Round((oFile.bounds.lat_max - oFile.bounds.lat_min)  / iStep);
  iColCount := Round((oFile.bounds.lon_max - oFile.bounds.lon_min)  / iStep);


  rXYRect_GK.TopLeft.X:=oFile.bounds.lat_max;
  rXYRect_GK.TopLeft.Y:=oFile.bounds.lon_min;
//  rXYRect_GK.TopLeft.Y:= 1000000*(Params.Zone_UTM-30) + Round(oFile.bounds.lon_min) mod 1000000;

  //Params.Zone_UTM-30;

  rXYRect_GK.BottomRight.X:=rXYRect_GK.TopLeft.X - iRowCount * iStep;
  rXYRect_GK.BottomRight.Y:=rXYRect_GK.TopLeft.Y + iColCount * iStep;


 //   header_Rec.ZoneNum_GK:=FGK_zone;
//  FDEM_Envi_File.GK_zone:=Params.Zone_UTM-30;

//  FDEM_Envi_File.SaveToIniFile (ChangeFileExt(Params.RLF_FileName,'.ini') );
//  oFile.SaveToXmlFile (ChangeFileExt(Params.RLF_FileName,'.xml') );


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;
  header_Rec.ZoneNum_GK:=Params.Zone_UTM-30;

  header_Rec.XYBounds:=rXYRect_GK;
//  header_Rec.XYBounds.TopLeft.Y     :=header_Rec.XYBounds.TopLeft.Y     + 1000000*(Params.Zone_UTM-30);
//  header_Rec.XYBounds.BottomRight.Y :=header_Rec.XYBounds.BottomRight.Y + 1000000*(Params.Zone_UTM-30);

 // sDir:=ExtractFilePath(aRLF_FileName);
//  sDir:=ExtractFilePath(Params.RLF_FileName);
//  ForceDirectories(sDir);


//  sRLF_FileName:=ChangeFileExt(Params.RLF_FileName, '.rlf');

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);
//  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

  FMemStream.SetSize (iColCount * SizeOf(rlf_rec));
  FMemStream.Position:=0;


  for iRow := 0 to iRowCount - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount, bTerminated);

    //


    for iCol := 0 to iColCount - 1 do
    begin
      if Terminated then  Break;

      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;


      FillChar(rlf_rec, SizeOf(rlf_rec), 0);

      if FDEM_Envi_File.FindValueByXY(xy.x, xy.y, iW) then
      begin
       // if iW > High(Smallint) then
        //  iW := High(Smallint);

        rlf_rec.Rel_H :=iW;

        if Params.IsUse_Clutter then
          if FClutter_Envi_File.FindValueByXY(xy.x, xy.y, wCluCode) then
        begin
          if (wCluCode>=1) and (wCluCode<100) then
          begin
            if FClutterInfo.GetOnegaCode(wCluCode,  rClutter) then
            begin
              rlf_rec.Clutter_Code := rClutter.Onega_Code;
              rlf_rec.Clutter_H    := rClutter.Onega_Height;
            end;


          //--------------------------------
          if Params.IsUse_Clutter_H then
            if FClutter_H_Envi_File.FindValueByXY(xy.x, xy.y, iW) then
            begin
             // if w>250 then
              //   w:=0;//250;

              try
                rlf_rec.Clutter_H := iW;
              except
                rlf_rec.Clutter_H :=0;

//CodeSite.SendError(IntToStr(iW));

              end;

              if rlf_rec.Clutter_Code in [0,2,4,5] then //������
                rlf_rec.Clutter_H := 0;


            end;

        end;

        end;

      end
      else
        rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;
//    FMemStream.Position:=0;
  end;



  FreeAndNil(oRlfFileStream);


  // ---------------------------------------------------------------
//  oMatrix:=TrlfMatrix.Create;
//  oMatrix.LoadHeaderFromFile (Params.RLF_FileName);
//  FreeAndNil(oMatrix);

  // ---------------------------------------------------------------



  if not Terminated then
    T_Rel_to_Clutter_map.Exec(Params.RLF_FileName);


//  if not Terminated then
//    T_Rel_to_Clutter_map.Exec(aRLF_FileName);
//    T_Rel_to_Clutter_map.Exec(aFileName: string);
//    ExportToClutterTab(aRLF_FileName);


end;


end.

{

var
  FDEM_Envi_File: TEnvi_File;

begin
  FDEM_Envi_File:=TEnvi_File.Create;

  FDEM_Envi_File.OpenFile('E:\Tasks\beeline\ASC\T_50_02m37_Moskovskaya_Cities_R1C1\C_50_02m37_Moskovskaya_Cities_R1C1.envi');
//  FDEM_Envi_File.GetDataByIndex(0);

  FDEM_Envi_File.GetDataByIndex(54100 * 4700);


  FreeAndNil(FDEM_Envi_File);


end.


