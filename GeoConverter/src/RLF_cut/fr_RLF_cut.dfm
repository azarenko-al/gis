inherited frame_RLF_cut: Tframe_RLF_cut
  Left = 1190
  Top = 477
  Caption = 'frame_RLF_cut'
  ClientHeight = 412
  ClientWidth = 558
  OnDestroy = FormDestroy
  ExplicitLeft = 1190
  ExplicitTop = 477
  ExplicitWidth = 566
  ExplicitHeight = 440
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 396
    Width = 558
    ExplicitTop = 396
    ExplicitWidth = 558
  end
  object Panel2: TPanel [1]
    Left = 0
    Top = 0
    Width = 558
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      558
      89)
    object lb_Relief: TLabel
      Left = 8
      Top = 8
      Width = 107
      Height = 13
      Caption = 'ONEGA rlf ('#1080#1089#1093#1086#1076#1085#1099#1081')'
    end
    object ed_DEM: TFilenameEdit
      Left = 8
      Top = 24
      Width = 543
      Height = 21
      Filter = 'Rlf (*.rlf)|*.rlf'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = 'S:\__TASKS\TADJIKISTAN_dem__.rlf'
    end
    object Button1: TButton
      Left = 8
      Top = 51
      Width = 107
      Height = 30
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' txt...'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  inherited ActionList_custom: TActionList
    Top = 112
  end
  object SaveTextFileDialog1: TSaveTextFileDialog
    DefaultExt = '.txt'
    Left = 256
    Top = 120
  end
end
