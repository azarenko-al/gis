unit u_RLF_cut;

interface

uses
  Classes, Sysutils, Graphics, Dialogs,

  u_BMP_file,



  

  u_CustomTask,

  
  u_geo_convert_new,

  I_rel_Matrix1,

  u_geo,
  u_geo_poly,

  u_rlf_Matrix

  ;


type
  TRLF_cut = class(TCustomTask)
  private
  public
    Params: record

      FileName : string;

      BorderBLPoints: TBLPointArray;

      RLF_FileName : string;

      Export_FileName: string;
      
    end;

    procedure Run;

    procedure Export_Txt;

    
  end;


implementation



procedure TRLF_cut.Export_Txt;
var
  oSrcRlfMatrix: TrlfMatrix;
begin
  oSrcRlfMatrix:=TrlfMatrix.Create;

  oSrcRlfMatrix.OpenFile(Params.FileName);

  oSrcRlfMatrix.SaveToDebugFiles (ExtractFileDir(Params.Export_FileName));  

  FreeAndNil(oSrcRlfMatrix);
  
  ShowMessage ('Выполнено.');
  
end;

// ---------------------------------------------------------------
procedure TRLF_cut.Run;
// ---------------------------------------------------------------
var
  b: Boolean;
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

  rXYRect_GK: TXYRect;

  xy : TXYPoint;

  bl_CK42: TBLPoint;

  bl,bl1,bl2: TBLPoint;
  bTerminated: Boolean;
  cl: integer;
  d: Double;

  iGK_zone: integer;
  iColCount1: integer;
  iRowCount1: integer;

  iRow: integer;
  iCol: integer;
  iColCount_new: Integer;
  iRowCount_new: Integer;
  iStep: Integer;

  rlf_rec: Trel_Record;
  iValue: integer;
  oBmpFile: TBmpFile;

  oRlfFileStream: TFileStream;
  oSrcRlfMatrix: TrlfMatrix;
  s: string;
  sBmpFileName: string;

  xyBorderXYPoints: TXYPointArray;

  wValue: Word;
  xyRect: TXYRect;
  xyRect_new: TXYRect;

begin
  oSrcRlfMatrix:=TrlfMatrix.Create;

  oSrcRlfMatrix.OpenFile(Params.FileName);

 // OpenFiles();

 // FFileName_DEM:=ed_DEM.FileName;

  Terminated := False;

 // btn_Run.Action := act_Stop;

//   Info.PixelSize



  if Terminated then
    Exit;
                    

  iStep := Round(oSrcRlfMatrix.StepX);
  iGK_zone := oSrcRlfMatrix.ZoneNum_GK;
  rXYRect_GK:= oSrcRlfMatrix.XYBounds;

  iRowCount1 := oSrcRlfMatrix.RowCount;
  iColCount1 := oSrcRlfMatrix.ColCount;

  // -------------------------
  // border
  // -------------------------
  SetLength(xyBorderXYPoints, Length(Params.BorderBLPoints));


  for i:=0 to Length(Params.BorderBLPoints)-1 do
  begin
    bl:=Params.BorderBLPoints[i];
    xyBorderXYPoints[i] := geo_Pulkovo42_to_XY(bl, iGK_zone );
  end;



  xyRect:=geo_RoundXYPointsToXYRectNew(xyBorderXYPoints);
  xyRect_new:=rXYRect_GK;


  i := trunc ((rXYRect_GK.TopLeft.X - xyRect.TopLeft.X ) / iStep);
  if i>0 then
    xyRect_new.TopLeft.X := rXYRect_GK.TopLeft.X - iStep * i;

  i := trunc ((xyRect.TopLeft.y  - rXYRect_GK.TopLeft.Y ) / iStep);
  if i>0 then
    xyRect_new.TopLeft.Y := rXYRect_GK.TopLeft.Y + iStep * i;

  i := trunc ((xyRect.BottomRight.X -rXYRect_GK.BottomRight.X  ) / iStep);
  if i>0 then
    xyRect_new.BottomRight.X := rXYRect_GK.BottomRight.X + iStep * i;

  i := trunc ((rXYRect_GK.BottomRight.Y - xyRect.BottomRight.y ) / iStep);
  if i>0 then
    xyRect_new.BottomRight.Y := rXYRect_GK.BottomRight.Y - iStep * i;


//  geo_XYPointInRect()


  //iStep :=round(FDEM.Cellsize);

//  Assert(iStep>0, 'Value <=0');


  geo_Check_XYRect (xyRect_new);

  iRowCount_new:=Round(Abs(xyRect_new.BottomRight.X - xyRect_new.TopLeft.X) / iStep);
  iColCount_new:=Round(Abs(xyRect_new.BottomRight.Y - xyRect_new.TopLeft.Y) / iStep);


(*  iColCount_new:=iColCount1;
  iRowCount_new:=iRowCount1;
  xyRect_new := rXYRect_GK;
*)
  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount_new;
  header_Rec.RowCount:=iRowCount_new;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.XYBounds:=xyRect_new;


  // ---------------------------------------------------------------

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);


  sBmpFileName :=ChangeFileExt(Params.RLF_FileName, '.bmp');

  oBmpFile:=TBmpFile.Create_Width_Height (sBmpFileName, iColCount_new, iRowCount_new);

  // ---------------------------------------------------------------



 // xy := rXYRect_GK.TopLeft;

  for iRow := 0 to iRowCount_new - 1 do
  begin
    if Terminated then
      Break;

    DoOnProgress(iRow, iRowCount_new, bTerminated);


    for iCol := 0 to iColCount_new - 1 do
    begin
      if Terminated then  Break;

      xy.x := xyRect_new.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := xyRect_new.TopLeft.Y + iCol*iStep + iStep/2;


    //  FillChar(rlf_rec, SizeOf(rlf_rec), 0);

      b:=True;

      if Length(xyBorderXYPoints)>0 then
        if not geo_IsXYPointInXYPolygon(xy, xyBorderXYPoints) then
           b:=False ;
     //   else
       //    b:=True;

      //  b:=True;

      FillChar(rlf_rec, SizeOf(rlf_rec), 0);
      rlf_rec.Rel_H := EMPTY_HEIGHT;
      cl:=clBlack;

      if b then
      begin
        b:=oSrcRlfMatrix.FindPointXY (xy, 0, rlf_rec);
        if b then
          cl:=clRed
        else
          cl:=clBlack;
      end else begin
     //   rlf_rec.Rel_H := EMPTY_HEIGHT;
       // cl:=clBlack;
      end;


      oBmpFile.Write(cl);



 //     if (not b) or (not oSrcRlfMatrix.GetItem1(iRow,iCol,rlf_rec)) then
   //     FillChar(rlf_rec, SizeOf(rlf_rec), 0);


      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;
    oBmpFile.Writeln;

  end;

  FreeAndNil(oRlfFileStream);

  FreeAndNil(oBmpFile);


 // sImgFileName:=ChangeFileExt(sBmpFileName, '.jpg');
 // img_BMP_moveto_JPEG(sBmpFileName, ChangeFileExt(sBmpFileName, '.jpg'));



end;



end.


