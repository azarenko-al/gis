unit fr_RLF_cut;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls,
  IniFiles,rxToolEdit,

  u_const,

  fr_Custom_Form,

  u_RLF_cut,

  frame_RLF, Mask, ActnList, Vcl.Dialogs, Vcl.ExtDlgs, System.Actions;

type
  Tframe_RLF_cut = class(Tfrm_Custom_Form)
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    Button1: TButton;
    SaveTextFileDialog1: TSaveTextFileDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
                          
  private

    FIniFileName: string;

    FRLF_cut: TRLF_cut;

    procedure SaveToIniFile(aFileName: String);

  protected
    procedure Run; override;
  public

    procedure LoadFromIniFile(aFileName: String);


  end;

var
  frame_RLF_cut: Tframe_RLF_cut;

implementation
{$R *.dfm}

const
  DEF_SECTION = 'RLF_cut';



procedure Tframe_RLF_cut.Button1Click(Sender: TObject);
var
  oRLF_cut: TRLF_cut;
begin
  if SaveTextFileDialog1.Execute then
  begin    
    oRLF_cut := TRLF_cut.Create();
    oRLF_cut.Params.FileName := ed_DEM.FileName;    
    oRLF_cut.Params.Export_FileName:=SaveTextFileDialog1.FileName;

    oRLF_cut.Export_Txt;
    
    FreeAndNil(oRLF_cut);

  end;
  
  
end;

// ---------------------------------------------------------------
procedure Tframe_RLF_cut.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  FRLF_cut := TRLF_cut.Create();
  FRLF_cut.OnProgress :=DoOnProgress;

  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadFromIniFile(FIniFileName);

  frm_RLF1.Init;

  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;

 // lb_Menu_txt.Caption  := STR_Menu_txt;


end;


procedure Tframe_RLF_cut.FormDestroy(Sender: TObject);
begin
  SaveToIniFile(FIniFileName);

  FreeAndNil(FRLF_cut);
end;

// ---------------------------------------------------------------
procedure Tframe_RLF_cut.Run;
// ---------------------------------------------------------------
begin
  inherited;

  FTerminated := False;

  //btn_Run.Action := act_Stop;

  FRLF_cut.Params.FileName := ed_DEM.FileName;



//  cb_Clutter_height

//  FIC2_to_RLF.Params.Passport_FileName := ed_Passport.FileName;
//  FIC2_to_RLF.Params.Menu_Txt_FileName := ed_Menu_TXT1.FileName;

//  frm_RLF1.GetBorderPoints(FRLF_cut.Params.BorderBLPoints);

  FRLF_cut.Params.Rlf_FileName := frm_RLF1.ed_RLF.FileName;


  FRLF_cut.Run;

//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;

end;

// ---------------------------------------------------------------
procedure Tframe_RLF_cut.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do
 // with TIniFile.Create(FIniFileName) do
  begin
    ed_DEM.FileName := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

  //  cb_Clutter_Heights.checked:= ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);

   // Free;
  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;

// ---------------------------------------------------------------
procedure Tframe_RLF_cut.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------
var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);

  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;


end.
