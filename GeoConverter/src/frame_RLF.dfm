object frame_RLF_: Tframe_RLF_
  Left = 0
  Top = 0
  Width = 704
  Height = 159
  TabOrder = 0
  OnClick = FrameClick
  DesignSize = (
    704
    159)
  object lb_File: TLabel
    Left = 8
    Top = 8
    Width = 48
    Height = 13
    Caption = 'ONEGA rlf'
  end
  object lb_Step: TLabel
    Left = 8
    Top = 51
    Width = 36
    Height = 13
    Caption = #1064#1072#1075', m'
  end
  object Bevel2: TBevel
    Left = 0
    Top = 0
    Width = 704
    Height = 9
    Align = alTop
    Shape = bsTopLine
    Visible = False
    ExplicitWidth = 582
  end
  object ed_RLF: TFilenameEdit
    Left = 8
    Top = 24
    Width = 687
    Height = 21
    Filter = 'RLF(*.rlf)|*.rlf'
    Anchors = [akLeft, akTop, akRight]
    NumGlyphs = 1
    TabOrder = 0
    Text = ''
    OnExit = ed_RLFExit
    ExplicitWidth = 626
  end
  object ed_Step1: TEdit
    Left = 56
    Top = 47
    Width = 49
    Height = 21
    TabOrder = 1
  end
end
