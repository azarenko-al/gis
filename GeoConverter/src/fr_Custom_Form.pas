unit fr_Custom_Form;

interface

uses
  Classes, Controls, Forms, DB,
  ComCtrls, StdCtrls, ExtCtrls, ActnList, System.Actions
  ;

type
  Tfrm_Custom_Form = class(TForm)
    ActionList_custom: TActionList;
    act_Stop: TAction;
    act_Run: TAction;
    ProgressBar1: TProgressBar;
    procedure act_StopExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    FTerminated : Boolean;

    FDataset: TDataset;

//    FormName : string;


//    procedure SaveRlfHeaderToHeaderRec(var aRec: TrelMatrixInfoRec);
//    FBilList: TBilList;

//    procedure DoOnProgressMsg(aProgress,aMax: integer; var aTerminated: boolean);
    procedure DoOnProgress(aProgress,aMax: integer; var aTerminated: boolean);

    procedure Run (aDataset: TDataset); virtual;

  public

  end;
  
 

implementation

{$R *.dfm}


procedure Tfrm_Custom_Form.act_StopExecute(Sender: TObject);
begin
  if Sender=act_Run then
    Run (FDataset) ;

  if Sender=act_Stop then
    FTerminated := True;
end;


procedure Tfrm_Custom_Form.DoOnProgress(aProgress,aMax: integer; var
    aTerminated: boolean);
begin
  aTerminated :=FTerminated;

  ProgressBar1.Max :=aMax;
  ProgressBar1.Position :=aProgress;

  Application.ProcessMessages;
end;


procedure Tfrm_Custom_Form.FormCreate(Sender: TObject);
begin
  act_Run.caption := 'Построить';

  //btn_Run.Action := act_Run;

  act_Run.OnExecute := act_StopExecute;
  act_Stop.OnExecute := act_StopExecute;



end;


procedure Tfrm_Custom_Form.Run;
begin
  FTerminated := False;

//  btn_Run.Action := act_Stop;
end;


end.
