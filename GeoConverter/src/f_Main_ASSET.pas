unit f_Main_ASSET;

interface

uses
  SysUtils, Classes, Controls, Forms, dialogs,
  StdCtrls, ExtCtrls, ComCtrls, rxPlacemnt, ActnList,

  

  //d_Config,

  

  u_func,

//  fr_GeoTiff,

//  fr_BIL_to_RLF_new,
//  fr_RLF_cut,
//  fr_RLF_to_UTM,
//  fr_Asc_to_RLF,
//  fr_BIL_to_RLF,
//  fr_ATDI_to_RLF,
  fr_Asset_to_RLF_,
//  fr_ASC_and_GRD_to_RLF,
//  fr_VM_GRD_to_RLF, 

  Menus, System.Actions;

type
  Tfrm_Main_ASSET = class(TForm)
    FormStorage1: TFormStorage;
    ActionList1: TActionList;
    PageControl1: TPageControl;
    TabSheet_Asset: TTabSheet;

    procedure act_SetupExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure Panel1Click(Sender: TObject);
  private
  public

  end;

var
  frm_Main_ASSET: Tfrm_Main_ASSET;

implementation

uses dm_Clutters;


{$R *.dfm}


// ---------------------------------------------------------------
procedure Tfrm_Main_ASSET.act_SetupExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
//  if Sender=act_Setup then
//    Tfrm_Clutters.ExecDlg_NoResult();

end;


procedure Tfrm_Main_ASSET.Button1Click(Sender: TObject);
var
  s: string;
begin
 // Tfrm_Clutters.ExecDlg(s);
end;

// ---------------------------------------------------------------
procedure Tfrm_Main_ASSET.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption := Caption + GetAppVersionStr;

  PageControl1.Align:=alClient;

//  CreateChildForm_(Tframe_ATDI_to_RLF,    frame_ATDI_to_RLF, TabSheet_ATDI);
//  CreateChildForm_(Tframe_VM_GRD_to_RLF,  frame_VM_GRD_to_RLF, TabSheet_GRD_GRC);
  CreateChildForm_(Tframe_Asset_to_RLF,    frame_Asset_to_RLF, TabSheet_Asset);
//  CreateChildForm_(Tframe_Asc_to_RLF,    frame_Asc_to_RLF, TabSheet_ASC);
//
//  CreateChildForm_(Tframe_BIL_to_RLF,        frame_BIL_to_RLF, TabSheet_Bil);
//  CreateChildForm_(Tframe_BIL_to_RLF_new,    frame_BIL_to_RLF_new, TabSheet_Bil_index);
//
//
//
//  CreateChildForm_(Tframe_ASC_and_GRD_to_RLF,    frame_ASC_and_GRD_to_RLF, TabSheet_ASC_and_GRD);
//  CreateChildForm_(Tframe_GeoTiff,               frame_GeoTiff, TabSheet_GeoTiff);
//  CreateChildForm_(Tframe_RLF_cut,               frame_RLF_cut, TabSheet_RLF_cut);
//  CreateChildForm_(Tframe_RLF_to_UTM,            frame_RLF_to_UTM, TabSheet_RLF_to_UTM);

  dmClutters();

end;

// ---------------------------------------------------------------
procedure Tfrm_Main_ASSET.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
//  FreeAndNil(frame_VM_GRD_to_RLF);
  FreeAndNil(frame_Asset_to_RLF);
//  FreeAndNil(frame_Asc_to_RLF);
//
//  FreeAndNil(frame_BIL_to_RLF);
//  FreeAndNil(frame_BIL_to_RLF_new);
//
//  FreeAndNil(frame_ATDI_to_RLF);
//  FreeAndNil(frame_ASC_and_GRD_to_RLF);
//  FreeAndNil(frame_GeoTiff);
//  FreeAndNil(frame_RLF_cut);
//  FreeAndNil(frame_RLF_to_UTM);

end;


end.



