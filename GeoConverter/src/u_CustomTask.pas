unit u_CustomTask;

interface

uses
  SysUtils,     

  u_Rel_to_Clutter_map,
//  u_3D_rel_to_bmp,

  
  System.Classes;



type
  TOnProgressEvent = procedure (aProgress,aMax: integer; var aTerminated: boolean) of object;

type
  TCustomTask = class(TObject)
  private
    FOnProgress: TOnProgressEvent;


  protected
    FMemStream: TMemoryStream;

    procedure DoOnProgress(aProgress,aMax: integer; var aTerminated: boolean);
    procedure ExportToClutterTab(aFileName: string);

    function OpenFiles: Boolean; virtual; abstract;
    procedure CloseFiles; virtual; abstract;

    
//  procedure Log(aMsg: string);

  public
    Terminated: Boolean;

    constructor Create;
    destructor Destroy; override;
    
    procedure Run; virtual; abstract;

    property OnProgress: TOnProgressEvent read FOnProgress write FOnProgress;
  end;

  

implementation


constructor TCustomTask.Create;
begin
  inherited;

  FMemStream:=TMemoryStream.Create;

end;

destructor TCustomTask.Destroy;
begin
  FreeAndNil(FMemStream);

  inherited;
end;

procedure TCustomTask.DoOnProgress(aProgress,aMax: integer; var aTerminated:
    boolean);
begin
  if assigned(FOnProgress) then
  begin
    FOnProgress(aProgress,aMax, Terminated);
    aTerminated := Terminated;
  end;
end;

// ---------------------------------------------------------------
procedure TCustomTask.ExportToClutterTab(aFileName: string);
// ---------------------------------------------------------------   
begin     
  T_Rel_to_Clutter_map.Exec(aFileName);
 // T3D_Rel_to_Bmp.Exec(aFileName);
  
       
end;


end.



