object dlg_Task: Tdlg_Task
  Left = 873
  Top = 262
  Width = 485
  Height = 492
  Caption = 'dlg_Task'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 477
    Height = 233
    Align = alTop
    LookAndFeel.Kind = lfUltraFlat
    OptionsView.RowHeaderWidth = 164
    TabOrder = 0
    object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
      Properties.Caption = 'Relief'
      Styles.Header = cxStyle1
      object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
        Properties.Caption = 'Relief'
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end
          item
            Caption = 'Info'
          end>
      end
    end
    object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
      object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
      end
    end
    object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
    end
    object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
    end
    object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
    end
  end
  object ActionList1: TActionList
    Left = 104
    Top = 272
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 40
    Top = 152
    object cxStyle1: TcxStyle
      AssignedValues = [svTextColor]
      TextColor = clBlack
    end
  end
end
