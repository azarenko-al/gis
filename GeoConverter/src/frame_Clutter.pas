unit frame_Clutter;

interface

uses
  Classes, Controls, Forms,  Dialogs,
  StdCtrls,

  u_const,

  f_Clutters

  , IniFiles, ExtCtrls, Mask  ;

type
  Tframe_Clutter_ = class(TFrame)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Bevel2: TBevel;
    procedure Button1Click(Sender: TObject);
    procedure ComboEdit1ButtonClick(Sender: TObject);
    procedure JvComboEdit1ButtonClick(Sender: TObject);
  private
    procedure SelectDlg;

  public
    function GetSection: string;
    procedure Init;
    //
    procedure LoadFromIniFile(aIniFile: TIniFile; aSection, aFileName: String);
    procedure SaveToIniFile(aIniFile: TIniFile; aSection, aFileName: String);

  end;

implementation
{$R *.dfm}


procedure Tframe_Clutter_.Init;
begin
  Height := 50;

  Label1.Caption := STR_Clutter_setup;

end;

// ---------------------------------------------------------------
procedure Tframe_Clutter_.SelectDlg;
// ---------------------------------------------------------------
var
  s: string;
begin
  s:=Edit1.Text;

  if Tfrm_Clutters.ExecDlg(s) then
  begin
    Edit1.Text :=s;
   // ComboEdit1.Text :=s;
  end;
end;


procedure Tframe_Clutter_.Button1Click(Sender: TObject);
begin
  SelectDlg;
end;

procedure Tframe_Clutter_.ComboEdit1ButtonClick(Sender: TObject);
begin
  SelectDlg;
end;

function Tframe_Clutter_.GetSection: string;
begin
  Result := Edit1.Text;
end;

procedure Tframe_Clutter_.JvComboEdit1ButtonClick(Sender: TObject);
begin
  SelectDlg;
end;

// ---------------------------------------------------------------
procedure Tframe_Clutter_.LoadFromIniFile(aIniFile: TIniFile; aSection,
    aFileName: String);
// ---------------------------------------------------------------
begin
//  with TIniFile.Create(aFileName) do
  with aIniFile do
  begin
    Edit1.Text := ReadString(aSection, Edit1.Name, Edit1.Text);
  //  ed_Step1.text    := ReadString(aSection, ed_Step1.Name, ed_Step1.text);


   // ComboEdit1.Text:= Edit1.Text;
   // Free;
  end;
end;

// ---------------------------------------------------------------
procedure Tframe_Clutter_.SaveToIniFile(aIniFile: TIniFile; aSection,
    aFileName: String);
// ---------------------------------------------------------------
begin
//  with TIniFile.Create(aFileName) do
  with aIniFile do
  begin
    WriteString(aSection, Edit1.Name, Edit1.Text);
 //  WriteString(aSection, ed_Step1.Name, ed_Step1.text);

  //  Free;
  end;
end;


end.
