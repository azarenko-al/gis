inherited frame_VM_GRD_to_RLF: Tframe_VM_GRD_to_RLF
  Left = 1323
  Top = 826
  Caption = 'frame_VM_GRD_to_RLF'
  ClientHeight = 590
  ClientWidth = 657
  OnDestroy = FormDestroy
  ExplicitLeft = 1323
  ExplicitTop = 826
  ExplicitWidth = 665
  ExplicitHeight = 618
  PixelsPerInch = 96
  TextHeight = 13
  inherited ProgressBar1: TProgressBar
    Top = 574
    Width = 657
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ExplicitTop = 789
    ExplicitWidth = 657
  end
  object GroupBox2: TGroupBox [1]
    Left = 0
    Top = 112
    Width = 657
    Height = 305
    Align = alTop
    TabOrder = 1
    ExplicitTop = 249
    DesignSize = (
      657
      305)
    object Label2: TLabel
      Left = 5
      Top = 64
      Width = 32
      Height = 13
      Caption = #1055#1072#1087#1082#1072
    end
    object Label3: TLabel
      Left = 5
      Top = 144
      Width = 32
      Height = 13
      Caption = #1055#1072#1087#1082#1072
    end
    object Label4: TLabel
      Left = 13
      Top = 224
      Width = 32
      Height = 13
      Caption = #1055#1072#1087#1082#1072
    end
    object ed_Clutter_Classes: TFilenameEdit
      Left = 5
      Top = 34
      Width = 570
      Height = 21
      Filter = 'grc (*.grc)|*.grc'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = ''
    end
    object ed_Clutter_Heights: TFilenameEdit
      Left = 5
      Top = 112
      Width = 570
      Height = 21
      Filter = 'grc (*.grc), grd (*.grd)|*.grc;*.grd'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 1
      Text = ''
    end
    object cb_Clutter_Classes: TCheckBox
      Left = 8
      Top = 14
      Width = 400
      Height = 17
      Caption = 'Clutter_Classes'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object cb_Clutter_Heights: TCheckBox
      Left = 8
      Top = 94
      Width = 400
      Height = 17
      Caption = 'Clutter_Heights'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    inline frame_Clutter_1: Tframe_Clutter_
      Left = 2
      Top = 253
      Width = 653
      Height = 50
      Align = alBottom
      Constraints.MaxHeight = 50
      TabOrder = 4
      ExplicitLeft = 2
      ExplicitTop = 253
      ExplicitWidth = 653
      inherited Label1: TLabel
        Width = 144
        ExplicitWidth = 144
      end
      inherited Bevel2: TBevel
        Width = 653
        ExplicitWidth = 654
      end
    end
    object Button2: TButton
      Left = 590
      Top = 31
      Width = 57
      Height = 25
      Action = act_Show_Clutter_Classes_file_Info
      Anchors = [akTop, akRight]
      TabOrder = 5
    end
    object Button3: TButton
      Left = 590
      Top = 101
      Width = 57
      Height = 25
      Action = act_Show_Clutter_H_file_Info
      Anchors = [akTop, akRight]
      TabOrder = 6
    end
    object cb_building_heights: TCheckBox
      Left = 8
      Top = 175
      Width = 300
      Height = 17
      Caption = #1042#1099#1089#1086#1090#1099' '#1089#1090#1088#1086#1077#1085#1080#1081
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object ed_Build_Heights: TFilenameEdit
      Left = 8
      Top = 195
      Width = 567
      Height = 21
      DefaultExt = '*.bil'
      Filter = 'grc (*.grc), grd (*.grd)|*.grc;*.grd'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 8
      Text = ''
    end
    object Button5: TButton
      Left = 590
      Top = 141
      Width = 57
      Height = 25
      Action = act_Show_Build_Heights_file_Info
      Anchors = [akTop, akRight]
      TabOrder = 9
    end
    object ed_Folder_Clutter_Classes: TDirectoryEdit
      Left = 50
      Top = 58
      Width = 525
      Height = 21
      NumGlyphs = 1
      TabOrder = 10
      Text = ''
    end
    object ed_Folder_Clutter_Heights: TDirectoryEdit
      Left = 50
      Top = 138
      Width = 525
      Height = 21
      NumGlyphs = 1
      TabOrder = 11
      Text = ''
    end
    object ed_Folder_Build_Heights: TDirectoryEdit
      Left = 58
      Top = 219
      Width = 519
      Height = 21
      NumGlyphs = 1
      TabOrder = 12
      Text = ''
    end
  end
  inline frm_RLF1: Tframe_RLF_ [2]
    Left = 0
    Top = 498
    Width = 657
    Height = 76
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 672
    ExplicitWidth = 657
    ExplicitHeight = 76
    DesignSize = (
      657
      76)
    inherited lb_File: TLabel
      Width = 49
      ExplicitWidth = 49
    end
    inherited lb_Step: TLabel
      Width = 34
      ExplicitWidth = 34
    end
    inherited Bevel2: TBevel
      Width = 657
      ExplicitWidth = 657
    end
    inherited ed_RLF: TFilenameEdit
      Width = 642
      ExplicitWidth = 642
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 657
    Height = 112
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 137
    DesignSize = (
      657
      112)
    object lb_Relief: TLabel
      Left = 5
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Relief'
    end
    object Label1: TLabel
      Left = 5
      Top = 56
      Width = 32
      Height = 13
      Caption = #1055#1072#1087#1082#1072
    end
    object ed_DEM: TFilenameEdit
      Left = 5
      Top = 24
      Width = 570
      Height = 21
      Filter = 'grd (*.grd)|*.grd'
      FilterIndex = 2
      Anchors = [akLeft, akTop, akRight]
      NumGlyphs = 1
      TabOrder = 0
      Text = ''
    end
    object Button4: TButton
      Left = 590
      Top = 20
      Width = 57
      Height = 25
      Action = act_Show_Relief_file_Info
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object cb_UseDifferentFiles: TCheckBox
      Left = 8
      Top = 79
      Width = 377
      Height = 17
      Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' RLF '#1076#1083#1103' '#1082#1072#1078#1076#1086#1075#1086' '#1092#1072#1081#1083#1072' '#1080#1079' index.txt'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object ed_Folder_Dem: TDirectoryEdit
      Left = 50
      Top = 48
      Width = 525
      Height = 21
      NumGlyphs = 1
      TabOrder = 3
      Text = ''
    end
  end
  inherited ActionList_custom: TActionList
    Left = 504
    Top = 440
  end
  object ActionList1: TActionList
    Left = 400
    Top = 440
    object act_Show_Clutter_Classes_file_Info: TAction
      Caption = 'Info'
      OnExecute = act_Select_common_FolderExecute
    end
    object act_Show_Clutter_H_file_Info: TAction
      Caption = 'Info'
      OnExecute = act_Select_common_FolderExecute
    end
    object act_Show_Relief_file_Info: TAction
      Caption = 'Info'
      OnExecute = act_Select_common_FolderExecute
    end
    object act_Show_Build_Heights_file_Info: TAction
      Caption = 'Info'
      OnExecute = act_Select_common_FolderExecute
    end
    object FileOpen_beeline: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.Filter = 'grd (*.grd)|*.grd'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_beelineAccept
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 144
    Top = 448
  end
end
