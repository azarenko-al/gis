unit fr_VM_GRD_to_RLF;

interface

uses
  SysUtils, Classes, rxToolEdit, Controls, Forms,
  StdCtrls, ExtCtrls, ComCtrls,IniFiles,

  u_const,

  VM_Grids,

  u_VM_GRD_to_rlf,
     

  frame_RLF,
  frame_Clutter,

  fr_Custom_Form,

  Mask, ActnList, Dialogs, 
  Vcl.StdActns,
  cxVGrid, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxEdit, cxButtonEdit, cxInplaceContainer, System.Actions, RxPlacemnt
  ;

type
  Tframe_VM_GRD_to_RLF = class(Tfrm_Custom_Form)
    GroupBox2: TGroupBox;
    ed_Clutter_Classes: TFilenameEdit;
    ed_Clutter_Heights: TFilenameEdit;
    cb_Clutter_Classes: TCheckBox;
    cb_Clutter_Heights: TCheckBox;
    frm_RLF1: Tframe_RLF_;
    Panel2: TPanel;
    lb_Relief: TLabel;
    ed_DEM: TFilenameEdit;
    frame_Clutter_1: Tframe_Clutter_;
    ActionList1: TActionList;
    Button2: TButton;
    act_Show_Clutter_Classes_file_Info: TAction;
    Button3: TButton;
    act_Show_Clutter_H_file_Info: TAction;
    Button4: TButton;
    act_Show_Relief_file_Info: TAction;
    cb_building_heights: TCheckBox;
    ed_Build_Heights: TFilenameEdit;
    Button5: TButton;
    act_Show_Build_Heights_file_Info: TAction;
    cb_UseDifferentFiles: TCheckBox;
    ed_Folder_Dem: TDirectoryEdit;
    ed_Folder_Clutter_Classes: TDirectoryEdit;
    ed_Folder_Clutter_Heights: TDirectoryEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ed_Folder_Build_Heights: TDirectoryEdit;
    Label4: TLabel;
    FileOpen_beeline: TFileOpen;
    FormStorage1: TFormStorage;
    procedure act_Select_common_FolderExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FileOpen_beelineAccept(Sender: TObject);
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
  private
  //  FTerminated : Boolean;

    FIniFileName: string;


    procedure ProcessFile(aFileName: string);
    procedure SaveToIniFile(aFileName: String);
  //  procedure Select_common_Folder;

    
  protected
    procedure Run; override;
  public
    procedure LoadFromIniFile(aFileName: String);

  end;

var
  frame_VM_GRD_to_RLF: Tframe_VM_GRD_to_RLF;

implementation

uses
  System.Types, System.IOUtils;
{$R *.dfm}

const
  DEF_SECTION = 'VM_GRD_to_RLF1111';

   {
// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.Select_common_Folder;
// ---------------------------------------------------------------
var
  sDir: string;
   
  oSList: TStringList;
begin

  if JvBrowseForFolderDialog1.Execute then
  begin
    sDir:= IncludeTrailingBackslash( JvBrowseForFolderDialog1.Directory);

//    sDir:= IncludeTrailingBackslash( aFolder);

   // sDir:= IncludeTrailingBackslash( PBFolderDialog1.Folder);

   // ScanDir();

    ed_DEM.FileName             :=sDir + 'Height\';
    ed_Clutter_Classes.FileName :=sDir + 'Clutter\';
    ed_Build_Heights.FileName   :=sDir + 'buildings\';

    oSList:=TStringList.Create;

    ScanDir1(sDir + 'Height\', '*.grd', oSList);
    if oSList.Count>0 then
      ed_DEM.FileName := oSList[0];

    oSList.Clear;
    ScanDir1(sDir + 'Clutter\', '*.grc', oSList);
    if oSList.Count>0 then
      ed_Clutter_Classes.FileName := oSList[0];

    oSList.Clear;
    ScanDir1(sDir + 'buildings\', '*.grd', oSList);
    if oSList.Count>0 then
      ed_Build_Heights.FileName := oSList[0];


    FreeAndNil(oSList);

  end;

end;

}      

// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.act_Select_common_FolderExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
//  if// Sender=act_Select_common_Folder then
//    Select_common_Folder else

  if Sender=act_Show_Relief_file_Info then
    TvmGRID.Dlg_ShowInfo(ed_DEM.FileName) else

  if Sender=act_Show_Clutter_Classes_file_Info then
    TvmGRID.Dlg_ShowInfo(ed_Clutter_Classes.FileName) else

  if Sender=act_Show_Clutter_H_file_Info then
    TvmGRID.Dlg_ShowInfo(ed_Clutter_Heights.FileName) else

  if Sender=act_Show_Build_Heights_file_Info then
    TvmGRID.Dlg_ShowInfo(ed_Build_Heights.FileName) else

end;

procedure Tframe_VM_GRD_to_RLF.Button2Click(Sender: TObject);
begin

 // TvmGRID.Dlg_ShowClassificatorList(ed_Clutter_Classes.FileName);

end;


procedure Tframe_VM_GRD_to_RLF.FileOpen_beelineAccept(Sender: TObject);
var
   sFileName: string;
begin
  sFileName:=TFileOpen(Sender).Dialog.FileName;

  ProcessFile (sFileName);
end;

// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.ProcessFile(aFileName: string);
// ---------------------------------------------------------------
var
  arFiles: TStringDynArray;    //  TStringDynArray       = array of string;
  sDir: string;  
 // sFileName: string;

begin
//  sFileName:=TFileOpen(Sender).Dialog.FileName;

//  ShowMessage( sFileName );

  sDir:=ExtractFilePath(aFileName);
//  TFileOpen(Sender).Dialog.FileName
  
  ed_DEM.FileName:=aFileName;
//  row_DEM.Properties.Value:=aFileName;

  frm_RLF1.ed_RLF.FileName:= ChangeFileExt(aFileName,'.rlf');

  
  arFiles:=TDirectory.GetFiles( sDir, '*.grc'); //, TSearchOption.soTopDirectoryOnly);

  if Length(arFiles)>0 then
  begin  
    ed_Clutter_Classes.FileName:=arFiles[0];

//    row_Clu.Properties.Value:=arFiles[0];  
  end;    

  
  
  
//  TFile.
                              
//  ShowMessage('FileOpen1Accept');

//  FileOpen
  
end;



// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;


  FIniFileName := ChangeFileExt(Application.ExeName, '.ini');

 // TdmConfig.Init;

//  DBLookupComboBox1.ListSource:=dmConfig.ds_Clutter_schema;
 // DBLookupComboBox1.KeyValue  :=dmConfig.ds_Clutter_schema.DataSet.FieldByName('Name').AsString;


  // ---------------------------------------------------------------
  lb_Relief.Caption  := STR_RELIEf;
  cb_Clutter_Classes.Caption  := STR_Clutter_Classes;
  cb_Clutter_Heights.Caption  := STR_Clutter_Heights;

  frm_RLF1.Init;
  frame_Clutter_1.Init;



  LoadFromIniFile(FIniFileName);


end;

// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.FormDestroy(Sender: TObject);
// --------------------------------------------------S------------
begin
  SaveToIniFile(FIniFileName);

end;

// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.LoadFromIniFile(aFileName: String);
// ---------------------------------------------------------------

var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do

 // with TIniFile.Create(FIniFileName) do
  begin
    ed_DEM.FileName             := ReadString(DEF_SECTION, ed_DEM.Name, ed_DEM.FileName);
  //  ed_INI_section.Text     := ReadString(DEF_SECTION, ed_INI_section.Name, ed_INI_section.Text);

    cb_Clutter_Classes.Checked := ReadBool(DEF_SECTION, cb_Clutter_Classes.Name, cb_Clutter_Classes.checked);
    cb_Clutter_Heights.Checked := ReadBool(DEF_SECTION, cb_Clutter_Heights.Name, cb_Clutter_Heights.checked);
    cb_building_heights.checked:= ReadBool(DEF_SECTION, cb_building_heights.Name, cb_building_heights.checked);

    ed_Clutter_Classes.FileName := ReadString(DEF_SECTION, ed_Clutter_Classes.Name,ed_Clutter_Classes.FileName);
    ed_Clutter_Heights.FileName := ReadString(DEF_SECTION, ed_Clutter_Heights.Name,ed_Clutter_Heights.FileName);


    ed_Folder_Dem.Text := ReadString(DEF_SECTION, ed_Folder_Dem.Name, '');
    ed_Folder_Clutter_Classes.Text := ReadString(DEF_SECTION, ed_Folder_Clutter_Classes.Name, '');
    ed_Folder_Clutter_Heights.Text := ReadString(DEF_SECTION, ed_Folder_Clutter_Heights.Name, '');
    ed_Folder_Build_Heights.Text   := ReadString(DEF_SECTION, ed_Folder_Build_Heights.Name, '');


 //   ed_Clutters_ini.FileName    := ReadString(DEF_SECTION, ed_Clutters_ini.Name,ed_Clutters_ini.FileName);

  //  ed_RLF.FileName  := ReadString(DEF_SECTION, ed_RLF.Name,ed_RLF.FileName);


    ed_Build_Heights.FileName := ReadString(DEF_SECTION, ed_Build_Heights.Name, ed_Build_Heights.FileName);


  //  JvBrowseForFolderDialog1.Directory :=
     //  ReadString(DEF_SECTION, JvBrowseForFolderDialog1.Name, JvBrowseForFolderDialog1.Directory);


  //  Free;
  end;

  frm_RLF1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.LoadFromIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);

end;

procedure Tframe_VM_GRD_to_RLF.Panel1Click(Sender: TObject);
begin

end;

// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.SaveToIniFile(aFileName: String);
// ---------------------------------------------------------------

var
  oIniFile: TIniFile;
begin
  oIniFile:=TIniFile.Create(aFileName);

  with oIniFile do

 // with TIniFile.Create(FIniFileName) do
  begin
    WriteString(DEF_SECTION, ed_DEM.Name,             ed_DEM.FileName);
    WriteString(DEF_SECTION, ed_Clutter_Classes.Name, ed_Clutter_Classes.FileName);
    WriteString(DEF_SECTION, ed_Clutter_Heights.Name, ed_Clutter_Heights.FileName);
    WriteString(DEF_SECTION, ed_Build_Heights.Name,   ed_Build_Heights.FileName);


  //  WriteString(DEF_SECTION, ed_Clutters_ini.Name, ed_Clutters_ini.FileName);

  //  WriteString(DEF_SECTION, ed_RLF.Name, ed_RLF.FileName);

    WriteBool(DEF_SECTION, cb_Clutter_Classes.Name,  cb_Clutter_Classes.checked);
    WriteBool(DEF_SECTION, cb_Clutter_Heights.Name,  cb_Clutter_Heights.checked);
    WriteBool(DEF_SECTION, cb_building_heights.Name, cb_building_heights.checked);

    WriteString(DEF_SECTION, ed_Folder_Dem.Name,             ed_Folder_Dem.Text);
    WriteString(DEF_SECTION, ed_Folder_Clutter_Classes.Name, ed_Folder_Clutter_Classes.Text);
    WriteString(DEF_SECTION, ed_Folder_Clutter_Heights.Name, ed_Folder_Clutter_Heights.Text);
    WriteString(DEF_SECTION, ed_Folder_Build_Heights.Name,   ed_Folder_Build_Heights.Text);


//    WriteBool(DEF_SECTION, cb_Clutter_building_heights.Name, cb_Clutter_building_heights.checked);

  //  WriteString(DEF_SECTION, JvBrowseForFolderDialog1.Name, JvBrowseForFolderDialog1.Directory );

   // Free;
  end;

  frm_RLF1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);
  frame_Clutter_1.SaveToIniFile(oIniFile, DEF_SECTION, aFileName);

  FreeAndNil(oIniFile);


end;

// ---------------------------------------------------------------
procedure Tframe_VM_GRD_to_RLF.Run;
// ---------------------------------------------------------------
var
  iValue: Integer;

    FVM_GRD_to_rlf: TVM_GRD_to_rlf;
  
begin
  FTerminated := False;

  //btn_Run.Action := act_Stop;


  FVM_GRD_to_rlf := TVM_GRD_to_rlf.Create();
  FVM_GRD_to_rlf.OnProgress :=DoOnProgress;

  
//  if cb_Clutter_Classes.Checked then
//  else
 //   FVM_GRD_to_rlf.Params.Clutter_Classes_FileName := '';

  FVM_GRD_to_rlf.Params.IsUse_Clutter_Classes := cb_Clutter_Classes.Checked;
  FVM_GRD_to_rlf.Params.IsUse_Clutter_Heights := cb_Clutter_heights.Checked;
  FVM_GRD_to_rlf.Params.IsUse_Build_Heights    := cb_building_heights.Checked;

//  if cb_Clutter_Heights.Checked then
  FVM_GRD_to_rlf.Params.Dem_FileName := ed_DEM.FileName;
  FVM_GRD_to_rlf.Params.Clutter_Classes_FileName := ed_Clutter_Classes.FileName;
  FVM_GRD_to_rlf.Params.Clutter_Heights_FileName := ed_Clutter_Heights.FileName;

  FVM_GRD_to_rlf.Params.Build_Heights_FileName   := ed_Build_Heights.FileName;


  FVM_GRD_to_rlf.Params.Folder_Dem               := ed_Folder_Dem.Text;
  FVM_GRD_to_rlf.Params.Folder_Clutter_Classes   := ed_Folder_Clutter_Classes.Text;
  FVM_GRD_to_rlf.Params.Folder_Clutter_Heights   := ed_Folder_Clutter_Heights.Text;
  FVM_GRD_to_rlf.Params.Folder_Build_Heights     := ed_Folder_Build_Heights.Text;

//  FVM_GRD_to_rlf.Params.Build_Heights_FileName   := ed_Build_Heights.FileName;



//  else
 //   FVM_GRD_to_rlf.Params.Clutter_Heights_FileName :='';

 // FVM_GRD_to_rlf.Params.Clutters_ini_fileName    := ed_Clutters_ini.FileName;
  FVM_GRD_to_rlf.Params.Clutters_section := frame_Clutter_1.GetSection;


  FVM_GRD_to_rlf.Params.Rlf_FileName1 := frm_RLF1.ed_RLF.FileName;
  FVM_GRD_to_rlf.Params.Rlf_Step     := frm_RLF1.GetStepM;

  FVM_GRD_to_rlf.Run;

//  btn_Run.Action := act_Run;
  ProgressBar1.Position :=0;


  FreeAndNil(FVM_GRD_to_rlf);
  
end;


end.
