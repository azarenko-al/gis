unit VM_Grids;

interface
uses
  Classes, SysUtils, strutils, IniFiles,  Dialogs, Math,

  System.IOUtils,
  
//  u_BufferedFileStream,

  u_data_classes,

  u_geo,

  u_mapinfo_TAB_file
  ;


type
  TColorInfoRec = packed record
    Inflection :Single;
    R          :Byte;
    G          :Byte;
    B          :Byte;
  end;

  TClsColorRec = packed record
    Buff_in  :Byte;
    R        :Byte;
    G        :Byte;
    B        :Byte;
    Buff_Out :Byte;
  end;

  PClassificator=^TClassificatorRec;

  TClassificatorRec = packed record
    ClsNum  : Word;
    ClrInfo : TClsColorRec;
    NameLen : Word;
    ClsName : AnsiString;
  end;

//  TXYBounds_MinMax = record
//              MinLat  : Double;
//              MaxLat  : Double;
//              MinLon  : Double;
//              MaxLon  : Double;
//            end;

  TGRIDHeader=string[5];

  // ---------------------------------------------------------------
  TvmGRID=Class (TMatrixDataSourceBase)
  // ---------------------------------------------------------------
  Private
   FFilename : string;

   FIsClassify: boolean; //'Character','Numeric';
       // Trim(FHeader_str) = 'HGPC8';


   FHeader_str   :AnsiString;
   FVersion      :Single;
   FMinZ         :Single;
   FMaxZ         :Single;
   FDesc         :AnsiString;

   FUnits_str    :AnsiString;
   FCoordSys_str :AnsiString;
   FUnitCode     :Byte;
   FDisplayFlag  :Byte;

   FColorCount   :Word;

   FColorTableArr: Array of TColorInfoRec;

   FShadingAzimuth     : single;
   FShadinginclination : Single;
   FValueSizeType      : byte;
   FBytesPerPoint      : byte;

   FReadData: Boolean;//������� ����, ��� ������ ���� ������� �� �����


   FRawData:  Array of ShortInt;
   FRawData2: Array of SmallInt;

   FClsList:TList;


//   FBufferStream:TStream;//��������� ����� ��� ������ ������

   FScale: Integer;
   FNormalize:boolean;

   FNullValue: smallint;//Single;//RawData ������������� ������������ Z

   FFileStream: TFileStream;
//   FFileStream: TReadOnlyCachedFileStream;


   function GetRawData(aIndex: Integer): Word;
   function GetNumericData(aIndex:Integer): double;

   Procedure SetData(index:Integer;Value:Word);
   procedure GenerateTabFile(aFileName, aGridType: string);
   procedure SetNumericData(aIndex:Integer; const Value: double);

  private
   function GetGridtype: String;
//   procedure GetCoordByIndex11111(aIndex: Integer; var aX, aY: Double);


   procedure SetValueSizeType(aValue: Byte);
   Procedure SetColors(Value:Word);

//   function GetGridtype: String;

   function GetIndexByCoord(aX, aY: Double; var aRow, aCol: Integer): boolean;
   function GetXYBounds_UTM: TXYRect; override;
   function GetFileName: string; override;


// DONE: GetValueByIndex1111
// function GetValueByIndex1111(aIndex: Integer; var aValue: Double): Boolean;
// TODO: PrepareBounds
// procedure PrepareBounds;

   procedure ReadHeader(aStream: TStream; var aCR: Int64);

   procedure ReadClassificator(aStream: TStream; var aCR: Int64);
//   procedure ReadFile_______(aFileName: String);
   function ReadTabFile(aFileName: String): String;
   procedure WriteToFile(aFileName: String);
//   procedure SetScale1111(const Value: Integer);

  published
//   XYBounds_MinMax: TXYBounds_MinMax;

  public
  //  XYBounds_MinMax: TXYBounds_MinMax;

//   XYBounds_MinMax: TXYBounds_MinMax;
//   XYBounds_MinMax: TXYBounds_MinMax;

   MinX  : Double;
   MaxX  : Double;
   MinY  : Double;
   MaxY  : Double;


   CoordSysRec: TCoordSysRec;

   ClassificatorArr: array of TClassificatorRec;

   NCols : Word;
   NRows : Word;

   StepX: Double;
   StepY: Double;

//   IsSouth: Boolean;
   UTM_Zone: Integer;

   constructor Create;
   destructor Destroy; override;

   function OpenFile(aFilename: String): Boolean;
   procedure CloseFile;

     //

 //  procedure Write(aFileName: String);

   procedure SaveToTxtFile(aFileName: String);

   function GetValueByLatLon(aLat, aLon: Double; var aValue: Double): Boolean;
       override;

   function GetMinCellSize: Double; override;
   function GetZoneUTM: Integer; override;


   function GetIndexByCoordLatLon(aLat, aLon: Double; var aRow, aCol: Integer):
       boolean;

   function GetValueByRowCol(aRow,aCol: Integer; var aValue: Double): Boolean;

   procedure SaveToBin(aFileName: string);

   class procedure Dlg_ShowInfo(aFileName: String);

   class procedure ExtractClassificatorList(aFileName: String; var aList:
       TStringList; var aInfo: string);

   function GetXYBounds_MinMax: TXYBounds_MinMax;
   
   procedure SaveHDR(aFileName: string; aZone: integer);



 //  Property Colors:Word       Read FColorCount write SetColors;

   Property RawData[Index:Integer]:word Read GetRawData Write SetData;//������ ��� �������� � �����

   property NumericData[Index:Integer]: double read GetNumericData write
       SetNumericData;
                                                                                      //������ ��������� � RawData �� ������������!!!
 //  Property Value[Index:Integer]:Double Read GetValueByIndex1111; //������, ��������� �� ����� � ���������������  � �������� (�� ��������� �����)

//   Property GridType: String Read GetGridtype;

//   property Scale1111: Integer read FScale1111 write SetScale1111;

   Property NullValue: smallint Read FNullValue;// Write FNullValue;
  end;



implementation

Const
  ONE_BYTE=2;
  TWO_BYTE=0;
  HEADER_SIZE=1024;

Const
  CRLF = #13#10;



// ---------------------------------------------------------------
constructor TvmGRID.Create;
// ---------------------------------------------------------------
begin
  SetLength(FHeader_str,5); //5
  FillChar(FHeader_str[1],5,#0);

  SetLength(FDesc,32);
  FillChar(FDesc[1],32,#0);

  SetLength(FUnits_str,163);
  Fillchar(FUnits_str[1],163,#0);

  SetLength(FCoordSys_str,256);
  FillChar(FCoordSys_str[1],256,#0);

  FClsList:=TList.Create;

//  FIsClassify:=false;
//  FReadData:=False;

//  FFileStream:=nil;

  FBytesPerPoint:=2;
  FScale:=1;

//  FBufferStream:=nil;
//  FNormalize:=False;

  FNullValue:=-9999;
end;


destructor TvmGRID.Destroy;
var
  i:Integer;
begin
  CloseFile;

  For i:=0 to FClsList.Count-1 do
    Dispose(FClsList[i]);

  FClsList.Free;
  
  inherited;
end;

procedure TvmGRID.CloseFile;
begin
  if Assigned(FFileStream) then
    FreeAndNil(FFileStream);
end;


//-------------------------------------------------------------------
procedure TvmGRID.GenerateTabFile(aFileName, aGridType: string);
//-------------------------------------------------------------------
const
  DEF_Tab_Template=
      '!table' + CRLF +
      '!Version 300'+CRLF+
      '!charset WindowsLatin1'+CRLF+CRLF+
      'Definition Table'+CRLF+
      'File "%s"'+#13#10+//��� �����
      'Type "RASTER"'+#13#10+
      '(%1.5f,%1.5f) (0,0) Label "Pt 1",'  +#13#10+ //MinX mMaxY
      '(%1.5f,%1.5f) (%d,%d) Label "Pt 2",'+#13#10+ //MaxX MinY NRows NCols
      '(%1.5f,%1.5f) (0,%d) Label "Pt 3"'  +#13#10+ //MinX MinY NCols
      '%s'+ #13#10+ //Coordsys;
      'UNITS "%s"'+#13#10#13#10+ //Units
      'ReadOnly'+#13#10+ //ReadOnly
      'begin_metadata'+#13#10+ //Metadata
      '"\Vm\Grid" = "%s"'+#13#10+
      '"\IsReadOnly" = "FALSE"'+#13#10#13#10+
      'end_metadata';

var F:TextFile;
    S:String;
   // FS_: TFormatSettings;
    SFN,FNTab:String;
begin
//  FS_.DecimalSeparator:='.';
  FormatSettings.DecimalSeparator:='.';

  sFN:=ExtractFileName(aFileName);
  FNTab:=ChangeFileExt(aFileName,'.Tab');

  AssignFile(F,FNTab);
  Rewrite(F);
  try
    S:=Format(DEF_Tab_Template,[sFN,
                                 MinX, MaxY,
                                 MaxX, MinY,
                                 NCols, NRows,
                                 MinX, MinY,
                                 NRows,
                                 FCoordSys_str,'m',aGridType]);
    Writeln(F,S);
  finally
    System.CloseFile(F)
  end;
end;


function TvmGRID.GetGridtype: String;
begin
  if FIsClassify then
    result:='Character'
  else
    result:='Numeric';
end;


function TvmGRID.GetIndexByCoordLatLon(aLat, aLon: Double; var aRow, aCol:
    Integer): boolean;
begin
  result := GetIndexByCoord(aLon,aLat,  aRow,aCol);
end;


//---------------------------------------------------------------------
function TvmGRID.GetIndexByCoord(aX, aY: Double; var aRow, aCol: Integer):
    boolean;
//--------------------------------------------sma-------------------------
//var
 // r,c: Integer;
var
  iIndex: Integer;
begin
  // r:=-1;
  // c:=-1;

   if (aX >= MinX) and (aX <= MaxX) and
      (aY >= MinY) and (aY <= MaxY)
   then begin
   //  if StepY>0 then
   //    r:=Round((MaxY-aY)/StepY);  //��� ���� ��������� ������, � ���������� �����

   //  aIndex:=r * NCols+c;



     if StepX>0 then
       aCol:=Round((aX-MinX)/StepX);

     if StepY>0 then
       aRow:=Round((MaxY-aY)/StepY);  //��� ���� ��������� ������, � ���������� �����

     iIndex:=aRow * NCols + aCol;

     result:=(aCol>=0) and (aRow>=0) and (iIndex>=0) and (iIndex < NCols * NRows);

  end else
     Result := False;
//     Exit;
 //  end;


end;


//-------------------------------------------------------------------
function TvmGRID.GetRawData(aIndex: Integer): Word;
//-------------------------------------------------------------------
begin
  Result:=0;

  if (FValueSizeType=ONE_BYTE) and (aIndex<=High(FRawData)) Then
     Result:=FRawData[aIndex];

  if (FValueSizeType=TWO_BYTE) and (aIndex<=High(FRawData2)) then
     Result:=FrawData2[aIndex];
     
end;


//---------------------------------------------------------------------
function TvmGRID.GetNumericData(aIndex:Integer): double;
//---------------------------------------------------------------------
Var V:Integer;
    range:double;
    kf:word;
begin
  result:=FNullValue;

 // result:=-9999;

  if FIsClassify then
  begin
     result:=RawData[aIndex];
  end
  else
  begin
    if FReadData then
    begin
      V:=RawData[aIndex];
      range:=FMaxZ-FMinZ;

      Case FValueSizeType of
        ONE_BYTE: kf:=$FF;
        TWO_BYTE: kf:=$FFFF;
      else
          kf:=$FFFF;
      end;

      if v<>0 then
        result:=(v-1)/kf*range+FMinZ
      else
        result:=-9999;//�� �������� �������

    end;
  end;
end;

// ---------------------------------------------------------------
function TvmGRID.GetValueByLatLon(aLat, aLon: Double; var aValue: Double):
    Boolean;
// ---------------------------------------------------------------
var
  r,c: Integer;    
begin
  Result := GetIndexByCoordLatLon(aLat, aLon, r, c);
  if Result then
  begin
    Result := GetValueByRowCol(r,c, aValue);
    if Result then
      Result := (aValue <> NullValue);
  end;

end;




//---------------------------------------------------------------------
function TvmGRID.GetValueByRowCol(aRow,aCol: Integer; var aValue: Double):
    Boolean;
//---------------------------------------------------------------------
var
  iIndex: Integer;
  w: Word;
  koef: Word;
  eScale: double;
begin
  Assert(Assigned(FFileStream), 'Value not assigned');

  Result := False;

//  Result:=FNullValue;
//  w:=0;

  iIndex := aRow * NCols + aCol;


  if (iIndex<NCols*NRows) then
//  if Assigned(FFileStream) And (iIndex<NCols*NRows) then
  begin
    FFileStream.Seek(HEADER_SIZE + iIndex*FBytesPerPoint, soFromBeginning);
    FFileStream.Read(w, FBytesPerPoint);

    if w=0 then
    begin
       aValue:=FNullValue;

  //     result:=FNullValue;
       Exit (False);
       //��� ��������, Null
    end;

    if not FIsClassify then //Numeric
    Begin
      if FBytesPerPoint=2 then
        koef:=$FFFF
      else
        koef:=$FF;

      w:=w-1;//������� �� ����.
      eScale:=w/koef;

      aValue:=(FMaxZ-FMinZ)*eScale + FMinZ;
    end
    else //classify
    begin
      if FBytesPerPoint=2 then
        aValue:=w
      else
        aValue:=w;
    end;

    result:=True;
  end;
end;

//-------------------------------------------------------------------
procedure TvmGRID.ReadHeader(aStream: TStream; var aCR: Int64);
//-------------------------------------------------------------------
var
  i: Integer;
  j: Integer;
  s: string;

begin
  aCR := 0;
  if aStream.Size > 1024 then
  begin

    aCR := aCR + aStream.Read(FHeader_str[1], 5);
    aCR := aCR + aStream.Read(FVersion, SizeOf(Single));
    aCR := aCR + aStream.read(NCols,  SizeOf(Word));
    aCR := aCR + aStream.read(NRows,  SizeOf(Word));
    aCR := aCR + aStream.read(MinX,   SizeOf(Double));
    aCR := aCR + aStream.read(MaxX,   SizeOf(Double));
    aCR := aCR + aStream.read(MinY,   SizeOf(Double));
    aCR := aCR + aStream.read(MaxY,   SizeOf(Double));
    aCR := aCR + aStream.read(FMinZ,   SizeOf(Single));
    aCR := aCR + aStream.read(FMaxZ,   SizeOf(Single));

    j:=aStream.Position;   //53    -> 61

    aCR := aStream.Seek(61, soFromBeginning);
    aCR := aCR + aStream.Read(FDesc[1], 32);
    aCR := aCR + aStream.Read(FUnits_str[1], 163);
    aCR := aCR + aStream.Read(FCoordSys_str[1], 256);
    aCR := aCR + aStream.Read(FUnitCode, SizeOf(Byte));
    aCR := aCR + aStream.Read(FDisplayFlag, SizeOf(Byte));
    aCR := aCR + aStream.Read(FColorCount, SizeOf(Word));

    j:=aStream.Position; //516   ->966

    if FColorCount > 0 then
    begin
      SetLength(FColorTableArr, FColorCount);
      aCR := aCR + aStream.Read(FColorTableArr[1], FColorCount * SizeOf(TColorInfoRec));
    end;

    j:=aStream.Position; //516   ->966

    aCR := aStream.Seek(966, soFromBeginning);
    aCR := aCR + aStream.Read(FShadingAzimuth, SizeOf(Single));
    aCR := aCR + aStream.Read(FShadingInclination, SizeOf(Single));

    j:=aStream.Position;   //974

    aCR := aStream.Seek(1023, soFromBeginning);
    aCR := aCR + aStream.Read(FValueSizeType, SizeOf(byte));

    j:=aStream.Position;   //1024

    if FValueSizeType = ONE_BYTE then
      FBytesPerPoint := 1
    else
      FBytesPerPoint := 2;

    FIsClassify := Trim(FHeader_str) = 'HGPC8';
    //���� � �����
    //aCR := aStream.Seek(1024 + FNRows * FNCols * FBytesPerPoint, soFromBeginning);
    //
  end;

  if NCols>0 then StepX:=(MaxX-MinX)/NCols;
  if NRows>0 then StepY:=(MaxY-MinY)/NRows;

 // UTM_Zone :=
  i :=Pos(#0, FCoordSys_str);
  s := Copy(FCoordSys_str, 1, i-1);

  tab_GetCoordSys1 (s, CoordSysRec);

//  IsSouth :=CoordSysRec.IsSouth;
  UTM_Zone:=CoordSysRec.UTM_ZoneNum;

  XYBounds_MinMax.MinLon := MinX;
  XYBounds_MinMax.MaxLon := MaxX;
  XYBounds_MinMax.MinLat := MinY;
  XYBounds_MinMax.MaxLat := MaxY;

//  PrepareBounds();

(*
  XYBounds.MinLon := MinX;
  XYBounds.MaxLon := MaxX;
  XYBounds.MinLat := MinY;
  XYBounds.MaxLat := MaxY;

  if CoordSysRec.IsSouth then
  begin
    XYBounds.MinLat := -XYBounds.MinLat;
    XYBounds.MaxLat := -XYBounds.MaxLat;
  end;

  if CoordSysRec.MeridianDegree<0 then
  begin
    XYBounds.MinLon := -XYBounds.MinLon;
    XYBounds.MaxLon := -XYBounds.MaxLon;
  end;
*)

end;

// ---------------------------------------------------------------
class procedure TvmGRID.Dlg_ShowInfo(aFileName: String);
// ---------------------------------------------------------------
var
  oSList: TStringList;
  sInfo: string;
begin
  oSList:=TStringList.Create;

  TvmGRID.ExtractClassificatorList(aFileName, oSList, sInfo);

  ShowMessage( sInfo + CRLF + CRLF + oSList.Text);

  FreeAndNil(oSList);
end;

// ---------------------------------------------------------------
class procedure TvmGRID.ExtractClassificatorList(aFileName: String; var aList:
    TStringList; var aInfo: string);
// ---------------------------------------------------------------
var
  I: Integer;

  oGRID: TvmGRID;
  s: string;
begin
  Assert(Assigned(aList), 'Value not assigned');


  oGRID:=TvmGRID.Create;

  oGRID.OpenFile (aFileName);

  aInfo := tab_CoordSysRecToStr(oGRID.CoordSysRec);


  s:= Format('Rows: %d, Cols: %d',[oGRID.NRows, oGRID.NCols]);
  

  aInfo:=aInfo + CRLF + s;
  
//  oGRID.
  
//  oGRID.

  for I := 0 to High(oGRID.ClassificatorArr) do
//  begin
    aList.AddObject(oGRID.ClassificatorArr[i].ClsName,
           Pointer( oGRID.ClassificatorArr[i].ClsNum));

//    ClassificatorArr[i].ClsName;

  //  aList.A
 // end;



  oGRID.CloseFile();

  FreeAndNil(oGRID);
end;

function TvmGRID.GetFileName: string;
begin
  Result :=FFileName;
end;

function TvmGRID.GetMinCellSize: Double;
begin
  Result := Math.Min(StepX,StepY);
end;

function TvmGRID.GetZoneUTM: Integer;
begin
  Result := UTM_Zone;
end;


function TvmGRID.GetXYBounds_MinMax: TXYBounds_MinMax;
begin
  Result := XYBounds_MinMax;
end;

// ---------------------------------------------------------------
function TvmGRID.GetXYBounds_UTM: TXYRect;
// ---------------------------------------------------------------
begin
  Result.TopLeft.X      := XYBounds_MinMax.MaxLat;
  Result.TopLeft.Y      := XYBounds_MinMax.MinLon;

  Result.BottomRight.X  := XYBounds_MinMax.MinLat;
  Result.BottomRight.Y  := XYBounds_MinMax.MaxLon;

end;



//-------------------------------------------------------------------
function TvmGRID.OpenFile(aFilename: String): Boolean;
//-------------------------------------------------------------------
Var
  iCR: Int64;
  iCR1: Int64;
  s: string;
//  oStream: TFileStream;
begin
  Result := False;

  FFilename:=aFilename;

  Assert(aFilename<>'');
  Assert(FileExists(aFilename));

  s:=LowerCase(ExtractFileExt(aFilename));

  Assert((s='.grd') or (s='.grc'));


  Assert(not Assigned(FFileStream), 'Assigned(FFileStream)');

  Try
    FFileStream:=TFileStream.Create(aFilename,fmOpenRead or fmShareDenyWrite);

    ReadHeader(FFileStream, iCR);

    iCR1 := FFileStream.Seek(1024 + NRows * NCols * FBytesPerPoint, soFromBeginning);

    //s:=FHeader_str;

    if (iCR1<FFileStream.Size) and (FHeader_str='HGPC8') then
      ReadClassificator(FFileStream,iCR1);//������ �������������

  except
    FreeAndNil(FFileStream);
    Exit;
  end;

  Result := True;
end;


//-------------------------------------------------------------------
function TvmGRID.ReadTabFile(aFileName: String): String;
//-------------------------------------------------------------------
var F:TextFile;
    S:String;
    sPath:String;
    sGridFileName:String;
begin
 Result:='';
 if FileExists(aFileName) then
 begin
   AssignFile(F,aFileName);
   Reset(f);
   
   sGridFileName:='';
   try
     while not eof(F) do
     begin
       Readln(F,S);

       if Trim(AnsiUpperCase(S))=AnsiUpperCase('Definition Table') then
       begin
         while not eof(f) do
         begin
           Readln(F,S);
           S:=Trim(AnsiUpperCase(S));

           if AnsiStartsStr('FILE ',S) then
           begin
             Delete(S,1,5);

               //Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);


             sGridFileName:=Trim(StringReplace(S,'"',' ',[]));
           end;
         end;
       end;
     end;

   finally
     System.CloseFile(f);
   end;

   sPath:=ExtractFilePath(aFileName);
   sGridFileName:=sPath+sGridFileName;

   If FileExists(sGridFileName) then
   begin
     result:=sGridFileName;//ReadFile_______(Grid);
   end
   else
     raise Exception.Create('Grid: '+sGridFileName+' Not Found');
 end
 else
   raise Exception.Create('File: '+aFileName+' Not Found');
   
end;


//-------------------------------------------------------------------
procedure TvmGRID.ReadClassificator(aStream: TStream; var aCR: Int64);
//-------------------------------------------------------------------
var
  iMaxCount: Word;
 // cnt:Integer;
  i: Integer;
  PCls: PClassificator;
begin
  for i:=0 to FClsList.Count-1 do
    Dispose(FClsList[i]);

//     aList.AddObject(oGRID.ClassificatorArr[i].ClsName,
  //         Pointer( oGRID.ClassificatorArr[i].ClsNum));



  FClsList.Clear;
  aCR:=aCR+aStream.Read(iMaxCount,SizeOf(Word));
 // cnt:=0;

  SetLength(ClassificatorArr, iMaxCount);
  i:=0;

  While (aCR<aStream.Size) and (i<iMaxCount) do
  begin
    New(PCls);
    FClsList.Add(Pcls);

    aCR:=aCR+aStream.Read(PCls^.ClsNum,  SizeOf(Word));
    aCR:=aCR+aStream.Read(PCls^.ClrInfo, SizeOf(TClsColorRec));
    aCR:=aCR+aStream.Read(PCls^.NameLen, SizeOf(Word));

    SetLength(PCls^.ClsName,  PCls^.NameLen);
    FillChar(PCls^.ClsName[1],  PCls^.NameLen, #0);

    aCR:=aCR+ aStream.Read(PCls^.ClsName[1],  PCls^.NameLen);

    ClassificatorArr[i]:=PCls^;

    Inc(i);

   // inc(Cnt);
    //PCls^.ClsName:=Trim(PCls^.ClsName);
  end;
end;



//-------------------------------------------------------------------
procedure TvmGRID.SetValueSizeType(aValue: Byte);
//-------------------------------------------------------------------
Begin
  FValueSizeType:=aValue;

  case aValue of
    ONE_BYTE :  begin
                  SetLength(FRawData, NCols*NRows);
                  SetLength(FRawData2,0);
                end;

    TWO_BYTE :  begin
                  SetLength(FRawData2, NCols*NRows);
                  SetLength(FRawData,0);
                end;
  end;
end;


Procedure TvmGRID.SetColors(Value:word);
begin
  FColorCount:=Value;
  SetLength(FColorTableArr, Value);
end;


//-------------------------------------------------------------------
procedure TvmGRID.SetData(index: Integer; Value: Word);
//-------------------------------------------------------------------
begin
  if (FValueSizeType=ONE_BYTE) and (index<=High(FRawData)) Then
     FRawData[index]:=Byte(Value);

  if (FValueSizeType=TWO_BYTE) and (index<=High(FRawData2)) then
     FrawData2[index]:=Value;
end;

//-------------------------------------------------------------------
procedure TvmGRID.SetNumericData(aIndex:Integer; const Value: double);
//-------------------------------------------------------------------
var
  Step:Double;
  w:Word;
begin
  FReadData:=true; //��� ������������ � �������� �������
  FNormalize:=true;

  if FIsClassify then
  begin
     RawData[aIndex]:=Round(Value);
     
  end else
  begin
     if (Value=FNullValue) then w:=0 else
     begin
       Step:=(FMaxZ-FMinZ)/($FFFF-1);
       w:=Word(Round((Value-FMinZ)/step))+1;
     end;

     RawData[aIndex]:=w;
  end;
end;


//-------------------------------------------------------------------
procedure TvmGRID.WriteToFile(aFileName: String);
//-------------------------------------------------------------------
var
  oFS:TFileStream;
  iCount: Integer;
  iCR:Integer;
  eOldValue:Single;
  wNewValue:Word;
  eStep:Single;
  i:integer;

  PCls: PClassificator;

  Nb:Byte;
  
begin
 //todo write
  FReadData:=False;
  NB:=0;

  oFS:=TFileStream.Create(aFileName,fmCreate);
  Try
    iCR:=0;
    iCR:=iCR+oFS.Write(FHeader_str[1],5);
    iCR:=iCR+oFS.Write(FVersion,SizeOf(Single));
    iCR:=iCR+oFS.Write(NCols,SizeOf(Word));
    iCR:=iCR+oFS.Write(NRows,SizeOf(Word));
    iCR:=iCR+oFS.Write(MinX,SizeOf(Double));
    iCR:=iCR+oFS.Write(MaxX,SizeOf(Double));
    iCR:=iCR+oFS.Write(MinY,SizeOf(Double));
    iCR:=iCR+oFS.Write(MaxY,SizeOf(Double));
    //�������� Z ������� 2 ����
    iCR:=iCR+oFS.Write(FMinZ,SizeOf(Single));
    iCR:=iCR+oFS.Write(FMaxZ,SizeOf(Single));
    iCR:=iCR+oFS.Write(FMinZ,SizeOf(Single));
    iCR:=iCR+oFS.Write(FMaxZ,SizeOf(Single));

    While iCR<61 do
    begin
      iCR:=iCR+oFS.Write(Nb,SizeOf(Byte));
    end;

    //CR:=oFS.Seek(61,soFromBeginning);
    iCR:=iCR+oFS.Write(FDesc[1],32);
    iCR:=iCR+oFS.Write(FUnits_str[1],163);
    iCR:=iCR+oFS.Write(FCoordSys_str[1],256);
    iCR:=iCR+oFS.Write(FUnitCode,SizeOf(Byte));
    iCR:=iCR+oFS.Write(FDisplayFlag,SizeOf(Byte));
    iCR:=iCR+oFS.Write(FColorCount,SizeOf(Word));

    if FColorCount>0 Then
    begin
      SetLength(FColorTableArr,FColorCount);
      iCR:=iCR+oFS.Write(FColorTableArr[1], FColorCount*SizeOf(TColorInfoRec));
    end;

    iCR:=oFS.Seek(966,soFromBeginning);
    iCR:=iCR+oFS.Write(FShadingAzimuth,SizeOf(Single));
    iCR:=iCR+oFS.Write(FShadingInclination,SizeOf(Single));
    iCR:=oFS.Seek(1023,soFromBeginning);
    iCR:=iCR+oFS.Write(FValueSizeType,SizeOf(byte));

    //Write raw Data Here;

    if not FIsClassify then
    begin
      For i:=0 to NCols*NRows-1 do
      begin
          case FValueSizeType of
          TWO_BYTE:
              begin
                eOldValue:=SmallInt(FRawData2[i]);

                if FNormalize Then
                  wNewValue:=Round(eOldValue)
                else
                begin
                  wNewValue:=1;
                  if (eOldValue=0) or (eOldValue=FNullValue) then wNewValue:=0;
                  if eOldValue<0 then eOldValue:=(eOldValue+1)/FScale;
                  if eOldValue>0 then eOldValue:=(eOldValue-1)/FScale;

                  eStep:=(FMaxZ-FMinZ)/($FFFF-1);
                  if wNewValue<>0 Then
                       wNewValue:=Word(Round((eOldValue-FMinZ)/eStep))+1;
                end;

                iCR:=iCR+oFS.Write(wNewValue,SizeOf(Word));
                FRawData2[i]:=wNewValue;

                //CR:=CR+oFS.Write(FRawData2[0],FNCols*FNRows*SizeOf(Smallint));
              end;

          ONE_BYTE:
              begin
                 eOldValue:=SmallInt(FRawData[i]);
                 if FNormalize then
                   wNewValue:=Round(eOldValue)
                 else
                 begin
                   wNewValue:=1;
                   if eOldValue=0 then wNewValue:=0;
                   if eOldValue<0 then eOldValue:=(eOldValue+1)/FScale;
                   if eOldValue>0 then eOldValue:=(eOldValue-1)/FScale;
                   eStep:=(FMaxZ-FMinZ)/($FF-1);
                   if  wNewValue<>0 then
                     wNewValue:=Byte(Round((eOldValue-FMinZ)/eStep))+1;
                 end;

                 iCR:=iCR+oFS.Write(wNewValue,SizeOf(Byte));
                 FRawData[i]:=wNewValue;
                 //CR:=CR+oFS.Write(FRawData[0],FNCols*FNRows*SizeOf(byte));
              end;
          else
              raise Exception.Create('Incorrect set format Grid File (Number of Bytes data)')//
          end;
      end;
    end;//if


    if FIsClassify Then
    begin
        case FValueSizeType of
          TWO_BYTE:
              begin
                iCR:=iCR+oFS.Write(FRawData2[0], NCols*NRows*SizeOf(Smallint));
              end;

          ONE_BYTE:
              begin
                iCR:=iCR+oFS.Write(FRawData[0], NCols*NRows);
              end;
          else
            raise Exception.Create('Incorrect set format Grid File (Number of Bytes data)')//
          end;


     //   ClassificatorArr[i]:=PCls^;


        iCount:=FClsList.Count;
        iCR:=iCR+oFS.Write(iCount,SizeOf(word));


        for I := 0 to High(ClassificatorArr) do
        begin

        end;     


        For i:=0 to FClsList.Count-1 do
        begin
          PCls:=PClassificator(FClsList[i]);
          iCR:=iCR+oFS.Write(PCls^.ClsNum,SizeOf(Word));
          iCR:=iCR+oFS.Write(PCls^.ClrInfo,SizeOf(TClsColorRec));
          iCR:=iCR+oFS.Write(PCls^.NameLen,SizeOf(Word));
          iCR:=iCR+oFS.Write(PCls^.ClsName[1],PCls^.NameLen);
        end;
    end;

    FReadData:=True;//������ �� RawData ����� ������;
    
    GenerateTabFile(aFileName, GetGridtype());
  Finally
    oFS.Free;
  end;
end;


// ---------------------------------------------------------------
procedure TvmGRID.SaveToTxtFile(aFileName: String);
// ---------------------------------------------------------------
var
  F: TextFile;

  c: Integer;
  e: Double;
  r: Integer;
  S: String;

   // FS_: TFormatSettings;
//  SFN,FNTab:String;
begin
//  FS_.DecimalSeparator:='.';
//  DecimalSeparator:='.';

 // sFN:=ExtractFileName(FN);
//  FNTab:=ChangeFileExt(FN,'.Tab');

  AssignFile(F, aFileName);
  Rewrite(F);

//  s := '';
//     NCols : Word;
//   NRows : Word;


  for r := 0 to NRows - 1 do
  begin
    s:='';
    
    for c := 0 to NCols - 1 do
    begin
      if GetValueByRowCol(r,c,  e) then
        s :=s+ Format('%6d', [round(e)])
      else
        s :=s+ '-null-  ';
    end;

    Writeln(F, S);
  end;

  System.CloseFile(F);

end;


// ---------------------------------------------------------------
procedure TvmGRID.SaveHDR(aFileName: string; aZone: integer);
// ---------------------------------------------------------------
const
  CRLF = #13+#10;
  LF = CRLF;  
  CR = CRLF;  

  DEF =
      'ENVI'+ LF+ 
      'samples = %d'+ LF+ 
      'lines   = %d'+ LF+ 
      'bands   = 1 '+ LF+ 
      'header offset = 0  '+ LF+ 
      'file type = ENVI Standard '+ LF+ 
      'data type = 2 '+ LF+ 
      'interleave = bsq '+ LF+ 
      'byte order = 0 '+ LF+ 
      'map info = {UTM, 1, 1, %d, %d, %d, %d, %d, North,WGS-84}' ;
 
 //map info = {UTM, 1, 1, 298297, 6685834, 5, 5, 36, North,WGS-84}
    
// --    procedure SaveHDR(aFileName: string);

var
  s: string;
begin

//   StepX: Double;
//   StepY: Double;


  s:= Format(DEF,[NCols, NRows,

    Trunc(MinY),
    Trunc(MaxX),    

    Trunc(StepX),
    Trunc(StepX),

    aZone

   ]);


   TFile.AppendAllText(ChangeFileExt(aFileName,'.hdr'), s, TEncoding.ASCII);
   // 'C:\users\documents\text.txt', 'Some text', TEncoding.ASCII);
 
   
//
//     oIni.WriteInteger('main','Rows',   NRows);
//  oIni.WriteInteger('main','Columns',NCols);
//
//  oIni.WriteFloat('bounds','max_x',MaxX);
//  oIni.WriteFloat('bounds','min_x',MinX);
//
//  oIni.WriteFloat('bounds','max_y',MaxY);
//  oIni.WriteFloat('bounds','min_y',MinY);

{
  StrToTextFile(ChangeFileExt(aFileName,'.hdr'), s  );
}

end;


// ---------------------------------------------------------------
procedure TvmGRID.SaveToBin(aFileName: string);
// ---------------------------------------------------------------
var
  c: Integer;
  I: Integer;
  iBytesPerCell: Integer;
  oFileStream: TFileStream;
  oIni: TIniFile;
  r: Integer;
begin
  oIni:=TIniFile.Create ( aFileName+ '.ini');
(*
    if (FValueSizeType=ONE_BYTE) and (index<=High(FRawData)) Then
     FRawData[index]:=Byte(Value);

  if (FValueSizeType=TWO_BYTE) and (index<=High(FRawData2)) then
     FrawData2[index]:=Value;


  if NCols>0 then StepX:=(MaxX-MinX)/NCols;
  if NRows>0 then StepY:=(MaxY-MinY)/NRows;


*)

  case FValueSizeType of
    ONE_BYTE : iBytesPerCell:=1;
    TWO_BYTE : iBytesPerCell:=2;
    else  raise Exception.Create('');
  end;


  oIni.WriteFloat('main','stepX',StepX);
  oIni.WriteFloat('main','stepY',StepY);

  oIni.WriteFloat('main','bytes_per_cell', iBytesPerCell);
  oIni.WriteInteger('main','NullValue', Round(FNullValue));

  oIni.WriteInteger('main','Rows',   NRows);
  oIni.WriteInteger('main','Columns',NCols);

  oIni.WriteFloat('bounds','max_x',MaxX);
  oIni.WriteFloat('bounds','min_x',MinX);

  oIni.WriteFloat('bounds','max_y',MaxY);
  oIni.WriteFloat('bounds','min_y',MinY);
  


(*
       := eNorth;
  BLRect_WGS.BottomRight.B := eSouth;

  BLRect_WGS.TopLeft.L     := eWest;
  BLRect_WGS.BottomRight.L := eEast;
*)


  FreeAndNil(oIni);

  // ---------------------------------------------------------------
//  oFileStream:=TFileStream.Create ( aFileName+ '.bin', fmCreate	);
//  oFileStream.Size := NRows *  NCols * iBytesPerCell;
//
//
//(*  for r := 0 to NRows - 1 do
//    for c := 0 to NCols - 1 do
//      oFileStream.WriteBuffer(buff_[r][c], 2);
//*)
//  FreeAndNil(oFileStream);



end;





procedure Test();
Var

(*
  e: Double;
  I: Integer;
  j: Integer;
  *)

  obj: TvmGRID;
  
begin
  
  obj:=TvmGRID.Create;

  obj.OpenFile('d:\Andr\code_01.grc');
  obj.SaveToTxtFile('d:\Andr\code_01___.txt');
  
(*
  obj.Open('W:\Task_GRD\data\clutter_managua.grc');
 // obj.Read('W:\Task_GRD\data\buildings_managua.grd');

//  obj.Read('W:\Task_GRD\data\TADJIKISTAN_dem.grd');
//  obj.Open('W:\Task_GRD\data\TADJIKISTAN_dem.grd');


  for I := 0 to obj.NRows - 1 do
   for j := 0 to obj.NCols - 1 do
     e:=obj.GetValueFromFile(i*obj.NCols+j);


 // obj.Read('W:\Task_GRD\data\clutter_managua.grc');

  obj.Free;
*)
end;


begin
  Test();
end.




(*
//-------------------------------------------------------------------
procedure TvmGRID.ReadFile_______(aFileName: String);
//-------------------------------------------------------------------
var
  iCR: int64;
  oFileStream: TFileStream;
begin
  oFileStream:=TFileStream.Create(aFileName,fmOpenRead);

  Try
    ReadHeader(oFileStream,iCR);

    //ReadFile_______ raw Data Here;
    //����� ���������� �� ���������� ������
    if FIsClassify then
    begin
      case FValueSizeType of
        ONE_BYTE: begin
            SetLength(FRawData, NCols*NRows);
            iCR:=iCR+ oFileStream.Read(FRawData[0], NCols*NRows*SizeOf(byte));
          end;

        TWO_BYTE: begin
            SetLength(FRawData2,NCols*NRows);
            iCR:=iCR+ oFileStream.Read(FRawData2[0], NCols*NRows*SizeOf(Smallint));
          end;
      else
        //
      end;
    end;//if

    if (iCR<oFileStream.Size) {and (FHeader_str='HGPC8')} then
    begin
      ReadClassificator(oFileStream,iCR);
    end;

    FReadData:=true;
  Finally
    FreeAndNil(oFileStream);
  end;
end;
*)

(*
// ---------------------------------------------------------------
function TvmGRID.GetValueByXY(aLat, aLon: Double; var aValue: Double):
    Boolean;
// ---------------------------------------------------------------
var
  r,c: Integer;
begin
  Result := GetIndexByCoordLatLon(aLat, aLon, r, c);
  if Result then
  begin
    Result := GetValueByRowCol(r,c, aValue);
    if Result then
      Result := aValue <> NullValue;
  end;
end;
*)

