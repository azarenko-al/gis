unit u_VM_GRD_to_rlf;

interface
uses 

  System.SysUtils, System.Classes, Vcl.Dialogs, System.IOUtils, System.Types,
  
  u_data_classes,

  dm_Clutters,

  u_geo_convert_new1,

  I_rel_Matrix1,

  u_rlf_Matrix,
  

  u_geo,

  u_CustomTask,

  u_clutter_classes,

  VM_Grids;


type


  TVM_GRD_to_rlf = class(TCustomTask)
  private

(*    FVM_DEM: TvmGRID;
    FVM_Clutter_Classes: TvmGRID;
    FVM_Clutter_Heights: TvmGRID;


    FBuild_Heights: TvmGRID;
*)

//    FMatrixDataSourceList: TMatrixDataSourceList;


//    FMatrixDataSourceList_DEM: TMatrixDataSourceList;
//    FMatrixDataSourceList_Clutter_Classes: TMatrixDataSourceList;
//    FMatrixDataSourceList_Clutter_Heights: TMatrixDataSourceList;
//    FMatrixDataSourceList_Build_Heights  : TMatrixDataSourceList;


    FClutterInfo: TClutterInfo;

    procedure PrepareClutterInfoFromGRD(aVMFile: TvmGRID; aClutterInfo:
        TClutterInfo);
    procedure RunFile_frame(aMatrixDataSource: TMatrixDataSourceBase;
        aRLF_FileName: string);
    procedure RunFile_index;


  protected
    procedure CloseFiles; override;
    function OpenFiles: Boolean; override;
  public


    Params: record
//     oFiles: TStringList;
//              UseDifferentFiles : Boolean;


              IsUse_Clutter_Classes    : Boolean;
              IsUse_Clutter_Heights    : Boolean;
              IsUse_Build_Heights      : Boolean;


              Dem_FileName             : string;
              Clutter_Classes_FileName : string;
              Clutter_Heights_fileName : string;
              Build_Heights_FileName   : string;

              // ---------------------------------
//              Folder_Dem             : string;
//              Folder_Clutter_Classes : string;
//              Folder_Clutter_Heights : string;
//              Folder_Build_Heights   : string;


              
//              Files_DEM             : TStringList;
//              Files_Clutter_Classes : TStringList;
//              Files_Clutter_Heights : TStringList;
//              Files_Build_Heights   : TStringList;

              // ---------------------------------

              Clutters_section : string;

              RLF_FileName1 : string;
              Rlf_Step     : Integer;

            end;

    constructor Create;
    destructor Destroy; override;

    procedure Run;

  //  procedure Run_File; override;

  end;


implementation

// ---------------------------------------------------------------
constructor TVM_GRD_to_rlf.Create;
// ---------------------------------------------------------------
begin
  inherited;
(*
  FVM_DEM:=TvmGRID.Create;
  FVM_Clutter_Classes:=TvmGRID.Create;
  FVM_Clutter_Heights:=TvmGRID.Create;
  FBuild_Heights:=TvmGRID.Create;
  *)



  FClutterInfo := TClutterInfo.Create();


//  FMatrixDataSourceList_DEM:= TMatrixDataSourceList.Create();
//  FMatrixDataSourceList_Clutter_Classes:= TMatrixDataSourceList.Create();
//  FMatrixDataSourceList_Clutter_Heights:= TMatrixDataSourceList.Create();
//  FMatrixDataSourceList_Build_Heights:= TMatrixDataSourceList.Create();


//
//  Params.Files_DEM             := TStringList.create;
//  Params.Files_Clutter_Classes := TStringList.Create;
//  Params.Files_Clutter_Heights := TStringList.Create;
//  Params.Files_Build_Heights   := TStringList.Create;
//




//  params.Clutters_section :='GRD';
end;

// ---------------------------------------------------------------
destructor TVM_GRD_to_rlf.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(FClutterInfo);

  
(*
  FreeAndNil(FVM_DEM);
  FreeAndNil(FVM_Clutter_Classes);
  FreeAndNil(FVM_Clutter_Heights);
  FreeAndNil(FBuild_Heights);
*)


//  FreeAndNil(FMatrixDataSourceList_DEM);
//  FreeAndNil(FMatrixDataSourceList_Clutter_Classes);
//  FreeAndNil(FMatrixDataSourceList_Clutter_Heights);
//  FreeAndNil(FMatrixDataSourceList_Build_Heights);


  inherited;
end;                


// ---------------------------------------------------------------
procedure TVM_GRD_to_rlf.CloseFiles;
// ---------------------------------------------------------------
begin

(*

  FVM_DEM.CloseFile;
  FVM_Clutter_Classes.CloseFile;
  FVM_Clutter_Heights.CloseFile;
  FBuild_Heights.CloseFile;
  *)


//  FMatrixDataSourceList_DEM.Clear;
//  FMatrixDataSourceList_Clutter_Classes.Clear;
//  FMatrixDataSourceList_Clutter_Heights.Clear;
//  FMatrixDataSourceList_Build_Heights.Clear;

end;

// ---------------------------------------------------------------
function TVM_GRD_to_rlf.OpenFiles: Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  oFiles: TStringList;
  oVM_DEM: TvmGRID;

  arFiles: TStringDynArray;    //  TStringDynArray       = array of string;
 
  
begin
 {

  if not FileExists(Params.DEM_fileName) then
    Exit;

  FVM_DEM.OpenFile(Params.DEM_fileName);
//  FVM_DEM.Test_SaveToTxtFile( ChangeFileExt(Params.DEM_fileName, '.txt'));

  if Params.IsUse_Clutter_Classes then
  begin
  //  Assert(FileExists(Params.Clutters_ini_fileName));

//    if FileExists(Params.Clutters_ini_fileName) then

    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);


   //// FClutterInfo.LoadCluttersFromDB(Params.Clutters_section);

    if FileExists(Params.Clutter_Classes_FileName) then
    begin
      FVM_Clutter_Classes.OpenFile(Params.Clutter_Classes_FileName);
    //  FVM_Clutter_Classes.OpenFile(Params.Clutter_Classes_FileName);

      PrepareClutterInfoFromGRD(FVM_Clutter_Classes, FClutterInfo);
    end else
      ShowMessage('not FileExists:'+ Params.Clutter_Classes_FileName);

    if Params.IsUse_Clutter_Heights then
      if FileExists(Params.Clutter_Heights_fileName) then
        FVM_Clutter_Heights.OpenFile(Params.Clutter_Heights_fileName)
      else
        ShowMessage('not FileExists:'+ Params.Clutter_Heights_fileName);

  end;


  /// FVM_DEM.OpenFile(Params.DEM_fileName);


  if FileExists(params.Build_Heights_fileName) then
  begin
    FBuild_Heights.OpenFile (params.Build_Heights_fileName)

  end
  else
    ShowMessage('not FileExists:'+ Params.Build_Heights_fileName);

 }



//  oFiles:=TStringList.Create;

{
  // ---------------------------------------------------------------
  if Params.Folder_DEM<>'' then
  // ---------------------------------------------------------------
  begin
  //  oFiles.Clear;

    if Params.DEM_fileName<>'' then
      oFiles.Add(Params.DEM_fileName);


//
//  sDir:= IncludeTrailingBackslash ( Trim (DirectoryEdit1.Text));
//  
//
////  obj.Params.Files_XML:=SplitString('D:\2\samples\doc3223259_priozersk.xml','');
//
//  
//    arFiles:=TDirectory.GetFiles( Params.Folder_DEM, '*.grd'); //, TSearchOption.soTopDirectoryOnly);
//         
   
    //ScanDir1(Params.Folder_DEM, '*.grd', oFiles);

    for I := 0 to High(arFiles) do
    begin
      oVM_DEM:=TvmGRID.Create;

      if oVM_DEM.OpenFile(arFiles[i]) then
        FMatrixDataSourceList_DEM.AddItem(oVM_DEM)
      else
        FreeAndNil(oVM_DEM);

    end;
  end;
 }
 
  // ---------------------------------------------------------------
  if Params.IsUse_Clutter_Classes then
 // --  if Params.Folder_Clutter_Classes<>'' then
  // ---------------------------------------------------------------
  begin
    dmClutters.LoadCluttersFromDB(FClutterInfo, Params.Clutters_section);


    oFiles.Clear;

    if Params.Clutter_Classes_FileName<>'' then
      oFiles.Add(Params.Clutter_Classes_FileName);

//    if Params.Folder_Clutter_Classes<>'' then
//    begin
//      arFiles:=TDirectory.GetFiles( Params.Folder_Clutter_Classes, '*.grc'); //, TSearchOption.soTopDirectoryOnly);
//
//            
////      oFiles.A
//     end;
   
 //   ScanDir1(Params.Folder_Clutter_Classes, '*.grc', oFiles);

    for I := 0 to High(arFiles) do
    begin
      oVM_DEM:=TvmGRID.Create;

      if oVM_DEM.OpenFile(arFiles[i]) then
      begin
        if i=0 then
          PrepareClutterInfoFromGRD(oVM_DEM, FClutterInfo);

  //      FMatrixDataSourceList_Clutter_Classes.AddItem(oVM_DEM)
      end
      else
        FreeAndNil(oVM_DEM);

    end;
  end;

  // ---------------------------------------------------------------
  if Params.IsUse_Clutter_Heights then
 // if Params.Folder_Clutter_Heights<>'' then
  // ---------------------------------------------------------------
  begin
//    oFiles.Clear;

 //   if Params.Clutter_Heights_FileName<>'' then
   //   oFiles.Add(Params.Clutter_Heights_FileName);

 //   arFiles:=TDirectory.GetFiles( Params.Folder_Clutter_Heights, '*.grd'); //, TSearchOption.soTopDirectoryOnly);
     
  //  ScanDir1(Params.Folder_Clutter_Heights, '*.grd', oFiles);

    for I := 0 to High(arFiles) do
    begin
      oVM_DEM:=TvmGRID.Create;

      if oVM_DEM.OpenFile(arFiles[i]) then
 //       FMatrixDataSourceList_Clutter_Heights.AddItem(oVM_DEM)
      else
        FreeAndNil(oVM_DEM);

    end;
  end;


  // ---------------------------------------------------------------
  if Params.IsUse_Build_Heights then
 // if Params.Folder_Clutter_Heights<>'' then
  // ---------------------------------------------------------------
  begin
    //oFiles.Clear;

 //   if Params.Build_Heights_FileName<>'' then
   //   oFiles.Add(Params.Build_Heights_FileName);

 //   arFiles:=TDirectory.GetFiles( Params.Folder_Build_Heights, '*.grd'); //, TSearchOption.soTopDirectoryOnly);
    
   
  //  ScanDir1(Params.Folder_Build_Heights, '*.grd', oFiles);

    for I := 0 to High(arFiles) do
    begin
      oVM_DEM:=TvmGRID.Create;

      if oVM_DEM.OpenFile(arFiles[i]) then
//        FMatrixDataSourceList_Build_Heights.AddItem(oVM_DEM)
      else
        FreeAndNil(oVM_DEM);

    end;
  end;




  FreeAndNil(oFiles);


(*

                Folder_Dem             : string;
              Folder_Clutter_Classes : string;
              Folder_Clutter_Heights : string;
              Folder_Build_Heights   : string;
*)


end;

// ---------------------------------------------------------------
procedure TVM_GRD_to_rlf.PrepareClutterInfoFromGRD(aVMFile: TvmGRID;
    aClutterInfo: TClutterInfo);
// ---------------------------------------------------------------
var
  b: Boolean;
  I: Integer;
  iNum: Integer;
  s: string;
begin

  for I := 1 to High(aVMFile.ClassificatorArr) do
  begin
    s    := aVMFile.ClassificatorArr[i].ClsName;
    iNum := aVMFile.ClassificatorArr[i].ClsNum;

  ///////////
    b:=aClutterInfo.SetCodeByName(s, iNum);
  end;


(*
  for I := 1 to High(FVM_Clutter_Classes.ClassificatorArr) do
  begin
    s    := FVM_Clutter_Classes.ClassificatorArr[i].ClsName;
    iNum := FVM_Clutter_Classes.ClassificatorArr[i].ClsNum;

  ///////////
    b:=aClutterInfo.SetCodeByName(s, iNum);
  end;
*)

end;


// ---------------------------------------------------------------
procedure TVM_GRD_to_rlf.RunFile_frame(aMatrixDataSource: TMatrixDataSourceBase; aRLF_FileName: string);
// ---------------------------------------------------------------

var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

//  arrXYRectArray_UTM: TXYRectArray;
//  arrXYPointArrayF_new: TXYPointArrayF;
 // arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_UTM: TXYRect;
  rXYRect_GK: TXYRect;

  xy_CK1, xy_CK2,
  xy_UTM1, xy_UTM2,
  xy, xy_gk, xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  c: Integer;
  eHeight: Double;
  eStepX: Double;
  eStepY: Double;
  eValue: Double;
  iGK_zone: integer;
  iColCount: integer;

  iRowCount: integer;
  iRow: integer;
  iCol: Integer;

  rlf_rec: Trel_Record;
 // rlf_rec1: Trel_Record;
  
  iValue: integer;

  iIntValue: Smallint;

  oRlfFileStream: TFileStream;

  s: string;
  iCluCode: smallint;
  iCluH: smallint;
  iIndex: Integer;
  iStep: Integer;
  iUTM_zone: Integer;
  k: Integer;
  r: Integer;
  s2: string;
  s1: string;

  xyBounds_UTM: TXYRect;

  rClutter: TClutterRec;


begin
  Terminated := False;


  rXYRect_UTM := aMatrixDataSource.GetXYBounds_UTM();
  iUTM_zone   := aMatrixDataSource.GetZoneUTM();

  iGK_zone := iUTM_zone - 30;

  iGK_zone := geo_UTM_to_GK_zone(iUTM_zone);


 //////// rXYRect_GK :=geo_UTM_to_GK (rXYRect_UTM, iUTM_zone);


  eStepY := aMatrixDataSource.GetMinCellSize();

  
(*
  eStepX :=FVM_DEM.StepX;
  eStepY :=FVM_DEM.StepY;
*)
 // iStep :=round(eStepY);

  if Params.RLF_Step>0 then
    iStep :=Params.RLF_Step
  else
    iStep :=round(eStepY);


//  rXYRect_GK:=aXYRect_GK;


  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.xyBounds:=rXYRect_GK;

  aRLF_FileName := ChangeFileExt(aRLF_FileName, '.rlf');

  oRlfFileStream:=TFileStream.Create(aRLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

  for iRow := 0 to iRowCount - 1 do
  begin
    DoOnProgress(iRow,iRowCount, Terminated);
    if Terminated then
      Break;


    for iCol := 0 to iColCount - 1 do
      if not Terminated then
    begin
      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;

      xy_UTM := geo_GK_to_UTM(xy, iGK_zone);

      FillChar(rlf_rec, SizeOf(rlf_rec), 0);
      rlf_rec.Rel_H :=EMPTY_HEIGHT;


      if FMatrixDataSourceList_DEM.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
      begin
        rlf_rec.Rel_H :=Round(eValue);


          ///////////////////////////////
          ////////////////////////
          if Params.IsUse_Clutter_Classes and (FMatrixDataSourceList_Clutter_Classes.Count>0)  then
            if FMatrixDataSourceList_Clutter_Classes.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
          ////////////////////////
          begin
             iCluCode := round(eValue);


             if (iCluCode>=0) and (iCluCode<100) then
              if FClutterInfo.GetOnegaCode(iCluCode,  rClutter) then
              begin
                rlf_rec.Clutter_Code := rClutter.Onega_Code;
                rlf_rec.Clutter_H    := rClutter.Onega_Height;
              end;

            // -------------------------
            // Clutter_Heights
            // -------------------------
            if Params.IsUse_Clutter_Heights and (rlf_rec.Clutter_Code>0) and
                   (FMatrixDataSourceList_Clutter_Heights.Count>0)
            then
                if FMatrixDataSourceList_Clutter_Heights.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
                begin
               //   if FVM_Clutter_Heights.GetValueByRowCol(r,c, eValue) then
                //    if eValue <> FVM_Clutter_Heights.NullValue then
                      if eValue>0 then
                        rlf_rec.Clutter_H  := Round(eValue);
                end;

          end;

           ///////////////////////////////
          if Params.IsUse_Build_Heights and
            (FMatrixDataSourceList_Build_Heights.Count>0) then
//             if rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD, DEF_CLU_CITY] then
            if FMatrixDataSourceList_Build_Heights.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eHeight) then
              if eHeight>0 then
              begin
                rlf_rec.Clutter_Code:=DEF_CLU_ONE_BUILD;

                if eHeight>255 then
                  eHeight:=255;

                try
                  rlf_rec.Clutter_H :=Round(eHeight);

                except // wrap up
                  ShowMessage( FloatToStr( eHeight));
                end;    // try/finally

              end;

       end;


       FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

      end;

      oRlfFileStream.CopyFrom(FMemStream, 0);
      FMemStream.Clear;

     // FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

  FreeAndNil(oRlfFileStream);


  if not Terminated then
    ExportToClutterTab(aRLF_FileName);


end;


//  FreeAndNil(oRlfFileStream);



//
//  FVM_DEM.CloseFile;
//  FVM_Clutter_Classes.CloseFile;
//  FVM_Clutter_Heights.CloseFile;

(*

  if not Terminated then
    ExportToClutterBMP(aRLF_FileName);


end;
*)


// ---------------------------------------------------------------
procedure TVM_GRD_to_rlf.RunFile_index;
// ---------------------------------------------------------------
var
  I: Integer;  
  rXYRect_GK: TXYRect;
  rXYRect_UTM: TXYRect;
  sDir: string;    
  sFileName: string;
begin

  sDir := IncludeTrailingBackslash(  ExtractFileDir(Params.RLF_FileName1));


  for I := 0 to FMatrixDataSourceList_DEM.Count - 1 do
  begin
//    rXYRect_UTM := FMatrixDataSourceList_DEM.Items[i].GetXYRect();
   // sFileName := FDEM_index_file.Items[i].ShortFileName;

  //  rXYRect_GK :=UTM_to_GK (rXYRect_UTM, FUTM_zone);

    sFileName:=FMatrixDataSourceList_DEM.Items[i].GetFileName();

    sFileName := ExtractFileName(sFileName);

    RunFile_frame(FMatrixDataSourceList_DEM.Items[i], sDir + sFileName);


  //  if not Terminated then
   //   ExportToClutterBMP(aRLF_FileName);


  //  Exit;
  //  .;
  end;

end;
           

// ---------------------------------------------------------------
procedure TVM_GRD_to_rlf.Run;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Terminated := False;

  OpenFiles;

  RunFile_index;

  CloseFiles;

(*
  if Params.UseDifferentFiles then
    RunFile_index
  else
    RunFile;
*)

end;


end.





{



// ---------------------------------------------------------------
procedure TVM_GRD_to_rlf.Run_File;
// ---------------------------------------------------------------
var
  I: Integer;

  header_Rec: TrelMatrixInfoRec;

//  arrXYRectArray_UTM: TXYRectArray;
//  arrXYPointArrayF_new: TXYPointArrayF;
 // arrXYPointArrayF_GK: TXYPointArrayF;

  rXYRect_UTM: TXYRect;
  rXYRect_GK: TXYRect;

  xy_CK1, xy_CK2,
  xy_UTM1, xy_UTM2,
  xy, xy_gk, xy_UTM: TXYPoint;

  bl,bl1,bl2: TBLPoint;
  c: Integer;
  eHeight: Double;
  eStepX: Double;
  eStepY: Double;
  eValue: Double;
  iGK_zone: integer;
  iColCount: integer;

  iRowCount: integer;
  iRow: integer;
  iCol: Integer;

  rlf_rec: Trel_Record;
  rlf_rec1: Trel_Record;
  
  iValue: integer;

  iIntValue: Smallint;

  oRlfFileStream: TFileStream;

  s: string;
  iCluCode: smallint;
  iCluH: smallint;
  iIndex: Integer;
  iStep: Integer;
  iUTM_zone: Integer;
  k: Integer;
  r: Integer;
  s2: string;
  s1: string;

  xyBounds_UTM: TXYRect;

    rClutter: TClutterRec;


begin
  Terminated := False;

  OpenFiles();

  iUTM_zone :=FVM_DEM.UTM_zone;
  iGK_zone := iUTM_zone - 30;

  iGK_zone := geo_UTM_to_GK_zone(iUTM_zone);

 // FVM_DEM.LoadFromHeaderFile(DEM_fileName);

  xyBounds_UTM.TopLeft.X    :=FVM_DEM.MaxY;  // ������ max
  xyBounds_UTM.TopLeft.Y    :=FVM_DEM.MinX;  // ������� min
  xyBounds_UTM.BottomRight.X:=FVM_DEM.MinY;  // ������ min
  xyBounds_UTM.BottomRight.Y:=FVM_DEM.MaxX;  // ������� max


//    rXYRect_UTM := FDEM_index_file.Items[i].GetXYRect();
   // sFileName := FDEM_index_file.Items[i].ShortFileName;

    rXYRect_GK :=UTM_to_GK (xyBounds_UTM, iUTM_zone);


 {
  SetLength(arrXYRectArray_UTM, 1);

  arrXYRectArray_UTM[0] := xyBounds_UTM; //FVM_DEM.xyBounds_UTM;

  rXYRect_UTM:=geo_GetRoundXYRect_(arrXYRectArray_UTM);

  geo_XYRectToXYPointsF(rXYRect_UTM, arrXYPointArrayF_new);

  arrXYPointArrayF_GK.Count :=4;


  for i:=0 to arrXYPointArrayF_new.Count-1 do
  begin
    xy :=arrXYPointArrayF_new.Items[i];

    xy_gk := geo_UTM_to_GK(xy, iUTM_zone);


    arrXYPointArrayF_GK.Items[i] := xy_gk;
  end;

  rXYRect_GK :=geo_RoundXYPointsToXYRect(arrXYPointArrayF_GK);
   }

  eStepX :=FVM_DEM.StepX;
  eStepY :=FVM_DEM.StepY;

 // iStep :=round(eStepY);

  if Params.RLF_Step>0 then
    iStep :=Params.RLF_Step
  else
    iStep :=round(eStepY);



  iRowCount := Round(Abs(rXYRect_GK.TopLeft.X - rXYRect_GK.BottomRight.X) / iStep);
  iColCount := Round(Abs(rXYRect_GK.TopLeft.Y - rXYRect_GK.BottomRight.Y) / iStep);


  // ---------------------------------------------------------------
  FillChar (header_Rec, SizeOf(header_Rec), 0);

  header_Rec.ColCount:=iColCount;
  header_Rec.RowCount:=iRowCount;

  header_Rec.StepX:=iStep;
  header_Rec.StepY:=iStep;

  header_Rec.xyBounds:=rXYRect_GK;

  oRlfFileStream:=TFileStream.Create(Params.RLF_FileName, fmCreate);

  TrlfMatrix.SaveFileHeaderToFileStream (oRlfFileStream, header_Rec);

  // ---------------------------------------------------------------

  for iRow := 0 to iRowCount - 1 do
  begin
    DoOnProgress(iRow,iRowCount, Terminated);
    if Terminated then
      Break;


    for iCol := 0 to iColCount - 1 do
      if not Terminated then
    begin
      xy.x := rXYRect_GK.TopLeft.X - iRow*iStep - iStep/2;
      xy.y := rXYRect_GK.TopLeft.Y + iCol*iStep + iStep/2;

      xy_UTM := geo_GK_to_UTM(xy, iGK_zone);

      FillChar(rlf_rec, SizeOf(rlf_rec), 0);
      rlf_rec.Rel_H :=EMPTY_HEIGHT;


      FillChar(rlf_rec1, SizeOf(rlf_rec1), 0);
      rlf_rec1.Rel_H :=EMPTY_HEIGHT;


//      if FVM_DEM.GetIndexByCoord(xy_UTM.x, xy_UTM.y, iIndex) then
  //      eValue := FVM_DEM.GetValueFromFile(iIndex);



      if FVM_DEM.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
      begin
        rlf_rec.Rel_H :=Round(eValue);

      end;


      if FMatrixDataSourceList_DEM.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
      begin
        rlf_rec1.Rel_H :=Round(eValue);
      end;


      if FVM_DEM.GetIndexByCoordLatLon(xy_UTM.x, xy_UTM.y, r,c) then
      begin
       // eValue :=
        if FVM_DEM.GetValueByRowCol(r,c, eValue) then
          if eValue <> FVM_DEM.NullValue then
        begin
          rlf_rec.Rel_H :=Round(eValue);

       // else
        //  rlf_rec.Rel_H :=EMPTY_HEIGHT;

          // -------------------------
          if Params.IsUse_Clutter_Classes then
             if FVM_Clutter_Classes.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
             begin

             end;

          ///////////////////////////////
          ////////////////////////
          if FMatrixDataSourceList_Clutter_Classes.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
          ////////////////////////
          begin
             iCluCode := round(eValue);


             if (iCluCode>=0) and (iCluCode<100) then
              if FClutterInfo.GetOnegaCode(iCluCode,  rClutter) then
              begin
                rlf_rec1.Clutter_Code := rClutter.Onega_Code;
                rlf_rec1.Clutter_H    := rClutter.Onega_Height;
              end;


            if Params.IsUse_Clutter_Heights and (rlf_rec1.Clutter_Code>0) then
              if FMatrixDataSourceList_Clutter_Heights.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eValue) then
              begin
             //   if FVM_Clutter_Heights.GetValueByRowCol(r,c, eValue) then
              //    if eValue <> FVM_Clutter_Heights.NullValue then
                    if eValue>0 then
                      rlf_rec1.Clutter_H  := Round(eValue);
              end;

          end;



          if Params.IsUse_Clutter_Classes then
            if FVM_Clutter_Classes.GetIndexByCoordLatLon(xy_UTM.x, xy_UTM.y, r,c) then
          begin
            if FVM_Clutter_Classes.GetValueByRowCol(r,c, eValue) then
              if eValue <> FVM_Clutter_Classes.NullValue then
            begin
              iCluCode := round(eValue);

              if (iCluCode>=0) and (iCluCode<100) then
              begin
                if FClutterInfo.GetOnegaCode(iCluCode,  rClutter) then
                begin
                  rlf_rec.Clutter_Code := rClutter.Onega_Code;
                  rlf_rec.Clutter_H    := rClutter.Onega_Height;
                end;


              //  rlf_rec.Clutter_Code := FClutterInfo.Clutters[iCluCode].Onega_Code;
              //  rlf_rec.Clutter_H    := FClutterInfo.Clutters[iCluCode].Onega_Height;
              end;
            end;


            if Params.IsUse_Clutter_Heights and (rlf_rec.Clutter_Code>0) then
              if FVM_Clutter_Heights.GetIndexByCoordLatLon(xy_UTM.x, xy_UTM.y, r,c) then
            begin
              if FVM_Clutter_Heights.GetValueByRowCol(r,c, eValue) then
                if eValue <> FVM_Clutter_Heights.NullValue then
                  if eValue>0 then
                    rlf_rec.Clutter_H  := Round(eValue);
            end;

          end;
          // -------------------------


            if Params.IsUse_Build_Heights then
//             if rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD, DEF_CLU_CITY] then
              if FBuild_Heights.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eHeight) then
                if eHeight>0 then
                begin
                  rlf_rec.Clutter_Code:=DEF_CLU_ONE_BUILD;

                  if eHeight>255 then
                    eHeight:=255;


                  try
                    rlf_rec.Clutter_H :=Round(eHeight);

                  except // wrap up
                    ShowMessage( FloatToStr( eHeight));
                  end;    // try/finally

                end;

                //FMatrixDataSourceList_Clutter_Classes


               ///////////////////////////////
               ///////////////////////////////
               ///////////////////////////////
              if Params.IsUse_Build_Heights then
//             if rlf_rec.Clutter_Code in [DEF_CLU_ONE_BUILD, DEF_CLU_CITY] then
              if FMatrixDataSourceList_Build_Heights.GetValueByLatLon(xy_UTM.x, xy_UTM.y, eHeight) then
                if eHeight>0 then
                begin
                  rlf_rec1.Clutter_Code:=DEF_CLU_ONE_BUILD;

                  if eHeight>255 then
                    eHeight:=255;


                  try
                    rlf_rec1.Clutter_H :=Round(eHeight);

                  except // wrap up
                    ShowMessage( FloatToStr( eHeight));
                  end;    // try/finally

                end;



        end;

      end;

      FMemStream.Write(rlf_rec, SizeOf(rlf_rec));

    end;

    oRlfFileStream.CopyFrom(FMemStream, 0);
    FMemStream.Clear;

  end;

  FreeAndNil(oRlfFileStream);


  CloseFiles;
  
//
//  FVM_DEM.CloseFile;
//  FVM_Clutter_Classes.CloseFile;
//  FVM_Clutter_Heights.CloseFile;


  if not Terminated then
    ExportToClutterBMP(Params.RLF_FileName);


end;

