unit dm_Tasks;

interface

//function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''):   Boolean;


uses
  System.SysUtils, System.Classes,  Forms, IOUtils, Variants,
               
  
  u_db,
  u_db_mdb, 

  u_Asset,
    
  u_classes_projection_TXT,
                        
  u_Asset_to_RLF,

  u_files,       
               
  Data.DB, Data.Win.ADODB, cxShellBrowserDialog, cxClasses
           
  ;

type
  TdmTasks = class(TDataModule)
    ADOConnection1: TADOConnection;
    t_Task: TADOTable;
    ds_Tasks: TDataSource;
    cxShellBrowserDialog1: TcxShellBrowserDialog;
    t_Project: TADOTable;
    ds_Project: TDataSource;
    q_Tasks_checked: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  //  procedure DataModuleCreate(Sender: TObject);
  private
    FTerminated : Boolean;
     
    FDataset: TDataset;
  
    class procedure Init;
    procedure Run_All;
  protected
    procedure Run(aDataset: TDataSet);
  public
//    procedure Open;
 
  end;

var
  dmTasks: TdmTasks;

const
  FLD_Filename_onega_rlf      = 'filename_onega_rlf';
  FLD_Filename_CLU_H          = 'filename_CLU_H';
  FLD_Filename_CLU            = 'filename_CLU';
  FLD_Is_use_Clutter_Heights  = 'is_use_Clutter_Heights';
  FLD_Is_use_clutter          = 'is_use_clutter';
  FLD_Filename_DTM            = 'filename_DTM';
  FLD_filename_projection_txt = 'filename_projection_txt';  //--filename_projection_txt
  FLD_clutter_schema          = 'clutter_schema';
  FLD_step                    = 'step';
  FLD_UTM_zone                = 'UTM_zone';

 FLD_DIR = 'Dir';

   

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmTasks.DataModuleCreate(Sender: TObject);
begin
  if not mdb_OpenConnection(ADOConnection1, GetApplicationDir() + 'GeoConverter.tasks.mdb') then
    Halt;

  //t_Task.Open;


  t_Task.Open;     
  t_Project.Open;     
 
  FDataset:=t_Task;   


  
  db_SetFieldCaptions(FDataset,
  [
    FLD_DIR, '�����',
  
    FLD_CHECKED,               '���',

    FLD_Filename_DTM            , 'DTM (index.txt)',

    FLD_Filename_CLU            , 'Clutter (index.txt)',
    FLD_Filename_CLU_H          , 'Clutter Height (index.txt)',

    FLD_Is_use_clutter          , '������������ �lutter',
    FLD_Is_use_Clutter_Heights  , '������������ Clutter Height',

    FLD_filename_projection_txt , 'Projection.txt',  
    FLD_clutter_schema          , '�������� �����',
    FLD_UTM_zone                , 'UTM ����',

    FLD_Filename_onega_rlf      , 'Onega RLF',
    FLD_step                    , '���' 
                                      
  ]
  );
  
end;

//----------------------------------------------------------------
class procedure TdmTasks.Init;
//----------------------------------------------------------------
begin
  if not Assigned(dmTasks) then
    dmTasks := TdmTasks.Create(Application);

end;


// ---------------------------------------------------------------
procedure TdmTasks.Run(aDataset: TDataSet);
// ---------------------------------------------------------------
var

  FASSET_to_rlf: TASSET_to_rlf;
  
begin
  FASSET_to_rlf := TASSET_to_rlf.Create();

//  FASSET_to_rlf.OnProgress :=DoOnProgress;


  FTerminated := False;

//  btn_Run.Action := act_Stop;

  FASSET_to_rlf.Params.Dem_Index_FileName             := aDataset.FieldByName(FLD_Filename_DTM).AsString;
  FASSET_to_rlf.Params.Projection_Txt_fileName        := aDataset.FieldByName(FLD_filename_projection_txt).AsString;

  FASSET_to_rlf.Params.IsUse_Clutter_Classes          := aDataset.FieldByName(FLD_Is_use_clutter).AsBoolean;
  FASSET_to_rlf.Params.Clutter_Classes_Index_FileName := aDataset.FieldByName(FLD_filename_CLU).AsString;

  FASSET_to_rlf.Params.IsUse_Clutter_H                := aDataset.FieldByName(FLD_Is_use_Clutter_Heights).AsBoolean;
  FASSET_to_rlf.Params.Clutter_H_Index_FileName       := aDataset.FieldByName(FLD_Filename_CLU_H).AsString;




  FASSET_to_rlf.Params.Clutters_section   :=aDataset.FieldByName(FLD_clutter_schema).AsString;


  FASSET_to_rlf.Params.RLF_FileName := aDataset.FieldByName(FLD_filename_onega_rlf).AsString;

  FASSET_to_rlf.Params.Rlf_Step     :=aDataset.FieldByName(FLD_step).AsInteger;

  
try 
  FASSET_to_rlf.Run;  
except

end;

 
  FreeAndNil(FASSET_to_rlf);  


 // btn_Run.Action := act_Run;
 // ProgressBar1.Position :=0;
  
end;

// ---------------------------------------------------------------
procedure TdmTasks.Run_All;
// ---------------------------------------------------------------
begin

  q_Tasks_checked.Close;
  q_Tasks_checked.Open;

  
 // FDataset.First;
  FTerminated:=False;

  db_View(q_Tasks_checked);
  
  
  with q_Tasks_checked do 
    while not EOF and not FTerminated do
    begin
//      if FieldByName(FLD_CHECKED).AsBoolean then
      Run (q_Tasks_checked);

//      if FTerminated then
  //      Break;
    
      Next;
    end;  
       

  
end;


end.


{

// ---------------------------------------------------------------
procedure TdmTasks.Process_Dir(aDir: string; aClutter_schema: string = '');
// ---------------------------------------------------------------
var
  k: Integer;
  oAsset_CLU: TAsset_index_file;
  oAsset_CLU_H: TAsset_index_file;
//  sDir: string;
  sDir_Clutter_Classes: string;
  sDir_Clutter_H: string;
  sDir_DTM: string;


  oAsset_DTM: TAsset_index_file;
  I: Integer;
  iInd: Integer;
  iStep: Integer;
  iZone: Integer;
  oProjection_file: TProjection_Txt_file;
//  sClutter_schema: string;
  sDEM: string;
  sDir_CLU: string;
  sDir_CLU_H: string;
  sDir_DEM: string;
  sFile_rlf: string;
  sName: string;
  sName_CLU: string;
  sName_CLU_H: string;
  sName_DTM: string;
  sPath_proj: string;
begin
  aDir:= IncludeTrailingBackslash(aDir);

 // if cxShellBrowserDialog1.Execute then 

    if aClutter_schema='' then
      aClutter_schema:= FDataset.FieldByName(FLD_clutter_schema).AsString;

  
    oAsset_DTM:=TAsset_index_file.Create;
    oAsset_CLU:=TAsset_index_file.Create;
    oAsset_CLU_H:=TAsset_index_file.Create;
    oProjection_file:=TProjection_Txt_file.Create;

    

  //  sDir:= IncludeTrailingBackslash(cxShellBrowserDialog1.Path);

//    sDir:= 'P:\_megafon\83_NW_NenetskyAO_UTM40\';
    
    
//    sDir_DTM            :=sDir + 'Height\';
//
   iZone:=0;



   

    for sPath_proj in TDirectory.GetFiles (aDir, '*.txt')  do
    begin
      oProjection_file.LoadFromFile(sPath_proj);
      iZone:=oProjection_file.ZOne; 
    end;  

 
    sDir_DEM:=aDir + 'Height\';

    if FileExists(sDir_DEM + 'index.txt') then
    begin
      oAsset_DTM.LoadFromFile(sDir_DEM + 'index.txt');
      oAsset_DTM.ParseToFiles;
    end;  

    //================================================
    
    sDir_CLU:=aDir + 'Clutter\';
    if FileExists(sDir_CLU + 'index.txt') then
    begin
      oAsset_CLU.LoadFromFile(sDir_CLU + 'index.txt');
      oAsset_CLU.ParseToFiles;
    end;

    //================================================
    
    sDir_CLU_H:=aDir + 'obstacle\';
    if FileExists(sDir_CLU_H + 'index.txt') then
    begin
      oAsset_CLU_H.LoadFromFile(sDir_CLU_H + 'index.txt');
      oAsset_CLU_H.ParseToFiles;
    end;  

    //================================================

    
    for I := 0 to oAsset_DTM.Groups.Count-1 do
    begin
      sName:=oAsset_DTM.Groups[i];
      iStep:=Integer(oAsset_DTM.Groups.Objects[i]);

      sName_DTM:=sDir_DEM + 'index_'+ sName +'.txt';

      
      //-------------------------------------------   
      iInd:=oAsset_CLU.Groups.IndexOf(sName);
      if iInd>=0 then
         sName_CLU:=sDir_CLU + 'index_'+ oAsset_CLU.Groups[iInd]+'.txt'
      else
         sName_CLU:='';

      //-------------------------------------------   
      iInd:=oAsset_CLU_H.Groups.IndexOf(sName);
      if iInd>=0 then
         sName_CLU_H:= sDir_CLU_H + 'index_'+ oAsset_CLU_H.Groups[iInd]+'.txt'
      else
         sName_CLU_H:='';

      //-------------------------------------------   
      sFile_rlf:= aDir + GetParentFolderName_v1 (aDir) +'_'+ sName + '.rlf';

      
         
     if FDataset.Locate(FLD_Filename_DTM, sName_DTM, []) then
        FDataset.Delete;
                       
         
     db_AddRecord_(FDataset, [

        FLD_DIR                    , aDir,

     
        FLD_Filename_DTM            , sName_DTM,
        FLD_filename_projection_txt , sPath_proj, //'filename_projection_txt',  
        FLD_UTM_zone                , iZone,      // 'UTM_zone',
     

        FLD_Is_use_clutter          , IIF(sDir_CLU='', null, True),
        FLD_Filename_CLU            , IIF(sDir_CLU='', null, sName_CLU),

        FLD_Is_use_Clutter_Heights  , IIF(sDir_CLU_H='', null, True),
        FLD_Filename_CLU_H          , IIF(sDir_CLU_H='', null, sName_CLU_H),
      
        FLD_clutter_schema          , aClutter_schema,

        FLD_Filename_onega_rlf      , sFile_rlf, //aDir + GetParentFolderName_v1 + sName + '.rlf',
        FLD_step                    , iStep
 

       ]);
   
    end;  
                      

    FreeAndNil(oAsset_DTM);
    FreeAndNil(oAsset_CLU);
    FreeAndNil(oAsset_CLU_H);
    FreeAndNil(oProjection_file);  
      
 // end;

    
end;

