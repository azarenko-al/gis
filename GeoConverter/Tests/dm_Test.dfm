object dmTest: TdmTest
  OldCreateOrder = False
  Left = 649
  Top = 466
  Height = 279
  Width = 302
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=GoogleMapLayer;Data Source=ALEX'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 64
    Top = 24
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    ProcedureName = 'test;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@LAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
      end
      item
        Name = '@LON'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
      end
      item
        Name = '@N'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
      end
      item
        Name = '@E'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
      end
      item
        Name = '@index'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Prepared = True
    Left = 64
    Top = 88
  end
end
