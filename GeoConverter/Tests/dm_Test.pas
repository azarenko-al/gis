unit dm_Test;

interface

uses
  SysUtils, Classes, DB, Forms, ADODB;

type
  TdmTest = class(TDataModule)
    ADOConnection1: TADOConnection;
    ADOStoredProc1: TADOStoredProc;
  private
 
  public
    procedure Add(aLat, aLon: double; aIndex: Integer);

    class procedure Init;

  end;

var
  dmTest: TdmTest;

implementation

{$R *.dfm}


class procedure TdmTest.Init;
begin
  if not Assigned(dmTest) then
    dmTest := TdmTest.Create(Application);

end;


procedure TdmTest.Add(aLat, aLon: double; aIndex: Integer);
begin
  ADOStoredProc1.Parameters.ParamByName('@Lat').Value :=aLat;
  ADOStoredProc1.Parameters.ParamByName('@Lon').Value :=aLon;
  ADOStoredProc1.Parameters.ParamByName('@Index').Value :=aIndex;
  ADOStoredProc1.ExecProc;
end;

end.
