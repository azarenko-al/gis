program test_ENVI;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  dm_Clutters in '..\src\dm_Clutters.pas' {dmClutters: TDataModule},
  u_envi_to_rlf in '..\src\ENVI\u_envi_to_rlf.pas',
  u_Test_envi in 'u_Test_envi.pas',
  u_clutter_classes in '..\src\u_clutter_classes.pas',
  u_CustomTask in '..\src\u_CustomTask.pas',
  u_Rel_to_clutter_map in '..\..\rel_to_bmp_3D\Project\Src\u_Rel_to_clutter_map.pas',
  u_classes_projection_TXT in '..\src\u_classes_projection_TXT.pas',
  u_config_ini in '..\src\u_config_ini.pas',
  u_Asset in '..\src\ASSET\u_Asset.pas';

begin
  try
    { TODO -oUser -cConsole Main : Insert code here }
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
