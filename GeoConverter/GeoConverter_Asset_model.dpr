program GeoConverter_Asset_model;

uses
  Forms,
  d_clutter_schema_change in 'src\d_clutter_schema_change.pas' {dlg_clutter_schema_change},
  dm_Asset_to_RLF in 'src\ASSET\dm_Asset_to_RLF.pas' {dmAsset_to_RLF: TDataModule},
  dm_Clutters in 'src\dm_Clutters.pas' {dmClutters: TDataModule},
  f_Clutters in 'src\f_Clutters.pas' {frm_Clutters},
  fr_ASSET_to_RLF_ in 'src\ASSET\fr_ASSET_to_RLF_.pas' {frame_ASSET_to_RLF},
  fr_Custom_Form in 'src\fr_Custom_Form.pas' {frm_Custom_Form},
  u_Asset in 'src\ASSET\u_Asset.pas',
  u_Asset_to_RLF in 'src\ASSET\u_Asset_to_RLF.pas',
  u_classes_projection_TXT in 'src\u_classes_projection_TXT.pas',
  u_clutter_classes in 'src\u_clutter_classes.pas',
  u_config_ini in 'src\u_config_ini.pas',
  u_CustomTask in 'src\u_CustomTask.pas',
  u_GDAL_classes in 'W:\common XE\GDAL\u_GDAL_classes.pas',
  u_GDAL_envi in 'W:\common XE\GDAL\u_GDAL_envi.pas',
  u_Rel_to_clutter_map in '..\rel_to_bmp_3D\Project\Src\u_Rel_to_clutter_map.pas';

//  u_data_classes111111111111 in 'src\u_data_classes111111111111.pas',
//  u_config_ini in 'src\u_config_ini.pas',
//  d_clutter_schema_change in 'src\d_clutter_schema_change.pas' {dlg_clutter_schema_change},
//  dm_Asset_to_RLF in 'src\ASSET\dm_Asset_to_RLF.pas' {dmAsset_to_RLF: TDataModule};

{$R *.res}


begin
//  CodeSite.Send('start');


  Application.Initialize;
  Application.CreateForm(TdmClutters, dmClutters);
  Application.CreateForm(Tframe_ASSET_to_RLF, frame_ASSET_to_RLF);
  Application.Run;
end.
